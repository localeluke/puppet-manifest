--
-- Dumping data for table `groups_reports_outcomes_types`
--

LOCK TABLES `groups_reports_outcomes_types` WRITE;
/*!40000 ALTER TABLE `groups_reports_outcomes_types` DISABLE KEYS */;
INSERT INTO `groups_reports_outcomes_types` VALUES (1,'allowed');
INSERT INTO `groups_reports_outcomes_types` VALUES (2,'deleted');
/*!40000 ALTER TABLE `groups_reports_outcomes_types` ENABLE KEYS */;
UNLOCK TABLES;
