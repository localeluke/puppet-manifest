--
-- Dumping data for table `binary_data`
--

LOCK TABLES `binary_data` WRITE;
/*!40000 ALTER TABLE `binary_data` DISABLE KEYS */;
INSERT INTO `binary_data` VALUES (1,'content',1,0,0,0,1,'my documents','','my-documents.png','image/png',225928,'',500,300,0,0,'',0,'','',0,0);
INSERT INTO `binary_data` VALUES (2,'content',1,1,0,1,1,'my documents_1','','my-documents_1.png','image/png',89740,'',300,180,0,0,'',0,'','',0,0);
INSERT INTO `binary_data` VALUES (3,'content',2,0,0,0,1,'useful contacts','','useful-contacts.png','image/png',227929,'',500,300,0,0,'',0,'','',0,0);
INSERT INTO `binary_data` VALUES (4,'content',2,3,0,1,1,'useful contacts_1','','useful-contacts_1.png','image/png',93323,'',300,180,0,0,'',0,'','',0,0);
INSERT INTO `binary_data` VALUES (5,'content',3,0,0,0,1,'training videos','','training-videos.png','image/png',132964,'',500,300,0,0,'',0,'','',0,0);
INSERT INTO `binary_data` VALUES (6,'content',3,5,0,1,1,'training videos_1','','training-videos_1.png','image/png',61714,'',300,180,0,0,'',0,'','',0,0);
INSERT INTO `binary_data` VALUES (7,'documents',1,0,0,0,1,'blank document','','blank-document.pdf','application/pdf',27007,'',0,0,0,0,'',0,'','',0,0);
INSERT INTO `binary_data` VALUES (8,'documents',2,0,0,0,1,'blank document_1','','blank-document_1.pdf','application/pdf',27007,'',0,0,0,0,'',0,'','',0,0);
INSERT INTO `binary_data` VALUES (9,'documents',3,0,0,0,1,'blank document_2','','blank-document_2.pdf','application/pdf',27007,'',0,0,0,0,'',0,'','',0,0);
INSERT INTO `binary_data` VALUES (10,'documents',4,0,0,0,1,'blank document_3','','blank-document_3.pdf','application/pdf',27007,'',0,0,0,0,'',0,'','',0,0);
INSERT INTO `binary_data` VALUES (11,'documents',1,0,0,0,2,'appliancemanual3 1','','appliancemanual3-1.png','image/png',17188,'',117,159,0,0,'',0,'','',0,0);
INSERT INTO `binary_data` VALUES (12,'documents',1,11,0,1,2,'appliancemanual3 1_1','','appliancemanual3-1_1.png','image/png',17188,'',117,159,0,0,'',0,'','',0,0);
INSERT INTO `binary_data` VALUES (13,'documents',1,11,0,2,2,'appliancemanual3 1 1','','appliancemanual3-1-1.png','image/png',17188,'',117,159,0,0,'',0,'','',0,0);
INSERT INTO `binary_data` VALUES (14,'documents',2,0,0,0,2,'appliancemanual3 1_2','','appliancemanual3-1_2.png','image/png',17188,'',117,159,0,0,'',0,'','',0,0);
INSERT INTO `binary_data` VALUES (15,'documents',2,14,0,1,2,'appliancemanual3 1 2','','appliancemanual3-1-2.png','image/png',17188,'',117,159,0,0,'',0,'','',0,0);
INSERT INTO `binary_data` VALUES (16,'documents',2,14,0,2,2,'appliancemanual3 1 2_1','','appliancemanual3-1-2_1.png','image/png',17188,'',117,159,0,0,'',0,'','',0,0);
INSERT INTO `binary_data` VALUES (17,'documents',3,0,0,0,2,'appliancemanual3 1_3','','appliancemanual3-1_3.png','image/png',17188,'',117,159,0,0,'',0,'','',0,0);
INSERT INTO `binary_data` VALUES (18,'documents',3,17,0,1,2,'appliancemanual3 1 3','','appliancemanual3-1-3.png','image/png',17188,'',117,159,0,0,'',0,'','',0,0);
INSERT INTO `binary_data` VALUES (19,'documents',3,17,0,2,2,'appliancemanual3 1 3_1','','appliancemanual3-1-3_1.png','image/png',17188,'',117,159,0,0,'',0,'','',0,0);
INSERT INTO `binary_data` VALUES (20,'documents',4,0,0,0,2,'appliancemanual3 1_4','','appliancemanual3-1_4.png','image/png',17188,'',117,159,0,0,'',0,'','',0,0);
INSERT INTO `binary_data` VALUES (21,'documents',4,20,0,1,2,'appliancemanual3 1 4','','appliancemanual3-1-4.png','image/png',17188,'',117,159,0,0,'',0,'','',0,0);
INSERT INTO `binary_data` VALUES (22,'documents',4,20,0,2,2,'appliancemanual3 1 4_1','','appliancemanual3-1-4_1.png','image/png',17188,'',117,159,0,0,'',0,'','',0,0);
INSERT INTO `binary_data` VALUES (23,'site_config',40,0,0,0,1,'smalllogo','','smalllogo.jpg','image/jpeg',3103,'',180,44,0,0,'',0,'','',0,0);
INSERT INTO `binary_data` VALUES (24,'site_config',40,23,0,1,1,'smalllogo_1','','smalllogo_1.jpg','image/jpeg',2383,'',122,29,0,0,'',0,'','',0,0);
INSERT INTO `binary_data` VALUES (28,'login_background_images',1,27,0,1,1,'2018 02 13 wooden stationairy_1','','2018-02-13-wooden-stationairy_1.jpg','image/jpeg',4081,'',122,81,0,0,'',0,'','',0,0);
INSERT INTO `binary_data` VALUES (27,'login_background_images',1,0,0,0,1,'2018 02 13 wooden stationairy','','2018-02-13-wooden-stationairy.jpg','image/jpeg',171675,'',1350,900,0,0,'',0,'','',0,0);
INSERT INTO `binary_data` VALUES (46,'site_config',65,45,0,1,8,'smalllogo 2','','smalllogo-2.jpg','image/jpeg',2383,'',122,29,0,0,'',0,'','',0,0);
INSERT INTO `binary_data` VALUES (45,'site_config',65,0,0,0,8,'smalllogo_2','','smalllogo_2.jpg','image/jpeg',3103,'',180,44,0,0,'',0,'','',0,0);
INSERT INTO `binary_data` VALUES (31,'announcement_bgimage',1,0,0,0,1,'2018 02 13 announcement 4 watch','','2018-02-13-announcement-4-watch.png','image/png',432057,'',1307,454,0,0,'',0,'','',0,0);
INSERT INTO `binary_data` VALUES (32,'announcement_bgimage',1,31,0,1,1,'2018 02 13 announcement 4 watch_1','','2018-02-13-announcement-4-watch_1.png','image/png',86163,'',454,157,0,0,'',0,'','',0,0);
INSERT INTO `binary_data` VALUES (43,'promotion',3,0,0,0,2,'tm lewin cover 1','','tm-lewin-cover-1.jpg','image/jpeg',2627,'',122,57,0,0,'',0,'','',0,0);
INSERT INTO `binary_data` VALUES (41,'promotion',1,0,0,0,2,'fitness first cover 1','','fitness-first-cover-1.jpg','image/jpeg',3411,'',122,57,0,0,'',0,'','',0,0);
INSERT INTO `binary_data` VALUES (42,'promotion',1,41,0,1,2,'fitness first cover 1_1','','fitness-first-cover-1_1.jpg','image/jpeg',3411,'',122,57,0,0,'',0,'','',0,0);
INSERT INTO `binary_data` VALUES (39,'promotion',2,0,0,0,2,'cb 1','','cb-1.jpg','image/jpeg',2831,'',122,57,0,0,'',0,'','',0,0);
INSERT INTO `binary_data` VALUES (40,'promotion',2,39,0,1,2,'cb 1_1','','cb-1_1.jpg','image/jpeg',2831,'',122,57,0,0,'',0,'','',0,0);
INSERT INTO `binary_data` VALUES (44,'promotion',3,43,0,1,2,'tm lewin cover 1_1','','tm-lewin-cover-1_1.jpg','image/jpeg',2627,'',122,57,0,0,'',0,'','',0,0);
/*!40000 ALTER TABLE `binary_data` ENABLE KEYS */;
UNLOCK TABLES;
