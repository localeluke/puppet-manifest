--
-- Dumping data for table `amenity_config`
--

LOCK TABLES `amenity_config` WRITE;
/*!40000 ALTER TABLE `amenity_config` DISABLE KEYS */;
INSERT INTO `amenity_config` VALUES (1,15,1,2,'Standard','Fixed',0,30,30,0,0,10,'week','0','day',0,0,'','',0,1,0,0,'Slot',0,'',1,'',1,0,1,'hour');
INSERT INTO `amenity_config` VALUES (2,15,2,2,'Standard','Fixed',0,30,30,0,0,10,'week','0','day',0,0,'','',0,1,0,0,'Slot',0,'',1,'',1,0,0,NULL);
INSERT INTO `amenity_config` VALUES (3,16,3,4,'Standard','Fixed',0,360,NULL,0,0,3,'month','1','day',0,NULL,NULL,'',0,1,0,0,'Slot',0,NULL,1,NULL,0,NULL,0,NULL);
INSERT INTO `amenity_config` VALUES (4,16,4,4,'Standard','Fixed',0,240,NULL,0,0,3,'month','1','day',0,NULL,NULL,'',0,1,1,0,'Slot',0,NULL,1,NULL,0,NULL,0,NULL);
INSERT INTO `amenity_config` VALUES (5,16,5,4,'Standard','Fixed',0,240,NULL,0,0,3,'month','1','day',0,NULL,NULL,'',0,1,1,0,'Slot',0,NULL,1,NULL,0,NULL,0,NULL);
INSERT INTO `amenity_config` VALUES (6,16,6,4,'Standard','Fixed',0,240,NULL,0,0,3,'month','1','day',0,NULL,NULL,'',0,1,1,0,'Slot',0,NULL,1,NULL,0,NULL,0,NULL);
INSERT INTO `amenity_config` VALUES (7,16,7,4,'Standard','Fixed',0,240,NULL,0,0,3,'month','1','day',0,NULL,NULL,'',0,1,1,0,'Slot',0,NULL,1,NULL,0,NULL,0,NULL);
INSERT INTO `amenity_config` VALUES (8,16,8,4,'Standard','Fixed',0,240,NULL,0,0,3,'month','1','day',0,NULL,NULL,'',0,1,1,0,'Slot',0,NULL,1,NULL,0,NULL,0,NULL);
INSERT INTO `amenity_config` VALUES (9,16,9,4,'Standard','Fixed',0,240,NULL,0,0,3,'month','1','day',0,NULL,NULL,'',0,1,1,0,'Slot',0,NULL,1,NULL,0,NULL,0,NULL);
INSERT INTO `amenity_config` VALUES (10,16,10,4,'Standard','Fixed',0,240,NULL,0,0,3,'month','1','day',0,NULL,NULL,'',0,1,1,0,'Slot',0,NULL,1,NULL,0,NULL,0,NULL);
INSERT INTO `amenity_config` VALUES (11,15,11,2,'Config - simultaneous bookings','Fixed',0,30,NULL,0,0,3,'month','0','day',0,NULL,NULL,'',0,1,1,0,'Slot',0,NULL,1,NULL,1,5,0,NULL);
INSERT INTO `amenity_config` VALUES (12,15,11,2,'Standard','Fixed',0,30,30,0,0,10,'week','0','day',0,0,'','',0,1,0,0,'Slot',0,'',1,'',1,0,0,NULL);
INSERT INTO `amenity_config` VALUES (13,15,12,2,'Simultaneous Bookings','Fixed',0,30,30,0,0,10,'week','0','day',0,0,'','',0,1,0,0,'Slot',0,'',1,'',1,4,0,NULL);
INSERT INTO `amenity_config` VALUES (14,15,13,2,'Standard','Fixed',0,30,NULL,0,0,10,'week','0','day',0,NULL,NULL,'',0,1,1,0,'Slot',0,NULL,1,NULL,1,NULL,0,NULL);
INSERT INTO `amenity_config` VALUES (15,15,14,2,'Standard','Fixed',0,30,NULL,0,0,10,'week','0','day',0,NULL,NULL,'',0,1,1,0,'Slot',0,NULL,1,NULL,1,NULL,0,NULL);
INSERT INTO `amenity_config` VALUES (16,15,15,2,'Standard','Fixed',0,30,NULL,0,0,10,'week','0','day',0,NULL,NULL,'',0,1,1,0,'Slot',0,NULL,1,NULL,1,NULL,0,NULL);
INSERT INTO `amenity_config` VALUES (17,15,16,2,'Standard','Fixed',0,30,NULL,0,0,10,'week','0','day',0,NULL,NULL,'',0,1,1,0,'Slot',0,NULL,1,NULL,1,NULL,0,NULL);
INSERT INTO `amenity_config` VALUES (18,15,17,2,'Standard','Fixed',0,30,NULL,0,0,10,'week','0','day',0,NULL,NULL,'',0,1,1,0,'Slot',0,NULL,1,NULL,1,NULL,0,NULL);
INSERT INTO `amenity_config` VALUES (19,15,18,2,'Standard','Fixed',0,30,NULL,0,0,10,'week','0','day',0,NULL,NULL,'',0,1,1,0,'Slot',0,NULL,1,NULL,1,NULL,0,NULL);
INSERT INTO `amenity_config` VALUES (20,15,19,2,'Standard','Fixed',0,30,NULL,0,0,10,'week','0','day',0,NULL,NULL,'',0,1,1,0,'Slot',0,NULL,1,NULL,1,NULL,0,NULL);
INSERT INTO `amenity_config` VALUES (21,15,20,2,'Standard','Fixed',0,30,NULL,0,0,10,'week','0','day',0,NULL,NULL,'',0,1,1,0,'Slot',0,NULL,1,NULL,1,NULL,0,NULL);
INSERT INTO `amenity_config` VALUES (22,63,22,0,'test','Fixed',120,120,NULL,0,0,7,'day','7','day',0,0,'','',0,1,0,0,'Slot',0,NULL,0,'',0,0,0,NULL);
/*!40000 ALTER TABLE `amenity_config` ENABLE KEYS */;
UNLOCK TABLES;
