function init_masonryIcon(){
	//========== Masonry initialization starts ==========//
	if($('#LiqBox').length > 0){
		if($('.masonry').length > 0){
			$('#LiqBox').masonry("destroy")
		}
		$('.standardHomepage #LiqBox').masonry({
			itemSelector: '.box',
			isAnimated: true,
			gutterWidth: 5,
			isResizable: true,
			isFitWidth: true
		});
		$('.adminDashboard #LiqBox').masonry({
			itemSelector: '.box',
			isAnimated: true,
			gutterWidth: 3,
			isResizable: true,
			isFitWidth: true
		});
		$("#LiqBox").removeClass("scatterFix");
	}
	//========== Masonry initialization ends ==========//
}

function positionCroppic(){
	//========== Announcement module position Croppic starts ==========//
	var upOffset = $(".dummyUpload").offset();
	var upWid = $(".dummyUpload").width();
	var upHi = $(".dummyUpload").height();
	
	$('.croppicWrapper').css({
		"top" : (upOffset.top + 2 ) +"px",
		"left" : (upOffset.left + 2) +"px",
		"width" : upWid,
		"height" : upHi
	});
	//========== Announcement module position Croppic ends ==========//
}
function CMS_popups(){
	//========== CMS popup initialization starts ==========//
	$('.popup-gallery').magnificPopup({
		delegate: 'a',
		type: 'image',
		tLoading: 'Loading image #%curr%...',
		mainClass: 'mfp-img-mobile',
		gallery: {
			enabled: true,
			navigateByImgClick: true,
			preload: [0,1] // Will preload 0 - before current, and 1 after the current image
		},
		image: {
			tError: '<a href="%url%">The image #%curr%</a> could not be loaded.',
			titleSrc: function(item) {
				return item.el.attr('title') + '<small>'+ item.el.attr('data-by') +'</small>';
			}
		}
	});
	
	$('.image-popup').magnificPopup({
		type: 'image',
		closeOnContentClick: true,
		closeBtnInside: false,
		fixedContentPos: true,
		mainClass: 'mfp-no-margins mfp-with-zoom', // class to remove default margin from left and right side
		image: {
			verticalFit: true
		},
		zoom: {
			enabled: true,
			duration: 300 // don't foget to change the duration also in CSS
		}
	});
	
	$('.popup-with-zoom-anim').magnificPopup({
		type: 'inline',

		fixedContentPos: false,
		fixedBgPos: true,

		overflowY: 'auto',

		closeBtnInside: true,
		preloader: false,
		
		midClick: true,
		removalDelay: 300,
		mainClass: 'my-mfp-zoom-in'
	});
	//========== CMS popup initialization ends ==========//
}

//========== homepage announcement starts ==========//
//$('.flexslider').flexslider();
//========== homepage announcement ends ==========//


//========== scroll Tab section starts ==========//
function home_scrollTabi(){
	$(".scrollTabi .tabCubeDiv .tabCube").click(function(){
		var firstPar = $(this).closest(".scrollTabi");
		var fadeAnimi = $(firstPar).attr("data-dtlsAnimi");
		var thisIndex = $(this, firstPar).index();
		if(fadeAnimi != "noFade"){
			//alert ($(".tabDetail:eq("+thisIndex+")", firstPar).attr("class"));
			$(".tabCube", firstPar).removeClass("current");
			$(".tabCube:eq("+thisIndex+")", firstPar).addClass("current");
			
			$(".tabDetail", firstPar).fadeOut(1500).promise();
			$(".tabDetail:eq("+thisIndex+")", firstPar).fadeIn(1500);
			return false;
		}
	});
	
	$(".scrollTabi .iconNavLinkDown").click(function(){
		var firstPar = $(this).closest(".scrollTabi");
		var fadeAnimi = $(firstPar).attr("data-dtlsAnimi");
		var cubeHi = $(".tabCube", firstPar).height();
		$(".iconNavOverlay", firstPar).show();
		
		if(fadeAnimi != "noFade"){
			//$(".tabDetail:first", firstPar).fadeOut(1500).promise();
			$(".tabDetail", firstPar).fadeOut(1500).promise();
			$(".tabDetail:nth-child(2)", firstPar).fadeIn(1500, function(){
				$(".tabDetail:last", firstPar).after($(".tabDetail:first", firstPar));
				$(".iconNavOverlay", firstPar).hide();
			});
		}
		
		$(".tabCubeDiv", firstPar).animate({
			"margin-top" : "-"+cubeHi+"px"
		}, 1000, function(){
			$(".tabCube:last", firstPar).after($(".tabCube:first", firstPar));
			$(".tabCubeDiv", firstPar).css("margin-top", 0);
			$(".tabCube", firstPar).removeClass("current");
			$(".tabCube:first", firstPar).addClass("current");
		});
		
		if(fadeAnimi == "noFade"){
			$(".tabCubeDiv2", firstPar).animate({
				"margin-top" : "-"+cubeHi+"px"
			}, 1000, function(){
				$(".tabDetail:last", firstPar).after($(".tabDetail:first", firstPar));
				$(".tabCubeDiv2", firstPar).css("margin-top", 0);
				$(".tabDetail", firstPar).removeClass("current");
				$(".tabDetail:first", firstPar).addClass("current");
				
				$(".iconNavOverlay", firstPar).hide();
			});
		}
		
		return false;
	});
	
	$(".scrollTabi .iconNavLinkUp").click(function(){
		var firstPar = $(this).closest(".scrollTabi");
		var fadeAnimi = $(firstPar).attr("data-dtlsAnimi");
		var cubeHi = $(".tabCube", firstPar).height();
		$(".iconNavOverlay", firstPar).show();
		
		if(fadeAnimi != "noFade"){
			$(".tabDetail:first", firstPar).before($(".tabDetail:last", firstPar));
			//$(".tabDetail:nth-child(2)", firstPar).fadeOut(1500).promise();
			$(".tabDetail", firstPar).fadeOut(1500).promise();
			$(".tabDetail:first", firstPar).fadeIn(1500, function(){
				$(".iconNavOverlay", firstPar).hide();
			});
		}
		
		$(".tabCubeDiv", firstPar).css("margin-top", "-"+cubeHi+"px");
		$(".tabCube:first", firstPar).before($(".tabCube:last", firstPar));
		
		$(".tabCubeDiv", firstPar).animate({
			"margin-top" : "0"
		}, 1000, function(){
			$(".tabCube", firstPar).removeClass("current");
			$(".tabCube:first", firstPar).addClass("current");
		});
		
		
		if(fadeAnimi == "noFade"){
			$(".tabCubeDiv2", firstPar).css("margin-top", "-"+cubeHi+"px");
			$(".tabDetail:first", firstPar).before($(".tabDetail:last", firstPar));
			
			$(".tabCubeDiv2", firstPar).animate({
				"margin-top" : "0"
			}, 1000, function(){
				$(".tabDetail", firstPar).removeClass("current");
				$(".tabDetail:first", firstPar).addClass("current");
				$(".iconNavOverlay", firstPar).hide();
			});
		}
		
		return false;
	});
}
//========== scroll Tab section ends ==========//

function init_homeWidget(){
	init_masonryIcon();
	
	//========== Announcement section starts ==========//
	$(".ancContent_1").show();
	$(".icon1").addClass("active");
	var contAnc = $(".ancContent").length;
	var Anc = 2;
	var auto_slide_seconds = 80000;
	var timer;
	function setAnnounce(){
		timer = setInterval(function(){
			$(".ancContent").fadeOut(1500).promise();
			$(".ancContent_"+ Anc).fadeIn(1500);
			$(".ico").removeClass("active");
			$(".icon"+ Anc).addClass("active");
			if(Anc ==contAnc){
				Anc=1;
			}else{
				Anc++;
			}
		}, auto_slide_seconds);
	}
	setAnnounce();
	
	$(".ancContent").mouseenter(function(){
		clearInterval(timer);
	});
	$(".ancContent").mouseleave(function(){
		setAnnounce();
	});
	
	$(".ico").click(function(){
		var thisRel = parseInt($(this).attr("data-rel"));
		clearInterval(timer);
		$(".ico").removeClass("active");
		$(".icon"+ thisRel).addClass("active");
		$(".ancContent").fadeOut("slow").promise();
		$(".ancContent_" + thisRel).fadeIn("slow");
		if (thisRel == contAnc){
			Anc = 1;
		}else{
			Anc = (thisRel + 1);
		}
		setAnnounce();
		
		
	});
	//========== Announcement section ends ==========//
}

String.prototype.splice = function( idx, rem, s ) {
	return (this.slice(0,idx) + s + this.slice(idx + Math.abs(rem)));
};


function thumbHover(){
	var imageUrl
	$(".thumbSmallBtn, .thumbBtn, .flatBtn").mouseenter(function(){
		if(!$(this).hasClass("active")){
			if ($(this).find('img').length){
				imageUrl = $(".btnImg", $(this)).attr("src");
				var n = imageUrl.indexOf(".");
				var result = imageUrl.splice( n, 0, "_hover" );

				$(".btnImg", $(this)).attr("src", result);
			}
		}else{
			if ($(this).find('img').length){
				imageUrl = $(".btnImg", $(this)).attr("src");
				var n = imageUrl.indexOf("_hover");
				var result = imageUrl.splice( n, 0, "" );

				$(".btnImg", $(this)).attr("src", imageUrl);
			}
		}
	});
	$(".thumbSmallBtn, .thumbBtn, .flatBtn").mouseleave(function(){
		$(".btnImg", $(this)).attr("src", imageUrl);
	});
	
	$(".thumbSmallBtn.active, .thumbBtn.active, .flatBtn.active").each(function(){
		if ($(this).find('img').length){
			imageUrl = $(".btnImg", $(this)).attr("src");
			var n = imageUrl.indexOf(".");
			var result = imageUrl.splice( n, 0, "_hover" );

			$(".btnImg", $(this)).attr("src", result);
		}
		imageUrl ="";
	});
}

//========== Searchbox animation function initialization starts ==========//
var srcClick = false;
function searchExpand(thisObj){
	$(thisObj).addClass("active");
	$(".srcInptDiv").show();
	$(".srcInpt").focus();
	$(".srcInptDiv").animate({
		"width" : ($(window).width() / 1.8 )
	}, 500, function(){
		$(".srcOverlay").hide();
	});
	srcClick = true;
}
function searchCollapse(thisObj){
	$(".srcInptDiv").animate({
		"width" : "0px"
	}, 500, function(){
		$(".srcOverlay").hide();
		$(thisObj).removeClass("active");
	});
	srcClick = false;
}
//========== Searchbox animation function initialization ends ==========//
$(window).load(function(){
	$(document).ready(function(){
		init_masonryIcon();
		
		//========== Profile Pic dropdown initialization starts ==========//
		
		$(".navi a, .profOptionsMenu a").bind("click", function(){
			$(".profilePic").removeClass("active");
		});
		//========== Profile Pic dropdown initialization ends ==========//
		
		//========== Searchbox click initialization starts ==========//
		$(".srcSubmit_new").click(function(){
			$(".srcOverlay").show();
			var thisObj = $(this);
			if(!srcClick){
				searchExpand(thisObj);
			}else{
				searchCollapse(thisObj);
			}
			return false;
		});
		
		$(document).bind("click", function(){
			var thisObj = $(".srcSubmit_new");
			searchCollapse(thisObj);
		});
		//========== Searchbox click initialization ends ==========//
		//========== sidemenu starts ==========//
			var set = 0;
			//function to expand or open sidepanel
			function panelExpand(){
				$(".sidePanel").animate({
					marginLeft : "0"
				}, 100);
				$(".pageWrapper").animate({
					marginLeft : "70%",
					width : $(window).width() + 'px'
				}, 300, function(){
					set = 1;
					$(".sidePanel").css("height", $(document).height()+"px");
					$(".mainWrapper").css({
						'width' : $(window).width() + 'px',
						'overflow' : 'hidden'
					});
				});
			}
			
			//function to close the sidepanel
			function panelClose(){
				$(".sidePanel").animate({
					marginLeft : "-" + "70%"
				}, 300);
				$(".pageWrapper").animate({
					marginLeft : "0px",
					width : '100%'
				}, 300, function(){
					set = 0;
					$(".sidePanel").css("height", "auto");
					$(".mainWrapper").css({
						'width' : 'auto',
						'overflow' : 'visible'
					});
				});
			}
			
			//click event on show sidepanel
			$(".showMenu").click(function(){
				if (set == 0){
					panelExpand();
				}else{
					panelClose();
				}
			});
			
			//code to check if main menu link has the submenu
			$(".sidePanel ul li, .LHS-menu ul li").each(function(){
				if($(this).has('ul').length){
					$(this).addClass('hasSub');
				}else{
					$(this).addClass('noSub');
				}
			});
			
			//code to show the submenu if the parent has active class
			$(".sideSubUl ul li a, .LHS-menu ul li a").each(function(){
				if($(this).hasClass('active')){
					$(this).parent().parent().css('display', 'block');
				}
				if($(this).hasClass('current')){
					$(this).parent().parent().parent().addClass('hasSubActive active');
				}
			});
			
			//click event for "hasSub" 
			$(".hasSub .mainLink").live('click', function(){
				var thisPar = $(this).parent().parent();
				var cl = false;
				//$(".hasSub ul", thisPar).slideUp("slow");
				//if(!$(this).parent().hasClass('active')){
					$(this).parent().children('ul').slideToggle("slow");
					cl = true;
				//}
				//$(".hasSub", thisPar).removeClass("active");
				//if(cl){
					$(this).parent().toggleClass("active");
				//}
				return false;
			});
			
			//$(".sidePanel ul li:first .mainLink").bind('click', function(){
			$(".sidePanel .noSub .mainLink").live('click', function(){
				//if (!$(this).parent().hasClass('hasSub')){
					if ($(".sideMenu").is(":visible")){
						$(".showMenu").click();
					}
				//}
			});
			
			/* start : done by elita */
			//click event for "hasSub" 
			$(".hasSub .mainLink1").live('click', function(){
				var thisPar = $(this).parent().parent();
				var cl = false;
				$(this).parent().children('ul').slideToggle("slow");
				cl = true;
				return false;
				
			});
			
			//click event for "hasSub" 
			$(".hasSub .mainLink2").live('click', function(){
				var thisPar = $(this).parent().parent();
				var cl = false;
				$(this).parent().children('ul').slideToggle("slow");
				cl = true;
				return false;
			});
			
			$(".hasSub ul li a, .LHS-menu ul li a").live('click', function(){
				$(".hasSub").unbind();
				var thisHref = $(this).attr("href");
				window.location.replace(thisHref);
				if ($(".sideMenu").is(":visible")){
					$(".showMenu").click();
				}
			});
			
			//hover effect for mobile menu button
			$(".showMenu").mouseenter(function(){
				$(this).addClass("hover")
			}).mouseleave(function(){
				$(this).removeClass("hover")
			});
			
			function adjContHi(){
				var contMinHi = $(window).height() - ($(".header").height() + $(".footer").height() + 100)
				$(".contentWrap").css("min-height", contMinHi);
			}
			adjContHi();
			
			
			$(window).resize(function(){
				if (!detectMobile()){
					adjContHi();
					if(set == 1){
						if ($(window).width() < 985){
							$("body").css({
								'width' : $(window).width() + 'px',
								'overflow' : 'hidden'
							});
							//panelClose();
						}else{
							$("body").css({
								'width' : 'auto',
								'overflow' : 'visible'
							});
							panelClose();
						}
						if (!window.screenTop && !window.screenY) {
							
						}else{
							//alert('Browser is in fullscreen');
							setInterval(function(){window.location.reload();}, 800);
						}
					}
					
					/*$(".LHS-menu").css({
						"width": $(".LHS").width() + "px"
					});*/
				}
			});
			
			$(window).bind('orientationchange', function(e) {
				if(set == 1){
					panelClose();
				}
			});
		//========== sidemenu ends ==========//
		
		
		//========== cookie section starts ==========//
			//function to create cookie
			/************************************************/
			function createCookie(name,value,days) {
				if (days) {
					var date = new Date();
					date.setTime(date.getTime()+(days*24*60*60*1000));
					var expires = "; expires="+date.toGMTString();
				}
				else var expires = "";
				document.cookie = name+"="+value+expires+"; path=/";
			}

			//function to read cookie
			function readCookie(name) {
				var nameEQ = name + "=";
				var ca = document.cookie.split(';');
				for(var i=0;i < ca.length;i++) {
					var c = ca[i];
					while (c.charAt(0)==' ') c = c.substring(1,c.length);
					if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
				}
				//return null;
				return false;
			}

			//function to erase cookie
			function eraseCookie(name) {
				createCookie(name,"",-1);
			}
			/************************************************/
			
			
			if(!readCookie("cookie_tower_portal")){
				createCookie("cookie_tower_portal", "set", 3650);
				$(".cookieWrap").slideDown();
				
				setTimeout(function(){
					$(".cookieWrap").slideUp("slow");
				}, 10000);
			}
			
			//cookie close action
			$(".cookieClose").live("click", function(){
				$(".cookieWrap").slideUp("slow");
				return false;
			});
		//========== cookie section ends ==========//
		
		home_scrollTabi();
		thumbHover()
	});
});

//Code for blackWrapper take full height of window
function setBlackWrapper(){
	$(".blackWrapper").css("min-height", $(window).height() - 52);
}

//========== Fancycheckbox and radio buttons functions starts ==========//
function fancyCheckboxInit(){
	//$(".checkChange").before("<div class='chk'></div>");
	$(".checkChange").each(function(){
		if(!$(this).prev().hasClass("chk"))
		$(this).before("<div class='chk'></div>");
	});
	$('.checkChange').css("opacity", 0);
	
	$('.checkChange').die();
	$('.checkChange').live("click", function(){
		var eleName = $(this).prop("name").split('[')[0];
		if($(this).prop('type')=='radio'){
			$(".radioCont ."+ eleName).removeClass("clicked");
			$(this).prev().toggleClass("clicked");
		}else{
			$(this).prev().toggleClass("clicked");
		}
	});
}

function fancyCheckAll(){
	$('.checkChange').each(function(){
		var eleName = $(this).prop("name").split('[')[0];
		$(this).prev().addClass(eleName);
  
		if ($(this).is(":checked")){
			$(this).prev().addClass("clicked");
		}
	});
}
//========== Fancycheckbox and radio buttons functions ends ==========//



(function ( $ ) {
	$.fn.tabify = function( options ) {
		// Default options.
		var settings = $.extend({
			// These are the defaults.
			tabPosition: "top"
		}, options );
		
		// main code.
		return this.each(function(index) {
			// Do something to each element here.
			$(this).append('<div>'+  settings.tabPosition +'--' + index + '</div>')
		});
	};
}( jQuery ));


/*var selectObj;
var selectObj2;
function init_select()
{
		//========== Custome select and multiselect initialization starts ==========//
		selectObj = $("select, .selectFancy").select2({
			"CloseOnSelect"     : false,
			"containerCssClass" : function (e) {$(this).attr("data-containerClass")},
			"dropdownCssClass"  : function (e) {$(this).attr("data-containerClass")}
		});
		
		$(".hiddenSelect").select2('destroy');
		
		selectObj2 = $(".hiddenSelect").select2({
			"containerCssClass" : function (e) {$(this).attr("data-containerClass")},
			"dropdownCssClass"  : function (e) {$(this).attr("data-containerClass")}
		});
		
		selectObj.on("select2-loaded", function (e) {
			initScroll();
		})
		.on("select2-open", function(e) {
			$(".select2-input").each(function(){
				var ulObj = $(this).parent().next();
				var liCount = $("li", ulObj).length;
				if (liCount < 14){
					$(this).parent().addClass("makeHidden");
				}else{
					$(this).parent().removeClass("makeHidden");
				}
			});
		});
		
		selectObj2.on("select2-loaded", function (e) {
			initScroll();
		})
		.on("select2-open", function(e) {
			$(".select2-input").each(function(){
				var ulObj = $(this).parent().next();
				var liCount = $("li", ulObj).length;
				if (liCount < 14){
					$(this).parent().addClass("makeHidden");
				}else{
					$(this).parent().removeClass("makeHidden");
				}
			});
		});
		
		function initScroll(){
			$("ul.select2-results").mCustomScrollbar("destroy");
			$("ul.select2-results").mCustomScrollbar({
				scrollButtons:{
					enable:false
				},
				contentTouchScroll: true
			});
		}
		//========== Custome select and multiselect initialization ends ==========//
}*/

function destroy_select()
{
	$(".filters").select2('destroy');
}
		
function format_elements()
	{
			//Responsive grid initialization.
			$(".flurid").flurid();
		
			//destroy_select();
			thumbHover();
			init_select2();
			home_scrollTabi();
			//init_select2();
			predictive_searchProperties();
			predictive_searchUsers();
			init_group_buttons();
			check_group_active();
			CMS_popups();
			modulePopup();
			init_helpful_text();
			$(".tabs a").live("click", function(){
			var thisPar 	= $(this).parent().parent().parent();
			var thisAjax 	= $(this).attr("data-ajax");
			var thisKeyTab 	= $(this).attr("data-keyTab");
			var thisDataRel	= $(this).attr("data-rel");
			
			$(".tabsDiv a", thisPar).removeClass("current");
			$(this).addClass("current");
			
			$(".tabCont", $(this).parent().parent()).hide();
			$(".tabCont", $(this).parent().parent()).css("background", "none");
			
			if(thisAjax){
				var st=thisAjax.indexOf("[");
				var methodName = thisAjax.substr(0, st );
				var methodParam = thisAjax.substr((st + 1), (thisAjax.length - (st + 2)) );
				methodParam = methodParam.split(' ');
				
				var method = eval('(' + methodName + ')');
				var methodData = method(methodParam); // calls foo()

				$(".tabCont" + thisDataRel + "", $(this).parent().parent()).html(methodData);
				var thisObj = $(this);
				setTimeout(function(){
					$(".tabCont" + thisDataRel + "", thisObj.parent().parent()).show();
				}, 1000);
				$(".tabCont" + thisDataRel + "", $(this).parent().parent()).css("background", "#262626");
			}else{
				$(".tabCont" + thisDataRel + "", $(this).parent().parent()).show();
				$(".tabCont" + thisDataRel + "", $(this).parent().parent()).css("background", "#262626");
			}
			
			return false;
		});
		$(".tabs").each(function(){
			$("a:first", $(this)).click();
		});
		
		function foo(paramArray){
			var t = paramArray[0] + " " + paramArray[1] + " " + paramArray[2];
			return t;
		}
		
		/*js for tabing system with provision to give function name and parameters ends*/
		
		
		//========== Magnific popup initialization starts ==========//
		$('.popup-zoom').magnificPopup({
			type: 'inline',

			fixedContentPos: false,
			fixedBgPos: true,

			overflowY: 'auto',

			closeBtnInside: true,
			preloader: false,
			
			midClick: true,
			removalDelay: 300,
			mainClass: 'my-mfp-zoom-in'
		});

		$('.popup-move').magnificPopup({
			type: 'inline',

			fixedContentPos: false,
			fixedBgPos: true,

			overflowY: 'auto',

			closeBtnInside: true,
			preloader: false,
			
			midClick: true,
			removalDelay: 300,
			mainClass: 'my-mfp-slide-bottom'
		});
		
		$('.ajaxPopup').magnificPopup({
			type: 'ajax',
			overflowY: 'scroll' // as we know that popup content is tall we set scroll overflow by default to avoid jump
		});
		
		
		
		//========== Magnific popup initialization ends ==========//
		
		
		//========== Ui spinner (timePicker) initialization starts ==========//	
		if ($( ".spinner" ).length > 0){
			var spinner = $( ".spinner" ).spinner({ min: 0 });
		}
		if ($( ".timeSpinner" ).length > 0){
			var timeSpinner = $( ".timeSpinner" ).timespinner();
		}
		//========== Ui spinner (timePicker) initialization ends ==========//
		
		
		
		
		//========== Fancy checkbox and radiobuttons initialization starts ==========//
		fancyCheckboxInit();
		fancyCheckAll();
		//========== Fancy checkbox and radiobuttons initialization ends ==========//
		
		
		
		//========== Custom Elements initialization starts ==========//
		$(".buttonpanel").buttonpanel(
										{ 
											multiple : false,
											singular : ['5']   //1,2,3,4,5 - a number to identify which in line it is. If this one is selected no other will be selected
										}
									 
									 );
		
		$(".toggleswitch").toggleswitch();
		//========== Custom Elements initialization ends ==========//
		
		
		//========== Script for uiPage starts ==========//
		/*$('.tblDiv .desktopTbl table tbody>tr:odd').css("background-color", "#222222");
		$('.tblDiv .desktopTbl table tbody>tr:odd').addClass("odd");
		$('.tblDiv .desktopTbl table tbody>tr:even').addClass("even");*/
		
		/*$('.tblDiv .desktopTbl table tbody>tr').live("mouseenter", function(){$(this).addClass("hover");});
		$('.tblDiv .desktopTbl table tbody>tr').live("mouseleave", function(){$(this).removeClass("hover");});*/
		
		/*$("a.showView, .tblTitleTxt").live("click", function(){
			var thisPar = $(this).parent().parent();
			$(this).parent().toggleClass("active");
			
			$(".tableDesc", thisPar).slideToggle("fast");
			
			return false;
		});*/
				
		$("input[type='button'], input[type='submit'], .btn-custom, .small-btn, .odd, .even").live("mouseenter", function(){
			$(this).addClass("hover");
		});
		$("input[type='button'], input[type='submit'], .btn-custom, .small-btn, .odd, .even").live("mouseleave", function(){
			$(this).removeClass("hover");
		});
		//========== Script for uiPage starts ==========//
		
		
		//========== Script for Mobile keypad starts ==========//
			var event;
			$('.telephone').click(function(event){
				$('#keypad').fadeToggle('fast');
				event.stopPropagation();
			});
			$('.key').click(function(event){
				var telephone = $('.telephone');
				telephone.val( telephone.val() + this.innerHTML);
				event.stopPropagation();
			});
			
			$('.btn-click').click(function(event){
				if(this.innerHTML == 'DEL'){
					var telephone = $('.telephone');
					if(telephone.val().length > 0){
						telephone.val(telephone.val().substring(0, telephone.val().length - 1));
					}
				}else{
					$('.telephone').val("");
				}
				event.stopPropagation();
			});
		//========== Script for Mobile keypad ends ==========//
		
		
		//========== fullcalendar script starts ==========//
		$(".fc-day").live("click", function(){
			$(".fc-day").removeClass("active");
			$(this).addClass("active");
		});
		//========== fullcalendar script ends ==========//
		
		
		//========== functions to be loaded on document load starts ==========//
		setBlackWrapper();
		init_homeWidget();
		init_select2();
		CMS_popups();
		modulePopup();
		//predictive_searchProperties();
		//========== functions to be loaded on document load ends ==========//
		
		$(window).scroll(function (){
			
		});
		
		$(window).resize(function(){
			setBlackWrapper();
			init_masonryIcon();
		});
		
		$(window).bind('orientationchange', function(e) {
			
		});

		$('.ajaxPopup').live('click', function(){
			fetchcontent($(this).attr('rel'));
		});
	}
	//END: Format Elements
	
		
// Initialize the widget 
function plupload_init()
{
	// Initialize the widget when the DOM is ready
		// Setup html5 version

}

function fetchcontent(rel)
{
	//Get the content & display to user here:
	var extension = rel.substr( (rel.lastIndexOf('.') +1) );
	 switch(extension) {
	   
	 //1. Image: Show in magnific popup
        case 'jpg':
        case 'png':
        case 'gif':
           {	
				$('.ajaxPopupCommon').attr('href', base_url()+'UserFiles/binary_data/'+rel);
				$('.ajaxPopupCommon').magnificPopup({
					  type	    : 'image',
					  preloader : false
				 });
				$('.ajaxPopupCommon').click();
		   }
        break;
	 //2. PDF : show in new tab   
		case 'pdf':
           {
				$('.ajaxPopupCommon').attr('href', 'UserFiles/binary_data/'+rel);
			    $('.ajaxPopupCommon').attr('target', '_blank');
				$('.ajaxPopupCommon').get(0).click();
		   }
        break;
	//3. Anything else : provide download link
        default:
           {
				$('.ajaxPopupCommon').attr('href', '#home/downloadfile/'+rel);
				$('.ajaxPopupCommon').click();
		   }
		break;
    }
}

function ajaximage_init()
{
	var uploader = new qq.FileUploader({		
		element				: document.getElementById('file-uploader'),
		action				: '{{ $DOCROOT }}home/uploader',
		allowedExtensions	: ['jpg', 'png', 'bmp'],
		sizeLimit			: 100000000,	// 100mb
		minSizeLimit		: 500,
		multiple 			: false,
		uploadButtonText	: '<div class="overlay-qqfileupload">Upload</div>',
		onProgress			: function(id, fileName, loaded, total){
									$('.qq-upload-button').css('background-image', '');
									$('.qq-upload-button').css('background-image', 'url("{{ $DOCROOT }}images/ajax-load.gif")');
								},
		onComplete			: function(id, fileName, responseJSON){
									$('.qq-upload-button').css('background-image', '');
									$('.qq-upload-button').css('background-image', 'url("{{ $DOCROOT }}UserFiles/'+fileName+'")');
									$('.qq-upload-button').css('background-size',  '125px 125px');
									
									//Save to users table this name
									var hash    = location.hash;
									var spl     = hash.split('/');
									var user_id = spl[2];
									$.ajax
									({
									  type    : "POST",
									  url     : '{{ $DOCROOT }}users/save_image',
									  data    : { 
												 user_id  : user_id,
												 filename : fileName
												}
									});
								$('#file-uploader').trigger('mouseleave');
								}
	}); 
}

	 
	 function ajax(url , data , callback , type ) {
			var type = (type === undefined) ? "POST" : type;
			$.ajax({
				url: url,
				async: false,
				type: type,
				data: data,
				dataType: 'html',
				success: callback
				
			});
	}
	
		
	function initScroll(){
		$("ul.select2-results").mCustomScrollbar("destroy");
		$("ul.select2-results").mCustomScrollbar({
			scrollButtons:{
				enable:false
			},
			contentTouchScroll: true
		});
	}

	function ajax_popup_init()
	{
			$('.ajaxPopup2').magnificPopup({
				type: 'ajax',
				alignTop: true,
				overflowY: 'scroll' // as we know that popup content is tall we set scroll overflow by default to avoid jump
			});
			
	}
