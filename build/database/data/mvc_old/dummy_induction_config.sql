--
-- Dumping data for table `induction_config`
--

LOCK TABLES `induction_config` WRITE;
/*!40000 ALTER TABLE `induction_config` DISABLE KEYS */;
INSERT INTO `induction_config` VALUES (1,'IB/','terms and conditions','<p>To receive a security access card to The Shard an induction must be completed.</p>\r\n\r\n<p><strong>Please ensure inductions for office occupiers are booked at 14:00 on either a&nbsp;Monday,&nbsp;Tuesday or a Thursday.</strong></p>\r\n\r\n<p>Inductions for HCA, Aqua, Hutong, Oblix, Shangri-La and contractors take place at 08:00 Monday to Sunday, meeting in the service entrance on St Thomas Street.</p>\r\n\r\n<p>Click &#39;Add New Starter&#39; below to add your new starter details. Once all new starters have been added, click &#39;Book Induction&#39; to select the induction date.</p>\r\n','<p>Thank you for making a booking on My Vertical City for your New Starter. They will receive an email notification highlighting their upcoming booking. They&#39;ll also receive an email notification one day before their induction.</p>\r\n\r\n<p><a href=\"#induction\"><input class=\"shadowEffect FB_btn1\" style=\"padding:0 10px 3px !important;\" type=\"button\" value=\"Make another booking\" /></a></p>\r\n',60,30,'day',1,'day',14,0,0,0,'{&quot;1&quot;:&quot;09:00-17:00&quot;,&quot;2&quot;:&quot;09:00-17:00&quot;,&quot;3&quot;:&quot;09:00-17:00&quot;,&quot;4&quot;:&quot;09:00-17:00&quot;,&quot;5&quot;:&quot;09:00-17:00&quot;,&quot;6&quot;:&quot;&quot;,&quot;7&quot;:&quot;&quot;}','56,64,68','');
/*!40000 ALTER TABLE `induction_config` ENABLE KEYS */;
UNLOCK TABLES;
