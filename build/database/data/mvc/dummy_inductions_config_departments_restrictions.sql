--
-- Dumping data for table `inductions_config_departments_restrictions`
--

LOCK TABLES `inductions_config_departments_restrictions` WRITE;
/*!40000 ALTER TABLE `inductions_config_departments_restrictions` DISABLE KEYS */;
INSERT INTO `inductions_config_departments_restrictions` VALUES (347,'user_groups','user_group_id','84',16);
INSERT INTO `inductions_config_departments_restrictions` VALUES (348,'user_groups','user_group_id','85',17);
INSERT INTO `inductions_config_departments_restrictions` VALUES (349,'user_groups','user_group_id','87',15);
INSERT INTO `inductions_config_departments_restrictions` VALUES (350,'user_groups','user_group_id','86',15);
INSERT INTO `inductions_config_departments_restrictions` VALUES (351,'user_groups','user_group_id','82',14);
INSERT INTO `inductions_config_departments_restrictions` VALUES (352,'user_groups','user_group_id','83',14);
/*!40000 ALTER TABLE `inductions_config_departments_restrictions` ENABLE KEYS */;
UNLOCK TABLES;
