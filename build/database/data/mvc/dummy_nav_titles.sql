--
-- Dumping data for table `nav_titles`
--

LOCK TABLES `nav_titles` WRITE;
/*!40000 ALTER TABLE `nav_titles` DISABLE KEYS */;
INSERT INTO `nav_titles` VALUES (21,1,'BUILDING SERVICES','',0,1,1,0);
INSERT INTO `nav_titles` VALUES (22,1,'VERTICAL CITY','',0,2,1,0);
INSERT INTO `nav_titles` VALUES (23,1,'SHARD MEMBERS','',0,3,1,0);
INSERT INTO `nav_titles` VALUES (24,1,'LONDON BRIDGE QUARTER','',0,4,1,0);
INSERT INTO `nav_titles` VALUES (25,1,'LIBRARY','',0,5,1,0);
INSERT INTO `nav_titles` VALUES (26,1,'Operations','',21,6,1,0);
/*!40000 ALTER TABLE `nav_titles` ENABLE KEYS */;
UNLOCK TABLES;
