--
-- Dumping data for table `nav_titles`
--

LOCK TABLES `nav_titles` WRITE;
/*!40000 ALTER TABLE `nav_titles` DISABLE KEYS */;
INSERT INTO `nav_titles` VALUES (1,1,'Home','/#home/dashboard',0,1,1,0);
INSERT INTO `nav_titles` VALUES (2,1,'My Building','',0,2,1,0);
INSERT INTO `nav_titles` VALUES (3,1,'My Property','',0,3,1,0);
INSERT INTO `nav_titles` VALUES (4,1,'Knowledge Base','',0,4,1,0);
/*!40000 ALTER TABLE `nav_titles` ENABLE KEYS */;
UNLOCK TABLES;
