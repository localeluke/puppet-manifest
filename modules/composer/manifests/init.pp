# Install composer

class composer::install {

  package { "curl":
    ensure  => installed,
    require => Class['php'],
  }

  exec { 'install_composer':
    environment => ["COMPOSER_HOME=/usr/local/bin"],
    command     => 'curl -sS https://getcomposer.org/installer | php && sudo mv composer.phar /usr/local/bin/composer',
    require     => Package['curl'],
    path        => '/usr/bin',
  }

}
