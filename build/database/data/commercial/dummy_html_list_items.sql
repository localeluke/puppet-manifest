--
-- Dumping data for table `html_list_items`
--

LOCK TABLES `html_list_items` WRITE;
/*!40000 ALTER TABLE `html_list_items` DISABLE KEYS */;
INSERT INTO `html_list_items` VALUES (1,1,'Main Entrance',0,0);
INSERT INTO `html_list_items` VALUES (2,1,'Service Entrance',1,0);
INSERT INTO `html_list_items` VALUES (3,2,'Mr',0,0);
INSERT INTO `html_list_items` VALUES (4,2,'Miss',2,0);
INSERT INTO `html_list_items` VALUES (5,2,'Mrs',1,0);
INSERT INTO `html_list_items` VALUES (6,2,'Dr',4,0);
INSERT INTO `html_list_items` VALUES (7,2,'Ms',3,0);
INSERT INTO `html_list_items` VALUES (8,2,'Other',6,0);
INSERT INTO `html_list_items` VALUES (9,2,'Sir',5,0);
INSERT INTO `html_list_items` VALUES (10,2,'Sgt',7,1);
INSERT INTO `html_list_items` VALUES (11,2,'HRH',8,1);
INSERT INTO `html_list_items` VALUES (12,3,'Emergency Works',0,0);
INSERT INTO `html_list_items` VALUES (13,3,'Routine Maintenance',1,0);
INSERT INTO `html_list_items` VALUES (14,3,'Occupier Request',2,0);
INSERT INTO `html_list_items` VALUES (15,4,'Key Holding',0,0);
INSERT INTO `html_list_items` VALUES (16,4,'Valet Parking',1,1);
INSERT INTO `html_list_items` VALUES (17,5,'Air Conditioning',0,0);
INSERT INTO `html_list_items` VALUES (18,5,'Cleaning',1,0);
INSERT INTO `html_list_items` VALUES (19,5,'Decorating',2,0);
INSERT INTO `html_list_items` VALUES (20,5,'Communal Areas',3,0);
INSERT INTO `html_list_items` VALUES (21,5,'Doors & Locks',4,0);
INSERT INTO `html_list_items` VALUES (22,5,'Electrical',5,0);
INSERT INTO `html_list_items` VALUES (23,5,'Lifts',6,0);
INSERT INTO `html_list_items` VALUES (24,5,'Lighting',7,0);
INSERT INTO `html_list_items` VALUES (25,5,'Plumbing',8,0);
INSERT INTO `html_list_items` VALUES (26,5,'Security',9,0);
INSERT INTO `html_list_items` VALUES (27,5,'Other',10,0);
INSERT INTO `html_list_items` VALUES (28,5,'Test',0,0);
INSERT INTO `html_list_items` VALUES (29,6,'Temporary',0,0);
INSERT INTO `html_list_items` VALUES (30,6,'Permanent',1,0);
/*!40000 ALTER TABLE `html_list_items` ENABLE KEYS */;
UNLOCK TABLES;
