--
-- Dumping data for table `company_floors`
--

LOCK TABLES `company_floors` WRITE;
/*!40000 ALTER TABLE `company_floors` DISABLE KEYS */;
INSERT INTO `company_floors` VALUES (1812,30,82);
INSERT INTO `company_floors` VALUES (1854,30,86);
INSERT INTO `company_floors` VALUES (1845,30,91);
INSERT INTO `company_floors` VALUES (1842,31,81);
INSERT INTO `company_floors` VALUES (1813,31,82);
INSERT INTO `company_floors` VALUES (1853,31,85);
INSERT INTO `company_floors` VALUES (1855,31,87);
INSERT INTO `company_floors` VALUES (1849,31,94);
INSERT INTO `company_floors` VALUES (1843,32,81);
INSERT INTO `company_floors` VALUES (1814,32,82);
INSERT INTO `company_floors` VALUES (1815,33,82);
INSERT INTO `company_floors` VALUES (1856,33,88);
INSERT INTO `company_floors` VALUES (1816,34,82);
INSERT INTO `company_floors` VALUES (1851,34,83);
INSERT INTO `company_floors` VALUES (1847,34,93);
INSERT INTO `company_floors` VALUES (1817,35,82);
INSERT INTO `company_floors` VALUES (1818,36,82);
INSERT INTO `company_floors` VALUES (1819,37,82);
INSERT INTO `company_floors` VALUES (1820,38,82);
INSERT INTO `company_floors` VALUES (1821,39,82);
INSERT INTO `company_floors` VALUES (1822,40,82);
INSERT INTO `company_floors` VALUES (1823,41,82);
INSERT INTO `company_floors` VALUES (1824,42,82);
INSERT INTO `company_floors` VALUES (1825,43,82);
INSERT INTO `company_floors` VALUES (1826,44,82);
INSERT INTO `company_floors` VALUES (1850,52,95);
INSERT INTO `company_floors` VALUES (1859,55,90);
INSERT INTO `company_floors` VALUES (1852,59,84);
INSERT INTO `company_floors` VALUES (1857,60,89);
INSERT INTO `company_floors` VALUES (1848,60,93);
INSERT INTO `company_floors` VALUES (1844,63,81);
INSERT INTO `company_floors` VALUES (1846,64,92);
/*!40000 ALTER TABLE `company_floors` ENABLE KEYS */;
UNLOCK TABLES;
