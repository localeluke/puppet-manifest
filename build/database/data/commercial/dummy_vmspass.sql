--
-- Dumping data for table `vmspass`
--

LOCK TABLES `vmspass` WRITE;
/*!40000 ALTER TABLE `vmspass` DISABLE KEYS */;
INSERT INTO `vmspass` VALUES (1,'1','global',1,0);
INSERT INTO `vmspass` VALUES (2,'2','company_specific',1,0);
INSERT INTO `vmspass` VALUES (3,'3','global',1,0);
/*!40000 ALTER TABLE `vmspass` ENABLE KEYS */;
UNLOCK TABLES;
