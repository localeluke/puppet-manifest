--
-- Dumping data for table `calendar_filter`
--

LOCK TABLES `calendar_filter` WRITE;
/*!40000 ALTER TABLE `calendar_filter` DISABLE KEYS */;
INSERT INTO `calendar_filter` VALUES (1,1,'blocks',4);
INSERT INTO `calendar_filter` VALUES (2,1,'blocks',3);
INSERT INTO `calendar_filter` VALUES (3,2,'blocks',4);
INSERT INTO `calendar_filter` VALUES (4,2,'blocks',3);
/*!40000 ALTER TABLE `calendar_filter` ENABLE KEYS */;
UNLOCK TABLES;
