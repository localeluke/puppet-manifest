--
-- Dumping data for table `html_fields`
--

LOCK TABLES `html_fields` WRITE;
/*!40000 ALTER TABLE `html_fields` DISABLE KEYS */;
INSERT INTO `html_fields` VALUES (1,'text','Name of Applicant',NULL,0,'required',NULL,'',1,0,NULL,0,0);
INSERT INTO `html_fields` VALUES (2,'checkbox','New Pass?',NULL,0,'none',NULL,'',1,1,NULL,0,0);
INSERT INTO `html_fields` VALUES (3,'checkbox','Replacement Pass?',NULL,0,'none',NULL,'',1,1,NULL,0,0);
INSERT INTO `html_fields` VALUES (4,'dropdown','Type of Pass Required',NULL,6,'required',NULL,'',1,0,NULL,0,0);
INSERT INTO `html_fields` VALUES (5,'text','Other Access Required?',NULL,0,'none',NULL,'',1,0,NULL,0,0);
/*!40000 ALTER TABLE `html_fields` ENABLE KEYS */;
UNLOCK TABLES;
