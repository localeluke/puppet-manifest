--
-- Dumping data for table `calendar_filter`
--

LOCK TABLES `calendar_filter` WRITE;
/*!40000 ALTER TABLE `calendar_filter` DISABLE KEYS */;
INSERT INTO `calendar_filter` VALUES (2,28,'blocks',1);
INSERT INTO `calendar_filter` VALUES (3,28,'company',1);
INSERT INTO `calendar_filter` VALUES (12,35,'company',4);
INSERT INTO `calendar_filter` VALUES (13,35,'user_groups',8);
INSERT INTO `calendar_filter` VALUES (16,40,'company',51);
INSERT INTO `calendar_filter` VALUES (17,42,'company',8);
INSERT INTO `calendar_filter` VALUES (18,43,'company',7);
INSERT INTO `calendar_filter` VALUES (19,46,'user_groups',8);
INSERT INTO `calendar_filter` VALUES (21,47,'user_groups',8);
INSERT INTO `calendar_filter` VALUES (22,53,'user_groups',8);
INSERT INTO `calendar_filter` VALUES (23,54,'user_groups',8);
INSERT INTO `calendar_filter` VALUES (24,55,'user_groups',8);
INSERT INTO `calendar_filter` VALUES (25,56,'user_groups',8);
INSERT INTO `calendar_filter` VALUES (27,48,'blocks',1);
INSERT INTO `calendar_filter` VALUES (28,86,'blocks',1);
INSERT INTO `calendar_filter` VALUES (41,95,'company',1);
/*!40000 ALTER TABLE `calendar_filter` ENABLE KEYS */;
UNLOCK TABLES;
