--
-- Dumping data for table `helpdesk_multidata`
--

LOCK TABLES `helpdesk_multidata` WRITE;
/*!40000 ALTER TABLE `helpdesk_multidata` DISABLE KEYS */;
INSERT INTO `helpdesk_multidata` VALUES (1,'admin_helpdesk_cat',1,'fault_list',17);
INSERT INTO `helpdesk_multidata` VALUES (2,'admin_helpdesk_cat',1,'fault_list',18);
INSERT INTO `helpdesk_multidata` VALUES (3,'admin_helpdesk_cat',1,'fault_list',20);
INSERT INTO `helpdesk_multidata` VALUES (4,'admin_helpdesk_cat',1,'fault_list',19);
INSERT INTO `helpdesk_multidata` VALUES (5,'admin_helpdesk_cat',1,'fault_list',21);
INSERT INTO `helpdesk_multidata` VALUES (6,'admin_helpdesk_cat',1,'fault_list',22);
INSERT INTO `helpdesk_multidata` VALUES (7,'admin_helpdesk_cat',1,'fault_list',23);
INSERT INTO `helpdesk_multidata` VALUES (8,'admin_helpdesk_cat',1,'fault_list',24);
INSERT INTO `helpdesk_multidata` VALUES (9,'admin_helpdesk_cat',1,'fault_list',27);
INSERT INTO `helpdesk_multidata` VALUES (10,'admin_helpdesk_cat',1,'fault_list',25);
INSERT INTO `helpdesk_multidata` VALUES (11,'admin_helpdesk_cat',1,'fault_list',26);
INSERT INTO `helpdesk_multidata` VALUES (12,'admin_helpdesk_cat',1,'fault_list',28);
INSERT INTO `helpdesk_multidata` VALUES (13,'admin_helpdesk_cat',1,'block',2);
INSERT INTO `helpdesk_multidata` VALUES (14,'admin_helpdesk_cat',1,'block',1);
INSERT INTO `helpdesk_multidata` VALUES (15,'admin_helpdesk_cat',1,'company',1);
INSERT INTO `helpdesk_multidata` VALUES (16,'admin_helpdesk_cat',1,'company',2);
INSERT INTO `helpdesk_multidata` VALUES (17,'admin_helpdesk_cat',1,'user_type',2);
INSERT INTO `helpdesk_multidata` VALUES (18,'admin_helpdesk_cat',1,'user_type',1);
INSERT INTO `helpdesk_multidata` VALUES (19,'admin_helpdesk_cat',1,'user_type',5);
INSERT INTO `helpdesk_multidata` VALUES (20,'admin_helpdesk_cat',1,'user_type',6);
INSERT INTO `helpdesk_multidata` VALUES (21,'admin_helpdesk_cat',1,'user_type',4);
INSERT INTO `helpdesk_multidata` VALUES (22,'admin_helpdesk_cat',1,'user_type',3);
INSERT INTO `helpdesk_multidata` VALUES (23,'admin_helpdesk_cat',1,'admin_notify_grp',2);
INSERT INTO `helpdesk_multidata` VALUES (24,'admin_helpdesk_cat',1,'admin_notify_grp',1);
INSERT INTO `helpdesk_multidata` VALUES (25,'admin_helpdesk_cat',1,'admin_users',1);
INSERT INTO `helpdesk_multidata` VALUES (26,'admin_helpdesk_cat',1,'admin_notify_grp2',2);
INSERT INTO `helpdesk_multidata` VALUES (27,'admin_helpdesk_cat',1,'admin_notify_grp2',1);
INSERT INTO `helpdesk_multidata` VALUES (28,'admin_helpdesk_cat',1,'admin_users2',1);
/*!40000 ALTER TABLE `helpdesk_multidata` ENABLE KEYS */;
UNLOCK TABLES;
