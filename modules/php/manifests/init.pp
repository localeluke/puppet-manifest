class php {
  exec { 'apt-update':
    command => '/usr/bin/apt-get update',
    require => Apt::Ppa['ppa:ondrej/php'],
  }

  package { 'php7.2':
    require => Exec['apt-update'],
    ensure => 'installed',
  }

  package { 'php7.2-mysql':
    require => Package['php7.2'],
    ensure => 'installed',
  }

  package { 'libapache2-mod-php7.2':
    ensure  => 'installed',
    require => Package['php7.2'],
  }

  package { 'php7.2-mbstring':
    ensure  => 'installed',
    require => Package['php7.2'],
  }

  package { 'php7.2-curl':
    ensure  => 'installed',
    require => Package['php7.2'],
  }

  package { 'php7.2-dom':
    ensure  => 'installed',
    require => Package['php7.2'],
  }

  package { 'php7.2-zip':
    ensure  => 'installed',
    require => Package['php7.2'],
  }

  package { 'php7.2-gd':
    ensure  => 'installed',
    require => Package['php7.2'],
  }
}

