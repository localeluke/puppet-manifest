--
-- Dumping data for table `announcement_posts`
--

LOCK TABLES `announcement_posts` WRITE;
/*!40000 ALTER TABLE `announcement_posts` DISABLE KEYS */;
INSERT INTO `announcement_posts` VALUES (110,5,'Announcement Template 1','<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n','2019-05-01','2019-05-15',16,1,0,0,0,'NULL',0,1,'2019-05-01 13:35:27',454,'2019-05-01','00:00:00',0,'N',NULL);
INSERT INTO `announcement_posts` VALUES (111,6,'Announcement Template 2','<p>t is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using &#39;Content here, content here&#39;, making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for &#39;lorem ipsum&#39; will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p>\r\n','2019-05-01','2019-05-15',19,1,0,0,0,'NULL',0,1,'2019-05-01 13:35:44',454,'2019-05-01','00:00:00',0,'N',NULL);
/*!40000 ALTER TABLE `announcement_posts` ENABLE KEYS */;
UNLOCK TABLES;
