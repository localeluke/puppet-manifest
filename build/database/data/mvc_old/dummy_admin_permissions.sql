--
-- Dumping data for table `admin_permissions`
--

LOCK TABLES `admin_permissions` WRITE;
/*!40000 ALTER TABLE `admin_permissions` DISABLE KEYS */;
INSERT INTO `admin_permissions` VALUES (1,2,25);
INSERT INTO `admin_permissions` VALUES (4,3,2);
INSERT INTO `admin_permissions` VALUES (5,3,3);
INSERT INTO `admin_permissions` VALUES (6,3,10);
INSERT INTO `admin_permissions` VALUES (7,3,14);
/*!40000 ALTER TABLE `admin_permissions` ENABLE KEYS */;
UNLOCK TABLES;
