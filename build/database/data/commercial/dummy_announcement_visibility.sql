--
-- Dumping data for table `announcement_visibility`
--

LOCK TABLES `announcement_visibility` WRITE;
/*!40000 ALTER TABLE `announcement_visibility` DISABLE KEYS */;
INSERT INTO `announcement_visibility` VALUES (1,1,'blocks',2);
INSERT INTO `announcement_visibility` VALUES (2,2,'blocks',2);
INSERT INTO `announcement_visibility` VALUES (3,2,'blocks',1);
/*!40000 ALTER TABLE `announcement_visibility` ENABLE KEYS */;
UNLOCK TABLES;
