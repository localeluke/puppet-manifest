--
-- Dumping data for table `tbl_url_expiry`
--

LOCK TABLES `tbl_url_expiry` WRITE;
/*!40000 ALTER TABLE `tbl_url_expiry` DISABLE KEYS */;
INSERT INTO `tbl_url_expiry` VALUES ('-YBs5hR8AtLjPMJoVncQ0u6WDX24eGgf','USER_DB_REGISTRATION','users',12,'2018-09-11 16:44:35',0);
INSERT INTO `tbl_url_expiry` VALUES ('7kfNAT5DQIOSFRtCwoEiUpyza~m2dB.c','USER_DB_REGISTRATION','users',6,'2016-11-18 15:59:08',1);
INSERT INTO `tbl_url_expiry` VALUES ('d7XviEgrcOFV.wB9SGCqQaJou8hemHzK','USER_DB_REGISTRATION','users',11,'2018-09-11 16:41:09',0);
INSERT INTO `tbl_url_expiry` VALUES ('dhQb7aAlp.uCzGINiqkyBTPXUr-~vKE3','PRE_REGISTER','users',7,'2017-03-23 12:38:36',0);
INSERT INTO `tbl_url_expiry` VALUES ('h-TQGVDad5qKuXZbzlr.OxpW7SNIR8Uw','STARTER','users',464,'2019-05-01 13:20:48',0);
INSERT INTO `tbl_url_expiry` VALUES ('kHAPZNXhOR7xf8vaq~mcEFn3.lbsuzoy','USER_DB_REGISTRATION','users',10,'2018-09-11 16:38:28',0);
INSERT INTO `tbl_url_expiry` VALUES ('mGR6FIYg7py0LNlUCZvcVH28BnDebEsP','USER_DB_REGISTRATION','users',13,'2018-09-11 16:47:04',0);
INSERT INTO `tbl_url_expiry` VALUES ('MuAP0pUt-xl.VDOySBIdEQ6wzrosb8R5','USER_DB_REGISTRATION','users',14,'2018-09-11 16:48:19',0);
/*!40000 ALTER TABLE `tbl_url_expiry` ENABLE KEYS */;
UNLOCK TABLES;
