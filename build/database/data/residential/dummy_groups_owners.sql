--
-- Dumping data for table `groups_owners`
--

LOCK TABLES `groups_owners` WRITE;
/*!40000 ALTER TABLE `groups_owners` DISABLE KEYS */;
INSERT INTO `groups_owners` VALUES (1,1,1,'2019-07-15 15:53:15');
INSERT INTO `groups_owners` VALUES (2,2,3,'2019-07-15 15:56:02');
INSERT INTO `groups_owners` VALUES (3,3,3,'2019-07-15 16:14:14');
INSERT INTO `groups_owners` VALUES (4,4,1,'2019-07-16 07:50:39');
/*!40000 ALTER TABLE `groups_owners` ENABLE KEYS */;
UNLOCK TABLES;
