--
-- Dumping data for table `properties`
--

LOCK TABLES `properties` WRITE;
/*!40000 ALTER TABLE `properties` DISABLE KEYS */;
INSERT INTO `properties` VALUES (1,2,4,'1B','1 Bed Apartment BBG','',NULL,NULL,NULL,NULL,1,1,0);
INSERT INTO `properties` VALUES (2,2,3,'1BA','1 Bed Apartment PEG','',NULL,NULL,NULL,NULL,2,1,0);
INSERT INTO `properties` VALUES (3,1,5,'2B','2 Bed BA','',NULL,NULL,NULL,NULL,3,1,0);
/*!40000 ALTER TABLE `properties` ENABLE KEYS */;
UNLOCK TABLES;
