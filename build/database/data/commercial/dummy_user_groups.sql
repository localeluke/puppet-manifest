--
-- Dumping data for table `user_groups`
--

LOCK TABLES `user_groups` WRITE;
/*!40000 ALTER TABLE `user_groups` DISABLE KEYS */;
INSERT INTO `user_groups` VALUES (1,1,'Development Manager',0,NULL,NULL,0,1,1,0,NULL,0,1,0);
INSERT INTO `user_groups` VALUES (2,1,'Building Manager',0,NULL,NULL,0,1,1,0,NULL,0,1,0);
INSERT INTO `user_groups` VALUES (3,1,'Security ',0,NULL,NULL,0,1,1,0,NULL,0,1,0);
INSERT INTO `user_groups` VALUES (4,1,'Reception',0,NULL,NULL,0,1,1,0,NULL,0,1,0);
INSERT INTO `user_groups` VALUES (5,1,'Occupier',1,NULL,NULL,0,1,0,0,NULL,1,0,0);
INSERT INTO `user_groups` VALUES (6,1,'Occupier Representative',1,NULL,NULL,0,1,0,0,NULL,1,0,0);
/*!40000 ALTER TABLE `user_groups` ENABLE KEYS */;
UNLOCK TABLES;
