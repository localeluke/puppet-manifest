--
-- Dumping data for table `companies`
--

LOCK TABLES `companies` WRITE;
/*!40000 ALTER TABLE `companies` DISABLE KEYS */;
INSERT INTO `companies` VALUES (1,'Al Jazeera',1,1,'ALJZ','','','','','',0,1,NULL);
INSERT INTO `companies` VALUES (2,'Aqua',2,1,'AQU','','','','',NULL,0,1,NULL);
INSERT INTO `companies` VALUES (3,'Como',NULL,1,'COM','','','','','',1,0,NULL);
INSERT INTO `companies` VALUES (4,'Duff & Phelps',4,1,'DUF','','','','',NULL,0,1,NULL);
INSERT INTO `companies` VALUES (5,'Gem Construct',5,1,'GEM',NULL,NULL,NULL,NULL,NULL,1,1,NULL);
INSERT INTO `companies` VALUES (6,'Good People',6,1,'GOO',NULL,NULL,NULL,NULL,NULL,1,1,NULL);
INSERT INTO `companies` VALUES (7,'Kone',NULL,1,'KON',NULL,NULL,NULL,NULL,NULL,1,1,NULL);
INSERT INTO `companies` VALUES (8,'Locale',8,1,'SQ','','','','','',0,1,NULL);
INSERT INTO `companies` VALUES (9,'Hutong',37,1,'HUT','','','','',NULL,0,1,NULL);
INSERT INTO `companies` VALUES (10,'Mace',10,1,'MAC',NULL,NULL,NULL,NULL,NULL,1,1,NULL);
INSERT INTO `companies` VALUES (11,'NEWS',11,1,'NEW',NULL,NULL,NULL,NULL,NULL,1,0,NULL);
INSERT INTO `companies` VALUES (12,'Oblix',12,1,'OBL','','','','',NULL,0,1,NULL);
INSERT INTO `companies` VALUES (13,'Paragon',51,1,'PAR','','','','',NULL,0,1,NULL);
INSERT INTO `companies` VALUES (14,'Phelans',NULL,1,'PHE',NULL,NULL,NULL,NULL,NULL,1,1,NULL);
INSERT INTO `companies` VALUES (15,'Powermode',NULL,1,'POW',NULL,NULL,NULL,NULL,NULL,1,1,NULL);
INSERT INTO `companies` VALUES (16,'Real Estate Management (UK) Ltd',16,1,'REM','','','','','',0,1,NULL);
INSERT INTO `companies` VALUES (17,'Ruddy',NULL,1,'RUD','','','','','55',0,1,NULL);
INSERT INTO `companies` VALUES (18,'Sellar',18,1,'SEL','','','','',NULL,0,1,NULL);
INSERT INTO `companies` VALUES (19,'Shangri-La',19,1,'SHA','','','','',NULL,0,1,NULL);
INSERT INTO `companies` VALUES (20,'Chorus Group',NULL,1,'CHO','','','','','',1,0,NULL);
INSERT INTO `companies` VALUES (21,'Skanska',NULL,1,'SKA',NULL,NULL,NULL,NULL,NULL,1,1,NULL);
INSERT INTO `companies` VALUES (22,'TVFTS',22,1,'TVF','','','','',NULL,0,1,NULL);
INSERT INTO `companies` VALUES (23,'WFC',NULL,1,'WFC',NULL,NULL,NULL,NULL,NULL,1,1,NULL);
INSERT INTO `companies` VALUES (24,'Rubicon',NULL,1,'RUB',NULL,NULL,NULL,NULL,NULL,1,1,NULL);
INSERT INTO `companies` VALUES (25,'Modus',25,1,'MOD',NULL,NULL,NULL,NULL,NULL,1,1,NULL);
INSERT INTO `companies` VALUES (26,'Cofely',NULL,1,'COF','','','','','',1,0,NULL);
INSERT INTO `companies` VALUES (27,'Foresight Group',27,1,'FSGP','','','','',NULL,0,1,NULL);
INSERT INTO `companies` VALUES (28,'Mathys and Squire',28,1,'MAS','','','','',NULL,0,1,NULL);
INSERT INTO `companies` VALUES (29,'Overbury',NULL,1,'OVB','','','','','8',0,1,NULL);
INSERT INTO `companies` VALUES (30,'South Hook Gas',30,1,'SUHG','','','','',NULL,0,1,NULL);
INSERT INTO `companies` VALUES (31,'HCA',31,1,'HCA','','','','',NULL,0,1,NULL);
INSERT INTO `companies` VALUES (32,'Arma Partners LLP',32,1,'APLLP','','','','',NULL,0,1,NULL);
INSERT INTO `companies` VALUES (33,'Maris Interiors',NULL,1,'MIT',NULL,NULL,NULL,NULL,NULL,1,1,NULL);
INSERT INTO `companies` VALUES (34,'REM Construction',35,1,'REMCON',NULL,NULL,NULL,NULL,NULL,1,1,NULL);
INSERT INTO `companies` VALUES (35,'QNB',NULL,1,'QNB','','','','',NULL,0,1,NULL);
INSERT INTO `companies` VALUES (36,'Method',NULL,1,'MET',NULL,NULL,NULL,NULL,NULL,1,1,NULL);
INSERT INTO `companies` VALUES (37,'Campari',39,1,'CAM','','','','','',0,1,NULL);
INSERT INTO `companies` VALUES (38,'BLUU',NULL,1,'BLUU','','','','','',1,0,NULL);
INSERT INTO `companies` VALUES (39,'Warwick Business School',41,1,'WBS','','','','',NULL,0,1,NULL);
INSERT INTO `companies` VALUES (40,'Area Sq',NULL,1,'ASQ','','','','','',0,1,NULL);
INSERT INTO `companies` VALUES (41,'Structure Tone',43,1,'STR','','','','',NULL,0,1,NULL);
INSERT INTO `companies` VALUES (42,'McCue Crafted Fit',44,1,'MCF',NULL,NULL,NULL,NULL,NULL,1,1,NULL);
INSERT INTO `companies` VALUES (43,'Collins',45,1,'COL','','','','','',1,0,NULL);
INSERT INTO `companies` VALUES (44,'The Office Group',46,1,'TOG','','','','',NULL,0,1,NULL);
INSERT INTO `companies` VALUES (45,'Tiffany & Co',47,1,'TIFF','','','','',NULL,0,1,NULL);
INSERT INTO `companies` VALUES (46,'Arcapita',48,1,'ARC','','','','',NULL,0,1,NULL);
INSERT INTO `companies` VALUES (47,'Global Water Development',49,1,'GWD',NULL,NULL,NULL,NULL,NULL,1,1,NULL);
INSERT INTO `companies` VALUES (51,'test',NULL,1,'test','','','','','',1,1,NULL);
INSERT INTO `companies` VALUES (52,'test',NULL,1,'test','test','test','test','test 22','test',1,1,NULL);
INSERT INTO `companies` VALUES (53,'Test',NULL,1,'Test','','','','','',1,1,NULL);
INSERT INTO `companies` VALUES (54,'Dods Group plc',NULL,1,'DOD','','','','','11',0,1,NULL);
INSERT INTO `companies` VALUES (55,'Engie',NULL,1,'TS-Engie','','','','','',0,1,NULL);
INSERT INTO `companies` VALUES (56,'Fulcrum Chambers',NULL,1,'FUL','','','','','',0,1,NULL);
INSERT INTO `companies` VALUES (57,'Gallup',NULL,1,'GAL','','','','','18',0,1,NULL);
INSERT INTO `companies` VALUES (58,'Greenberg Traurig, LLP',NULL,1,'GRE','','','','','8',0,1,NULL);
INSERT INTO `companies` VALUES (59,'io Oil and Gas',NULL,1,'IOG','','','','','9',0,1,NULL);
INSERT INTO `companies` VALUES (60,'Jellyfish Group',NULL,1,'JEL','','','','','22',0,1,NULL);
INSERT INTO `companies` VALUES (61,'Khazanah',NULL,1,'KHZ','','','','','22 North ',0,1,NULL);
INSERT INTO `companies` VALUES (62,'Kraft Heinz',NULL,1,'KH','','','','','',0,1,NULL);
INSERT INTO `companies` VALUES (63,'Leonteq',NULL,1,'LEON','','','','','26',0,1,NULL);
INSERT INTO `companies` VALUES (64,'Level 2 Visitors - Even',NULL,1,'LEVEL2EVEN','','','','','Even Floors',0,1,NULL);
INSERT INTO `companies` VALUES (65,'Level 2 Visitors - Odd',NULL,1,'LEVEL2ODD','','','','','Odd Floors',0,1,NULL);
INSERT INTO `companies` VALUES (66,'M Moser Associates',NULL,1,'MMA','','','','','13',0,1,NULL);
INSERT INTO `companies` VALUES (67,'matchesfashion.com',NULL,1,'MAT','','','','','',0,1,NULL);
INSERT INTO `companies` VALUES (68,'Medical Protection Society',NULL,1,'MPS','','','','','19',0,1,NULL);
INSERT INTO `companies` VALUES (69,'O\'Sullivan\'s',NULL,1,'OSUL','','','','','Arcade',0,1,NULL);
INSERT INTO `companies` VALUES (70,'Protiviti',NULL,1,'PTI','','','','','10',0,1,NULL);
INSERT INTO `companies` VALUES (71,'Robert Half',NULL,1,'RHI','','','','','10',0,1,NULL);
INSERT INTO `companies` VALUES (72,'Sage',NULL,1,'SAG','','','','','17',0,1,NULL);
INSERT INTO `companies` VALUES (73,'Sapphire Systems',NULL,1,'SAP','','','','','9',0,1,NULL);
INSERT INTO `companies` VALUES (74,'Tabcorp',NULL,1,'TAB','','','','','13',0,1,NULL);
INSERT INTO `companies` VALUES (75,'Tetris-bluu',NULL,1,'TSB','','','','','17',0,1,NULL);
INSERT INTO `companies` VALUES (76,'The Retail Arcade',NULL,1,'TRA','','','','','',0,1,NULL);
INSERT INTO `companies` VALUES (77,'Unispace',NULL,1,'UNI','','','','','',0,1,NULL);
INSERT INTO `companies` VALUES (78,'XIO Group',NULL,1,'XIO','','','','','15',0,1,NULL);
INSERT INTO `companies` VALUES (79,'Locale',NULL,4,'Locale','','','','',NULL,1,1,NULL);
INSERT INTO `companies` VALUES (80,'Shard Quarter',NULL,1,'SHQ','','','','',NULL,0,1,NULL);
/*!40000 ALTER TABLE `companies` ENABLE KEYS */;
UNLOCK TABLES;
