--
-- Dumping data for table `user_groups`
--

LOCK TABLES `user_groups` WRITE;
/*!40000 ALTER TABLE `user_groups` DISABLE KEYS */;
INSERT INTO `user_groups` VALUES (1,1,'Estate Manager',0,NULL,NULL,0,1,1,0,NULL,0,1,0);
INSERT INTO `user_groups` VALUES (2,1,'Building Manager',0,NULL,NULL,0,1,1,0,NULL,0,1,0);
INSERT INTO `user_groups` VALUES (3,1,'Concierge',0,NULL,NULL,0,1,1,0,NULL,0,1,0);
INSERT INTO `user_groups` VALUES (4,1,'Owner',1,NULL,NULL,0,1,0,0,NULL,1,0,0);
INSERT INTO `user_groups` VALUES (5,1,'Owner Absent',1,NULL,NULL,0,1,0,0,NULL,1,0,0);
INSERT INTO `user_groups` VALUES (6,1,'Tenant',1,NULL,NULL,0,1,0,0,NULL,1,0,0);
/*!40000 ALTER TABLE `user_groups` ENABLE KEYS */;
UNLOCK TABLES;
