class localemariadb {
  class {'::mysql::server':
    package_name     => 'mariadb-server',
    package_ensure   => '10.0.38+maria-1~xenial',
    service_name     => 'mysql',
    root_password    => '',
    override_options => {
      mysqld => {
        'log-error' => '/var/log/mysql/mariadb.log',
        'pid-file'  => '/var/run/mysqld/mysqld.pid',
      },
      mysqld_safe => {
        'log-error' => '/var/log/mysql/mariadb.log',
      },
    }
  }

  # Dependency management. Only use that part if you are installing the repository
  # as shown in the Preliminary step of this example.
  Apt::Source['mariadb'] ~>
  Class['apt::update'] ->
  Class['::mysql::server']

  class {'::mysql::client':
    package_name    => 'mariadb-client',
    package_ensure  => '10.0.38+maria-1~xenial',
    bindings_enable => true,
  }

  # Dependency management. Only use that part if you are installing the repository as shown in the Preliminary step of this example.
  Apt::Source['mariadb'] ~>
  Class['apt::update'] ->
  Class['::mysql::client']

  class { 'localemariadb::database':
    table    => 'test',
    user     => 'user',
    password => 'password',
  }

  file { 'mysql-structure':
    path    => '/root/mysql-structure.sql',
    ensure  => present,
    owner   => 'root',
    group   => 'root',
    mode    => '0664',
    source  => 'puppet:///modules/files/structure',
    require => Service["mysql"],
  }

  file { 'version':
    path    => '/root/version.sql',
    ensure  => present,
    owner   => 'root',
    group   => 'root',
    mode    => '0664',
    source  => 'puppet:///modules/files/version',
    require => Service["mysql"],
  }

  exec { 'create-schema':
    command => 'mysql -uuser -ppassword test < /root/mysql-structure.sql',
    path    => '/usr/bin',
    require => Class['Localemariadb::Database'],
  }

  exec { 'insert-version':
    command => 'mysql -uuser -ppassword test < /root/version.sql',
    path    => '/usr/bin',
    require => [ Exec["create-schema"], File["version"] ],
  }

  exec { 'upgrade-version':
    command => 'php /home/www/locale-portal/private/scripts/upgrade.php -uuser -ppassword -hlocalhost -dtest --run-upgrade ',
    path    => '/usr/bin',
    require => [ Exec["insert-version"], Package['php7.2']],
  }
}
