
var Exports = {
  Modules : {}
};

Exports.Modules.Gallery = (function($, undefined) {
	var $grid,
	$shapes,
	$cat,
	shapes = [],
	cat = [],

	// Using shuffle with specific column widths
	columnWidths = 80,
	gutterWidths = 55,

	init = function() {
		
		setTimeout(function() {
		setVars();
		initFilters();
		initShuffle();
			setupSearching();
		},100);
	},

	setupSearching = function() {

	    // Advanced filtering
	    $('.js-shuffle-search').on('keyup change', function() {
	      
	      var val = this.value.toLowerCase();
	      
	      // Check sub-categories first
	      if($('.subCat .btn.active') && $('.subCat .btn.active').val() != "View all"){
	      	var current_category = $('.subCat .btn.active').attr('data-filter-value');
	      	// Change this into 'subcat'
	      	current_category = 'sub'+current_category;
	      }else{
	      	var current_category = $('.active').not('.subCat').attr('data-filter-value');
	      }

	      $grid.shuffle('shuffle', function($el, shuffle) {

	        // Only search elements in the current group
	        if (current_category !== 'all' && !$el.hasClass(current_category) ) {
	          return false;
	        }

	        var title       = $.trim( $el.find('.offerTitle').text() ).toLowerCase();
	        var text        = $.trim( $el.find('.offerContnet').text() ).toLowerCase();
	        var description = $.trim( $el.find('.offerDescription').text() ).toLowerCase();

	        return ( (title.indexOf(val) !== -1) || (text.indexOf(val) !== -1) || (description.indexOf(val) !== -1) );
	      });
	    });
	},

	setVars = function() {
		$grid = $('.js-shuffle');
		$shapes = $('.js-shapes');
		$cat = $('.js-cat');
	},

	initShuffle = function() {
		// instantiate the plugin
		$grid.shuffle({
			speed : 250,
			easing : 'cubic-bezier(0.165, 0.840, 0.440, 1.000)', // easeOutQuart
			columnWidth: 218,
			gutterWidth: 55
		});
	},

	initFilters = function() {
		// shapes
		$shapes.find('input').on('change', function() {
		var $checked = $shapes.find('input:checked'),
		groups = [];

		// At least one checkbox is checked, clear the array and loop through the checked checkboxes
		// to build an array of strings
		if ($checked.length !== 0) {
			$checked.each(function() {
				groups.push(this.value);
			});
		}
		shapes = groups;

		filter();
    });

    // cat

		$cat.on('click', '.btn', function(){
			var $this = $(this),
			$alreadyChecked,
			checked = [],
			active = 'active',
			isActive;

			if(!$(this).hasClass("active")){

				// Already checked buttons which are not this one
				$alreadyChecked = $this.siblings('.' + active);

				$this.toggleClass( active );

				// Remove active on already checked buttons to act like radio buttons
				if ( $alreadyChecked.length ) {
					$alreadyChecked.removeClass( active );
				}

				isActive = $this.hasClass( active );

				if ( isActive ) {
					checked.push( $this.data( 'filterValue' ) );
				}

				cat = checked;
				
				if ($this.val() == "View all" && !$(this).hasClass("sub-category")){
					//alert (1)
					cat = "";
					$('.subCat').fadeOut();
				}else{
					// Only do this check if we aren't clicking on a sub-cat already
					if(!$(this).hasClass("sub-category")){
						var subcats = [];
						// Load our correct sub categories if there are any
						for(var i=0; i<window.sub_categories.length;i++){
							var catreplace = cat[0].replace('cat_', '');
							
							if(window.sub_categories[i].parent == catreplace){
								subcats.push(window.sub_categories[i]);
							}
						}
						if(subcats.length>0){
							// Display our sub categories.
							var subhtml = '<input type="button" class="btn active sub-category" data-filter-value="cat_'+catreplace+'" value="View all">';
							for(var i=0; i<subcats.length;i++){
								// build our HTML.
								subhtml+='<input type="button" value="'+subcats[i].name+'" data-filter-value="cat_'+subcats[i].id+'" class="btn sub-category subcat_'+subcats[i].id+'" />';
								$('.subCat .buttonWrap').html(subhtml);

							}
							$('.subCat').fadeIn();
						}else{
							// Don't 
							$('.subCat').fadeOut();
						}
				}

				}
				// console.log(cat);
				// console.log(cat);
				filter();

			}
		});
	},

	filter = function() {
		if ( hasActiveFilters() ) {

			$grid.shuffle('shuffle', function($el) {
				return itemPassesFilters( $el.data() );
			});
		} else {
			$grid.shuffle( 'shuffle', 'all' );
		}
	},

	itemPassesFilters = function(data) {

		// If a shapes filter is active
		if ( shapes.length > 0 && !valueInArray(data.shape, shapes) ) {
		return false;
		}

		// If a cat filter is active

		if ( cat.length > 0 && !valueInArray(data.cat, cat) ) {
			// Check if in subcat before dismissing.
			if(!valueInArray(data.subcat, cat)){
		return false;
		}

		}

		return true;
	},

	hasActiveFilters = function() {
		// Remove all sub categories at this point.
		return cat.length > 0 || shapes.length > 0;
	},

	valueInArray = function(value, arr) {
		return $.inArray(value, arr) !== -1;
	};

  

  return {
    init: init
  };
}(jQuery));



$(document).ready(function() {
	Exports.Modules.Gallery.init();
});
