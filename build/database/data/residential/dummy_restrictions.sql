--
-- Dumping data for table `restrictions`
--

LOCK TABLES `restrictions` WRITE;
/*!40000 ALTER TABLE `restrictions` DISABLE KEYS */;
INSERT INTO `restrictions` VALUES (42,'blocks','block_id','multiselect-all',2);
INSERT INTO `restrictions` VALUES (43,'blocks','block_id','4',2);
INSERT INTO `restrictions` VALUES (44,'blocks','block_id','3',2);
INSERT INTO `restrictions` VALUES (45,'properties','property_id','multiselect-all',2);
INSERT INTO `restrictions` VALUES (46,'properties','property_id','1',2);
INSERT INTO `restrictions` VALUES (47,'properties','property_id','2',2);
INSERT INTO `restrictions` VALUES (48,'property_types','property_type_id','multiselect-all',2);
INSERT INTO `restrictions` VALUES (49,'property_types','property_type_id','5',2);
INSERT INTO `restrictions` VALUES (50,'property_types','property_type_id','3',2);
INSERT INTO `restrictions` VALUES (51,'property_types','property_type_id','2',2);
INSERT INTO `restrictions` VALUES (52,'property_types','property_type_id','1',2);
INSERT INTO `restrictions` VALUES (53,'property_types','property_type_id','4',2);
INSERT INTO `restrictions` VALUES (54,'user_groups','user_group_id','4',2);
INSERT INTO `restrictions` VALUES (55,'user_groups','user_group_id','5',2);
INSERT INTO `restrictions` VALUES (56,'user_groups','user_group_id','6',2);
INSERT INTO `restrictions` VALUES (57,'blocks','block_id','multiselect-all',1);
INSERT INTO `restrictions` VALUES (58,'blocks','block_id','4',1);
INSERT INTO `restrictions` VALUES (59,'blocks','block_id','3',1);
INSERT INTO `restrictions` VALUES (60,'properties','property_id','multiselect-all',1);
INSERT INTO `restrictions` VALUES (61,'properties','property_id','1',1);
INSERT INTO `restrictions` VALUES (62,'properties','property_id','2',1);
INSERT INTO `restrictions` VALUES (63,'property_types','property_type_id','multiselect-all',1);
INSERT INTO `restrictions` VALUES (64,'property_types','property_type_id','5',1);
INSERT INTO `restrictions` VALUES (65,'property_types','property_type_id','3',1);
INSERT INTO `restrictions` VALUES (66,'property_types','property_type_id','2',1);
INSERT INTO `restrictions` VALUES (67,'property_types','property_type_id','1',1);
INSERT INTO `restrictions` VALUES (68,'property_types','property_type_id','4',1);
INSERT INTO `restrictions` VALUES (69,'user_groups','user_group_id','2',1);
INSERT INTO `restrictions` VALUES (70,'user_groups','user_group_id','3',1);
INSERT INTO `restrictions` VALUES (71,'user_groups','user_group_id','1',1);
/*!40000 ALTER TABLE `restrictions` ENABLE KEYS */;
UNLOCK TABLES;
