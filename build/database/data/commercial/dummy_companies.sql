--
-- Dumping data for table `companies`
--

LOCK TABLES `companies` WRITE;
/*!40000 ALTER TABLE `companies` DISABLE KEYS */;
INSERT INTO `companies` VALUES (1,'Locale',2,'L','','','','','1',0,1);
INSERT INTO `companies` VALUES (2,'Semmle',1,'S','','','','','2',0,1);
INSERT INTO `companies` VALUES (3,'Printhouse',3,'PH','','','','','',0,1);
INSERT INTO `companies` VALUES (4,'Misty Day',3,'MD','','','','','',0,1);
/*!40000 ALTER TABLE `companies` ENABLE KEYS */;
UNLOCK TABLES;
