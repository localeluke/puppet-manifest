--
-- Dumping data for table `nav_titles`
--

LOCK TABLES `nav_titles` WRITE;
/*!40000 ALTER TABLE `nav_titles` DISABLE KEYS */;
INSERT INTO `nav_titles` VALUES (1,1,'Building','',0,1,1,1);
INSERT INTO `nav_titles` VALUES (2,1,'Nav Title 2','',0,0,1,1);
INSERT INTO `nav_titles` VALUES (3,1,'Facilities','',0,2,1,1);
INSERT INTO `nav_titles` VALUES (4,1,'Library','',0,3,1,1);
INSERT INTO `nav_titles` VALUES (5,1,'Modules','',0,3,1,1);
INSERT INTO `nav_titles` VALUES (6,1,'HOME','#home/dashboard',0,1,1,1);
INSERT INTO `nav_titles` VALUES (7,1,'BUILDING SERVICES','',0,2,1,0);
INSERT INTO `nav_titles` VALUES (8,1,'VERTICAL CITY','',0,3,1,0);
INSERT INTO `nav_titles` VALUES (9,1,'SHARD MEMBERS','',0,4,1,0);
INSERT INTO `nav_titles` VALUES (10,1,'LONDON BRIDGE QUARTER','',0,5,1,0);
INSERT INTO `nav_titles` VALUES (11,1,'LIBRARY','',0,6,1,0);
INSERT INTO `nav_titles` VALUES (12,1,'HELPDESK','',7,7,1,1);
INSERT INTO `nav_titles` VALUES (13,1,'Test nav','',0,8,1,1);
INSERT INTO `nav_titles` VALUES (14,1,'Test nav 2','',6,9,1,1);
INSERT INTO `nav_titles` VALUES (15,1,'DEMO','',16,7,1,0);
INSERT INTO `nav_titles` VALUES (16,1,'WELLNESS WEEK','',0,8,1,0);
INSERT INTO `nav_titles` VALUES (17,1,'TEST','',7,9,1,0);
INSERT INTO `nav_titles` VALUES (18,1,'Access','',7,10,1,0);
INSERT INTO `nav_titles` VALUES (19,1,'Deliveries','',7,11,1,0);
INSERT INTO `nav_titles` VALUES (20,1,'Operations','',7,12,1,0);
/*!40000 ALTER TABLE `nav_titles` ENABLE KEYS */;
UNLOCK TABLES;
