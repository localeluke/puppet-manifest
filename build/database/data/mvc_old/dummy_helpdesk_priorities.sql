--
-- Dumping data for table `helpdesk_priorities`
--

LOCK TABLES `helpdesk_priorities` WRITE;
/*!40000 ALTER TABLE `helpdesk_priorities` DISABLE KEYS */;
INSERT INTO `helpdesk_priorities` VALUES (1,'1. Immediate','#E74C3C',4,'02:00:00',0);
INSERT INTO `helpdesk_priorities` VALUES (2,'2. High','#C0392B',5,'04:00:00',0);
INSERT INTO `helpdesk_priorities` VALUES (3,'3. Medium','#F1C40F',3,'72:00:00',0);
INSERT INTO `helpdesk_priorities` VALUES (4,'4. Low','#2980B9',2,'168:00:00',0);
INSERT INTO `helpdesk_priorities` VALUES (5,'test priority','#40FF46',8,'01:30:00',1);
INSERT INTO `helpdesk_priorities` VALUES (6,'Test Priority','#8CF7FF',6,'01:30:00',1);
INSERT INTO `helpdesk_priorities` VALUES (7,'test','#C9C9FF',7,'01:00:00',1);
INSERT INTO `helpdesk_priorities` VALUES (8,'Test Status','#871AC7',4,'10:00:00',1);
INSERT INTO `helpdesk_priorities` VALUES (9,'Test 2','#3DFF71',4,'01:00:00',1);
INSERT INTO `helpdesk_priorities` VALUES (10,'test priority on v2','#FF6E6E',7,'12:30:00',1);
INSERT INTO `helpdesk_priorities` VALUES (11,'tedfgg','#FFFFFF',9,'02:30:00',1);
INSERT INTO `helpdesk_priorities` VALUES (12,'test multiple assignees','#7AEDFF',3,'00:00:00',1);
INSERT INTO `helpdesk_priorities` VALUES (13,'Super Urgent (test)','#FF1414',10,'01:00:00',1);
INSERT INTO `helpdesk_priorities` VALUES (14,'5. Quotable Work','#99C2FF',1,'00:00:00',0);
/*!40000 ALTER TABLE `helpdesk_priorities` ENABLE KEYS */;
UNLOCK TABLES;
