$(document).ready(function(){
	var mySwiper = new Swiper('.swiper-container',{
		//centeredSlides: true,
		slidesPerView: 'auto'
	});
	
	$('.arrow-left').on('click', function(e){
		e.preventDefault()
		mySwiper.swipePrev()
	})
	$('.arrow-right').on('click', function(e){
		e.preventDefault()
		mySwiper.swipeNext()
	})
	
	$('.download_doc').off('click').on('click', function() {
		window.location.href = $(this).attr('longdesc');
	});
});