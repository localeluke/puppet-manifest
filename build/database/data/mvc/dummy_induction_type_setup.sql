--
-- Dumping data for table `induction_type_setup`
--

LOCK TABLES `induction_type_setup` WRITE;
/*!40000 ALTER TABLE `induction_type_setup` DISABLE KEYS */;
INSERT INTO `induction_type_setup` VALUES (7,'Online Induction Only',1,0);
INSERT INTO `induction_type_setup` VALUES (8,'Online & Personal',1,0);
/*!40000 ALTER TABLE `induction_type_setup` ENABLE KEYS */;
UNLOCK TABLES;
