--
-- Dumping data for table `user_meta`
--

LOCK TABLES `user_meta` WRITE;
/*!40000 ALTER TABLE `user_meta` DISABLE KEYS */;
INSERT INTO `user_meta` VALUES (1,9,'calendar_token','c4d722783dc86b0c956987d7718ad819e79cce5c055c35f6318ba0a8f08e');
INSERT INTO `user_meta` VALUES (2,1,'calendar_token','58d6f27bde3b12db1ce238b0bae8cdfd1f04c1e0fd1ba1a4756f806444c0');
INSERT INTO `user_meta` VALUES (3,3,'calendar_token','8f5941e374d2805e1a2e2fbe9e262139f3a73344a9be5beba4d68bcd59a2');
/*!40000 ALTER TABLE `user_meta` ENABLE KEYS */;
UNLOCK TABLES;
