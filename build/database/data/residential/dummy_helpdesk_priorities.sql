--
-- Dumping data for table `helpdesk_priorities`
--

LOCK TABLES `helpdesk_priorities` WRITE;
/*!40000 ALTER TABLE `helpdesk_priorities` DISABLE KEYS */;
INSERT INTO `helpdesk_priorities` VALUES (1,'High','#DB1616',1,'24:00:00',0);
INSERT INTO `helpdesk_priorities` VALUES (2,'Medium','#FFEE00',2,'48:00:00',0);
INSERT INTO `helpdesk_priorities` VALUES (3,'Low','#08C202',3,'72:00:00',0);
/*!40000 ALTER TABLE `helpdesk_priorities` ENABLE KEYS */;
UNLOCK TABLES;
