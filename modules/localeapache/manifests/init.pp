class localeapache {
  class { 'apache':
    default_vhost => false,
    mpm_module    => false,
  }

  file { '/etc/ssl/certs/apache-selfsigned.crt':
    ensure  => present,
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    source  => 'puppet:///modules/files/apache-selfsigned.crt',
    require => Apache::Vhost['default'],
  }

  file { '/etc/ssl/private/apache-selfsigned.key':
    ensure  => present,
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    source  => 'puppet:///modules/files/apache-selfsigned.key',
    require => Apache::Vhost['default'],
  }

  apache::vhost { 'empty':
    port    => '80',
    docroot => '/home/www/puppet',
  }

  file { '/etc/apache2/sites-enabled/locale.conf':
    source  => 'puppet:///modules/files/locale.conf',
    require => Apache::Vhost['default'],
  }

  file { '/etc/apache2/sites-enabled/locale-ssl.conf':
    source  => 'puppet:///modules/files/default-ssl.conf',
    require => Apache::Vhost['default'],
  }

  file { '/etc/apache2/ports.conf':
    ensure  => present,
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    source  => 'puppet:///modules/files/ports.conf',
    require => [ File["/etc/ssl/private/apache-selfsigned.key"], File["/etc/ssl/certs/apache-selfsigned.crt"] ],
  }

  class { 'apache::mod::ssl': }

  class { 'apache::mod::vhost_alias': }

  class { 'apache::mod::prefork': }

  class { 'apache::mod::rewrite': }

  class { 'apache::mod::php':
    require => Class['apache::mod::prefork'],
  }

  exec { 'restart-service':
    command => 'sudo service apache2 restart',
    path    => '/usr/bin',
    require => [
      File["/etc/apache2/ports.conf"],
      File["/etc/apache2/sites-enabled/locale-ssl.conf"],
      File["/etc/apache2/sites-enabled/locale.conf"],
      Class['apache::mod::ssl'],
      Class['apache::mod::vhost_alias'],
      Class['apache::mod::php'],
      Class['apache::mod::rewrite'],
    ],
  }
}
