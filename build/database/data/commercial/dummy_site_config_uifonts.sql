--
-- Dumping data for table `site_config_uifonts`
--

LOCK TABLES `site_config_uifonts` WRITE;
/*!40000 ALTER TABLE `site_config_uifonts` DISABLE KEYS */;
INSERT INTO `site_config_uifonts` VALUES (1,'Open Sans','GOOGLE','fonts.googleapis.com/css?family=Open+Sans:300,400,700,600');
INSERT INTO `site_config_uifonts` VALUES (2,'brown-regular','EMBEDDED','');
INSERT INTO `site_config_uifonts` VALUES (3,'Lato','GOOGLE','fonts.googleapis.com/css?family=Lato:300,400,700,900');
INSERT INTO `site_config_uifonts` VALUES (4,'Roboto Condensed','GOOGLE','fonts.googleapis.com/css?family=Roboto+Condensed:400,300,700');
INSERT INTO `site_config_uifonts` VALUES (5,'Source Sans Pro','GOOGLE','fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700');
INSERT INTO `site_config_uifonts` VALUES (6,'PT Sans Narrow','GOOGLE','fonts.googleapis.com/css?family=PT+Sans+Narrow:400,700');
/*!40000 ALTER TABLE `site_config_uifonts` ENABLE KEYS */;
UNLOCK TABLES;
