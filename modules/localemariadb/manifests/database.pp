class localemariadb::database (
  $table = 'test',
  $user = 'user',
  $password = 'password'
) {
  mysql::db { $table:
    user     => $user,
    password => $password,
    host     => 'localhost',
    grant    => ['ALL'],
    require  => Class['::mysql::client'],
  }
}
