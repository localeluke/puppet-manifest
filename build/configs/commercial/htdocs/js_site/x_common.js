$(window).load(function(){
	$(document).ready(function(){
		//Code for background Image strach
		//$.backstretch(cDocRoot + "proimages/eBuryBg.jpg");
		/*$.backstretch("proimages/eBuryBg.jpg");
		$(".backstretch").append("<div class='backStretchOverlay'></div>");
		$(".backstretch").fadeIn(1200);*/
		//to hide slider
		if(location.hash)
		$(".slider").hide();
		
		//Responsive grid initialization.
		$(".flurid").flurid();
		
		$(".mainBox").mouseenter(function(){
			$(this).addClass("hover");
		});
		$(".mainBox").mouseleave(function(){
			$(this).removeClass("hover");
		});
		
		
		//========== top search starts ==========//
		$(".topSearch").click(function(){
			if($(this).val() == "search"){
				$(this).val("");
			}
		});
		
		$(".topSearch").blur(function(){
			if($(this).val() == ""){
				$(this).val("search");
			}
		});
		//========== top search ends ==========//
		
		
		
		
		
		//========== Profile Pic dropdown initialization starts ==========//
		$(".profilePic .profImg").click(function(){$(this).parent().toggleClass("active");});
		$(".expand").click(function(){
			var thisPar = $(this).parent();
			$(".subSpan", thisPar).stop(true).slideToggle("slow", function(){
				$(".profilePic .profOptionsMenu .link .subSpan").mCustomScrollbar("destroy");
				$(".profilePic .profOptionsMenu .link .subSpan").mCustomScrollbar({
					scrollButtons:{
						enable:false
					},
					contentTouchScroll: true
				});
				$(".profilePic .profOptionsMenu .link .subSpan").css("opacity", 1);
			});
			
			return false;
		});
		
		$(".profilePic .profOptionsMenu .link .subSpan span").click(function(){
			var thisTxt = $(this).text();
			$(".subSpan").stop(true).slideUp("slow", function(){
				$(".profilePic").removeClass("active");
			});
			$(".profileAddr").text(thisTxt);
		})
		//========== Profile Pic dropdown initialization ends ==========//
		
		
		//========== Announcement section starts ==========//
		$(".ancContent_1").show();
		$(".icon1").addClass("active");
		var contAnc = $(".ancContent").length;
		var Anc = 2;
		var auto_slide_seconds = 8000;
		var timer;
		function setAnnounce(){
			timer = setInterval(function(){
				$(".ancContent").fadeOut(1500).promise();
				$(".ancContent_"+ Anc).fadeIn(1500);
				$(".ico").removeClass("active");
				$(".icon"+ Anc).addClass("active");
				if(Anc ==contAnc){
					Anc=1;
				}else{
					Anc++;
				}
			}, auto_slide_seconds);
		}
		setAnnounce();
		
		$(".ancContent").mouseenter(function(){
			clearInterval(timer);
		});
		$(".ancContent").mouseleave(function(){
			setAnnounce();
		});
		
		$(".ico").click(function(){

			var thisRel = parseInt($(this).attr("data-rel"));
			clearInterval(timer);



			$(".ico").removeClass("active");
			$(".icon"+ thisRel).addClass("active");
			$(".ancContent").fadeOut("slow").promise();
			$(".ancContent_" + thisRel).fadeIn("slow");
			if (thisRel == contAnc){
				Anc = 1;
			}else{
				Anc = (thisRel + 1);
			}
			setAnnounce();
			
			return false;
		});
		//========== Announcement section ends ==========//
		

		//========== Magnific popup initialization starts ==========//
		$('.popup-zoom').magnificPopup({
			type: 'inline',

			fixedContentPos: false,
			fixedBgPos: true,

			overflowY: 'auto',

			closeBtnInside: true,
			preloader: false,
			
			midClick: true,
			removalDelay: 300,
			mainClass: 'my-mfp-zoom-in'
		});

		$('.popup-move').magnificPopup({
			type: 'inline',

			fixedContentPos: false,
			fixedBgPos: true,

			overflowY: 'auto',

			closeBtnInside: true,
			preloader: false,
			
			midClick: true,
			removalDelay: 300,
			mainClass: 'my-mfp-slide-bottom'
		});
		$('.ajaxPopup').magnificPopup({
			type: 'ajax',
			alignTop: true,
			overflowY: 'scroll' // as we know that popup content is tall we set scroll overflow by default to avoid jump
		});
		//========== Magnific popup initialization ends ==========//
		
		
		//========== Ui spinner (timePicker) initialization starts ==========//	
		if ($( ".spinner" ).length > 0){
			var spinner = $( ".spinner" ).spinner();
		}
		
		if ($( ".timeSpinner" ).length > 0){
			var timeSpinner = $( ".timeSpinner" ).timespinner();
		}
		
		//========== Ui spinner (timePicker) initialization ends ==========//
		
		
		
		//========== sidemenu starts ==========//
			var set = 0;
			//function to expand or open sidepanel
			function panelExpand(){
				$(".sidePanel").animate({
					marginLeft : "0"
				}, 100);
				$(".pageWrapper").animate({
					marginLeft : "70%",
					width : $(window).width() + 'px'
				}, 300, function(){
					set = 1;
					$(".sidePanel").css("height", $(".pageWrapper").height()+"px");
					$(".mainWrapper").css({
						'width' : $(window).width() + 'px',
						'overflow' : 'hidden'
					});
				});
			}
			
			//function to close the sidepanel
			function panelClose(){
				$(".sidePanel").animate({
					marginLeft : "-" + "70%"
				}, 300);
				$(".pageWrapper").animate({
					marginLeft : "0px",
					width : '100%'
				}, 300, function(){
					set = 0;
					$(".sidePanel").css("height", "auto");
					$(".mainWrapper").css({
						'width' : 'auto',
						'overflow' : 'visible'
					});
				});
			}
			
			//click event on show sidepanel
			$(".showMenu").click(function(){
				if (set == 0){
					panelExpand();
				}else{
					panelClose();
				}
			});
			
			//code to check if main menu link has the submenu
			$(".sidePanel ul li, .LHS-menu ul li").each(function(){
				if($(this).has('ul').length){
					$(this).addClass('hasSub');
				}
			});
			
			//code to show the submenu if the parent has active class
			$(".sideSubUl ul li a, .LHS-menu ul li a").each(function(){
				if($(this).hasClass('active')){
					$(this).parent().parent().css('display', 'block');
				}
				if($(this).hasClass('current')){
					$(this).parent().parent().parent().addClass('hasSubActive active');
				}
			});
			
			//click event for "hasSub" 
			$(".hasSub .mainLink").live('click', function(){
				var thisPar = $(this).parent().parent();
				var cl = false;
				//$(".hasSub ul", thisPar).slideUp("slow");
				//if(!$(this).parent().hasClass('active')){
					$(this).parent().children('ul').slideToggle("slow");
					cl = true;
				//}
				//$(".hasSub", thisPar).removeClass("active");
				//if(cl){
					$(this).parent().toggleClass("active");
				//}
				return false;
			});
			
			$(".hasSub ul li a, .LHS-menu ul li a").live('click', function(){
				$(".hasSub").unbind();
				var thisHref = $(this).attr("href");
				window.location.replace(thisHref);
				if ($(".sidePanel").css("margin-left") == "0px"){
					$(".showMenu").click();
				}
			});
			
			//hover effect for mobile menu button
			$(".showMenu").mouseenter(function(){
				$(this).addClass("hover")
			}).mouseleave(function(){
				$(this).removeClass("hover")
			});
			
			function adjContHi(){
				var contMinHi = $(window).height() - ($(".header").height() + $(".footer").height() + 100)
				$(".contentWrap").css("min-height", contMinHi);
			}
			adjContHi();
			
			
			$(window).resize(function(){
				adjContHi();
				if(set == 1){
					if ($(window).width() < 985){
						$("body").css({
							'width' : $(window).width() + 'px',
							'overflow' : 'hidden'
						});
						//panelClose();
					}else{
						$("body").css({
							'width' : 'auto',
							'overflow' : 'visible'
						});
						panelClose();
					}
					if (!window.screenTop && !window.screenY) {
						
					}else{
						//alert('Browser is in fullscreen');
						setInterval(function(){window.location.reload();}, 800);
					}
				}
				
				$(".LHS-menu").css({
					"width": $(".LHS").width() + "px"
				});
				
				$('#LiqBox').masonry("reload");
			});
			
			$(window).bind('orientationchange', function(e) {
				if(set == 1){
					panelClose();
				}
			});
		//========== sidemenu ends ==========//
		
		
		
		
		
		
		
		//========== cookie section starts ==========//
			//function to create cookie
			/************************************************/
			function createCookie(name,value,days) {
				if (days) {
					var date = new Date();
					date.setTime(date.getTime()+(days*24*60*60*1000));
					var expires = "; expires="+date.toGMTString();
				}
				else var expires = "";
				document.cookie = name+"="+value+expires+"; path=/";
			}

			//function to read cookie
			function readCookie(name) {
				var nameEQ = name + "=";
				var ca = document.cookie.split(';');
				for(var i=0;i < ca.length;i++) {
					var c = ca[i];
					while (c.charAt(0)==' ') c = c.substring(1,c.length);
					if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
				}
				//return null;
				return false;
			}

			//function to erase cookie
			function eraseCookie(name) {
				createCookie(name,"",-1);
			}
			/************************************************/
			
			
			if(!readCookie("cookie_tower_portal")){
				createCookie("cookie_tower_portal", "set", 3650);
				$(".cookieWrap").slideDown();
				
				setTimeout(function(){
					$(".cookieWrap").slideUp("slow");
				}, 10000);
			}
			
			//cookie close action
			$(".cookieClose").live("click", function(){
				$(".cookieWrap").slideUp("slow");
				return false;
			});
		//========== cookie section ends ==========//
		
		
		
		
		//========== Fancy checkbox and radiobuttons initialization starts ==========//
		fancyCheckboxInit();
		fancyCheckAll();
		modulePopup();
		//========== Fancy checkbox and radiobuttons initialization ends ==========//
		
		//========== Script for uiPage starts ==========//
		/*$('.tblDiv .desktopTbl table tbody>tr:odd').css("background-color", "#222222");
		$('.tblDiv .desktopTbl table tbody>tr:odd').addClass("odd");
		$('.tblDiv .desktopTbl table tbody>tr:even').addClass("even");*/
		
		/*$('.tblDiv .desktopTbl table tbody>tr').live("mouseenter", function(){$(this).addClass("hover");});
		$('.tblDiv .desktopTbl table tbody>tr').live("mouseleave", function(){$(this).removeClass("hover");});*/
		
		/*$("a.showView, .tblTitleTxt").live("click", function(){
			var thisPar = $(this).parent().parent();
			$(this).parent().toggleClass("active");
			
			$(".tableDesc", thisPar).slideToggle("fast");
			
			return false;
		});*/
				
		$("input[type='button'], input[type='submit'], .btn-custom, .small-btn, .odd, .even, .widget_tabWrap a").live("mouseenter", function(){
			$(this).addClass("hover");
		});
		$("input[type='button'], input[type='submit'], .btn-custom, .small-btn, .odd, .even, .widget_tabWrap a").live("mouseleave", function(){
			$(this).removeClass("hover");
		});
		//========== Script for uiPage starts ==========//
		
		
		//========== Script for Mobile keypad starts ==========//
			var event;
			$('.telephone').click(function(event){
				$('#keypad').fadeToggle('fast');
				event.stopPropagation();
			});
			$('.key').click(function(event){
				var telephone = $('.telephone');
				telephone.val( telephone.val() + this.innerHTML);
				event.stopPropagation();
			});
			
			$('.btn-click').click(function(event){
				if(this.innerHTML == 'DEL'){
					var telephone = $('.telephone');
					if(telephone.val().length > 0){
						telephone.val(telephone.val().substring(0, telephone.val().length - 1));
					}
				}else{
					$('.telephone').val("");
				}
				event.stopPropagation();
			});
		//========== Script for Mobile keypad ends ==========//
		
		
		//========== fullcalendar script starts ==========//
		$(".fc-day").live("click", function(){
			$(".fc-day").removeClass("active");
			$(this).addClass("active");
			var childObj = $(this).find("div:first");
			$(childObj).css({
				width: ($(this).width() - 4),
				height: ($(this).height() - 4)
			})
		});
		//========== fullcalendar script ends ==========//
		
		//========== input field focus code starts ==========//
		$(".inpt, .hasDatepicker, .datepicker, .ui-spinner, textarea").live("focus", function(){
			$(this).parent().parent().addClass("focus");
		});
		$(".inpt, .hasDatepicker, .datepicker, .ui-spinner, textarea").live("blur", function(){
			$(this).parent().parent().removeClass("focus");
		});
		//========== input field focus code ends ==========//
		
		
		//========== functions to be loaded on document load starts ==========//
		setBlackWrapper();
		dashboradIcons();
		checkFooter();
		init_select2();
		
		ui_ajaxTabing_init()
		$(".tabWrapper .tab2").click();
		
		//predictive_searchUsers();
		//========== functions to be loaded on document load ends ==========//
		
		$(window).scroll(function (){
			
		});
		
		$(window).resize(function(){
			setBlackWrapper();
			checkFooter();
		});
		
		$(window).bind('orientationchange', function(e) {
			checkFooter();

		});
		
		
						
	});
});

function ajax_popup_init()
{
	$('.ajaxPopup2').magnificPopup({
			type: 'ajax',
			alignTop: false,
			overflowY: 'scroll' // as we know that popup content is tall we set scroll overflow by default to avoid jump
	});
}

function checkFooter(){
	var footerHi = $(".footerWrap").height();
	var bottomSpacer = $(".bottomSpacer").height();
	var maxWrap = $(".mainWrapper").height();
	var windowHi = $(window).height();
	
	var addHi = $(window).height() - (99 + $(".footerWrap").height() + $(".header").height());
	
	$("#content").css("min-height", addHi+"px");
	
	/*if (maxWrap > windowHi){
		$(".footerWrap").css("position", "relative");
		//$(".bottomSpacer").css("height", "10px");
	}else{
		$(".footerWrap").css("position", "fixed");
		//$(".bottomSpacer").removeAttr("style");
	}*/
	
	/*if (maxWrap > windowHi){
		$(".footerWrap").css("position", "relative");
	}else{
		$(".footerWrap").css("position", "fixed");
	}*/
	$(".footerWrap").css("position", "relative");
	

}




function ui_ajaxTabing_init(){
	$(".tabs a").live("click", function(){
		var thisPar 	= $(this).parent().parent().parent();

		var thisAjax 	= $(this).attr("data-ajax");
		var thisKeyTab 	= $(this).attr("data-keyTab");
		var thisDataRel	= $(this).attr("data-rel");
		showPageLoader();
		$(".tabsDiv a", thisPar).removeClass("current");
		$(this).addClass("current");

		
		$(".tabCont", $(this).parent().parent()).hide();
		$(".tabCont", $(this).parent().parent()).css("background", "none");
		
		if(thisAjax){
			var st=thisAjax.indexOf("[");
			var methodName = thisAjax.substr(0, st );
			var methodParam = thisAjax.substr((st + 1), (thisAjax.length - (st + 2)) );
			methodParam = methodParam.split(' ');
			var method = eval('(' + methodName + ')');
			var methodData = method(methodParam); // calls foo()

			$(".tabCont" + thisDataRel + "", $(this).parent().parent()).html(methodData);
			var thisObj = $(this);
			setTimeout(function(){
				$(".tabCont" + thisDataRel + "", thisObj.parent().parent()).show();


			}, 1000);
			$(".tabCont" + thisDataRel + "", $(this).parent().parent()).css("background", "#ffffff");
		}else{
			$(".tabCont" + thisDataRel + "", $(this).parent().parent()).show();
			$(".tabCont" + thisDataRel + "", $(this).parent().parent()).css("background", "#ffffff");
			setTimeout(function(){
				hidePageLoader()
			}, 800);

		}
		
		return false;
	});
}




function foo(paramArray){
	var t = paramArray[0] + " " + paramArray[1] + " " + paramArray[2];
	return t;
}

//========== Masonry initialization starts ==========//
function dashboradIcons(){
	if($('#LiqBox').length > 0){
		$('#LiqBox').masonry({
			itemSelector: '.box',
			isAnimated: true,
			gutterWidth: 1,
			isResizable: true,
			isFitWidth: true
		});
		$("#LiqBox").removeClass("scatterFix");
	}
}
//========== Masonry initialization ends ==========//

//Code for blackWrapper take full height of window
function setBlackWrapper(){
	$(".blackWrapper").css("min-height", $(window).height() - 52);
}

//========== Fancycheckbox and radio buttons functions starts ==========//
function fancyCheckboxInit(){
	
	//$(".checkChange").before("<div class='chk'></div>");
	$(".checkChange").each(function(){
		if(!$(this).prev().hasClass("chk"))
		$(this).before("<div class='chk'></div>");
	});
	$('.checkChange').css("opacity", 0);
	
	$('.checkChange').die();
	$('.checkChange').live("click", function(){
		var eleName = $(this).prop("name").split('[')[0];
		if($(this).prop('type')=='radio'){
			$(".radioCont ."+ eleName).removeClass("clicked");
			$(this).prev().toggleClass("clicked");
		}else{
			$(this).prev().toggleClass("clicked");
		}
	});
}

function fancyCheckAll(){
	$('.checkChange').each(function(){
		var eleName = $(this).prop("name").split('[')[0];
		$(this).prev().addClass(eleName);
  
		if ($(this).is(":checked")){
			$(this).prev().addClass("clicked");
		}
	});
}
//========== Fancycheckbox and radio buttons functions ends ==========//




function responsiveDT(thisObj){
	/* start: code for responsive */
	var firstCol = $(thisObj).attr('data-firstCol');
	var secondCol = $(thisObj).attr('data-secondCol');
	var altColumn = $(thisObj).attr('data-altColumn');
	
	$('tr td:nth-child('+firstCol+')', $(thisObj)).addClass('showDataCol');
	$('tr td:nth-child('+secondCol+')', $(thisObj)).addClass('showDataCol');
	$('tr td:nth-child('+altColumn+')', $(thisObj)).addClass('altShowCol');
	
	$('tr th:nth-child('+firstCol+')', $(thisObj)).addClass('showDataCol');
	$('tr th:nth-child('+secondCol+')', $(thisObj)).addClass('showDataCol');
	$('tr th:nth-child('+altColumn+')', $(thisObj)).addClass('altShowCol');
	
	$('tr td, tr th', $(thisObj)).each(function(){
		if(!$(this).hasClass('showDataCol')){
			$(this).addClass('hideDataCol');
		}
	});
	$(".altShowCol").removeClass("hideDataCol");
	/* end: code for responsive */
}


(function ( $ ) {
	$.fn.tabify = function( options ) {
		// Default options.
		var settings = $.extend({
			// These are the defaults.
			tabPosition: "top"
		}, options );
		
		// main code.
		return this.each(function(index) {
			// Do something to each element here.
			$(this).append('<div>'+  settings.tabPosition +'--' + index + '</div>')
		});
	};
}( jQuery ));




function format_elements()
	{
			checkFooter();
			//Responsive grid initialization.
			$(".flurid").flurid();
			
			$(".mainBox").mouseenter(function(){
				$(this).addClass("hover");
			});
			
			$(".mainBox").mouseleave(function(){
				$(this).removeClass("hover");
			});
		
			init_select2();
			predictive_searchUsers();
			predictive_searchProperties();
			init_group_buttons();
			check_group_active();
			modulePopup();
			init_helpful_text();
			ui_ajaxTabing_init()
			$(".tabWrapper .tab2").click();
			
		$(".tabWrapper .tab2").click();
		
		function foo(paramArray){
			var t = paramArray[0] + " " + paramArray[1] + " " + paramArray[2];
			return t;
		}
		
		/*js for tabing system with provision to give function name and parameters ends*/
		
		
		//========== Magnific popup initialization starts ==========//
		$('.popup-zoom').magnificPopup({
			type: 'inline',

			fixedContentPos: false,
			fixedBgPos: true,

			overflowY: 'auto',

			closeBtnInside: true,
			preloader: false,
			
			midClick: true,
			removalDelay: 300,
			mainClass: 'my-mfp-zoom-in'
		});

		$('.popup-move').magnificPopup({
			type: 'inline',

			fixedContentPos: false,
			fixedBgPos: true,

			overflowY: 'auto',

			closeBtnInside: true,
			preloader: false,
			
			midClick: true,
			removalDelay: 300,
			mainClass: 'my-mfp-slide-bottom'
		});
		$('.ajaxPopup').magnificPopup({
			type: 'ajax',
			alignTop: true,
			overflowY: 'scroll' // as we know that popup content is tall we set scroll overflow by default to avoid jump
		});
		//========== Magnific popup initialization ends ==========//
		
		
		//========== Ui spinner (timePicker) initialization starts ==========//	

		if ($( ".spinner" ).length > 0){
			var spinner = $( ".spinner" ).spinner({ min: 1 });
		}
		/*

		if ($( ".timeSpinner" ).length > 0){
			var timeSpinner = $( ".timeSpinner" ).timespinner();
		}
		*/

		//========== Ui spinner (timePicker) initialization ends ==========//
		
		
		
		
		//========== Fancy checkbox and radiobuttons initialization starts ==========//
		fancyCheckboxInit();
		fancyCheckAll();
		//========== Fancy checkbox and radiobuttons initialization ends ==========//
		
		
		
		//========== Fancy Button Panel initialization starts ==========//
		$(".buttonpanel").buttonpanel(
							{ 

								singular : ['5']   //1,2,3,4,5 - a number to identify which in line it is. If this one is selected no other will be selected
							}
						 
						 );
		//========== Fancy Button Panel initialization ends ==========//
		
				
		$("input[type='button'], input[type='submit'], .btn-custom, .small-btn, .odd, .even").live("mouseenter", function(){
			$(this).addClass("hover");
		});
		$("input[type='button'], input[type='submit'], .btn-custom, .small-btn, .odd, .even").live("mouseleave", function(){
			$(this).removeClass("hover");
		});
		//========== Script for uiPage starts ==========//
		
		
		//========== Script for Mobile keypad starts ==========//
			var event;
			$('.telephone').click(function(event){
				$('#keypad').fadeToggle('fast');
				event.stopPropagation();
			});
			$('.key').click(function(event){
				var telephone = $('.telephone');
				telephone.val( telephone.val() + this.innerHTML);
				event.stopPropagation();
			});
			
			$('.btn-click').click(function(event){
				if(this.innerHTML == 'DEL'){
					var telephone = $('.telephone');
					if(telephone.val().length > 0){
						telephone.val(telephone.val().substring(0, telephone.val().length - 1));
					}
				}else{
					$('.telephone').val("");
				}
				event.stopPropagation();
			});
		//========== Script for Mobile keypad ends ==========//
		
		
		//========== fullcalendar script starts ==========//
		$(".fc-day").live("click", function(){
			$(".fc-day").removeClass("active");
			$(this).addClass("active");
		});
		//========== fullcalendar script ends ==========//
		
		
		//========== functions to be loaded on document load starts ==========//
		setBlackWrapper();
		//========== functions to be loaded on document load ends ==========//
		
		$(window).scroll(function (){
			
		});
		
		$(window).resize(function(){
			setBlackWrapper();
		});
		
		$(window).bind('orientationchange', function(e) {
			
		});
	
	
		//========== Custom Elements initialization starts ==========//
		//if($(".buttonpanel").length > 0)
		//{		
		$(".buttonpanel").buttonpanel(
										{ 
											multiple : false,
											singular : ['5']   //1,2,3,4,5 - a number to identify which in line it is. If this one is selected no other will be selected
										}
									 
									 );
		//}
		if($(".toggleswitch").length > 0)
		{
			$(".toggleswitch").toggleswitch();
		}
		//========== Custom Elements initialization ends ==========//
		
			
	}
	//END: Format Elements

	function initScroll(){
		$("ul.select2-results").mCustomScrollbar("destroy");
		$("ul.select2-results").mCustomScrollbar({
			scrollButtons:{
				enable:false
			},
			contentTouchScroll: true
		});
	}
	
	
function ajaximage_init()
{
	var uploader = new qq.FileUploader({		
		element				: document.getElementById('file-uploader'),
		action				: '{{ $DOCROOT }}home/uploader',
		allowedExtensions	: ['jpg', 'png', 'bmp'],
		sizeLimit			: 100000000,	// 100mb
		minSizeLimit		: 500,
		multiple 			: false,
		uploadButtonText	: '<div class="overlay-qqfileupload">Upload</div>',
		onProgress			: function(id, fileName, loaded, total){
									$('.qq-upload-button').css('background-image', '');
									$('.qq-upload-button').css('background-image', 'url("{{ $DOCROOT }}images/ajax-load.gif")');
								},
		onComplete			: function(id, fileName, responseJSON){
									$('.qq-upload-button').css('background-image', '');
									$('.qq-upload-button').css('background-image', 'url("{{ $DOCROOT }}UserFiles/'+fileName+'")');
									$('.qq-upload-button').css('background-size',  '125px 125px');
									
									//Save to users table this name
									var hash    = location.hash;
									var spl     = hash.split('/');
									var user_id = spl[2];
									$.ajax
									({
									  type    : "POST",
									  url     : '{{ $DOCROOT }}users/save_image',
									  data    : { 
												 user_id  : user_id,
												 filename : fileName
												}
									});
								$('#file-uploader').trigger('mouseleave');
								}
	}); 
}
