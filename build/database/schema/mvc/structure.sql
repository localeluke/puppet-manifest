-- MySQL dump 10.15  Distrib 10.0.38-MariaDB, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: mvc
-- ------------------------------------------------------
-- Server version	10.0.37-MariaDB-1~xenial

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `activity_log`
--

DROP TABLE IF EXISTS `activity_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `activity_log` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned DEFAULT NULL,
  `module` varchar(50) NOT NULL,
  `action` text NOT NULL,
  `action_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `action_by` int(11) unsigned DEFAULT NULL COMMENT 'user_id from session',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1289 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `admin`
--

DROP TABLE IF EXISTS `admin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admin` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `domain_id` int(11) unsigned NOT NULL DEFAULT '0',
  `firstname` varchar(50) NOT NULL DEFAULT '',
  `lastname` varchar(50) NOT NULL DEFAULT '',
  `username` varchar(225) NOT NULL DEFAULT '',
  `password` varchar(100) NOT NULL DEFAULT '',
  `email` varchar(50) NOT NULL DEFAULT '',
  `active` char(1) NOT NULL DEFAULT '',
  `admin_type` enum('superadmin','ma_access') DEFAULT NULL,
  `block_id` int(11) unsigned NOT NULL DEFAULT '0',
  `deleted` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  KEY `admin_type` (`admin_type`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `admin_blocks`
--

DROP TABLE IF EXISTS `admin_blocks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admin_blocks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `block_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `admin_calendar_config`
--

DROP TABLE IF EXISTS `admin_calendar_config`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admin_calendar_config` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mod_id` int(11) NOT NULL,
  `cat_name` varchar(255) DEFAULT NULL,
  `cat_colour` varchar(255) DEFAULT NULL,
  `existing_cat` int(11) NOT NULL,
  `active` tinyint(4) NOT NULL DEFAULT '1',
  `deleted` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `admin_deliveries_config`
--

DROP TABLE IF EXISTS `admin_deliveries_config`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admin_deliveries_config` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `intro_text` varchar(1024) CHARACTER SET utf8 NOT NULL,
  `scanning` tinyint(4) NOT NULL DEFAULT '1',
  `label_printing` tinyint(4) NOT NULL DEFAULT '1',
  `signing` tinyint(4) NOT NULL DEFAULT '1',
  `collection_log` tinyint(4) NOT NULL DEFAULT '1',
  `identification_ID` tinyint(4) NOT NULL DEFAULT '1',
  `new_package_msg` varchar(1024) CHARACTER SET utf8 NOT NULL,
  `package_collection_msg` varchar(1024) CHARACTER SET utf8 NOT NULL,
  `signature_device` varchar(512) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `admin_helpdesk_cat`
--

DROP TABLE IF EXISTS `admin_helpdesk_cat`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admin_helpdesk_cat` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category` varchar(50) NOT NULL,
  `module_id` int(11) NOT NULL,
  `tagName` varchar(50) NOT NULL,
  `form_id` int(11) NOT NULL DEFAULT '0',
  `job_reference` varchar(255) NOT NULL,
  `admin_permission` varchar(50) NOT NULL,
  `notification` varchar(50) CHARACTER SET latin1 DEFAULT NULL,
  `submitted_date` datetime NOT NULL,
  `deleted` tinyint(4) unsigned NOT NULL DEFAULT '0',
  `active` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `module_id` (`module_id`),
  KEY `form_id` (`form_id`),
  KEY `tagName` (`tagName`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `admin_helpdesk_config`
--

DROP TABLE IF EXISTS `admin_helpdesk_config`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admin_helpdesk_config` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `module_id` int(10) unsigned NOT NULL,
  `form_id` varchar(255) DEFAULT NULL,
  `category` varchar(50) NOT NULL,
  `sla` tinyint(4) unsigned NOT NULL,
  `sla_time` tinyint(4) unsigned DEFAULT NULL,
  `sla_duration` varchar(50) NOT NULL,
  `feedback_link` tinyint(4) unsigned NOT NULL,
  `submission_message` longtext NOT NULL,
  `tc` tinyint(4) unsigned NOT NULL,
  `t_c_message` longtext NOT NULL,
  `active` tinyint(4) unsigned NOT NULL,
  `submitted_date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `admin_permissions`
--

DROP TABLE IF EXISTS `admin_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admin_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `module_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `amenities`
--

DROP TABLE IF EXISTS `amenities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `amenities` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `module_id` int(11) NOT NULL,
  `amenity_name` varchar(255) NOT NULL,
  `description` text,
  `prefix` varchar(255) DEFAULT NULL,
  `confirm_msg` text,
  `t_c` text,
  `active` tinyint(4) NOT NULL DEFAULT '1',
  `deleted` tinyint(4) NOT NULL DEFAULT '0',
  `resource_id` varchar(255) DEFAULT NULL COMMENT 'Used to sync with the portal amenity (meeting room) and the respective google calendar resource',
  `resource_address` varchar(255) DEFAULT NULL,
  `floor` varchar(255) DEFAULT NULL,
  `capacity` int(11) unsigned DEFAULT NULL,
  `default_intro` tinyint(4) NOT NULL DEFAULT '0',
  `show_tnc_tickbox` tinyint(4) NOT NULL DEFAULT '1',
  `show_notify_checkbox` tinyint(1) NOT NULL DEFAULT '0',
  `quickbook_form_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `amenity_config`
--

DROP TABLE IF EXISTS `amenity_config`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `amenity_config` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `module_id` int(11) NOT NULL,
  `amenity_id` int(11) NOT NULL,
  `form_id` int(11) DEFAULT NULL,
  `config_name` varchar(255) NOT NULL,
  `type` enum('Fixed','Variable') DEFAULT NULL COMMENT ' Fixed - display slots directly fragmented with duration/ Variable - Do not fragment',
  `buffer` tinyint(4) DEFAULT NULL,
  `duration` int(11) DEFAULT NULL COMMENT 'for fixed type to fragment the slots',
  `max_duration` int(11) DEFAULT NULL COMMENT 'for variable type to increment the duration upto this value',
  `slots_per_day` tinyint(4) DEFAULT NULL COMMENT 'Max booking slots per day in days',
  `book_today` tinyint(4) DEFAULT NULL,
  `max_advance` tinyint(4) NOT NULL COMMENT ' max no of days Given for advance booking',
  `max_advance_type` enum('day','week','month','year') DEFAULT NULL,
  `min_advance` varchar(255) NOT NULL COMMENT 'how many days intimation required for booking',
  `min_advance_type` enum('day','week','month','year') DEFAULT NULL,
  `active_bookings` tinyint(4) DEFAULT NULL COMMENT 'how many bookings a user can book in advance',
  `reminder` int(11) DEFAULT NULL COMMENT 'no of Hours in advance a reminder should be sent',
  `reminder_to` enum('admin','user','both') DEFAULT NULL,
  `confirm_msg` text NOT NULL,
  `signature` tinyint(4) NOT NULL,
  `active` tinyint(4) NOT NULL,
  `deleted` tinyint(4) NOT NULL DEFAULT '0',
  `book_fragments` tinyint(4) DEFAULT NULL,
  `fixed_style` enum('Slot','Dropdown') DEFAULT NULL,
  `spl_req` tinyint(4) NOT NULL DEFAULT '0',
  `new_booking_mail` enum('admin','user','both') DEFAULT NULL,
  `recurrance` tinyint(4) NOT NULL DEFAULT '0',
  `signature_device` varchar(512) DEFAULT NULL,
  `end_recur_notify` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'notify the user about end of recurrance',
  `simultaneous` int(11) DEFAULT NULL,
  `max_booking_length` tinyint(4) NOT NULL,
  `max_booking_length_type` enum('minute','hour') DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `amenity_slots`
--

DROP TABLE IF EXISTS `amenity_slots`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `amenity_slots` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `amenity_config_id` int(11) NOT NULL,
  `start_time` time DEFAULT NULL,
  `end_time` time DEFAULT NULL,
  `weekday` tinyint(4) NOT NULL COMMENT '1-mon, 2-tue, 3-wed,4-thurs, 5-fri, 6- sat,7-sun',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=183 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `announcement_bgimage`
--

DROP TABLE IF EXISTS `announcement_bgimage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `announcement_bgimage` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `deleted` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `announcement_calendar`
--

DROP TABLE IF EXISTS `announcement_calendar`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `announcement_calendar` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `announcement_id` int(11) unsigned NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `start_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `end_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `location` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `announcement_email_only`
--

DROP TABLE IF EXISTS `announcement_email_only`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `announcement_email_only` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `announcement_id` int(11) unsigned DEFAULT NULL,
  `value` varchar(150) DEFAULT NULL,
  `value_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=251 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `announcement_posts`
--

DROP TABLE IF EXISTS `announcement_posts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `announcement_posts` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `template_id` int(11) unsigned NOT NULL,
  `subject` varchar(255) NOT NULL,
  `message` text NOT NULL,
  `display_from` date DEFAULT NULL,
  `display_to` date DEFAULT NULL,
  `bg_image` int(11) unsigned DEFAULT NULL,
  `flag_homepage` tinyint(4) unsigned NOT NULL DEFAULT '1' COMMENT '1: show on homepage',
  `flag_calendar` tinyint(4) unsigned NOT NULL DEFAULT '0' COMMENT '1: show on calendar',
  `flag_repeat` tinyint(4) unsigned NOT NULL DEFAULT '0',
  `flag_email` smallint(6) unsigned NOT NULL DEFAULT '0' COMMENT '0: No, 1: Yes, 2: Later',
  `ann_type` enum('public','NULL') DEFAULT NULL,
  `deleted` tinyint(4) unsigned NOT NULL DEFAULT '0',
  `active` tinyint(4) unsigned NOT NULL DEFAULT '1',
  `posted_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `posted_by` int(11) unsigned DEFAULT NULL,
  `mail_date` date DEFAULT NULL COMMENT 'flag_email = defer then this will be set',
  `mail_time` time DEFAULT NULL,
  `parent_id` int(11) unsigned NOT NULL DEFAULT '0',
  `send_mail` enum('Y','N') NOT NULL DEFAULT 'N' COMMENT 'Y-Mail Sent, N- Not Sent',
  `tags_facility` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=113 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `announcement_recurrence`
--

DROP TABLE IF EXISTS `announcement_recurrence`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `announcement_recurrence` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `announcement_id` int(11) unsigned DEFAULT NULL,
  `start_date` datetime DEFAULT NULL,
  `start_time` time DEFAULT NULL,
  `recurrence_type` enum('none','hourly','daily','weekdays','weekly','monthly','yearly') NOT NULL DEFAULT 'none',
  `recurrence_count` int(11) unsigned NOT NULL DEFAULT '0',
  `recurrence_info` text,
  `end_type` enum('never','after','ondate') NOT NULL DEFAULT 'never',
  `end_info` text NOT NULL,
  `end_date` date DEFAULT NULL,
  `recurrence_end_occurance` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `announcement_templates`
--

DROP TABLE IF EXISTS `announcement_templates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `announcement_templates` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `template_name` varchar(150) DEFAULT NULL,
  `subject` varchar(255) DEFAULT NULL,
  `message` text,
  `active` tinyint(4) unsigned NOT NULL DEFAULT '0',
  `deleted` tinyint(4) unsigned DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `announcement_visibility`
--

DROP TABLE IF EXISTS `announcement_visibility`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `announcement_visibility` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `announcement_id` int(11) unsigned DEFAULT NULL,
  `value` varchar(150) DEFAULT NULL,
  `value_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=130 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `auth_domains`
--

DROP TABLE IF EXISTS `auth_domains`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_domains` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `active` tinyint(4) NOT NULL DEFAULT '1',
  `deleted` tinyint(4) NOT NULL DEFAULT '0',
  `company_id` int(11) NOT NULL,
  `block_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `binary_data`
--

DROP TABLE IF EXISTS `binary_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `binary_data` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `table_name` varchar(100) NOT NULL DEFAULT '',
  `org_id` bigint(20) NOT NULL DEFAULT '0',
  `parent_id` int(10) NOT NULL DEFAULT '0',
  `alt_id` int(11) NOT NULL DEFAULT '0',
  `thumb_id` int(11) NOT NULL DEFAULT '0',
  `order_no` smallint(6) NOT NULL DEFAULT '0',
  `alt_text` longtext NOT NULL,
  `caption` text NOT NULL,
  `filename` varchar(100) NOT NULL DEFAULT '',
  `file_type` varchar(100) NOT NULL DEFAULT '',
  `filesize` mediumint(9) NOT NULL DEFAULT '0',
  `file_contents` longblob NOT NULL,
  `width` bigint(10) NOT NULL DEFAULT '0',
  `height` bigint(10) NOT NULL DEFAULT '0',
  `unlimited_id` mediumint(9) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `link_table_name` varchar(100) NOT NULL DEFAULT '',
  `article_id` bigint(20) NOT NULL DEFAULT '0',
  `link_url` varchar(255) NOT NULL DEFAULT '',
  `link_target` varchar(25) NOT NULL DEFAULT '',
  `general_id` bigint(20) NOT NULL DEFAULT '0',
  `lang_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `table_name` (`table_name`),
  KEY `org_id` (`org_id`),
  KEY `parent_id` (`parent_id`),
  KEY `unlimited_id` (`unlimited_id`)
) ENGINE=MyISAM AUTO_INCREMENT=592 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `blocked_ips`
--

DROP TABLE IF EXISTS `blocked_ips`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `blocked_ips` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ipAddress` int(11) NOT NULL,
  `login_attempts` int(11) NOT NULL DEFAULT '0',
  `login_failed` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=90 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `blocks`
--

DROP TABLE IF EXISTS `blocks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `blocks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `landlord_id` int(11) NOT NULL DEFAULT '0',
  `domain_id` int(11) NOT NULL DEFAULT '0',
  `region_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(250) DEFAULT NULL,
  `development_id` int(11) unsigned NOT NULL,
  `bodytext` longtext,
  `postcode` varchar(10) DEFAULT '',
  `meta_title` varchar(255) DEFAULT '',
  `order_no` int(11) DEFAULT NULL,
  `plot_reference` varchar(255) NOT NULL,
  `jamcam_postcode` varchar(15) DEFAULT NULL,
  `block_id` varchar(20) NOT NULL,
  `block_ref` varchar(20) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '0',
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `latitude` varchar(100) NOT NULL,
  `longitude` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `domain_id` (`domain_id`),
  KEY `region_id` (`region_id`),
  CONSTRAINT `blocks_ibfk_1` FOREIGN KEY (`domain_id`) REFERENCES `domains` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `blocks_ibfk_2` FOREIGN KEY (`region_id`) REFERENCES `regions` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `calendar_email_only`
--

DROP TABLE IF EXISTS `calendar_email_only`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `calendar_email_only` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cal_id` int(11) DEFAULT NULL,
  `value` varchar(150) DEFAULT NULL,
  `value_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=200 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `calendar_events`
--

DROP TABLE IF EXISTS `calendar_events`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `calendar_events` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) unsigned NOT NULL DEFAULT '0',
  `event_name` varchar(1024) NOT NULL,
  `created_by` int(11) NOT NULL,
  `date_from` date DEFAULT NULL,
  `date_to` date DEFAULT NULL,
  `time_from` time DEFAULT NULL,
  `time_to` time DEFAULT NULL,
  `location` varchar(1024) DEFAULT NULL,
  `category` int(11) DEFAULT NULL,
  `description` text NOT NULL,
  `recur` tinyint(1) NOT NULL DEFAULT '0',
  `visibility` tinyint(1) NOT NULL DEFAULT '0',
  `email` tinyint(1) NOT NULL DEFAULT '0',
  `created_on` date NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `active` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=98 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `calendar_filter`
--

DROP TABLE IF EXISTS `calendar_filter`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `calendar_filter` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cal_id` int(11) DEFAULT NULL,
  `value` varchar(150) DEFAULT NULL,
  `value_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `companies`
--

DROP TABLE IF EXISTS `companies`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `companies` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `company_name` varchar(255) DEFAULT NULL,
  `old_id` int(11) DEFAULT NULL,
  `block_id` int(11) NOT NULL,
  `company_ref` varchar(100) DEFAULT NULL,
  `street` varchar(70) DEFAULT NULL,
  `city` varchar(70) DEFAULT NULL,
  `postcode` varchar(10) DEFAULT NULL,
  `borough` varchar(512) DEFAULT NULL,
  `floor` varchar(512) DEFAULT NULL,
  `deleted` tinyint(4) NOT NULL DEFAULT '0',
  `active` tinyint(4) NOT NULL DEFAULT '1',
  `gal_division_id` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=96 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `company`
--

DROP TABLE IF EXISTS `company`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `company` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `company_name` varchar(255) NOT NULL,
  `csv_notation` varchar(20) NOT NULL,
  `active` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=56 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `company_floors`
--

DROP TABLE IF EXISTS `company_floors`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `company_floors` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `floor_id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uniqfloorcomp` (`floor_id`,`company_id`),
  KEY `floor_id` (`floor_id`),
  KEY `company_id` (`company_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1860 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `company_secondary_departments`
--

DROP TABLE IF EXISTS `company_secondary_departments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `company_secondary_departments` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `sd_id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uniqsdcomp` (`sd_id`,`company_id`),
  KEY `sd_id` (`sd_id`),
  KEY `company_id` (`company_id`)
) ENGINE=InnoDB AUTO_INCREMENT=122 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `content`
--

DROP TABLE IF EXISTS `content`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `content` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nav_title_id` int(11) DEFAULT NULL,
  `content_type` enum('page','email','static') DEFAULT 'page',
  `domain_id` int(11) NOT NULL,
  `block_id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `label` varchar(50) NOT NULL,
  `bodytext` longtext,
  `page_type` enum('public','public_only','group_specific','global','property_specific','development_specific') DEFAULT 'public',
  `extra_info1` varchar(255) DEFAULT NULL COMMENT 'email - subject, pages - intro text',
  `extra_info2` varchar(255) DEFAULT NULL COMMENT 'email - from address',
  `active` tinyint(1) DEFAULT '0',
  `order_no` int(11) NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `show_on_homepage` enum('0','1') NOT NULL,
  `short_description` text NOT NULL,
  `homepage_order_no` int(11) NOT NULL,
  `meta_keyword` text,
  `meta_description` text,
  `show_sidebar` tinyint(4) NOT NULL DEFAULT '0',
  `sidebar_content` text,
  `page_type_alt` enum('public','public_only','group_specific','global','property_specific','development_specific','company_specific') DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `block_id` (`block_id`),
  KEY `domain_id` (`domain_id`),
  CONSTRAINT `content_ibfk_2` FOREIGN KEY (`domain_id`) REFERENCES `domains` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=103 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `content_permissions`
--

DROP TABLE IF EXISTS `content_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `content_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `content_id` int(11) NOT NULL,
  `field_name` varchar(50) NOT NULL COMMENT 'e.g. "block", "group","apartment"',
  `field_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `content_id` (`content_id`)
) ENGINE=InnoDB AUTO_INCREMENT=140 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `csv_list`
--

DROP TABLE IF EXISTS `csv_list`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `csv_list` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `tablename` enum('PRE_REGISTER','STARTER') NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=115 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `deliveries`
--

DROP TABLE IF EXISTS `deliveries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `deliveries` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `property_id` int(11) NOT NULL,
  `alternate_user` varchar(255) DEFAULT NULL,
  `package_count` int(11) NOT NULL,
  `notify` enum('email','sms','both','none') NOT NULL,
  `notes` text NOT NULL,
  `del_date` datetime NOT NULL,
  `delivery_by` text,
  `delivery_logged` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `mobile` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `delivery_log`
--

DROP TABLE IF EXISTS `delivery_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `delivery_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `package_id` int(11) NOT NULL,
  `status` enum('awaiting','collected') NOT NULL,
  `date_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `delivery_packages`
--

DROP TABLE IF EXISTS `delivery_packages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `delivery_packages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `delivery_id` int(11) NOT NULL,
  `apartment` int(11) NOT NULL,
  `package_ref` varchar(255) NOT NULL,
  `barcode` text NOT NULL,
  `status` enum('awaiting','collected') NOT NULL,
  `addresse` text,
  `collected_by` varchar(255) DEFAULT NULL,
  `collection_logged` int(11) NOT NULL,
  `id_proof` text,
  `collection_notes` text,
  `collected_on` datetime DEFAULT NULL,
  `date_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `signature` longtext NOT NULL,
  `signature_device` enum('ipad','topaz') DEFAULT NULL,
  `deleted` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `departments`
--

DROP TABLE IF EXISTS `departments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `departments` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `company_id` varchar(255) CHARACTER SET utf8 NOT NULL,
  `company_name` varchar(255) CHARACTER SET utf8 NOT NULL,
  `deleted` tinyint(4) unsigned NOT NULL DEFAULT '0',
  `active` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `developments`
--

DROP TABLE IF EXISTS `developments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `developments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET latin1 NOT NULL,
  `active` int(11) NOT NULL DEFAULT '1',
  `deleted` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `document_categories`
--

DROP TABLE IF EXISTS `document_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `document_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `active` tinyint(4) NOT NULL DEFAULT '1',
  `deleted` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `document_permissions`
--

DROP TABLE IF EXISTS `document_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `document_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `document_id` int(11) NOT NULL,
  `field_name` varchar(255) NOT NULL,
  `field_value` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=49 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `documents`
--

DROP TABLE IF EXISTS `documents`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `documents` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `type` varchar(255) DEFAULT NULL,
  `active` tinyint(4) NOT NULL DEFAULT '1',
  `deleted` tinyint(4) NOT NULL DEFAULT '0',
  `inform_date` date DEFAULT NULL,
  `expiry_date` date DEFAULT NULL,
  `type_alt` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `domains`
--

DROP TABLE IF EXISTS `domains`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `domains` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `domain` varchar(512) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '0',
  `ga_script` text COMMENT 'google analytics script',
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='Domain Specific Information (URLs for the site)';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `duty_handover`
--

DROP TABLE IF EXISTS `duty_handover`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `duty_handover` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `notes` text NOT NULL,
  `note_type` tinyint(4) NOT NULL COMMENT '1-Shift Note, 2-End of shift note, 3-Shift note visble in both tabs, 4-Shift note transferred to End of shift',
  `note_datetime` datetime DEFAULT NULL,
  `status` enum('no_action','action_req','in_progress','resolved') NOT NULL,
  `category` int(11) unsigned NOT NULL,
  `table_id` int(11) unsigned NOT NULL,
  `calendar` tinyint(4) unsigned NOT NULL,
  `from_datetime` datetime DEFAULT NULL,
  `to_datetime` datetime DEFAULT NULL,
  `added_by` int(11) unsigned NOT NULL,
  `added_on` datetime NOT NULL,
  `end_by` int(11) unsigned NOT NULL,
  `end_on` datetime NOT NULL,
  `end_id` int(11) unsigned NOT NULL,
  `deleted` tinyint(4) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=970 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `duty_handover_log_activity`
--

DROP TABLE IF EXISTS `duty_handover_log_activity`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `duty_handover_log_activity` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `duty_id` int(11) NOT NULL,
  `action` text NOT NULL,
  `action_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `duty_handover_visibility`
--

DROP TABLE IF EXISTS `duty_handover_visibility`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `duty_handover_visibility` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `duty_handover_id` int(11) NOT NULL,
  `value` varchar(255) NOT NULL DEFAULT '',
  `value_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `duty_schedule`
--

DROP TABLE IF EXISTS `duty_schedule`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `duty_schedule` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `active` tinyint(4) NOT NULL DEFAULT '0',
  `table_name` varchar(255) CHARACTER SET utf8 NOT NULL,
  `table_id` int(11) unsigned NOT NULL,
  `domain_id` int(11) unsigned NOT NULL DEFAULT '0',
  `language_ids` varchar(255) NOT NULL,
  `user_groups_id` int(11) unsigned NOT NULL DEFAULT '0',
  `submitted_date` datetime NOT NULL,
  `submitted_by` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `duty_schedule_availability`
--

DROP TABLE IF EXISTS `duty_schedule_availability`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `duty_schedule_availability` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `contact_id` int(10) unsigned NOT NULL,
  `date` date NOT NULL,
  `status` enum('OFF','DAY','NIGHT','HOLIDAY','TRAINING','SICK') CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `email`
--

DROP TABLE IF EXISTS `email`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `email` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `label` varchar(255) NOT NULL DEFAULT '',
  `email_variable` varchar(255) NOT NULL DEFAULT '',
  `subject` varchar(255) NOT NULL DEFAULT '',
  `description` text NOT NULL,
  `from_address` varchar(100) NOT NULL DEFAULT '',
  `domain_id` int(11) unsigned NOT NULL DEFAULT '0',
  `deleted` tinyint(4) NOT NULL DEFAULT '0',
  `visibility` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `domain_id` (`domain_id`)
) ENGINE=InnoDB AUTO_INCREMENT=135 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `f_bookings`
--

DROP TABLE IF EXISTS `f_bookings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `f_bookings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `module_id` int(11) NOT NULL,
  `amenity_id` int(11) NOT NULL,
  `amenity_config_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `table_name` varchar(512) NOT NULL,
  `booking_date` date NOT NULL,
  `status` varchar(512) NOT NULL,
  `booked_on` date NOT NULL,
  `booked_by` int(11) NOT NULL,
  `company` varchar(512) NOT NULL,
  `approval` int(11) NOT NULL,
  `description` text NOT NULL,
  `hazardous` tinyint(4) NOT NULL,
  `fragile` tinyint(4) NOT NULL,
  `destination` text NOT NULL,
  `vehicle_type` int(11) NOT NULL,
  `delivery_company` varchar(255) NOT NULL,
  `delivery_contact` varchar(255) NOT NULL,
  `driver_name` varchar(255) NOT NULL,
  `register_id` varchar(255) NOT NULL,
  `resources` varchar(255) NOT NULL,
  `old_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `facility_booking_custom_log`
--

DROP TABLE IF EXISTS `facility_booking_custom_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `facility_booking_custom_log` (
  `facility_id` int(11) NOT NULL,
  `field_label_1` varchar(50) DEFAULT NULL,
  `html_field_id_1` varchar(11) DEFAULT NULL,
  `field_label_2` varchar(50) DEFAULT NULL,
  `html_field_id_2` varchar(11) DEFAULT NULL,
  `field_label_3` varchar(50) DEFAULT NULL,
  `html_field_id_3` varchar(11) DEFAULT NULL,
  `field_label_4` varchar(50) DEFAULT NULL,
  `html_field_id_4` varchar(11) DEFAULT NULL,
  `field_label_5` varchar(50) DEFAULT NULL,
  `html_field_id_5` varchar(11) DEFAULT NULL,
  `field_label_6` varchar(50) DEFAULT NULL,
  `html_field_id_6` varchar(11) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`facility_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `facility_booking_fields`
--

DROP TABLE IF EXISTS `facility_booking_fields`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `facility_booking_fields` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `booking_id` int(11) NOT NULL,
  `html_field_id` int(11) NOT NULL,
  `field_value` text NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `binary_org_id` varchar(255) DEFAULT NULL COMMENT 'this is for the orig id of the file stored in binary_dat table',
  PRIMARY KEY (`id`),
  KEY `booking_id` (`booking_id`),
  KEY `html_field_id` (`html_field_id`)
) ENGINE=InnoDB AUTO_INCREMENT=53235 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `facility_bookings`
--

DROP TABLE IF EXISTS `facility_bookings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `facility_bookings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `module_id` int(11) NOT NULL,
  `amenity_id` int(11) NOT NULL,
  `amenity_config_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `table_id` int(11) NOT NULL,
  `table_name` varchar(255) NOT NULL,
  `booking_date` date NOT NULL,
  `contact_emails` text,
  `special_request` text,
  `booking_ref_id` int(11) NOT NULL DEFAULT '0',
  `status` enum('pending','open','closed') NOT NULL DEFAULT 'pending' COMMENT '''  pending-Sign Out'',''open - Sign In'',''Closed''',
  `booked_on` date DEFAULT NULL,
  `sign_out` longtext,
  `sign_in` longtext,
  `deleted` tinyint(4) NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `recur_booking` tinyint(4) NOT NULL DEFAULT '0',
  `recur_type` enum('daily','weekly','monthly') DEFAULT NULL,
  `recur_end_date` date DEFAULT NULL,
  `booked_by` int(11) DEFAULT NULL,
  `signed_out_device` enum('ipad','topaz') DEFAULT NULL,
  `signed_in_device` enum('ipad','topaz') DEFAULT NULL,
  `company` varchar(255) DEFAULT NULL,
  `approval` tinyint(4) DEFAULT NULL,
  `description` text,
  `hazardous` tinyint(4) DEFAULT NULL,
  `fragile` tinyint(4) DEFAULT NULL,
  `destination` text,
  `vehicle_type` int(11) DEFAULT NULL,
  `delivery_company` varchar(255) DEFAULT NULL,
  `delivery_contact` varchar(255) DEFAULT NULL,
  `driver_name` varchar(255) DEFAULT NULL,
  `register_id` varchar(255) DEFAULT NULL,
  `resources` varchar(255) DEFAULT NULL,
  `old_id` int(11) DEFAULT NULL,
  `arrived_at` datetime DEFAULT NULL,
  `departed_at` datetime DEFAULT NULL,
  `searched_at` datetime DEFAULT NULL,
  `search_notes` text,
  `notify_me` tinyint(1) NOT NULL DEFAULT '0',
  `create_visitor` tinyint(1) NOT NULL DEFAULT '0',
  `linked_vms_booking` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `module_id` (`module_id`),
  KEY `amenity_id` (`amenity_id`),
  KEY `amenity_config_id` (`amenity_config_id`),
  KEY `user_id` (`user_id`),
  KEY `table_id` (`table_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5435 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `facility_config`
--

DROP TABLE IF EXISTS `facility_config`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `facility_config` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `module_id` int(11) NOT NULL,
  `type` enum('Fixed','Variable') DEFAULT 'Fixed' COMMENT 'Fixed - display slots directly fragmented with duration/ Variable - Do not fragment',
  `buffer` tinyint(4) DEFAULT NULL,
  `duration` int(11) DEFAULT NULL COMMENT 'for fixed type to fragment the slots',
  `max_duration` int(11) DEFAULT NULL COMMENT 'for variable type to increment the duration upto this value',
  `max_advance` tinyint(4) NOT NULL COMMENT ' max no of days Given for advance booking',
  `max_advance_type` enum('day','week','month','year') DEFAULT NULL,
  `min_advance` varchar(255) NOT NULL COMMENT 'how many days intimation required for booking',
  `min_advance_type` enum('day','week','month','year') DEFAULT NULL,
  `active_bookings` tinyint(4) DEFAULT NULL COMMENT 'how many bookings a user can book in advance',
  `active` tinyint(4) NOT NULL,
  `deleted` tinyint(4) NOT NULL DEFAULT '0',
  `book_fragments` tinyint(4) DEFAULT NULL,
  `fixed_style` enum('Slot','Dropdown') DEFAULT 'Slot',
  `max_booking_length` tinyint(4) NOT NULL,
  `max_booking_length_type` enum('minute','hour') DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `facility_slots`
--

DROP TABLE IF EXISTS `facility_slots`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `facility_slots` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `facility_config_id` int(11) NOT NULL,
  `start_time` time DEFAULT NULL,
  `end_time` time DEFAULT NULL,
  `weekday` tinyint(4) NOT NULL COMMENT '1-mon, 2-tue, 3-wed,4-thurs, 5-fri, 6- sat,7-sun',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `feeds`
--

DROP TABLE IF EXISTS `feeds`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `feeds` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(150) NOT NULL,
  `basic_feed` enum('road','bus','bike','tube','jamcam') NOT NULL,
  `details` text NOT NULL,
  `width` int(11) NOT NULL,
  `height` int(11) NOT NULL,
  `active` tinyint(4) unsigned NOT NULL,
  `deleted` tinyint(4) unsigned NOT NULL,
  `order_no` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `floors`
--

DROP TABLE IF EXISTS `floors`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `floors` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=65 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `form_log`
--

DROP TABLE IF EXISTS `form_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `form_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `form_log_config_id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `table_id` int(11) DEFAULT NULL,
  `table_name` varchar(512) NOT NULL,
  `status` varchar(512) DEFAULT NULL,
  `decline_msg` text,
  `submitted_date` datetime DEFAULT NULL,
  `modified_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `expiry_date` date DEFAULT NULL,
  `active` tinyint(4) NOT NULL DEFAULT '1',
  `deleted` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `form_log_activity`
--

DROP TABLE IF EXISTS `form_log_activity`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `form_log_activity` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `form_log_id` int(11) NOT NULL,
  `activity` text,
  `internal` tinyint(4) NOT NULL DEFAULT '0',
  `activity_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted` tinyint(4) NOT NULL DEFAULT '0',
  `note` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `form_log_config`
--

DROP TABLE IF EXISTS `form_log_config`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `form_log_config` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `module_id` int(11) NOT NULL,
  `id_prefix` varchar(512) DEFAULT NULL,
  `confirmation` text,
  `form_id` int(11) NOT NULL,
  `approval_reqd` tinyint(4) NOT NULL DEFAULT '0',
  `max_approved_company` tinyint(4) NOT NULL DEFAULT '0',
  `approval_pass_reqd` tinyint(4) NOT NULL DEFAULT '0',
  `max_approved` int(11) DEFAULT NULL,
  `completion_reqd` tinyint(4) NOT NULL DEFAULT '0',
  `expiry_length` int(11) NOT NULL DEFAULT '0',
  `expiry_notify` tinyint(4) NOT NULL DEFAULT '0',
  `first_refusal` tinyint(4) NOT NULL DEFAULT '0',
  `custom_headers` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `form_log_restrictions`
--

DROP TABLE IF EXISTS `form_log_restrictions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `form_log_restrictions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `form_log_config_id` int(11) NOT NULL,
  `table_id` int(11) NOT NULL,
  `table_name` varchar(255) NOT NULL,
  `submissions` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `form_log_values`
--

DROP TABLE IF EXISTS `form_log_values`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `form_log_values` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `html_field_id` int(11) NOT NULL,
  `field_value` longtext,
  `form_log_id` int(11) DEFAULT NULL,
  `form_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  FULLTEXT KEY `form_value_idx` (`field_value`)
) ENGINE=InnoDB AUTO_INCREMENT=180 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `forum`
--

DROP TABLE IF EXISTS `forum`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `forum` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `module_id` int(11) DEFAULT NULL,
  `id_prefix` varchar(512) DEFAULT NULL,
  `intro` varchar(512) DEFAULT NULL,
  KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `forum_comments`
--

DROP TABLE IF EXISTS `forum_comments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `forum_comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `topic_id` int(11) NOT NULL,
  `creation_date` datetime DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `approval_required` tinyint(1) DEFAULT '0' COMMENT ' 0 - dont need approval / 1 - need approval',
  `approval` tinyint(1) DEFAULT NULL COMMENT ' NULL - pending approval / 0 - refused / 1 - approved',
  `approval_date` datetime DEFAULT NULL,
  `approver_id` int(11) DEFAULT NULL COMMENT ' id of the user who approved the topic',
  `comment` text,
  `deleted` tinyint(1) DEFAULT '0',
  KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=145 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `forum_topic`
--

DROP TABLE IF EXISTS `forum_topic`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `forum_topic` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `topic_name` varchar(512) DEFAULT NULL,
  `body` text,
  `creation_date` datetime DEFAULT NULL,
  `lastUpdated` datetime DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `forum_id` int(11) NOT NULL,
  `approval_required` tinyint(1) DEFAULT '0' COMMENT ' 0 - topic dont need approval / 1 - topic need approval',
  `approval` tinyint(1) DEFAULT NULL COMMENT ' NULL - pending approval / 0 - refused / 1 - approved',
  `approval_date` datetime DEFAULT NULL,
  `approver_id` int(11) DEFAULT NULL COMMENT ' id of the user who approved the topic',
  `deleted` tinyint(1) DEFAULT '0',
  KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=102 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `g_analytics`
--

DROP TABLE IF EXISTS `g_analytics`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `g_analytics` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `property_id` varchar(20) NOT NULL,
  `script` mediumtext NOT NULL,
  `active` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `gallagher_soap_faults`
--

DROP TABLE IF EXISTS `gallagher_soap_faults`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gallagher_soap_faults` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fault_code` varchar(250) CHARACTER SET utf8 NOT NULL,
  `fault_string` text CHARACTER SET utf8 NOT NULL,
  `fault_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=483 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `gallagher_sync_data`
--

DROP TABLE IF EXISTS `gallagher_sync_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gallagher_sync_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date_added` datetime NOT NULL,
  `xml_data` longtext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `helpdesk`
--

DROP TABLE IF EXISTS `helpdesk`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `helpdesk` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `table_name` varchar(255) NOT NULL,
  `table_id` int(11) NOT NULL,
  `cat_id` int(11) NOT NULL,
  `fault_id` int(11) NOT NULL,
  `fault_description` varchar(255) NOT NULL,
  `submitted_date` datetime NOT NULL,
  `due_date` datetime NOT NULL,
  `submitted_by` int(10) unsigned DEFAULT NULL,
  `reason` varchar(255) NOT NULL,
  `satisfied` tinyint(4) NOT NULL DEFAULT '0',
  `feedback` varchar(255) NOT NULL,
  `status` enum('New','In Progress','Completed') NOT NULL DEFAULT 'New',
  `priority` int(11) NOT NULL,
  `assigned_to` int(11) NOT NULL DEFAULT '0',
  `deleted` tinyint(4) NOT NULL DEFAULT '0',
  `closed_by` int(11) DEFAULT NULL,
  `Closing_date` datetime DEFAULT NULL,
  `closed_by_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `assigned_to` (`assigned_to`),
  KEY `fault_id` (`fault_id`),
  KEY `cat_id` (`cat_id`),
  KEY `priority` (`priority`),
  KEY `table_name` (`table_name`),
  KEY `table_id` (`table_id`)
) ENGINE=InnoDB AUTO_INCREMENT=235 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `helpdesk_activity_log`
--

DROP TABLE IF EXISTS `helpdesk_activity_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `helpdesk_activity_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `helpdesk_id` int(11) NOT NULL,
  `action` text NOT NULL,
  `action_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=638 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `helpdesk_enquiry_fields`
--

DROP TABLE IF EXISTS `helpdesk_enquiry_fields`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `helpdesk_enquiry_fields` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `enquiry_id` int(11) NOT NULL,
  `html_field_id` int(11) NOT NULL,
  `field_value` text NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=109 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `helpdesk_multidata`
--

DROP TABLE IF EXISTS `helpdesk_multidata`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `helpdesk_multidata` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `table_name` varchar(50) NOT NULL,
  `item_id` int(11) unsigned NOT NULL,
  `field_name` varchar(50) NOT NULL,
  `field_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `field_id` (`field_id`),
  KEY `item_id` (`item_id`),
  KEY `field_name_field_id` (`field_name`,`field_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10766 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `helpdesk_priorities`
--

DROP TABLE IF EXISTS `helpdesk_priorities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `helpdesk_priorities` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `color` varchar(255) NOT NULL,
  `pr_order` int(11) NOT NULL,
  `sla_hours` time NOT NULL,
  `deleted` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `html_fields`
--

DROP TABLE IF EXISTS `html_fields`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `html_fields` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `field_type` varchar(25) NOT NULL,
  `field_label` varchar(250) NOT NULL,
  `field_name` varchar(50) DEFAULT NULL COMMENT 'specify the name and id of the field',
  `html_list_id` int(10) unsigned DEFAULT NULL,
  `validations` text,
  `order_by` tinyint(4) DEFAULT NULL,
  `helpful_text` text NOT NULL,
  `form_id` int(11) NOT NULL,
  `dependent_form_id` int(11) DEFAULT NULL,
  `extra_code` text COMMENT 'specify all the extra code like onchange event etc',
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `radio_answer_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=150 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `html_fieldvalues`
--

DROP TABLE IF EXISTS `html_fieldvalues`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `html_fieldvalues` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `html_field_id` int(11) unsigned NOT NULL,
  `field_value` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `html_forms`
--

DROP TABLE IF EXISTS `html_forms`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `html_forms` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(512) DEFAULT NULL,
  `description` text,
  `js_code` text COMMENT 'specify all js code required for the form',
  `is_questionnaire` tinyint(1) DEFAULT '0',
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `deleted` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `html_list_items`
--

DROP TABLE IF EXISTS `html_list_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `html_list_items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `list_id` int(11) NOT NULL,
  `item_name` varchar(500) NOT NULL,
  `order_no` int(11) NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `list_id` (`list_id`),
  CONSTRAINT `html_list_items_ibfk_1` FOREIGN KEY (`list_id`) REFERENCES `html_lists` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=262 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `html_list_values`
--

DROP TABLE IF EXISTS `html_list_values`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `html_list_values` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tableName` varchar(255) NOT NULL,
  `tableId` int(11) NOT NULL,
  `list_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `html_lists`
--

DROP TABLE IF EXISTS `html_lists`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `html_lists` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(500) NOT NULL,
  `label` varchar(512) DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `induction_booking_fields`
--

DROP TABLE IF EXISTS `induction_booking_fields`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `induction_booking_fields` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `booking_id` int(11) NOT NULL,
  `html_field_id` int(11) NOT NULL,
  `field_value` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `induction_bookings`
--

DROP TABLE IF EXISTS `induction_bookings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `induction_bookings` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `ref_id` varchar(512) NOT NULL,
  `booking_date` date NOT NULL,
  `booked_from` time NOT NULL,
  `booked_to` time NOT NULL,
  `booked_by` int(11) NOT NULL,
  `booked_on` datetime NOT NULL,
  `notify` tinyint(4) NOT NULL DEFAULT '0',
  `form_id` int(11) NOT NULL,
  `company` varchar(512) DEFAULT NULL,
  `allocated_slot` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=140 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `induction_bookings_temp`
--

DROP TABLE IF EXISTS `induction_bookings_temp`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `induction_bookings_temp` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `booking_date` date NOT NULL,
  `booked_from` time NOT NULL,
  `booked_to` time NOT NULL,
  `booked_by` int(11) NOT NULL,
  `booked_on` datetime NOT NULL,
  `sess_id` varchar(512) NOT NULL,
  `book_start_time` time NOT NULL,
  `book_start_date` date DEFAULT NULL,
  `notify` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `induction_config`
--

DROP TABLE IF EXISTS `induction_config`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `induction_config` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `prefix` varchar(255) DEFAULT NULL,
  `tnc` text,
  `intro` text,
  `confirm_msg` text,
  `duration` int(11) DEFAULT NULL,
  `spaces` int(11) NOT NULL DEFAULT '0',
  `min_advance_type` enum('day','week','month','year') NOT NULL,
  `min_advance` int(11) DEFAULT NULL,
  `max_advance_type` enum('day','week','month','year') NOT NULL,
  `max_advance` int(11) DEFAULT NULL,
  `form_id` int(11) DEFAULT NULL,
  `reminder` int(11) DEFAULT NULL,
  `fragments` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'Fragments bookable or not',
  `slot_summary` longtext COMMENT 'json encoded slots',
  `admin_groups` varchar(512) DEFAULT NULL,
  `induction_activation_msg` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `induction_config_restrictions`
--

DROP TABLE IF EXISTS `induction_config_restrictions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `induction_config_restrictions` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `tablename` varchar(255) NOT NULL,
  `fieldname` varchar(255) NOT NULL,
  `fieldvalue` varchar(255) NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `cms_table` enum('induction_department_setup','induction_type','induction_online_setup') NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5118 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `induction_config_slots`
--

DROP TABLE IF EXISTS `induction_config_slots`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `induction_config_slots` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `induction_config_id` int(11) NOT NULL DEFAULT '1',
  `start_time` time DEFAULT NULL,
  `end_time` time DEFAULT NULL,
  `weekday` tinyint(4) DEFAULT NULL COMMENT ' 1-mon, 2-tue, 3-wed,4-thurs, 5-fri, 6- sat,7-sun',
  `fragment` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=303 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `induction_department_setup`
--

DROP TABLE IF EXISTS `induction_department_setup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `induction_department_setup` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `config_name` varchar(1000) DEFAULT NULL,
  `active` decimal(10,0) DEFAULT NULL,
  `company` decimal(10,0) DEFAULT NULL,
  `user_groups` decimal(10,0) DEFAULT NULL,
  `filters` varchar(1000) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `induction_online_setup`
--

DROP TABLE IF EXISTS `induction_online_setup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `induction_online_setup` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `config_name` varchar(255) DEFAULT NULL,
  `active` tinyint(1) DEFAULT '1',
  `deleted` tinyint(1) DEFAULT '0',
  `video_url` varchar(500) DEFAULT NULL,
  `video_width` int(4) DEFAULT '600',
  `duration` int(3) DEFAULT NULL,
  `attempts_number` int(2) DEFAULT NULL,
  `questionnaire` int(11) DEFAULT NULL,
  `title` text,
  `intro` text,
  `video_intro` text,
  `questionnaire_intro` text,
  `successful_intro` text,
  `unsuccessful_intro` text,
  `email_successful_user` varchar(100) DEFAULT NULL,
  `email_successful_internal` varchar(100) DEFAULT NULL,
  `email_unsuccessful_user` varchar(100) DEFAULT NULL,
  `email_unsuccessful_internal` varchar(100) DEFAULT NULL,
  `email_activation_user` varchar(100) DEFAULT NULL,
  `email_activation_internal` varchar(100) DEFAULT NULL,
  `email_expiry_internal` varchar(100) DEFAULT NULL,
  `email_reset_user` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `induction_online_status`
--

DROP TABLE IF EXISTS `induction_online_status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `induction_online_status` (
  `id` tinyint(1) NOT NULL,
  `status_name` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `induction_type`
--

DROP TABLE IF EXISTS `induction_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `induction_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `active` tinyint(1) DEFAULT '1',
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `induction_type_setup`
--

DROP TABLE IF EXISTS `induction_type_setup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `induction_type_setup` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `config_name` varchar(1000) DEFAULT NULL,
  `active` decimal(10,0) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `inductions`
--

DROP TABLE IF EXISTS `inductions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `inductions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `cardax_id` varchar(512) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5301 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `inductions_config_departments_restrictions`
--

DROP TABLE IF EXISTS `inductions_config_departments_restrictions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `inductions_config_departments_restrictions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tablename` varchar(255) NOT NULL,
  `fieldname` varchar(255) NOT NULL,
  `fieldvalue` varchar(255) NOT NULL,
  `parent_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=353 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `key_holding`
--

DROP TABLE IF EXISTS `key_holding`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `key_holding` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `authorise` tinyint(4) NOT NULL,
  `email_alert` char(4) NOT NULL,
  `key_out` tinyint(4) NOT NULL DEFAULT '1',
  `status` int(11) NOT NULL COMMENT '0)New1)In2)Out3)inactive',
  `property_id` int(11) NOT NULL,
  `key_out_days` int(11) DEFAULT '0',
  `name` varchar(250) DEFAULT NULL,
  `company` varchar(250) DEFAULT NULL,
  `key_type` int(11) DEFAULT NULL,
  `image` varchar(500) DEFAULT NULL,
  `access_allowed` int(11) DEFAULT NULL COMMENT '1)whenever reauired2)single collection3)recurring collection',
  `additional_info` varchar(500) DEFAULT NULL,
  `inactive` int(11) DEFAULT NULL,
  `owner` int(11) NOT NULL DEFAULT '0',
  `signature` longtext,
  `id_shown` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id` (`id`,`user_id`),
  KEY `property_id` (`property_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `key_holding_auth_users`
--

DROP TABLE IF EXISTS `key_holding_auth_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `key_holding_auth_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `key_holding_id` int(11) NOT NULL,
  `name` varchar(250) NOT NULL,
  `company` varchar(250) DEFAULT NULL,
  `key_type` int(11) DEFAULT NULL,
  `image` varchar(1024) DEFAULT NULL,
  `access_allowed` int(11) NOT NULL COMMENT '1)whenerver Req 2)single Collection 3)recurring collection',
  `additional_info` varchar(500) DEFAULT NULL,
  `inactive` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `key_holding_id` (`key_holding_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `key_holding_config`
--

DROP TABLE IF EXISTS `key_holding_config`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `key_holding_config` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `tc` tinyint(4) unsigned NOT NULL,
  `t_c_message` text NOT NULL,
  `notify_grp` enum('Admin','Standard','Both') NOT NULL,
  `when_notify` varchar(10) NOT NULL,
  `key_type` int(11) unsigned NOT NULL,
  `info_check` tinyint(4) unsigned NOT NULL,
  `img_check` tinyint(4) unsigned NOT NULL,
  `id_check` tinyint(4) unsigned NOT NULL,
  `keyout_check` tinyint(4) unsigned NOT NULL,
  `sign_check` tinyint(4) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `key_holding_logs`
--

DROP TABLE IF EXISTS `key_holding_logs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `key_holding_logs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `keyId` int(10) unsigned NOT NULL,
  `activityBy` varchar(50) DEFAULT NULL,
  `activityDt` datetime DEFAULT NULL,
  `modifiedBy` int(10) unsigned NOT NULL,
  `log_status` int(11) DEFAULT NULL COMMENT '0)New1)In2)Out3)inactive',
  `logType` enum('Key Holding','Valet Parking') DEFAULT NULL,
  `owner` int(11) DEFAULT NULL,
  `property_id` int(11) unsigned DEFAULT NULL,
  `id_shown` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `key_signature` longtext CHARACTER SET utf8,
  `signature_device` enum('ipad','topaz') CHARACTER SET utf8 DEFAULT NULL COMMENT 'Device used for signing',
  `signature` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `languages`
--

DROP TABLE IF EXISTS `languages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `languages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL DEFAULT '',
  `active` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=22 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `live_users`
--

DROP TABLE IF EXISTS `live_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `live_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `domain_id` int(11) NOT NULL DEFAULT '0',
  `user_group_id` int(11) NOT NULL DEFAULT '0',
  `title` enum('Mr','Miss','Mrs','Ms','Dr','HRH','HE') DEFAULT 'Mr',
  `firstname` varchar(255) NOT NULL,
  `lastname` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `mobile` varchar(50) NOT NULL,
  `telephone` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `contact_preference` varchar(500) NOT NULL DEFAULT 'none' COMMENT '''email'',''phone'',''sms'',''any'',''none''',
  `additional` text,
  `user_photo` varchar(1024) NOT NULL DEFAULT 'no',
  `contact_email` varchar(500) DEFAULT NULL,
  `activation_mail_sent` enum('email','contact_email','none') NOT NULL DEFAULT 'none',
  `main_user_id` int(11) DEFAULT NULL COMMENT 'user id to which the user is linked',
  `is_main` tinyint(4) NOT NULL DEFAULT '0',
  `validated` tinyint(4) NOT NULL DEFAULT '0',
  `csv_name` varchar(50) DEFAULT NULL,
  `encrypted_id` varchar(32) DEFAULT NULL,
  `approved_by` int(11) NOT NULL,
  `approved_on` datetime NOT NULL,
  `username` varchar(255) DEFAULT NULL,
  `login_failed` datetime NOT NULL,
  `active` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0 - Inactive, 1 - Active, 2 - Pre-registered Inactive',
  `deleted` tinyint(4) NOT NULL DEFAULT '0',
  `car_parking` int(11) DEFAULT NULL,
  `car_registration` varchar(100) DEFAULT NULL,
  `storage_locker` int(11) DEFAULT NULL,
  `groceries` tinyint(4) NOT NULL,
  `laundry` tinyint(4) NOT NULL,
  `luggage` tinyint(4) NOT NULL,
  `package` tinyint(4) NOT NULL,
  `emergency` tinyint(4) NOT NULL,
  `portal_access` tinyint(4) NOT NULL DEFAULT '0',
  `position` varchar(512) DEFAULT NULL,
  `announcements` tinyint(4) unsigned NOT NULL DEFAULT '1',
  `status` varchar(20) NOT NULL,
  `newletters` tinyint(4) NOT NULL DEFAULT '0',
  `reg_on` datetime DEFAULT NULL,
  `last_login` datetime DEFAULT NULL,
  `login_ip` varchar(20) DEFAULT NULL,
  `login_browser` text,
  `tenancy_start` date NOT NULL,
  `tenancy_end` date NOT NULL,
  `subletting_user` enum('Y','N') NOT NULL DEFAULT 'N',
  `added_by` int(11) unsigned NOT NULL COMMENT 'This is used for Subletting',
  `act_mail` tinyint(4) DEFAULT '0' COMMENT '1 - Activation email sent, 0 - No activation email sent',
  `act_link` text COMMENT 'Onboarding Link',
  `host_id` varchar(60) DEFAULT NULL COMMENT 'Gallagher Host Id',
  `division_id` varchar(60) DEFAULT NULL COMMENT 'Gallagher Division Id',
  PRIMARY KEY (`id`),
  KEY `domain_id` (`domain_id`),
  KEY `user_group_id` (`user_group_id`)
) ENGINE=InnoDB AUTO_INCREMENT=35679 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `login_logs`
--

DROP TABLE IF EXISTS `login_logs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `login_logs` (
  `uType` enum('Admin','Web') NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `username` varchar(255) NOT NULL,
  `activity` enum('Successful Login','Failed login','Logout','Failed password','Auto login') NOT NULL,
  `datetimeStamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `ipAddress` int(11) NOT NULL COMMENT 'stored in long format ( ip2long )',
  `sessid` varchar(255) DEFAULT NULL,
  KEY `uType` (`uType`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `module_permissions`
--

DROP TABLE IF EXISTS `module_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `module_permissions` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_group_id` int(11) NOT NULL,
  `module_id` int(11) NOT NULL,
  `permission_token_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_group_id` (`user_group_id`),
  KEY `module_id` (`module_id`),
  KEY `permission_token_id` (`permission_token_id`),
  CONSTRAINT `module_permissions_ibfk_2` FOREIGN KEY (`module_id`) REFERENCES `modules` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `module_permissions_ibfk_4` FOREIGN KEY (`user_group_id`) REFERENCES `user_groups` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3253 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `modules`
--

DROP TABLE IF EXISTS `modules`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `modules` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `module_name` varchar(500) NOT NULL,
  `description` text COMMENT 'This is for Facility Bookings',
  `url` mediumtext,
  `permission_token_ids` text,
  `active` tinyint(4) NOT NULL DEFAULT '0' COMMENT '“|” separated ids from the permisions token table. This is provided here to specify which permission tokens are applicable to which specific module',
  `generated` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'module or facility generated via facility bookings module',
  `deleted` tinyint(4) NOT NULL DEFAULT '0',
  `tagName` varchar(50) NOT NULL,
  `grp_config` text,
  `order_no` int(11) NOT NULL,
  `top` enum('Yes','No') NOT NULL DEFAULT 'No',
  `navigation_id` int(11) DEFAULT NULL,
  `notify` enum('Y','N') DEFAULT 'N',
  `show_on_calendar` tinyint(4) NOT NULL DEFAULT '0',
  `show_search_wizard` enum('N','Y') NOT NULL DEFAULT 'N' COMMENT 'this is used for facility booking - meeting room wizard',
  `automatic_sel` enum('N','Y') NOT NULL COMMENT 'this is used for facility booking - if automatic selection of amenity has to be done',
  `show_assignee` tinyint(4) NOT NULL DEFAULT '0',
  `show_pass_number_drpdwn` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'this is used for VMS v2 - if selected pass no will be drop down field else text field',
  `calendar_default_view` enum('agendaDay','agendaWeek','month','agenda') DEFAULT NULL,
  `calendar_view_type` enum('standard','basic') DEFAULT NULL,
  `block_ids` text,
  `vms_booking_email_destination` enum('booker','host','both') DEFAULT NULL,
  `show_arrival_departure` enum('Y','N') DEFAULT 'N',
  `show_searched` enum('Y','N') DEFAULT 'N',
  `show_quickbook` enum('Y','N') DEFAULT 'N',
  `capture_visitor` enum('Y','N') DEFAULT 'N',
  `vms_entrance` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=82 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `nav_titles`
--

DROP TABLE IF EXISTS `nav_titles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `nav_titles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `domain_id` int(11) NOT NULL,
  `nav_title` varchar(255) NOT NULL,
  `nav_link` varchar(255) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `order_no` int(11) NOT NULL,
  `active` tinyint(4) unsigned NOT NULL DEFAULT '1',
  `deleted` tinyint(4) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `domain_id` (`domain_id`),
  CONSTRAINT `nav_titles_ibfk_1` FOREIGN KEY (`domain_id`) REFERENCES `domains` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `notes`
--

DROP TABLE IF EXISTS `notes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `notes` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `table_id` int(10) unsigned NOT NULL,
  `table_name` varchar(30) NOT NULL,
  `note_content` longtext,
  `user_id` int(11) unsigned NOT NULL,
  `added_by` int(11) unsigned NOT NULL,
  `status` tinyint(4) unsigned NOT NULL,
  `date_modified` datetime NOT NULL,
  `deleted` tinyint(4) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=140 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `notification_data`
--

DROP TABLE IF EXISTS `notification_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `notification_data` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `module` varchar(100) NOT NULL,
  `item_id` int(11) unsigned NOT NULL,
  `end_date` date NOT NULL,
  `user_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `onboarding_config`
--

DROP TABLE IF EXISTS `onboarding_config`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `onboarding_config` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `modal1_intro` longtext,
  `modal2_intro` longtext,
  `modal3_intro` longtext,
  `modal4_intro` longtext,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `permission_tokens`
--

DROP TABLE IF EXISTS `permission_tokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permission_tokens` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `token` varchar(100) NOT NULL,
  `comments` text,
  `type` enum('admin','standard') DEFAULT 'admin',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=70 DEFAULT CHARSET=utf8 COMMENT='egs: add_new, view_all, search, edit_own, edit_all, notes, d';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `promotion`
--

DROP TABLE IF EXISTS `promotion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `promotion` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `headline` varchar(255) NOT NULL,
  `bodytext` text NOT NULL,
  `active_from` date NOT NULL,
  `active_to` date NOT NULL,
  `category_id` int(11) unsigned NOT NULL,
  `feature_on_homepage` tinyint(4) NOT NULL,
  `active` tinyint(4) NOT NULL,
  `deleted` tinyint(4) unsigned DEFAULT '0',
  `postcode` varchar(16) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `category_id` (`category_id`)
) ENGINE=InnoDB AUTO_INCREMENT=59 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `promotion_category`
--

DROP TABLE IF EXISTS `promotion_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `promotion_category` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` mediumtext NOT NULL,
  `active` tinyint(3) unsigned NOT NULL,
  `deleted` tinyint(4) unsigned DEFAULT '0',
  `parent` smallint(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `recurrence`
--

DROP TABLE IF EXISTS `recurrence`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `recurrence` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `table_name` varchar(30) NOT NULL,
  `table_id` int(11) NOT NULL,
  `start_date` date NOT NULL,
  `start_time` time NOT NULL,
  `end_date` date NOT NULL,
  `end_time` time NOT NULL,
  `type` enum('none','hourly','daily','weekdays','weekly','monthly','yearly') NOT NULL,
  `occurence` int(11) NOT NULL,
  `info` text NOT NULL,
  `end_type` enum('never','after','ondate') NOT NULL,
  `repeat_every` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `regions`
--

DROP TABLE IF EXISTS `regions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `regions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `intro` longtext,
  `bodytext` longtext,
  `roads_url` text,
  `trains_url` text,
  `airport_url` text,
  `buses_url` text,
  `ferries_url` text,
  `jamcam_url` text,
  `weather_url` text,
  `bike_url` text NOT NULL,
  `tube_url` text NOT NULL,
  `walking_url` text NOT NULL,
  `active` tinyint(4) unsigned DEFAULT '0',
  `deleted` tinyint(4) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `restrictions`
--

DROP TABLE IF EXISTS `restrictions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `restrictions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tablename` varchar(255) NOT NULL,
  `fieldname` varchar(255) NOT NULL,
  `fieldvalue` varchar(255) NOT NULL,
  `parent_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2514 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `riskwise_users`
--

DROP TABLE IF EXISTS `riskwise_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `riskwise_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `riskid` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `user_gateway_key` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=210 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `secondary_departments`
--

DROP TABLE IF EXISTS `secondary_departments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `secondary_departments` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=105 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `service`
--

DROP TABLE IF EXISTS `service`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `service` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `headline` varchar(255) NOT NULL,
  `bodytext` text NOT NULL,
  `active_from` date NOT NULL,
  `active_to` date NOT NULL,
  `feature_on_homepage` tinyint(4) NOT NULL,
  `active` tinyint(4) NOT NULL,
  `deleted` tinyint(4) unsigned DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `service_categories`
--

DROP TABLE IF EXISTS `service_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `service_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `service_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unq` (`service_id`,`category_id`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `service_category`
--

DROP TABLE IF EXISTS `service_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `service_category` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` mediumtext NOT NULL,
  `active` tinyint(3) unsigned NOT NULL,
  `deleted` tinyint(4) unsigned DEFAULT '0',
  `parent` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `shortcut_icons_admin`
--

DROP TABLE IF EXISTS `shortcut_icons_admin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shortcut_icons_admin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `icon` varchar(255) NOT NULL,
  `link` text NOT NULL,
  `permission_type` enum('global','group_specific','development_specific') NOT NULL,
  `active` tinyint(1) NOT NULL,
  `deleted` tinyint(4) NOT NULL,
  `order_no` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `shortcut_permissions`
--

DROP TABLE IF EXISTS `shortcut_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shortcut_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `shortcut_id` int(11) NOT NULL,
  `field_name` varchar(50) NOT NULL,
  `field_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `site_config`
--

DROP TABLE IF EXISTS `site_config`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `site_config` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `field_name` varchar(512) DEFAULT NULL,
  `field_value` varchar(255) DEFAULT NULL,
  `section` enum('home','ui','login','email','general','app') NOT NULL,
  `options` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `slot_booked`
--

DROP TABLE IF EXISTS `slot_booked`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `slot_booked` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `facility_booking_id` int(11) NOT NULL,
  `booked_from` time NOT NULL,
  `booked_to` time NOT NULL,
  `buffer` int(11) unsigned NOT NULL,
  `mail_sent` tinyint(4) unsigned NOT NULL,
  `deleted` tinyint(4) NOT NULL DEFAULT '0',
  `event_id` int(11) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `old_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `facility_booking_id` (`facility_booking_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5215 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_url_expiry`
--

DROP TABLE IF EXISTS `tbl_url_expiry`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_url_expiry` (
  `token_key` varchar(32) NOT NULL,
  `code_type` varchar(30) NOT NULL COMMENT 'PRE_REGISTER,SUBLET,STARTER ',
  `table_name` varchar(30) NOT NULL,
  `table_id` int(10) unsigned NOT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `expired` tinyint(4) NOT NULL,
  UNIQUE KEY `tokenKey` (`token_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user_auth_device`
--

DROP TABLE IF EXISTS `user_auth_device`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_auth_device` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `browser` varchar(255) NOT NULL,
  `ip_address` varchar(20) NOT NULL,
  `cookie` varchar(80) DEFAULT NULL,
  `pin` int(4) NOT NULL,
  `pin_expiry` datetime DEFAULT NULL,
  `verified` tinyint(1) NOT NULL DEFAULT '0',
  `attempts` tinyint(2) NOT NULL DEFAULT '0',
  `expiry` datetime DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=79 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user_company_secondary_departments`
--

DROP TABLE IF EXISTS `user_company_secondary_departments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_company_secondary_departments` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `sd_id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uniq` (`user_id`,`sd_id`,`company_id`)
) ENGINE=InnoDB AUTO_INCREMENT=263 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user_device`
--

DROP TABLE IF EXISTS `user_device`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_device` (
  `user_id` int(11) NOT NULL,
  `device_token` varchar(255) NOT NULL,
  UNIQUE KEY `device_token` (`device_token`),
  KEY `user_device_user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user_extra`
--

DROP TABLE IF EXISTS `user_extra`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_extra` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `access_expiry` date DEFAULT NULL,
  `access_reason` text,
  `vms_permit` enum('No','Yes') DEFAULT NULL,
  `access_type` enum('Temporary','Permanent') DEFAULT NULL,
  `activation_email` varchar(50) NOT NULL,
  `cardax_id` int(11) DEFAULT NULL,
  `induction` enum('New','Uninducted','Inducted') DEFAULT NULL COMMENT 'new - When a new starter is added, Uninducted - when a booking is done for a starter, Inducted - when a respose appears from galleghar as inducted',
  `allocated_date` date DEFAULT NULL,
  `mail` tinyint(4) NOT NULL DEFAULT '0',
  `department_id` int(11) DEFAULT NULL,
  `added_for` varchar(255) DEFAULT NULL,
  `induction_type` tinyint(1) DEFAULT NULL COMMENT '1 = Personal Induction / 2 = Online Induction',
  `online_induction_attempts` tinyint(1) DEFAULT '0',
  `online_induction_status` tinyint(1) DEFAULT '9' COMMENT '0 = Not Attempted / 1 = In Progress / 2 = Unsuccessful / 3 = Successful',
  `online_induction_date_expiry` date DEFAULT NULL,
  `online_induction_date_complete` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `department_id` (`department_id`)
) ENGINE=InnoDB AUTO_INCREMENT=446 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user_floors`
--

DROP TABLE IF EXISTS `user_floors`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_floors` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `floor_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id` (`user_id`,`floor_id`)
) ENGINE=InnoDB AUTO_INCREMENT=210 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user_groups`
--

DROP TABLE IF EXISTS `user_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `domain_id` int(11) NOT NULL,
  `user_group` varchar(255) NOT NULL,
  `approval` tinyint(1) NOT NULL DEFAULT '0',
  `restrict_domain_to` varchar(255) DEFAULT NULL,
  `show_portal` tinyint(1) NOT NULL,
  `order_no` int(11) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `reg_notification` tinyint(4) unsigned NOT NULL DEFAULT '0',
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `tag_name` varchar(100) NOT NULL,
  `admin_group` tinyint(4) NOT NULL DEFAULT '0',
  `show_on_starter` tinyint(4) NOT NULL DEFAULT '0',
  `2fa_enabled` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `domain_id` (`domain_id`),
  CONSTRAINT `user_groups_ibfk_1` FOREIGN KEY (`domain_id`) REFERENCES `domains` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=88 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user_meta`
--

DROP TABLE IF EXISTS `user_meta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_meta` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `meta_key` varchar(32) NOT NULL,
  `meta_value` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user_properties`
--

DROP TABLE IF EXISTS `user_properties`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_properties` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `block_id` int(11) NOT NULL,
  `tableName` varchar(255) NOT NULL,
  `tableId` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `block_id` (`block_id`),
  CONSTRAINT `user_properties_ibfk_5` FOREIGN KEY (`block_id`) REFERENCES `blocks` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `user_properties_ibfk_6` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3164 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user_token`
--

DROP TABLE IF EXISTS `user_token`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_token` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `token` varchar(80) DEFAULT NULL,
  `context` varchar(40) NOT NULL,
  `expiry` datetime DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `domain_id` int(11) NOT NULL DEFAULT '0',
  `user_group_id` int(11) NOT NULL DEFAULT '0',
  `title` varchar(100) DEFAULT NULL,
  `firstname` varchar(255) NOT NULL,
  `lastname` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `mobile` varchar(50) NOT NULL,
  `telephone` varchar(15) NOT NULL,
  `password` varchar(50) NOT NULL,
  `contact_preference` varchar(500) NOT NULL DEFAULT 'none' COMMENT '''email'',''phone'',''sms'',''any'',''none''',
  `additional` text,
  `user_photo` varchar(1024) NOT NULL DEFAULT 'no',
  `contact_email` varchar(500) DEFAULT NULL,
  `activation_mail_sent` enum('email','contact_email','none') NOT NULL DEFAULT 'none',
  `main_user_id` int(11) DEFAULT NULL COMMENT 'user id to which the user is linked',
  `is_main` tinyint(4) NOT NULL DEFAULT '0',
  `validated` tinyint(4) NOT NULL DEFAULT '0',
  `csv_name` varchar(50) DEFAULT NULL,
  `encrypted_id` varchar(32) DEFAULT NULL,
  `approved_by` int(11) NOT NULL,
  `approved_on` datetime NOT NULL,
  `username` varchar(255) DEFAULT NULL,
  `login_failed` datetime NOT NULL,
  `company` varchar(50) DEFAULT NULL,
  `active` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0 - Inactive, 1 - Active, 2 - Pre-registered Inactive',
  `deleted` tinyint(4) NOT NULL DEFAULT '0',
  `car_parking` int(11) DEFAULT NULL,
  `car_registration` varchar(100) DEFAULT NULL,
  `storage_locker` int(11) DEFAULT NULL,
  `groceries` tinyint(4) NOT NULL,
  `laundry` tinyint(4) NOT NULL,
  `luggage` tinyint(4) NOT NULL,
  `package` tinyint(4) NOT NULL,
  `emergency` tinyint(4) NOT NULL,
  `portal_access` tinyint(4) NOT NULL DEFAULT '0',
  `position` varchar(512) DEFAULT NULL,
  `announcements` tinyint(4) unsigned NOT NULL DEFAULT '1',
  `status` varchar(20) NOT NULL,
  `newletters` tinyint(4) NOT NULL DEFAULT '0',
  `reg_on` datetime DEFAULT NULL,
  `last_login` datetime DEFAULT NULL,
  `login_ip` varchar(20) DEFAULT NULL,
  `login_browser` text,
  `tenancy_start` date NOT NULL,
  `tenancy_end` date NOT NULL,
  `subletting_user` enum('Y','N') NOT NULL DEFAULT 'N',
  `added_by` int(11) unsigned NOT NULL COMMENT 'This is used for Subletting',
  `host_id` varchar(60) DEFAULT NULL COMMENT 'Gallagher Host Id',
  `division_id` varchar(60) DEFAULT NULL COMMENT 'Gallagher Division Id',
  `token` varchar(255) DEFAULT NULL,
  `series` varchar(255) DEFAULT NULL,
  `login_attempts` int(11) NOT NULL DEFAULT '0',
  `creation_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `helpdesk_properties` varchar(255) DEFAULT NULL,
  `push_notifications` tinyint(1) NOT NULL DEFAULT '0',
  `tags_facility` varchar(255) DEFAULT NULL,
  `gdpr_consent_dtg` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `domain_id` (`domain_id`),
  KEY `user_group_id` (`user_group_id`),
  CONSTRAINT `users_ibfk_1` FOREIGN KEY (`domain_id`) REFERENCES `domains` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=465 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `version`
--

DROP TABLE IF EXISTS `version`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `version` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `version` varchar(20) NOT NULL,
  `release` varchar(20) NOT NULL,
  `maturity` enum('stable','testing','development') NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `vms_booking`
--

DROP TABLE IF EXISTS `vms_booking`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vms_booking` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `table_name` enum('companies','properties') NOT NULL DEFAULT 'companies',
  `table_id` int(10) unsigned NOT NULL,
  `telephone` varchar(30) NOT NULL,
  `visit_date` date NOT NULL,
  `end_date` date DEFAULT NULL,
  `visit_time` varchar(10) NOT NULL,
  `visit_type` enum('None','Daily','Weekly','Monthly') DEFAULT NULL,
  `description` text NOT NULL,
  `entrance` varchar(250) NOT NULL,
  `booking_id` int(10) unsigned DEFAULT NULL,
  `booked_date` datetime NOT NULL,
  `submitted_by` int(10) unsigned NOT NULL,
  `deleted` enum('Y','N') NOT NULL DEFAULT 'N',
  `status` enum('None','Due','Signed In','Signed Out','No Show','Closed') NOT NULL DEFAULT 'None',
  `gallagher_visit_id` varchar(60) DEFAULT 'none',
  `gallagher_host_id` varchar(60) DEFAULT 'none',
  `host_name` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `booking_id` (`booking_id`),
  KEY `table_id` (`table_name`,`table_id`)
) ENGINE=InnoDB AUTO_INCREMENT=430 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `vms_booking_visitor`
--

DROP TABLE IF EXISTS `vms_booking_visitor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vms_booking_visitor` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `booking_id` int(10) unsigned NOT NULL,
  `visitor_id` int(10) unsigned NOT NULL,
  `visitor_notify` enum('Y','N') NOT NULL DEFAULT 'N',
  `visitor_pass_no` varchar(30) NOT NULL,
  `visitor_status` enum('Due','Signed In','Signed Out','No Show') NOT NULL DEFAULT 'Due',
  `signed_in` time DEFAULT NULL,
  `signed_out` time DEFAULT NULL,
  `deleted` enum('Y','N') NOT NULL DEFAULT 'N',
  PRIMARY KEY (`id`),
  KEY `booking_id` (`booking_id`),
  KEY `visitor_id` (`visitor_id`)
) ENGINE=InnoDB AUTO_INCREMENT=562 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `vms_config`
--

DROP TABLE IF EXISTS `vms_config`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vms_config` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `max_advance` tinyint(4) NOT NULL,
  `max_advance_type` enum('day','week','month','year') CHARACTER SET latin1 NOT NULL COMMENT 'max no of days given for advance booking',
  `fullname_field` tinyint(4) DEFAULT NULL,
  `display_notify_field` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `vms_gallagher_hosts`
--

DROP TABLE IF EXISTS `vms_gallagher_hosts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vms_gallagher_hosts` (
  `host_id` varchar(60) NOT NULL,
  `firstname` varchar(255) NOT NULL,
  `lastname` varchar(255) NOT NULL,
  `division_id` varchar(60) NOT NULL,
  `telephone` varchar(30) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`host_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `vms_user_visitor`
--

DROP TABLE IF EXISTS `vms_user_visitor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vms_user_visitor` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) NOT NULL,
  `name` varchar(255) NOT NULL,
  `lastname` varchar(50) NOT NULL,
  `company` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `email_address` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `visitor_pass_no` varchar(30) NOT NULL,
  `deleted` enum('Y','N') NOT NULL DEFAULT 'N',
  `gallagher_vid` varchar(60) CHARACTER SET utf8 DEFAULT 'none',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=225 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `vmspass`
--

DROP TABLE IF EXISTS `vmspass`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vmspass` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pass_number` varchar(255) NOT NULL,
  `type` varchar(255) DEFAULT NULL,
  `active` tinyint(4) NOT NULL DEFAULT '1',
  `deleted` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `vmspass_config`
--

DROP TABLE IF EXISTS `vmspass_config`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vmspass_config` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `active` tinyint(11) DEFAULT NULL,
  `reverse` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `vmspass_permissions`
--

DROP TABLE IF EXISTS `vmspass_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vmspass_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vmspass_id` int(11) NOT NULL,
  `field_name` varchar(255) NOT NULL,
  `field_value` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
