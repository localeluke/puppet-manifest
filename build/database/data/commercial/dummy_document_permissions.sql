--
-- Dumping data for table `document_permissions`
--

LOCK TABLES `document_permissions` WRITE;
/*!40000 ALTER TABLE `document_permissions` DISABLE KEYS */;
INSERT INTO `document_permissions` VALUES (10,3,'companies',1);
INSERT INTO `document_permissions` VALUES (11,3,'blocks',2);
INSERT INTO `document_permissions` VALUES (12,4,'companies',2);
/*!40000 ALTER TABLE `document_permissions` ENABLE KEYS */;
UNLOCK TABLES;
