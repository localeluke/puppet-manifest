--
-- Dumping data for table `announcement_visibility`
--

LOCK TABLES `announcement_visibility` WRITE;
/*!40000 ALTER TABLE `announcement_visibility` DISABLE KEYS */;
INSERT INTO `announcement_visibility` VALUES (4,4,'blocks',1);
INSERT INTO `announcement_visibility` VALUES (5,4,'company',1);
INSERT INTO `announcement_visibility` VALUES (6,4,'company',2);
INSERT INTO `announcement_visibility` VALUES (7,3,'blocks',1);
INSERT INTO `announcement_visibility` VALUES (8,3,'company',1);
INSERT INTO `announcement_visibility` VALUES (9,3,'company',2);
INSERT INTO `announcement_visibility` VALUES (99,8,'blocks',1);
INSERT INTO `announcement_visibility` VALUES (100,8,'user_groups',2);
INSERT INTO `announcement_visibility` VALUES (101,8,'user_groups',28);
INSERT INTO `announcement_visibility` VALUES (102,8,'users',102);
INSERT INTO `announcement_visibility` VALUES (103,8,'users',78);
INSERT INTO `announcement_visibility` VALUES (104,13,'users',102);
INSERT INTO `announcement_visibility` VALUES (105,13,'users',43);
INSERT INTO `announcement_visibility` VALUES (106,13,'users',112);
INSERT INTO `announcement_visibility` VALUES (109,59,'users',43);
INSERT INTO `announcement_visibility` VALUES (110,60,'users',22);
INSERT INTO `announcement_visibility` VALUES (112,62,'company',38);
INSERT INTO `announcement_visibility` VALUES (114,103,'company',46);
INSERT INTO `announcement_visibility` VALUES (115,105,'users',101);
INSERT INTO `announcement_visibility` VALUES (116,106,'users',87);
INSERT INTO `announcement_visibility` VALUES (117,107,'user_groups',48);
INSERT INTO `announcement_visibility` VALUES (118,107,'users',87);
INSERT INTO `announcement_visibility` VALUES (120,108,'users',209);
/*!40000 ALTER TABLE `announcement_visibility` ENABLE KEYS */;
UNLOCK TABLES;
