
var Exports = {
  Modules : {}
};

Exports.Modules.Gallery = (function($, undefined) {
	var $grid,
	$shapes,
	$cat,
	shapes = [],
	cat = [],

	// Using shuffle with specific column widths
	columnWidths = 80,
	gutterWidths = 55,

	init = function() {
		setVars();
		initFilters();
		initShuffle();
	},

	setVars = function() {
		$grid = $('.js-shuffle');
		$shapes = $('.js-shapes');
		$cat = $('.js-cat');
	},

	initShuffle = function() {
		// instantiate the plugin
		$grid.shuffle({
			speed : 250,
			easing : 'cubic-bezier(0.165, 0.840, 0.440, 1.000)', // easeOutQuart
			columnWidth: 218,
			gutterWidth: 55
		});
	},

	initFilters = function() {
		// shapes
		$shapes.find('input').on('change', function() {
		var $checked = $shapes.find('input:checked'),
		groups = [];

		// At least one checkbox is checked, clear the array and loop through the checked checkboxes
		// to build an array of strings
		if ($checked.length !== 0) {
			$checked.each(function() {
				groups.push(this.value);
			});
		}
		shapes = groups;

		filter();
    });

    // cat
		$cat.find('.btn').on('click', function() {
			var $this = $(this),
			$alreadyChecked,
			checked = [],
			active = 'active',
			isActive;

			if(!$(this).hasClass("active")){
				// Already checked buttons which are not this one
				$alreadyChecked = $this.siblings('.' + active);

				$this.toggleClass( active );

				// Remove active on already checked buttons to act like radio buttons
				if ( $alreadyChecked.length ) {
					$alreadyChecked.removeClass( active );
				}

				isActive = $this.hasClass( active );

				if ( isActive ) {
					checked.push( $this.data( 'filterValue' ) );
				}

				cat = checked;
				if ($this.val() == "View all"){
					//alert (1)
					cat = "";
				}
				// console.log(cat);

				filter();
			}
		});
	},

	filter = function() {
		if ( hasActiveFilters() ) {
			$grid.shuffle('shuffle', function($el) {
				return itemPassesFilters( $el.data() );
			});
		} else {
			$grid.shuffle( 'shuffle', 'all' );
		}
	},

	itemPassesFilters = function(data) {

		// If a shapes filter is active
		if ( shapes.length > 0 && !valueInArray(data.shape, shapes) ) {
		return false;
		}

		// If a cat filter is active
		if ( cat.length > 0 && !valueInArray(data.cat, cat) ) {
		return false;
		}

		return true;
	},

	hasActiveFilters = function() {
		return cat.length > 0 || shapes.length > 0;
	},

	valueInArray = function(value, arr) {
		return $.inArray(value, arr) !== -1;
	};

  

  return {
    init: init
  };
}(jQuery));



$(document).ready(function() {
	Exports.Modules.Gallery.init();
});
