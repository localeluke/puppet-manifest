--
-- Dumping data for table `html_lists`
--

LOCK TABLES `html_lists` WRITE;
/*!40000 ALTER TABLE `html_lists` DISABLE KEYS */;
INSERT INTO `html_lists` VALUES (1,'Testing','testing labels',1,1);
INSERT INTO `html_lists` VALUES (2,'Helpdesk List','helpdesk_fault_list',1,1);
INSERT INTO `html_lists` VALUES (3,'meeting_room_equipments','meeting_room_equipments',1,1);
INSERT INTO `html_lists` VALUES (4,'storage_lockers','storage_lockers',1,1);
INSERT INTO `html_lists` VALUES (5,'helpdesk_fault_list','helpdesk_fault_list',1,0);
INSERT INTO `html_lists` VALUES (6,'User Starter Form - Book VMS Starters','us_book_vms_starters',1,0);
INSERT INTO `html_lists` VALUES (7,'User Starter Form - Type of access','us_type_access',1,0);
INSERT INTO `html_lists` VALUES (8,'Waste Collections','waste-collections',1,0);
INSERT INTO `html_lists` VALUES (9,'Area of Incident','area-of-incident',1,0);
INSERT INTO `html_lists` VALUES (10,'Lost Item Categories','lost-item-categories',1,0);
INSERT INTO `html_lists` VALUES (11,'Loading bay - High Value','high_value',1,0);
INSERT INTO `html_lists` VALUES (12,'Loading bay -  Fragile','fragile',1,0);
INSERT INTO `html_lists` VALUES (13,'Loading bay - Delivery Vehicle Type','delivery_vehicle_type',1,0);
INSERT INTO `html_lists` VALUES (14,'VMS - Entrance Types','vms_entrance_types',1,0);
INSERT INTO `html_lists` VALUES (15,'Helpdesk: Landlord/Tenant','landlord_tenant',1,0);
INSERT INTO `html_lists` VALUES (16,'Helpdesk Levels','helpdesk-levels',1,0);
INSERT INTO `html_lists` VALUES (17,'Faults-helpdesk-accidents-incidents','F-H-Acc',1,0);
INSERT INTO `html_lists` VALUES (18,'Faults-helpdesk-lost-found','F-H-LostFound',1,0);
INSERT INTO `html_lists` VALUES (19,'Faults-helpdesk-FoH-Events','F-H-FoH-Events',1,0);
INSERT INTO `html_lists` VALUES (20,'Waste Collection Timings','waste-collection-timings',1,0);
INSERT INTO `html_lists` VALUES (21,'List Questionnaire 1','',1,0);
INSERT INTO `html_lists` VALUES (22,'List Questionnaire 2','',1,0);
INSERT INTO `html_lists` VALUES (23,'Quick Book','Quick Book',1,0);
INSERT INTO `html_lists` VALUES (24,'Online Induction Form','Online Induction Form',1,0);
INSERT INTO `html_lists` VALUES (25,'Yes or No','Yes or No',1,0);
/*!40000 ALTER TABLE `html_lists` ENABLE KEYS */;
UNLOCK TABLES;
