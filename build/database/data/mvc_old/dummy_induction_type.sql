--
-- Dumping data for table `induction_type`
--

LOCK TABLES `induction_type` WRITE;
/*!40000 ALTER TABLE `induction_type` DISABLE KEYS */;
INSERT INTO `induction_type` VALUES (1,'Personal Induction',1,0);
INSERT INTO `induction_type` VALUES (2,'Online Induction',1,0);
/*!40000 ALTER TABLE `induction_type` ENABLE KEYS */;
UNLOCK TABLES;
