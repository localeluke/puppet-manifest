--
-- Dumping data for table `binary_data`
--

LOCK TABLES `binary_data` WRITE;
/*!40000 ALTER TABLE `binary_data` DISABLE KEYS */;
INSERT INTO `binary_data` VALUES (1,'promotion',1,0,0,0,2,'fitness','','fitness.jpg','image/jpeg',11552,'',218,145,0,0,'',0,'','',0,0);
INSERT INTO `binary_data` VALUES (2,'promotion',1,1,0,1,2,'fitness_1','','fitness_1.jpg','image/jpeg',5161,'',122,81,0,0,'',0,'','',0,0);
INSERT INTO `binary_data` VALUES (3,'promotion',1,0,0,0,1,'fitness_2','','fitness_2.jpg','image/jpeg',24818,'',373,249,0,0,'',0,'','',0,0);
INSERT INTO `binary_data` VALUES (4,'promotion',1,3,0,1,1,'fitness 2','','fitness-2.jpg','image/jpeg',5161,'',122,81,0,0,'',0,'','',0,0);
INSERT INTO `binary_data` VALUES (5,'promotion',2,0,0,0,2,'tmlewin 1','','tmlewin-1.jpg','image/jpeg',7362,'',218,193,0,0,'',0,'','',0,0);
INSERT INTO `binary_data` VALUES (6,'promotion',2,5,0,1,2,'tmlewin 1_1','','tmlewin-1_1.jpg','image/jpeg',3898,'',122,108,0,0,'',0,'','',0,0);
INSERT INTO `binary_data` VALUES (7,'promotion',3,0,0,0,2,'450x225 s dining sunday lunch 2','','450x225-s-dining-sunday-lunch-2.jpg','image/jpeg',7654,'',218,109,0,0,'',0,'','',0,0);
INSERT INTO `binary_data` VALUES (8,'promotion',3,7,0,1,2,'450x225 s dining sunday lunch 2_1','','450x225-s-dining-sunday-lunch-2_1.jpg','image/jpeg',3512,'',122,61,0,0,'',0,'','',0,0);
INSERT INTO `binary_data` VALUES (9,'promotion',3,0,0,0,1,'450x225 s dining sunday lunch 2_2','','450x225-s-dining-sunday-lunch-2_2.jpg','image/jpeg',16274,'',373,186,0,0,'',0,'','',0,0);
INSERT INTO `binary_data` VALUES (10,'promotion',3,9,0,1,1,'450x225 s dining sunday lunch 2 2','','450x225-s-dining-sunday-lunch-2-2.jpg','image/jpeg',3512,'',122,61,0,0,'',0,'','',0,0);
INSERT INTO `binary_data` VALUES (11,'announcement_bgimage',1,0,0,0,1,'2018 02 13 announcement 4 watch 1','','2018-02-13-announcement-4-watch-1.png','image/png',86163,'',454,157,0,0,'',0,'','',0,0);
INSERT INTO `binary_data` VALUES (12,'announcement_bgimage',1,11,0,1,1,'2018 02 13 announcement 4 watch 1_1','','2018-02-13-announcement-4-watch-1_1.png','image/png',86163,'',454,157,0,0,'',0,'','',0,0);
INSERT INTO `binary_data` VALUES (13,'documents',1,0,0,0,1,'blank document 45','','blank-document-45.pdf','application/pdf',27007,'',0,0,0,0,'',0,'','',0,0);
INSERT INTO `binary_data` VALUES (14,'documents',1,0,0,0,2,'appliancemanual3 1 1','','appliancemanual3-1-1.png','image/png',17188,'',117,159,0,0,'',0,'','',0,0);
INSERT INTO `binary_data` VALUES (15,'documents',1,14,0,1,2,'appliancemanual3 1 1_1','','appliancemanual3-1-1_1.png','image/png',17188,'',117,159,0,0,'',0,'','',0,0);
INSERT INTO `binary_data` VALUES (16,'documents',1,14,0,2,2,'appliancemanual3 1 1 1','','appliancemanual3-1-1-1.png','image/png',17188,'',117,159,0,0,'',0,'','',0,0);
INSERT INTO `binary_data` VALUES (17,'documents',2,0,0,0,1,'blank document 45_1','','blank-document-45_1.pdf','application/pdf',27007,'',0,0,0,0,'',0,'','',0,0);
INSERT INTO `binary_data` VALUES (18,'documents',2,0,0,0,2,'appliancemanual3 1 1_2','','appliancemanual3-1-1_2.png','image/png',17188,'',117,159,0,0,'',0,'','',0,0);
INSERT INTO `binary_data` VALUES (19,'documents',2,18,0,1,2,'appliancemanual3 1 1 2','','appliancemanual3-1-1-2.png','image/png',17188,'',117,159,0,0,'',0,'','',0,0);
INSERT INTO `binary_data` VALUES (20,'documents',2,18,0,2,2,'appliancemanual3 1 1 2_1','','appliancemanual3-1-1-2_1.png','image/png',17188,'',117,159,0,0,'',0,'','',0,0);
INSERT INTO `binary_data` VALUES (21,'documents',3,0,0,0,1,'blank document 45_2','','blank-document-45_2.pdf','application/pdf',27007,'',0,0,0,0,'',0,'','',0,0);
INSERT INTO `binary_data` VALUES (22,'documents',3,0,0,0,2,'appliancemanual3 1 1_3','','appliancemanual3-1-1_3.png','image/png',17188,'',117,159,0,0,'',0,'','',0,0);
INSERT INTO `binary_data` VALUES (23,'documents',3,22,0,1,2,'appliancemanual3 1 1 3','','appliancemanual3-1-1-3.png','image/png',17188,'',117,159,0,0,'',0,'','',0,0);
INSERT INTO `binary_data` VALUES (24,'documents',3,22,0,2,2,'appliancemanual3 1 1 3_1','','appliancemanual3-1-1-3_1.png','image/png',17188,'',117,159,0,0,'',0,'','',0,0);
INSERT INTO `binary_data` VALUES (25,'documents',4,0,0,0,1,'blank document 45_3','','blank-document-45_3.pdf','application/pdf',27007,'',0,0,0,0,'',0,'','',0,0);
INSERT INTO `binary_data` VALUES (26,'documents',4,0,0,0,2,'appliancemanual3 1 1_4','','appliancemanual3-1-1_4.png','image/png',17188,'',117,159,0,0,'',0,'','',0,0);
INSERT INTO `binary_data` VALUES (27,'documents',4,26,0,1,2,'appliancemanual3 1 1 4','','appliancemanual3-1-1-4.png','image/png',17188,'',117,159,0,0,'',0,'','',0,0);
INSERT INTO `binary_data` VALUES (28,'documents',4,26,0,2,2,'appliancemanual3 1 1 4_1','','appliancemanual3-1-1-4_1.png','image/png',17188,'',117,159,0,0,'',0,'','',0,0);
INSERT INTO `binary_data` VALUES (29,'content',150,0,0,0,1,'my documents 1','','my-documents-1.png','image/png',89740,'',300,180,0,0,'',0,'','',0,0);
INSERT INTO `binary_data` VALUES (30,'content',150,29,0,1,1,'my documents 1_1','','my-documents-1_1.png','image/png',89740,'',300,180,0,0,'',0,'','',0,0);
INSERT INTO `binary_data` VALUES (31,'content',151,0,0,0,1,'training videos 1','','training-videos-1.png','image/png',61714,'',300,180,0,0,'',0,'','',0,0);
INSERT INTO `binary_data` VALUES (32,'content',151,31,0,1,1,'training videos 1_1','','training-videos-1_1.png','image/png',61714,'',300,180,0,0,'',0,'','',0,0);
INSERT INTO `binary_data` VALUES (33,'content',152,0,0,0,1,'useful contacts 1','','useful-contacts-1.png','image/png',93323,'',300,180,0,0,'',0,'','',0,0);
INSERT INTO `binary_data` VALUES (34,'content',152,33,0,1,1,'useful contacts 1_1','','useful-contacts-1_1.png','image/png',93323,'',300,180,0,0,'',0,'','',0,0);
INSERT INTO `binary_data` VALUES (35,'site_config',40,0,0,0,1,'smalllogo','','smalllogo.jpg','image/jpeg',3085,'',180,44,0,0,'',0,'','',0,0);
INSERT INTO `binary_data` VALUES (36,'site_config',40,35,0,1,1,'smalllogo_1','','smalllogo_1.jpg','image/jpeg',2383,'',122,29,0,0,'',0,'','',0,0);
INSERT INTO `binary_data` VALUES (37,'login_background_images',1,0,0,0,1,'2017 05 23 highendlounge','','2017-05-23-highendlounge.jpg','image/jpeg',477194,'',1920,1280,0,0,'',0,'','',0,0);
INSERT INTO `binary_data` VALUES (38,'login_background_images',1,37,0,1,1,'2017 05 23 highendlounge_1','','2017-05-23-highendlounge_1.jpg','image/jpeg',4163,'',122,81,0,0,'',0,'','',0,0);
INSERT INTO `binary_data` VALUES (39,'groups',0,0,0,0,1,'img 20190507 201641','','img-20190507-201641.jpg','image/jpeg',86333,'',501,230,0,0,'',0,'','',0,0);
INSERT INTO `binary_data` VALUES (40,'groups',0,0,0,0,1,'img 20180719 171537','','img-20180719-171537.jpg','image/jpeg',89858,'',438,328,0,0,'',0,'','',0,0);
INSERT INTO `binary_data` VALUES (41,'groups',0,0,0,0,1,'img 20190507 201641','','img-20190507-201641.jpg','image/jpeg',86448,'',501,230,0,0,'',0,'','',0,0);
INSERT INTO `binary_data` VALUES (42,'groups',0,0,0,0,1,'2019 05 02 12 30 52','','2019-05-02-12-30-52.png','image/png',62951,'',333,278,0,0,'',0,'','',0,0);
INSERT INTO `binary_data` VALUES (43,'groups',0,0,0,0,1,'img 20180720 092459','','img-20180720-092459.jpg','image/jpeg',77380,'',438,328,0,0,'',0,'','',0,0);
/*!40000 ALTER TABLE `binary_data` ENABLE KEYS */;
UNLOCK TABLES;
