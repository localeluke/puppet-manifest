/*function base_url()
{
	if (!window.location.origin) {
		window.location.origin = window.location.protocol + "//" + window.location.hostname + (window.location.port ? ':' + window.location.port: '');
	}
	return location.origin+location.pathname;
}*/

$(window).load(function(){
	$(document).ready(function(){
		//Responsive grid initialization.
		$(".flurid").flurid();
		
		//========== Custome select and multiselect initialization starts ==========//
		var selectObj = $("select").select2({
			"containerCssClass" : function (e) {$(this).attr("data-containerClass")},
			"dropdownCssClass"  : function (e) {$(this).attr("data-containerClass")}
		});
		
		selectObj.on("select2-loaded", function (e) {
            initScroll();
        })
		.on("select2-open", function(e) {
			$(".select2-input").each(function(){
				var ulObj = $(this).parent().next();
				var liCount = $("li", ulObj).length;
				if (liCount < 14){
					$(this).parent().addClass("makeHidden");
				}else{
					$(this).parent().removeClass("makeHidden");
				}
			});
		})
		
		function initScroll(){
			$("ul.select2-results").mCustomScrollbar("destroy");
			$("ul.select2-results").mCustomScrollbar({
				scrollButtons:{
					enable:false
				},
				contentTouchScroll: true
			});
		}
		
		function reset_loginFrm()
		{
			document.getElementById("login_form").reset();
		}
		
		function reset_forgotPwdFrm()
		{
			document.getElementById("forgotPwd_form").reset();
			document.getElementById('captcha').src= base_url()+'home/captcha/'+Math.random();
		}
		
		function reset_registerFrm()
		{
			document.getElementById("register_form").reset();
			$("#title").select2("val", "");
			$("#company").select2("val", "");
			$("#user_group_id").select2("val", "");
			$("div.t_c").removeClass("clicked");
			document.getElementById('t_c').checked = false;
		}
		
		$(".forgotPass").live("click", function(){
			reset_forgotPwdFrm();
			reset_registerFrm();
			reset_loginFrm();
			$(".topRegBtn").fadeOut("slow", function(){
				$(".confirmFpass").fadeOut("slow");
				//$('#register_form').trigger("reset");
				$(".topLoginBtn").fadeIn("slow");
				
			});
			$(".confirmOauth").fadeOut("slow");
			
			$(".loginDiv").slideUp("slow", function(){
				$(".forgotDiv").slideDown("slow");
				$(".confirmFpass").fadeOut("slow");
				$(".confirmOauth").fadeOut("slow");
				$(".loginFooterLogo").fadeOut("slow");
			});
			
			return false;
		});
		
		$(".registerNow, .topRegBtn").live("click", function(){
			$(".confirmOauth").fadeOut("slow");
			reset_forgotPwdFrm();
			reset_registerFrm();
			reset_loginFrm();
			
			$(".topRegBtn").fadeOut("slow", function(){
				$(".topLoginBtn").fadeIn("slow");
				$(".confirmFpass").fadeOut("slow");
				
			});
			
			$(".loginDiv").slideUp("slow", function(){
				$(".registerDiv").slideDown("slow");
				$(".loginFooterLogo").fadeOut("slow");
				$(".confirmFpass").fadeOut("slow");
			});
			
			return false;
		});
		
		$(".topLoginBtn, .rtn2Login, .bck2LI").live("click", function(){
			$(".confirmOauth").fadeOut("slow");
			reset_forgotPwdFrm();
			reset_registerFrm();
			reset_loginFrm();
			
			$(".topLoginBtn").fadeOut("slow", function(){
				$(".topRegBtn").fadeIn("slow");
			});
			
			$(".registerDiv, .forgotDiv, .confirmReg").slideUp("slow", function(){
				setTimeout(function(){
					$(".confirmFpass").fadeOut("slow");
					$(".loginDiv").slideDown("slow");
				}, 1000);
			});
			return false;
		});
		
		$(".cancelBtn").live("click", function(){
			$(this).parent().parent().slideUp("slow", function(){
				$(".loginDiv").slideDown("slow");
				$(".loginFooterLogo").fadeIn("slow");
			});
			return false;
		});
		
		//========== Custome select and multiselect initialization ends ==========//
		
		
		
		
		//========== Magnific popup initialization starts ==========//
		$('.popup-zoom').magnificPopup({
			type: 'inline',

			fixedContentPos: false,
			fixedBgPos: true,

			overflowY: 'auto',

			closeBtnInside: true,
			preloader: false,
			
			midClick: true,
			removalDelay: 300,
			mainClass: 'my-mfp-zoom-in'
		});

		$('.popup-move').magnificPopup({
			type: 'inline',

			fixedContentPos: false,
			fixedBgPos: true,

			overflowY: 'auto',

			closeBtnInside: true,
			preloader: false,
			
			midClick: true,
			removalDelay: 300,
			mainClass: 'my-mfp-slide-bottom'
		});
		$('.ajaxPopup').magnificPopup({
			type: 'ajax',
			alignTop: true,
			overflowY: 'scroll' // as we know that popup content is tall we set scroll overflow by default to avoid jump
		});
		//========== Magnific popup initialization ends ==========//
		
		
		//========== Ui spinner (timePicker) initialization starts ==========//	
		if ($( ".spinner" ).length > 0){
			var spinner = $( ".spinner" ).spinner();
		}
		if ($( ".timeSpinner" ).length > 0){
			var timeSpinner = $( ".timeSpinner" ).timespinner();
		}
		//========== Ui spinner (timePicker) initialization ends ==========//
		
		
		
		
		//========== cookie section starts ==========//
			//function to create cookie
			/************************************************/
			function createCookie(name,value,days) {
				if (days) {
					var date = new Date();
					date.setTime(date.getTime()+(days*24*60*60*1000));
					var expires = "; expires="+date.toGMTString();
				}
				else var expires = "";
				document.cookie = name+"="+value+expires+"; path=/";
			}

			//function to read cookie
			function readCookie(name) {
				var nameEQ = name + "=";
				var ca = document.cookie.split(';');
				for(var i=0;i < ca.length;i++) {
					var c = ca[i];
					while (c.charAt(0)==' ') c = c.substring(1,c.length);
					if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
				}
				//return null;
				return false;
			}

			//function to erase cookie
			function eraseCookie(name) {
				createCookie(name,"",-1);
			}
			/************************************************/
			
			
			if(!readCookie("cookie_tower_portal")){
				createCookie("cookie_tower_portal", "set", 3650);
				$(".cookieWrap").slideDown();
				
				setTimeout(function(){
					$(".cookieWrap").slideUp("slow");
				}, 10000);
			}
			
			//cookie close action
			$(".cookieClose").live("click", function(){
				$(".cookieWrap").slideUp("slow");
				return false;
			});
		//========== cookie section ends ==========//
		
		
		
		
		//========== Fancy checkbox and radiobuttons initialization starts ==========//
		fancyCheckboxInit();
		fancyCheckAll();
		//========== Fancy checkbox and radiobuttons initialization ends ==========//
		
		
		
		
		//========== Script for uiPage starts ==========//
		/*$('.tblDiv .desktopTbl table tbody>tr:odd').css("background-color", "#222222");
		$('.tblDiv .desktopTbl table tbody>tr:odd').addClass("odd");
		$('.tblDiv .desktopTbl table tbody>tr:even').addClass("even");*/
		
		/*$('.tblDiv .desktopTbl table tbody>tr').live("mouseenter", function(){$(this).addClass("hover");});
		$('.tblDiv .desktopTbl table tbody>tr').live("mouseleave", function(){$(this).removeClass("hover");});*/
		
		/*$("a.showView, .tblTitleTxt").live("click", function(){
			var thisPar = $(this).parent().parent();
			$(this).parent().toggleClass("active");
			
			$(".tableDesc", thisPar).slideToggle("fast");
			
			return false;
		});*/
				
		$("input[type='button'], input[type='submit'], .btn-custom, .small-btn, .odd, .even").live("mouseenter", function(){
			$(this).addClass("hover");
		});
		$("input[type='button'], input[type='submit'], .btn-custom, .small-btn, .odd, .even").live("mouseleave", function(){
			$(this).removeClass("hover");
		});
		//========== Script for uiPage starts ==========//
		
		
		//========== Script for Mobile keypad starts ==========//
			var event;
			$('.telephone').click(function(event){
				$('#keypad').fadeToggle('fast');
				event.stopPropagation();
			});
			$('.key').click(function(event){
				var telephone = $('.telephone');
				telephone.val( telephone.val() + this.innerHTML);
				event.stopPropagation();
			});
			
			$('.btn-click').click(function(event){
				if(this.innerHTML == 'DEL'){
					var telephone = $('.telephone');
					if(telephone.val().length > 0){
						telephone.val(telephone.val().substring(0, telephone.val().length - 1));
					}
				}else{
					$('.telephone').val("");
				}
				event.stopPropagation();
			});
		//========== Script for Mobile keypad ends ==========//
		
		
		//========== fullcalendar script starts ==========//
		$(".fc-day").live("click", function(){
			$(".fc-day").removeClass("active");
			$(this).addClass("active");
			var childObj = $(this).find("div:first");
			$(childObj).css({
				width: ($(this).width() - 4),
				height: ($(this).height() - 4)
			})
		});
		//========== fullcalendar script ends ==========//
		
		$(".inpt").focus(function(){
			$(this).parent().parent().addClass("focus");
		});
		$(".inpt").blur(function(){
			$(this).parent().parent().removeClass("focus");
		});
		
		
		//========== functions to be loaded on document load starts ==========//
		setBlackWrapper();
		//========== functions to be loaded on document load ends ==========//
		
		$(window).scroll(function (){
			
		});
		
		$(window).resize(function(){
			setBlackWrapper();
		});
		
		$(window).bind('orientationchange', function(e) {
			
		});
	});
});

//Code for blackWrapper take full height of window
function setBlackWrapper(){
	$(".blackWrapper").css("min-height", $(window).height() - 52);
}

//========== Fancycheckbox and radio buttons functions starts ==========//
function fancyCheckboxInit(){
	//$(".checkChange").before("<div class='chk'></div>");
	$(".checkChange").each(function(){
		if(!$(this).prev().hasClass("chk"))
		$(this).before("<div class='chk'></div>");
	});
	$('.checkChange').css("opacity", 0);
	
	$('.checkChange').die();
	$('.checkChange').live("click", function(){
		var eleName = $(this).prop("name").split('[')[0];
		if($(this).prop('type')=='radio'){
			$(".radioCont ."+ eleName).removeClass("clicked");
			$(this).prev().toggleClass("clicked");
		}else{
			$(this).prev().toggleClass("clicked");
		}
	});
}

function fancyCheckAll(){
	$('.checkChange').each(function(){
		var eleName = $(this).prop("name").split('[')[0];
		$(this).prev().addClass(eleName);
  
		if ($(this).is(":checked")){
			$(this).prev().addClass("clicked");
		}
	});
}
//========== Fancycheckbox and radio buttons functions ends ==========//

function ajax_popup_init()
{
		$('.ajaxPopup2').magnificPopup({
			type: 'ajax',
			alignTop: true,
			overflowY: 'scroll' // as we know that popup content is tall we set scroll overflow by default to avoid jump
		});
}

(function ( $ ) {
	$.fn.tabify = function( options ) {
		// Default options.
		var settings = $.extend({
			// These are the defaults.
			tabPosition: "top"
		}, options );
		
		// main code.
		return this.each(function(index) {
			// Do something to each element here.
			$(this).append('<div>'+  settings.tabPosition +'--' + index + '</div>')
		});
	};
}( jQuery ));