--
-- Dumping data for table `induction_department_setup`
--

LOCK TABLES `induction_department_setup` WRITE;
/*!40000 ALTER TABLE `induction_department_setup` DISABLE KEYS */;
INSERT INTO `induction_department_setup` VALUES (14,'Building Manager/Estate Manager',1,NULL,NULL,NULL,0);
INSERT INTO `induction_department_setup` VALUES (15,'Occupier/Occupier Rep',1,NULL,NULL,NULL,0);
INSERT INTO `induction_department_setup` VALUES (16,'Security',1,NULL,NULL,NULL,0);
INSERT INTO `induction_department_setup` VALUES (17,'Reception',1,NULL,NULL,NULL,0);
/*!40000 ALTER TABLE `induction_department_setup` ENABLE KEYS */;
UNLOCK TABLES;
