--
-- Dumping data for table `vmspass_config`
--

LOCK TABLES `vmspass_config` WRITE;
/*!40000 ALTER TABLE `vmspass_config` DISABLE KEYS */;
INSERT INTO `vmspass_config` VALUES (1,1,'<p><b>IMPORTANT HEALTH AND SAFETY INFORMATION</b></p>\r\n\r\n<p><b>Health &amp; Safety at Work:</b><br />\r\nA safe working environment is provided and all visitors are requested to co-operate in ensuring that Health &amp; Safety Rules and Regulations are followed at all times.</p>\r\n\r\n<p><b>Emergency Procedure:</b><br />\r\nIn emergencies, please follow the instructions given by your host or Grosvenor Place Security. The Fire evacuation assembly point is located at St Peters Church, Eaton Square. Please remain there until you receive further instructions.</p>\r\n\r\n<p><b>Note: The fire alarm is tested every Friday at 10am.</b></p>\r\n\r\n<p><b>Smoking Policy:</b><br />\r\nGrosvenor Place has a no smoking policy. Please observe this policy.</p>\r\n\r\n<p><b>General Precautions:</b><br />\r\nVisitors should be accompanied at all times.&nbsp; Hi Dan, How are you today?</p>\r\n');
/*!40000 ALTER TABLE `vmspass_config` ENABLE KEYS */;
UNLOCK TABLES;
