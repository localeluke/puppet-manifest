class localeportal {
  exec { 'set_language':
    command => 'sudo locale-gen en_GB.UTF-8',
    path     => [ '/usr/bin/', '/usr/local/bin/' ],
  }

  package { "clamav":
    ensure  => installed,
  }

  package { "clamdscan":
    ensure  => installed,
    require => Package["clamav"],
  }

  package { "clamav-daemon":
    ensure  => installed,
    require => Package["clamdscan"],
  }

  file { "/etc/clamav/clamd.conf":
    ensure  => present,
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    source  => 'puppet:///modules/files/clamav',
    require => Package["clamdscan"],
  }

  service { 'clamav-daemon':
    ensure  => 'running',
    require => [ File[ "/etc/clamav/clamd.conf" ], Package[ "clamav-daemon" ] ],
  }

  vcsrepo { '/home/www/locale-portal':
    ensure            => present,
    provider          => git,
    source            => 'git@bitbucket.org:localeltd/locale-portal.git',
    trust_server_cert => true,
    revision          => 'luke/LPD-540_TobFacilityClashes',
    require           => [ File["/root/.ssh/config"], File["/root/.ssh/ssh-rsa"], File["/root/.ssh/ssh-rsa.pub"] ],
  }

  exec { 'composer-dependancies':
    environment => ["COMPOSER_HOME=/usr/local/bin"],
    command     => 'composer install -d /home/www/locale-portal/private/',
    path        => [ '/usr/bin/', '/usr/local/bin/' ],
    require     => [ Vcsrepo["/home/www/locale-portal"], Exec["install_composer"] ],
  }

  file { '/home/www/locale-portal/private/tests/Unit/config.php':
    ensure  => present,
    owner   => 'root',
    group   => 'root',
    mode    => '0664',
    source  => '/home/www/locale-portal/private/tests/Unit/config.php.temp',
    require => Exec["composer-dependancies"],
  }

  exec { 'install_phing':
    environment => ["COMPOSER_HOME=/usr/local/bin"],
    command     => 'composer global require "phing/phing":"2.16.1"',
    path        => [ '/usr/bin/', '/usr/local/bin/' ],
    require     => Exec["composer-dependancies"],
  }

  file { '/etc/environment':
    ensure  => present,
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    source  => 'puppet:///modules/files/path',
    require => Exec["install_phing"],
  }
}
