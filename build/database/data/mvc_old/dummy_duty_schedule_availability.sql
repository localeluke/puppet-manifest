--
-- Dumping data for table `duty_schedule_availability`
--

LOCK TABLES `duty_schedule_availability` WRITE;
/*!40000 ALTER TABLE `duty_schedule_availability` DISABLE KEYS */;
INSERT INTO `duty_schedule_availability` VALUES (1,1,'2014-11-08','NIGHT');
INSERT INTO `duty_schedule_availability` VALUES (2,1,'2014-10-23','HOLIDAY');
/*!40000 ALTER TABLE `duty_schedule_availability` ENABLE KEYS */;
UNLOCK TABLES;
