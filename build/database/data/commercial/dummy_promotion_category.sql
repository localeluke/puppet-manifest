--
-- Dumping data for table `promotion_category`
--

LOCK TABLES `promotion_category` WRITE;
/*!40000 ALTER TABLE `promotion_category` DISABLE KEYS */;
INSERT INTO `promotion_category` VALUES (1,'Health & Fitness ','Health & Fitness ',1,0,0,NULL,NULL);
INSERT INTO `promotion_category` VALUES (2,'Dining','',1,0,0,NULL,NULL);
INSERT INTO `promotion_category` VALUES (3,'Retail','',1,0,0,NULL,NULL);
/*!40000 ALTER TABLE `promotion_category` ENABLE KEYS */;
UNLOCK TABLES;
