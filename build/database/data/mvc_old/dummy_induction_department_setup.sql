--
-- Dumping data for table `induction_department_setup`
--

LOCK TABLES `induction_department_setup` WRITE;
/*!40000 ALTER TABLE `induction_department_setup` DISABLE KEYS */;
INSERT INTO `induction_department_setup` VALUES (1,'aqua',0,NULL,NULL,NULL,1);
INSERT INTO `induction_department_setup` VALUES (2,'aqua 2',0,NULL,NULL,NULL,1);
INSERT INTO `induction_department_setup` VALUES (3,'test123',0,NULL,NULL,NULL,1);
INSERT INTO `induction_department_setup` VALUES (4,'test123654',0,NULL,NULL,NULL,1);
INSERT INTO `induction_department_setup` VALUES (5,'Company Reps and Occupier Plus',0,NULL,NULL,NULL,1);
INSERT INTO `induction_department_setup` VALUES (6,'TVFTS Company Rep',0,NULL,NULL,NULL,1);
INSERT INTO `induction_department_setup` VALUES (7,'LBQ Configuration',0,NULL,NULL,NULL,1);
INSERT INTO `induction_department_setup` VALUES (8,'Principle Company Rep Confguration',0,NULL,NULL,NULL,1);
INSERT INTO `induction_department_setup` VALUES (9,'Principle Company Rep',1,NULL,NULL,NULL,0);
INSERT INTO `induction_department_setup` VALUES (10,'Shard Quarter Contract Admin',1,NULL,NULL,NULL,0);
INSERT INTO `induction_department_setup` VALUES (11,'TVFTS Principle Company Rep',1,NULL,NULL,NULL,0);
INSERT INTO `induction_department_setup` VALUES (12,'Shard Quarter SSU',1,NULL,NULL,NULL,0);
INSERT INTO `induction_department_setup` VALUES (13,'Demo',0,NULL,NULL,NULL,0);
/*!40000 ALTER TABLE `induction_department_setup` ENABLE KEYS */;
UNLOCK TABLES;
