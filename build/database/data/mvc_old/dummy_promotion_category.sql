--
-- Dumping data for table `promotion_category`
--

LOCK TABLES `promotion_category` WRITE;
/*!40000 ALTER TABLE `promotion_category` DISABLE KEYS */;
INSERT INTO `promotion_category` VALUES (1,'Events','',1,0,0);
INSERT INTO `promotion_category` VALUES (2,'Food & Drink','',1,0,0);
INSERT INTO `promotion_category` VALUES (3,'Health','',1,1,1);
INSERT INTO `promotion_category` VALUES (4,'Lifestyle','',1,0,1);
INSERT INTO `promotion_category` VALUES (5,'Travel','',1,0,0);
INSERT INTO `promotion_category` VALUES (6,'Shard Services','',0,0,0);
INSERT INTO `promotion_category` VALUES (7,'Shard Benefits','',1,0,0);
INSERT INTO `promotion_category` VALUES (8,'Travel 2','',1,0,7);
INSERT INTO `promotion_category` VALUES (9,'Really long category','',1,0,0);
INSERT INTO `promotion_category` VALUES (10,'Richs test','test',1,1,0);
INSERT INTO `promotion_category` VALUES (11,'Health','',1,0,0);
INSERT INTO `promotion_category` VALUES (12,'Launches','test description',1,0,1);
/*!40000 ALTER TABLE `promotion_category` ENABLE KEYS */;
UNLOCK TABLES;
