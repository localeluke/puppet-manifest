--
-- Dumping data for table `user_properties`
--

LOCK TABLES `user_properties` WRITE;
/*!40000 ALTER TABLE `user_properties` DISABLE KEYS */;
INSERT INTO `user_properties` VALUES (5,3,4,'properties',1);
INSERT INTO `user_properties` VALUES (6,4,3,'properties',2);
INSERT INTO `user_properties` VALUES (7,5,4,'properties',1);
INSERT INTO `user_properties` VALUES (8,6,3,'properties',2);
INSERT INTO `user_properties` VALUES (10,8,3,'properties',2);
INSERT INTO `user_properties` VALUES (12,9,4,'properties',1);
INSERT INTO `user_properties` VALUES (13,7,4,'properties',1);
INSERT INTO `user_properties` VALUES (15,10,3,'properties',2);
INSERT INTO `user_properties` VALUES (16,11,3,'properties',2);
INSERT INTO `user_properties` VALUES (17,2,5,'properties',3);
INSERT INTO `user_properties` VALUES (18,2,4,'properties',1);
INSERT INTO `user_properties` VALUES (19,2,3,'properties',2);
INSERT INTO `user_properties` VALUES (20,1,5,'properties',3);
INSERT INTO `user_properties` VALUES (21,1,4,'properties',1);
INSERT INTO `user_properties` VALUES (22,1,3,'properties',2);
INSERT INTO `user_properties` VALUES (23,12,5,'properties',3);
/*!40000 ALTER TABLE `user_properties` ENABLE KEYS */;
UNLOCK TABLES;
