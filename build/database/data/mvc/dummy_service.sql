--
-- Dumping data for table `service`
--

LOCK TABLES `service` WRITE;
/*!40000 ALTER TABLE `service` DISABLE KEYS */;
INSERT INTO `service` VALUES (27,'BAXTER STOREY','independent catering service provider','<p style=\"text-align: center;\"><strong>Baxter Storey</strong></p>\r\n\r\n<p style=\"text-align: center;\">Independent catering service provider.</p>\r\n\r\n<hr />\r\n<p style=\"text-align: center;\"><br />\r\nLeading independent catering service provider for business and industry.</p>\r\n\r\n<p style=\"text-align: center;\"><a href=\"http://www.baxterstorey.com\" target=\"_blank\">www.baxterstorey.com</a></p>\r\n\r\n<hr />\r\n<p style=\"text-align: center;\"><br />\r\n<strong>For more information, please contact:</strong><br />\r\n<br />\r\nSimon Esner<br />\r\n<br />\r\n<strong>Mobile:</strong> 07967 664 497<br />\r\n<strong>Email:</strong>&nbsp;<a href=\"mailto: sesner@baxterstorey.com\">sesner@baxterstorey.com</a></p>\r\n','2019-04-30','2019-06-30',0,1,0);
INSERT INTO `service` VALUES (28,'Havebike','Bicycle servicing and repairs','<p style=\"text-align: center;\"><img alt=\"\" src=\"/UserFiles/images/bikes (1).jpg\" /></p>\r\n\r\n<p style=\"text-align: center;\"><strong>Havebike</strong></p>\r\n\r\n<p style=\"text-align: center;\">Bicycle servicing and repairs.</p>\r\n\r\n<hr />\r\n<p style=\"text-align: center;\"><br />\r\nBicycle servicing and repairs based in Bermondsey.<br />\r\n<br />\r\n<a href=\"http://www.havebike.co.uk\" target=\"_blank\">www.havebike.co.uk</a></p>\r\n\r\n<hr />\r\n<p style=\"text-align: center;\"><br />\r\n<strong>For more information, please contact:</strong><br />\r\n<br />\r\nJames Ashburnham<br />\r\n<br />\r\n<strong>Telephone:</strong> 020 3397 0888<br />\r\n<strong>Email:</strong>&nbsp;<a href=\"mailto:james.ashburnham@havebike.co.uk\">james.ashburnham@havebike.co.uk</a></p>\r\n','2019-04-30','2020-06-30',0,1,0);
INSERT INTO `service` VALUES (29,'Test Service','Test Service','<p>This is a test</p>\r\n','2019-04-30','2019-06-30',0,1,0);
INSERT INTO `service` VALUES (30,'Test Service 2','Test Service 2','<p>This is another test</p>\r\n','2019-04-30','2019-06-30',0,1,0);
/*!40000 ALTER TABLE `service` ENABLE KEYS */;
UNLOCK TABLES;
