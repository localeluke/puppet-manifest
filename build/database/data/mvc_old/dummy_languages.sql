--
-- Dumping data for table `languages`
--

LOCK TABLES `languages` WRITE;
/*!40000 ALTER TABLE `languages` DISABLE KEYS */;
INSERT INTO `languages` VALUES (1,'English',1);
INSERT INTO `languages` VALUES (2,'Mandarin',1);
INSERT INTO `languages` VALUES (3,'Portugese',1);
INSERT INTO `languages` VALUES (4,'Bengali',1);
INSERT INTO `languages` VALUES (5,'Swedish',1);
INSERT INTO `languages` VALUES (6,'Hindi',1);
INSERT INTO `languages` VALUES (7,'Spanish',1);
INSERT INTO `languages` VALUES (8,'French',1);
INSERT INTO `languages` VALUES (9,'German',1);
INSERT INTO `languages` VALUES (10,'Japanese',1);
INSERT INTO `languages` VALUES (11,'Russian',1);
INSERT INTO `languages` VALUES (12,'Italian',1);
INSERT INTO `languages` VALUES (13,'Polish',1);
INSERT INTO `languages` VALUES (14,'Slovak',1);
INSERT INTO `languages` VALUES (15,'Turkish',1);
INSERT INTO `languages` VALUES (16,'Dutch',1);
INSERT INTO `languages` VALUES (17,'Arabic',1);
INSERT INTO `languages` VALUES (18,'Finnish',1);
INSERT INTO `languages` VALUES (19,'Danish',1);
INSERT INTO `languages` VALUES (20,'Flemish',1);
INSERT INTO `languages` VALUES (21,'czech',1);
/*!40000 ALTER TABLE `languages` ENABLE KEYS */;
UNLOCK TABLES;
