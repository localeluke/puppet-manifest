--
-- Dumping data for table `groups_members`
--

LOCK TABLES `groups_members` WRITE;
/*!40000 ALTER TABLE `groups_members` DISABLE KEYS */;
INSERT INTO `groups_members` VALUES (1,1,1,'2019-07-15 15:53:15');
INSERT INTO `groups_members` VALUES (2,2,3,'2019-07-15 15:56:02');
INSERT INTO `groups_members` VALUES (3,3,3,'2019-07-15 16:14:14');
INSERT INTO `groups_members` VALUES (4,4,1,'2019-07-16 07:50:39');
/*!40000 ALTER TABLE `groups_members` ENABLE KEYS */;
UNLOCK TABLES;
