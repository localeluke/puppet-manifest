--
-- Dumping data for table `documents`
--

LOCK TABLES `documents` WRITE;
/*!40000 ALTER TABLE `documents` DISABLE KEYS */;
INSERT INTO `documents` VALUES (25,'12','Desk Phone Pick Up Group User Guide','public',1,0,NULL,'0000-00-00','');
INSERT INTO `documents` VALUES (26,'12','Fit Out Waste Guide','public',1,0,NULL,'0000-00-00','');
INSERT INTO `documents` VALUES (27,'11','LBQ House Rules & Regulations','public',1,0,NULL,'0000-00-00','');
INSERT INTO `documents` VALUES (28,'12','Lift Guide','public',1,0,NULL,'0000-00-00','');
INSERT INTO `documents` VALUES (29,'12','Occupier Service Entrance Access Guide','public',1,0,NULL,'0000-00-00','');
INSERT INTO `documents` VALUES (30,'12','Office Passenger Lift','public',1,0,NULL,'0000-00-00','');
/*!40000 ALTER TABLE `documents` ENABLE KEYS */;
UNLOCK TABLES;
