--
-- Dumping data for table `floors`
--

LOCK TABLES `floors` WRITE;
/*!40000 ALTER TABLE `floors` DISABLE KEYS */;
INSERT INTO `floors` VALUES (1,'3',1,0);
INSERT INTO `floors` VALUES (2,'7',1,0);
INSERT INTO `floors` VALUES (3,'8',1,0);
INSERT INTO `floors` VALUES (4,'4-6',1,0);
INSERT INTO `floors` VALUES (5,'9',1,0);
INSERT INTO `floors` VALUES (6,'10',1,0);
INSERT INTO `floors` VALUES (7,'11',1,0);
INSERT INTO `floors` VALUES (8,'12',1,0);
INSERT INTO `floors` VALUES (9,'13',1,0);
INSERT INTO `floors` VALUES (10,'15',1,0);
INSERT INTO `floors` VALUES (11,'14',1,0);
INSERT INTO `floors` VALUES (12,'16',1,0);
INSERT INTO `floors` VALUES (13,'17',1,0);
INSERT INTO `floors` VALUES (14,'18',1,0);
INSERT INTO `floors` VALUES (15,'19',1,0);
INSERT INTO `floors` VALUES (16,'20',1,0);
INSERT INTO `floors` VALUES (17,'21',1,0);
INSERT INTO `floors` VALUES (18,'22',1,0);
INSERT INTO `floors` VALUES (19,'23',1,0);
INSERT INTO `floors` VALUES (20,'24',1,0);
INSERT INTO `floors` VALUES (21,'25',1,0);
INSERT INTO `floors` VALUES (22,'26',1,0);
INSERT INTO `floors` VALUES (23,'27',1,0);
INSERT INTO `floors` VALUES (24,'28',1,0);
INSERT INTO `floors` VALUES (25,'31',1,0);
INSERT INTO `floors` VALUES (26,'32',1,0);
INSERT INTO `floors` VALUES (27,'33',1,0);
INSERT INTO `floors` VALUES (28,'34-52',1,0);
INSERT INTO `floors` VALUES (29,'68-72',1,0);
/*!40000 ALTER TABLE `floors` ENABLE KEYS */;
UNLOCK TABLES;
