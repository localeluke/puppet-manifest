--
-- Dumping data for table `concierge_contacts`
--

LOCK TABLES `concierge_contacts` WRITE;
/*!40000 ALTER TABLE `concierge_contacts` DISABLE KEYS */;
INSERT INTO `concierge_contacts` VALUES (1,'Building Management','2,1',NULL,0,1,0);
INSERT INTO `concierge_contacts` VALUES (2,'Security','2,1,3',NULL,0,1,0);
INSERT INTO `concierge_contacts` VALUES (3,'Reception','2,4',NULL,0,1,0);
/*!40000 ALTER TABLE `concierge_contacts` ENABLE KEYS */;
UNLOCK TABLES;
