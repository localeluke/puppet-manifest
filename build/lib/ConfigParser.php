<?php

require_once "phing/Task.php";

class ConfigParser extends Task
{
    const PREFIX_SEPARATOR = '.';
    const CONFIG_PROPERTIES_FILE = 'build/config.properties';

    /**
     * @var resource
     */
    protected $configFile;

    /**
     * The message passed in the buildfile.
     */
    private $key = '';

    /**
     * The message passed in the buildfile.
     */
    private $value = '';

    public function setKey( $key )
    {
        $this->key = $key;
    }

    public function setValue( $value )
    {
        $this->value = $value;
    }

    public function init()
    {
        $this->configFile = $this->loadConfigFile();
    }

    public function main()
    {
        if( empty( $this->key ) || empty( $this->value ) ) {
            throw new Exception( 'The suite has not been defined' );
        }

        $this->append( $this->key, $this->value );

        fclose( $this->configFile );
    }

    /**
     * @return resource
     */
    protected function loadConfigFile()
    {
        return fopen( static::CONFIG_PROPERTIES_FILE, 'w' );
    }


    protected function processProperties( array $data, $prefix = null )
    {
        $prefixString = ( is_null( $prefix ) ) ? '' : $prefix . self::PREFIX_SEPARATOR;

        foreach( $data as $key => $value ) {
            $propertyKey = $prefixString . $key;
            if( is_array( $value ) ) {
                $this->processProperties( $value, $propertyKey );
            } else {
                $this->append( $propertyKey, $value );
            }
        }
    }

    protected function append( $property, $value )
    {
        fwrite($this->configFile, "$property = $value\n");
    }
}
