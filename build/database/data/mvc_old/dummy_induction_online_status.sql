--
-- Dumping data for table `induction_online_status`
--

LOCK TABLES `induction_online_status` WRITE;
/*!40000 ALTER TABLE `induction_online_status` DISABLE KEYS */;
INSERT INTO `induction_online_status` VALUES (0,'Not Attempted');
INSERT INTO `induction_online_status` VALUES (1,'In Progress');
INSERT INTO `induction_online_status` VALUES (2,'Unsuccessful');
INSERT INTO `induction_online_status` VALUES (3,'Completed');
INSERT INTO `induction_online_status` VALUES (9,'Personal Induction');
/*!40000 ALTER TABLE `induction_online_status` ENABLE KEYS */;
UNLOCK TABLES;
