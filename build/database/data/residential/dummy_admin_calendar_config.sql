--
-- Dumping data for table `admin_calendar_config`
--

LOCK TABLES `admin_calendar_config` WRITE;
/*!40000 ALTER TABLE `admin_calendar_config` DISABLE KEYS */;
INSERT INTO `admin_calendar_config` VALUES (1,0,'Announcements','#FFFFFF',0,1,0);
INSERT INTO `admin_calendar_config` VALUES (2,0,'Building Event','#42A1FF',0,1,0);
INSERT INTO `admin_calendar_config` VALUES (3,0,'Duty Handover','#FFFFFF',0,1,0);
INSERT INTO `admin_calendar_config` VALUES (4,0,'Loading Bay','#FFFFFF',0,1,0);
INSERT INTO `admin_calendar_config` VALUES (5,0,'Maintenance','#FF6242',0,1,0);
INSERT INTO `admin_calendar_config` VALUES (6,1,'Announcements',NULL,1,1,0);
INSERT INTO `admin_calendar_config` VALUES (7,22,'Duty Handover',NULL,1,1,0);
/*!40000 ALTER TABLE `admin_calendar_config` ENABLE KEYS */;
UNLOCK TABLES;
