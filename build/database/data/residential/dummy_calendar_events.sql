--
-- Dumping data for table `calendar_events`
--

LOCK TABLES `calendar_events` WRITE;
/*!40000 ALTER TABLE `calendar_events` DISABLE KEYS */;
INSERT INTO `calendar_events` VALUES (1,0,'Test Event',1,'2019-04-05','2019-04-10','00:00:00','23:00:00',NULL,2,'test',0,0,0,'2019-04-05',0,1);
INSERT INTO `calendar_events` VALUES (2,0,'test',1,'2019-04-05','2019-04-10','01:00:00','17:30:00',NULL,5,'test',0,1,0,'2019-04-05',0,1);
/*!40000 ALTER TABLE `calendar_events` ENABLE KEYS */;
UNLOCK TABLES;
