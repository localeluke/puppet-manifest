--
-- Dumping data for table `blocks`
--

LOCK TABLES `blocks` WRITE;
/*!40000 ALTER TABLE `blocks` DISABLE KEYS */;
INSERT INTO `blocks` VALUES (1,0,1,1,'The Shard',1,NULL,'SE1 9SG','',NULL,'B1ref',NULL,'TS','TS',1,0,'51.5049619','-0.0876506');
INSERT INTO `blocks` VALUES (2,0,1,1,'test',1,NULL,'test','',NULL,'',NULL,'','test',1,1,'','');
INSERT INTO `blocks` VALUES (3,0,1,1,'Test',1,NULL,'TEST','',NULL,'',NULL,'test','test',0,0,'45.512101','-122.6770504');
INSERT INTO `blocks` VALUES (4,0,1,1,'Locale',1,NULL,'OX1 4HH','',NULL,'',NULL,'','L',1,1,'','');
/*!40000 ALTER TABLE `blocks` ENABLE KEYS */;
UNLOCK TABLES;
