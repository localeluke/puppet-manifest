<?php
session_start();
include dirname(dirname(dirname(__FILE__))).'/config/config.php';

$path = ($_SERVER['HTTPS'] == "on") ? "https" : "http";
$path .= "://".$_SERVER['HTTP_HOST'];
$path .= (stristr($_config['doc_root'],"http")) ? "" : $_config['doc_root'];
$path = rtrim($path,"/");
$path = $path."/";

$load_base_url		= 	$_SESSION[$_SESSION['load_app_mode']]['load_base_url'];
$_SESSION['site_email_btn_std_color']	=   '#E2E2E2';
$_SESSION['site_email_btn_text_color']	=   '#3B3B3B';
?>
var cDocRoot 			= "<?=$_config['doc_root']?>";
var mainPath 			= "<?=$path?>";
var loginPath 			= "<?= $_SESSION["domainName"] ? 'http://'.$_SERVER["SERVER_NAME"].$GLOBALS[config][main_doc_root].$_SESSION["domainName"] : $path?>";
var _globalDocRoot 		= '<?= GLOBAL_MAIN_DOC_ROOT ?>';
var email_std_btn 		= "#E2E2E2";
var email_std_btn_txt 	= "#3B3B3B";
var load_base_url 		= 	"<?= $load_base_url ?>";
