--
-- Dumping data for table `helpdesk_multidata`
--

LOCK TABLES `helpdesk_multidata` WRITE;
/*!40000 ALTER TABLE `helpdesk_multidata` DISABLE KEYS */;
INSERT INTO `helpdesk_multidata` VALUES (25,'admin_helpdesk_cat',1,'fault_list',17);
INSERT INTO `helpdesk_multidata` VALUES (26,'admin_helpdesk_cat',1,'fault_list',18);
INSERT INTO `helpdesk_multidata` VALUES (27,'admin_helpdesk_cat',1,'fault_list',20);
INSERT INTO `helpdesk_multidata` VALUES (28,'admin_helpdesk_cat',1,'fault_list',19);
INSERT INTO `helpdesk_multidata` VALUES (29,'admin_helpdesk_cat',1,'fault_list',21);
INSERT INTO `helpdesk_multidata` VALUES (30,'admin_helpdesk_cat',1,'fault_list',22);
INSERT INTO `helpdesk_multidata` VALUES (31,'admin_helpdesk_cat',1,'fault_list',23);
INSERT INTO `helpdesk_multidata` VALUES (32,'admin_helpdesk_cat',1,'fault_list',24);
INSERT INTO `helpdesk_multidata` VALUES (33,'admin_helpdesk_cat',1,'fault_list',27);
INSERT INTO `helpdesk_multidata` VALUES (34,'admin_helpdesk_cat',1,'fault_list',25);
INSERT INTO `helpdesk_multidata` VALUES (35,'admin_helpdesk_cat',1,'fault_list',26);
INSERT INTO `helpdesk_multidata` VALUES (36,'admin_helpdesk_cat',1,'fault_list',28);
INSERT INTO `helpdesk_multidata` VALUES (37,'admin_helpdesk_cat',1,'block',4);
INSERT INTO `helpdesk_multidata` VALUES (38,'admin_helpdesk_cat',1,'block',3);
INSERT INTO `helpdesk_multidata` VALUES (39,'admin_helpdesk_cat',1,'property',1);
INSERT INTO `helpdesk_multidata` VALUES (40,'admin_helpdesk_cat',1,'property',2);
INSERT INTO `helpdesk_multidata` VALUES (41,'admin_helpdesk_cat',1,'user_type',2);
INSERT INTO `helpdesk_multidata` VALUES (42,'admin_helpdesk_cat',1,'user_type',3);
INSERT INTO `helpdesk_multidata` VALUES (43,'admin_helpdesk_cat',1,'user_type',1);
INSERT INTO `helpdesk_multidata` VALUES (44,'admin_helpdesk_cat',1,'user_type',4);
INSERT INTO `helpdesk_multidata` VALUES (45,'admin_helpdesk_cat',1,'user_type',5);
INSERT INTO `helpdesk_multidata` VALUES (46,'admin_helpdesk_cat',1,'user_type',6);
INSERT INTO `helpdesk_multidata` VALUES (47,'admin_helpdesk_cat',1,'admin_notify_grp',2);
INSERT INTO `helpdesk_multidata` VALUES (48,'admin_helpdesk_cat',1,'admin_notify_grp',1);
INSERT INTO `helpdesk_multidata` VALUES (49,'admin_helpdesk_cat',1,'admin_users',1);
INSERT INTO `helpdesk_multidata` VALUES (50,'admin_helpdesk_cat',1,'admin_notify_grp2',2);
INSERT INTO `helpdesk_multidata` VALUES (51,'admin_helpdesk_cat',1,'admin_notify_grp2',1);
INSERT INTO `helpdesk_multidata` VALUES (52,'admin_helpdesk_cat',1,'admin_users2',1);
/*!40000 ALTER TABLE `helpdesk_multidata` ENABLE KEYS */;
UNLOCK TABLES;
