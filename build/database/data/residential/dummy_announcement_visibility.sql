--
-- Dumping data for table `announcement_visibility`
--

LOCK TABLES `announcement_visibility` WRITE;
/*!40000 ALTER TABLE `announcement_visibility` DISABLE KEYS */;
INSERT INTO `announcement_visibility` VALUES (3,1,'blocks',4);
INSERT INTO `announcement_visibility` VALUES (4,1,'blocks',3);
/*!40000 ALTER TABLE `announcement_visibility` ENABLE KEYS */;
UNLOCK TABLES;
