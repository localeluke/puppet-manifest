--
-- Dumping data for table `induction_config_slots`
--

LOCK TABLES `induction_config_slots` WRITE;
/*!40000 ALTER TABLE `induction_config_slots` DISABLE KEYS */;
INSERT INTO `induction_config_slots` VALUES (1,1,'09:00:00','10:00:00',1,0);
INSERT INTO `induction_config_slots` VALUES (2,1,'10:00:00','11:00:00',1,0);
INSERT INTO `induction_config_slots` VALUES (3,1,'11:00:00','12:00:00',1,0);
INSERT INTO `induction_config_slots` VALUES (4,1,'12:00:00','13:00:00',1,0);
INSERT INTO `induction_config_slots` VALUES (5,1,'13:00:00','14:00:00',1,0);
INSERT INTO `induction_config_slots` VALUES (6,1,'14:00:00','15:00:00',1,0);
INSERT INTO `induction_config_slots` VALUES (7,1,'15:00:00','16:00:00',1,0);
INSERT INTO `induction_config_slots` VALUES (8,1,'16:00:00','17:00:00',1,0);
INSERT INTO `induction_config_slots` VALUES (9,1,'09:00:00','10:00:00',2,0);
INSERT INTO `induction_config_slots` VALUES (10,1,'10:00:00','11:00:00',2,0);
INSERT INTO `induction_config_slots` VALUES (11,1,'11:00:00','12:00:00',2,0);
INSERT INTO `induction_config_slots` VALUES (12,1,'12:00:00','13:00:00',2,0);
INSERT INTO `induction_config_slots` VALUES (13,1,'13:00:00','14:00:00',2,0);
INSERT INTO `induction_config_slots` VALUES (14,1,'14:00:00','15:00:00',2,0);
INSERT INTO `induction_config_slots` VALUES (15,1,'15:00:00','16:00:00',2,0);
INSERT INTO `induction_config_slots` VALUES (16,1,'16:00:00','17:00:00',2,0);
INSERT INTO `induction_config_slots` VALUES (17,1,'09:00:00','10:00:00',3,0);
INSERT INTO `induction_config_slots` VALUES (18,1,'10:00:00','11:00:00',3,0);
INSERT INTO `induction_config_slots` VALUES (19,1,'11:00:00','12:00:00',3,0);
INSERT INTO `induction_config_slots` VALUES (20,1,'12:00:00','13:00:00',3,0);
INSERT INTO `induction_config_slots` VALUES (21,1,'13:00:00','14:00:00',3,0);
INSERT INTO `induction_config_slots` VALUES (22,1,'14:00:00','15:00:00',3,0);
INSERT INTO `induction_config_slots` VALUES (23,1,'15:00:00','16:00:00',3,0);
INSERT INTO `induction_config_slots` VALUES (24,1,'16:00:00','17:00:00',3,0);
INSERT INTO `induction_config_slots` VALUES (25,1,'09:00:00','10:00:00',4,0);
INSERT INTO `induction_config_slots` VALUES (26,1,'10:00:00','11:00:00',4,0);
INSERT INTO `induction_config_slots` VALUES (27,1,'11:00:00','12:00:00',4,0);
INSERT INTO `induction_config_slots` VALUES (28,1,'12:00:00','13:00:00',4,0);
INSERT INTO `induction_config_slots` VALUES (29,1,'13:00:00','14:00:00',4,0);
INSERT INTO `induction_config_slots` VALUES (30,1,'14:00:00','15:00:00',4,0);
INSERT INTO `induction_config_slots` VALUES (31,1,'15:00:00','16:00:00',4,0);
INSERT INTO `induction_config_slots` VALUES (32,1,'16:00:00','17:00:00',4,0);
INSERT INTO `induction_config_slots` VALUES (33,1,'09:00:00','10:00:00',5,0);
INSERT INTO `induction_config_slots` VALUES (34,1,'10:00:00','11:00:00',5,0);
INSERT INTO `induction_config_slots` VALUES (35,1,'11:00:00','12:00:00',5,0);
INSERT INTO `induction_config_slots` VALUES (36,1,'12:00:00','13:00:00',5,0);
INSERT INTO `induction_config_slots` VALUES (37,1,'13:00:00','14:00:00',5,0);
INSERT INTO `induction_config_slots` VALUES (38,1,'14:00:00','15:00:00',5,0);
INSERT INTO `induction_config_slots` VALUES (39,1,'15:00:00','16:00:00',5,0);
INSERT INTO `induction_config_slots` VALUES (40,1,'16:00:00','17:00:00',5,0);
/*!40000 ALTER TABLE `induction_config_slots` ENABLE KEYS */;
UNLOCK TABLES;
