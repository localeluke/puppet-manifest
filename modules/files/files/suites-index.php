<?php

$realm = 'Restricted area';

//user => password
$users = array('admin' => 'Locale899#');

if (empty($_SERVER['PHP_AUTH_DIGEST'])) {
    header('HTTP/1.1 401 Unauthorized');
    header('WWW-Authenticate: Digest realm="'.$realm.'",qop="auth",nonce="'.uniqid().'",opaque="'.md5($realm).'"');

    die('Text to send if user hits Cancel button');
}

// analyze the PHP_AUTH_DIGEST variable
if (!($data = http_digest_parse($_SERVER['PHP_AUTH_DIGEST'])) || !isset($users[$data['username']])) {
    die('Wrong Credentials!');
}

// generate the valid response
$A1 = md5($data['username'] . ':' . $realm . ':' . $users[$data['username']]);
$A2 = md5($_SERVER['REQUEST_METHOD'].':'.$data['uri']);
$valid_response = md5($A1.':'.$data['nonce'].':'.$data['nc'].':'.$data['cnonce'].':'.$data['qop'].':'.$A2);

if ($data['response'] != $valid_response) {
    die('Wrong Credentials!');
}

$page = explode('/', $_SERVER['REQUEST_URI']);
$action = (isset($page[1]) ? $page[1] : '');

switch ($action) {
    case 'build':
        createForm();
        break;
    case 'create':
        createSuite($_GET['title'], $_GET['branch'], $_GET['type']);
        break;
    case 'delete':
        deleteSuite($page[2]);
        break;
    default:
        listTestSuites();
        break;
}

// function to parse the http auth header
function http_digest_parse($txt)
{
    // protect against missing data
    $needed_parts = array('nonce'=>1, 'nc'=>1, 'cnonce'=>1, 'qop'=>1, 'username'=>1, 'uri'=>1, 'response'=>1);
    $data = array();
    $keys = implode('|', array_keys($needed_parts));

    preg_match_all('@(' . $keys . ')=(?:([\'"])([^\2]+?)\2|([^\s,]+))@', $txt, $matches, PREG_SET_ORDER);

    foreach ($matches as $m) {
        $data[$m[1]] = $m[3] ? $m[3] : $m[4];
        unset($needed_parts[$m[1]]);
    }

    return $needed_parts ? false : $data;
}

function listTestSuites()
{
    echo '<a href="/build">Create new test suite</a><br/><br/>';
    if ($handle = opendir('../../')) {

        while (false !== ($entry = readdir($handle))) {

            if ($entry != "." && $entry != ".." && substr($entry, 0, 1) != '.' && $entry != 'suites') {
                echo "<a href='https://{$entry}.test.locale.co.uk'>https://{$entry}.test.locale.co.uk</a> - <a href='/delete/{$entry}'>Delete</a><br/>";
            }
        }

        closedir($handle);
    }
}

function deleteSuite($suite)
{
    exec("cd /home/www/puppet/build && /usr/local/bin/vendor/bin/phing command_delete -Dsuite.name={$suite};", $output, $return);
    if($return == 0) {
        echo "Deleted suite {$suite} - <a href='/'>Back to suites</a>";
    } else {
        echo "Something has gone wrong";
        print_r($output);
    }
}

function createSuite($suite, $branch, $type)
{
    putenv('COMPOSER_HOME=/root/.composer');
    exec("cd /home/www/puppet/build && /usr/local/bin/vendor/bin/phing command_create -Dsuite.name={$suite} -Dgit.branch={$branch} -Dsuite.type={$type};", $output, $return);
    if($return == 0) {
        echo "Created suite {$suite} from branch {$branch} - <a href='/'>Back to suites</a>";
    } else {
        echo "Something has gone wrong";
        print_r($output);
    }
}

function createForm()
{
    ?>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.4/jquery.js"></script>
    Title: <input id="title" name="title"/><br/><br/>
    Branch: <input id="branch" name="branch"/><br/><br/>
    Type: <select id="type" name="type"/>
    <option value="residential">Residential</option>
    <option value="mvc">MVC</option>
    <option value="commercial">Commercial</option>
    </select><br/><br/>
    <input id="create" type="button" value="Create"/>
    <script>
        $(document).ready(function () {
            $("#create").live("click",function(){
                if($("#title").val() && $("#branch").val()){
                    window.location.href = "/create/?title=" + $("#title").val() + "&branch=" + encodeURI($("#branch").val()) + "&type=" + $("#type").val();
                } else {
                    alert("You have not filled out all the form data");
                }
            });
        });
    </script>
    <?php
}
