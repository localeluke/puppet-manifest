--
-- Dumping data for table `company_secondary_departments`
--

LOCK TABLES `company_secondary_departments` WRITE;
/*!40000 ALTER TABLE `company_secondary_departments` DISABLE KEYS */;
INSERT INTO `company_secondary_departments` VALUES (1,1,1);
INSERT INTO `company_secondary_departments` VALUES (3,1,2);
INSERT INTO `company_secondary_departments` VALUES (5,1,8);
INSERT INTO `company_secondary_departments` VALUES (60,1,80);
INSERT INTO `company_secondary_departments` VALUES (2,2,2);
INSERT INTO `company_secondary_departments` VALUES (4,2,8);
INSERT INTO `company_secondary_departments` VALUES (57,2,80);
INSERT INTO `company_secondary_departments` VALUES (6,3,8);
INSERT INTO `company_secondary_departments` VALUES (62,3,80);
INSERT INTO `company_secondary_departments` VALUES (7,4,8);
INSERT INTO `company_secondary_departments` VALUES (8,5,8);
INSERT INTO `company_secondary_departments` VALUES (65,5,80);
INSERT INTO `company_secondary_departments` VALUES (9,6,8);
INSERT INTO `company_secondary_departments` VALUES (11,7,8);
INSERT INTO `company_secondary_departments` VALUES (12,8,8);
INSERT INTO `company_secondary_departments` VALUES (13,9,8);
INSERT INTO `company_secondary_departments` VALUES (14,10,8);
INSERT INTO `company_secondary_departments` VALUES (15,11,8);
INSERT INTO `company_secondary_departments` VALUES (16,12,8);
INSERT INTO `company_secondary_departments` VALUES (17,13,8);
INSERT INTO `company_secondary_departments` VALUES (66,13,80);
INSERT INTO `company_secondary_departments` VALUES (10,14,8);
INSERT INTO `company_secondary_departments` VALUES (18,15,8);
INSERT INTO `company_secondary_departments` VALUES (19,16,8);
INSERT INTO `company_secondary_departments` VALUES (20,17,8);
INSERT INTO `company_secondary_departments` VALUES (21,18,8);
INSERT INTO `company_secondary_departments` VALUES (22,19,8);
INSERT INTO `company_secondary_departments` VALUES (56,19,80);
INSERT INTO `company_secondary_departments` VALUES (23,20,8);
INSERT INTO `company_secondary_departments` VALUES (24,21,8);
INSERT INTO `company_secondary_departments` VALUES (58,21,80);
INSERT INTO `company_secondary_departments` VALUES (25,22,8);
INSERT INTO `company_secondary_departments` VALUES (59,22,80);
INSERT INTO `company_secondary_departments` VALUES (26,23,8);
INSERT INTO `company_secondary_departments` VALUES (61,23,80);
INSERT INTO `company_secondary_departments` VALUES (27,24,8);
INSERT INTO `company_secondary_departments` VALUES (28,25,8);
INSERT INTO `company_secondary_departments` VALUES (63,25,80);
INSERT INTO `company_secondary_departments` VALUES (29,26,8);
INSERT INTO `company_secondary_departments` VALUES (64,26,80);
INSERT INTO `company_secondary_departments` VALUES (30,27,8);
INSERT INTO `company_secondary_departments` VALUES (31,28,8);
INSERT INTO `company_secondary_departments` VALUES (32,29,8);
INSERT INTO `company_secondary_departments` VALUES (33,30,8);
INSERT INTO `company_secondary_departments` VALUES (34,31,8);
INSERT INTO `company_secondary_departments` VALUES (35,32,8);
INSERT INTO `company_secondary_departments` VALUES (36,33,8);
INSERT INTO `company_secondary_departments` VALUES (37,34,8);
INSERT INTO `company_secondary_departments` VALUES (38,35,8);
INSERT INTO `company_secondary_departments` VALUES (39,36,8);
INSERT INTO `company_secondary_departments` VALUES (40,37,8);
INSERT INTO `company_secondary_departments` VALUES (41,38,8);
INSERT INTO `company_secondary_departments` VALUES (42,39,8);
INSERT INTO `company_secondary_departments` VALUES (43,40,8);
INSERT INTO `company_secondary_departments` VALUES (44,41,8);
INSERT INTO `company_secondary_departments` VALUES (45,42,8);
INSERT INTO `company_secondary_departments` VALUES (46,43,8);
INSERT INTO `company_secondary_departments` VALUES (67,43,80);
INSERT INTO `company_secondary_departments` VALUES (47,44,8);
INSERT INTO `company_secondary_departments` VALUES (48,45,8);
INSERT INTO `company_secondary_departments` VALUES (49,46,8);
INSERT INTO `company_secondary_departments` VALUES (50,47,8);
INSERT INTO `company_secondary_departments` VALUES (68,47,80);
INSERT INTO `company_secondary_departments` VALUES (51,48,8);
INSERT INTO `company_secondary_departments` VALUES (69,48,80);
INSERT INTO `company_secondary_departments` VALUES (52,49,8);
INSERT INTO `company_secondary_departments` VALUES (70,49,80);
INSERT INTO `company_secondary_departments` VALUES (53,50,8);
INSERT INTO `company_secondary_departments` VALUES (71,50,80);
INSERT INTO `company_secondary_departments` VALUES (54,51,8);
INSERT INTO `company_secondary_departments` VALUES (55,52,8);
INSERT INTO `company_secondary_departments` VALUES (72,52,80);
/*!40000 ALTER TABLE `company_secondary_departments` ENABLE KEYS */;
UNLOCK TABLES;
