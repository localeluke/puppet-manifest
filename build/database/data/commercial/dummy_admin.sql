--
-- Dumping data for table `admin`
--

LOCK TABLES `admin` WRITE;
/*!40000 ALTER TABLE `admin` DISABLE KEYS */;
INSERT INTO `admin` VALUES (1,1,'Master1','Admin','masteradmin','$2y$10$COQpPyzb1gLpnbLMnC0Vg.tfpWWTjHzBJffQGjZYyH4/4WBrDb3iO','masteradmin@gmail.com','1','superadmin',0);
/*!40000 ALTER TABLE `admin` ENABLE KEYS */;
UNLOCK TABLES;
