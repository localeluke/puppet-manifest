--
-- Dumping data for table `html_forms`
--

LOCK TABLES `html_forms` WRITE;
/*!40000 ALTER TABLE `html_forms` DISABLE KEYS */;
INSERT INTO `html_forms` VALUES (3,'Users','Additional fields for LBQ users module','$(\'#access_expiry\').datepicker({ dateFormat: \"dd/mm/yy\" }); showAccessFields(); function showAccessFields(){ if($(\'#access_type\').val()==\"Temporary\"){ $(\'#row_div_access_expiry\').show(); $(\'#row_div_access_reason\').show(); }else{ $(\'#row_div_access_expiry\').hide(); $(\'#row_div_access_reason\').hide(); $(\'#access_expiry\').val(\'\'); } }',0,1,0);
INSERT INTO `html_forms` VALUES (26,'Online Induction Form','Online Induction Form',NULL,1,1,0);
INSERT INTO `html_forms` VALUES (27,'Accidents & Incidents','Accidents & Incidents',NULL,0,1,0);
INSERT INTO `html_forms` VALUES (28,'Cycle Bay Permit Request','Cycle Bay Permit Request',NULL,0,1,0);
INSERT INTO `html_forms` VALUES (29,'Anticipated Waste Production','Anticipated Waste Production',NULL,0,1,0);
INSERT INTO `html_forms` VALUES (30,'Outgoing Courier Demo','Outgoing Courier Demo',NULL,0,1,0);
INSERT INTO `html_forms` VALUES (31,'Waste Collection Bookings','Waste Collection Bookings',NULL,0,1,0);
/*!40000 ALTER TABLE `html_forms` ENABLE KEYS */;
UNLOCK TABLES;
