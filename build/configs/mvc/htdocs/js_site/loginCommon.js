function base_url()
{
	return load_base_url;
}

$(window).load(function(){
	$(document).ready(function(){
		//========== cookie section starts ==========//
			//function to create cookie
			/************************************************/
			function createCookie(name,value,days) {
				if (days) {
					var date = new Date();
					date.setTime(date.getTime()+(days*24*60*60*1000));
					var expires = "; expires="+date.toGMTString();
				}
				else var expires = "";
				document.cookie = name+"="+value+expires+"; path=/";
			}

			//function to read cookie
			function readCookie(name) {
				var nameEQ = name + "=";
				var ca = document.cookie.split(';');
				for(var i=0;i < ca.length;i++) {
					var c = ca[i];
					while (c.charAt(0)==' ') c = c.substring(1,c.length);
					if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
				}
				//return null;
				return false;
			}

			//function to erase cookie
			function eraseCookie(name) {
				createCookie(name,"",-1);
			}
			/************************************************/
			
			
			if(!readCookie("cookie_tower_portal")){
				createCookie("cookie_tower_portal", "set", 3650);
				$(".cookieWrap").slideDown();
				
				setTimeout(function(){
					$(".cookieWrap").slideUp("slow");
				}, 10000);
			}
			
			//cookie close action
			$(".cookieClose").live("click", function(){
				$(".cookieWrap").slideUp("slow");
				return false;
			});
		//========== cookie section ends ==========//
		
		$('.popup-zoom').magnificPopup({
			type: 'inline',

			fixedContentPos: false,
			fixedBgPos: true,

			overflowY: 'auto',

			closeBtnInside: true,
			preloader: false,
			
			midClick: true,
			removalDelay: 300,
			mainClass: 'my-mfp-zoom-in'
		});

		$('.popup-move').magnificPopup({
			type: 'inline',

			fixedContentPos: false,
			fixedBgPos: true,

			overflowY: 'auto',

			closeBtnInside: true,
			preloader: false,
			
			midClick: true,
			removalDelay: 300,
			mainClass: 'my-mfp-slide-bottom'
		});
		
	    $('.ajaxPopup').magnificPopup({
			type: 'ajax',
			overflowY: 'scroll' // as we know that popup content is tall we set scroll overflow by default to avoid jump
		});
		
		//Code for background Image strach
		//$.backstretch(cDocRoot + "proimages/eBuryBg.jpg");
		/*$.backstretch(cDocRoot + "proimages/loginBg.jpg");
		$(".backstretch").append("<div class='backStretchOverlay'></div>");
		$(".backstretch").fadeIn(1200);*/
		
		
		//Responsive grid initialization.
		$(".flurid").flurid();
		
		
		//========== Custome select and multiselect initialization starts ==========//
		var selectObj = $("select").select2({
			"containerCssClass" : function (e) {$(this).attr("data-containerClass")},
			"dropdownCssClass"  : function (e) {$(this).attr("data-containerClass")}
		}).on("select2-open", function() {
			
		}).on("select2-close", function() {
			/*if($(this).hasClass("floatLbl")){
				if($(this).select2("val").length > 0){
					$(this).prev().animate({
						"margin-top"	:	"20px"
					});
					$(this).prev().prev().animate({
						"opacity"		:	"1",
						"margin-top"	:	"0px"
					});
				}
				
				if($(this).select2("val") == $("option:first", $(this)).text()){
					$(this).prev().animate({
						"margin-top"	:	"0px"
					});
					$(this).prev().prev().animate({
						"opacity"		:	"0",
						"margin-top"	:	"5px"
					});
				}
			}*/
		});
		
		selectObj.on("select2-loaded", function (e) {
            initScroll();
        })
		.on("select2-open", function(e) {
			$(".select2-input").each(function(){
				var ulObj = $(this).parent().next();
				var liCount = $("li", ulObj).length;
				if (liCount < 14){
					$(this).parent().addClass("makeHidden");
				}else{
					$(this).parent().removeClass("makeHidden");
				}
			});
		})
		
		function initScroll(){
			$("ul.select2-results").mCustomScrollbar("destroy");
			$("ul.select2-results").mCustomScrollbar({
				scrollButtons:{
					enable:false
				},
				contentTouchScroll: true
			});
		}
		
		$(".forgotPass").on("click", function(){
			$(".loginDiv").slideUp("normal", function(){
				$(".forgotDiv").slideDown("normal");
				checkHeight();
				//$(".loginFooterLogo, .footerLinks").fadeOut("slow");
			});
		});
		
		$(".registerNow").on("click", function(){
			$(".loginDiv").slideUp("normal", function(){
				$(".registerDiv").slideDown("normal");
				checkHeight();
				//$(".loginFooterLogo, .footerLinks").fadeOut("slow");
			});
		});
		
		$(".cancelBtn").on("click", function(){
			$(this).parent().parent().slideUp("normal", function(){
				checkHeight();
				$(".loginDiv").slideDown("normal");
				//$(".loginFooterLogo, .footerLinks").fadeIn("slow");
			});
			return false;
		});
		//========== Custome select and multiselect initialization ends ==========//
		
		
		
		
		
		
		
		
		
		function checkHeight(){
			$(".blackWrapper").css("min-height", $(window).height());
			$(".vAlignLogin table").css("height", $(window).height());
			/*var hi2check;
			if ($('.registerDiv').css('display') == 'none') {
				hi2check = 300;
			}else{
				hi2check = 891;
			}
			
			
			var winHi = $(window).height();
			if (winHi < hi2check){
				$(".loginMaxwrap").addClass("smallHeight");
			}else{
				$(".loginMaxwrap").removeClass("smallHeight");
			}*/
		}
		
		
		
		
		
		
		
		//========== Fancy checkbox and radiobuttons initialization starts ==========//
		fancyCheckboxInit();
		fancyCheckAll();
		//========== Fancy checkbox and radiobuttons initialization ends ==========//
		
		
		
		
		//========== Script for uiPage starts ==========//
		$("input[type='button'], input[type='submit'], .btn-custom, .small-btn, .odd, .even").live("mouseenter", function(){
			$(this).addClass("hover");
		});
		$("input[type='button'], input[type='submit'], .btn-custom, .small-btn, .odd, .even").live("mouseleave", function(){
			$(this).removeClass("hover");
		});
		
			
			
			/*init_floatLbls();
			
			$(".inpt").keyup(function(){
				if($(this).val().length > 0){
					$(this).animate({
						"margin-top"	:	"20px"
					});
					$(this).prev().animate({
						"opacity"		:	"1",
						"margin-top"	:	"0px"
					});
					//$(".transition-All.shadowEffect").val($(this).val());
				}else{
					$(this).animate({
						"margin-top"	:	"0"
					});
					$(this).prev().animate({
						"opacity"	:	"0",
						"margin-top"	:	"5px"
					});
				}
			});*/
			
			
		//========== Script for uiPage starts ==========//
		
		
		//========== functions to be loaded on document load starts ==========//
		setBlackWrapper();
		checkHeight();
		//========== functions to be loaded on document load ends ==========//
		
		$(window).scroll(function (){
			
		});
		
		$(window).resize(function(){
			setBlackWrapper();
			checkHeight();
		});
		
		$(window).bind('orientationchange', function(e) {
			checkHeight();
		});
	});
});

//Code for blackWrapper take full height of window
function setBlackWrapper(){
	//if ($(window).height() > 710){
	if ($(window).height() > 610){
		$(".loginMaxwrap").css("min-height", $(window).height() - 7);
		$(".allWrap").css("min-height", $(window).height() - 25);
	}else{
		//$(".loginMaxwrap").css("min-height", 710);
		$(".loginMaxwrap").css("min-height", 610);
		$(".allWrap").css("min-height", 685);
	}
}

function init_floatLbls(){
	$(".floatLbl").each(function(){
		if( ! $(this).hasClass("select2-container") ) {
			$(this).parent().prepend('<div class="floatLabel txt-upr ltr-spacing1 font12">'+ $(this).attr("placeholder") +'</div>');
		}
	});
}
//========== Fancycheckbox and radio buttons functions starts ==========//
function fancyCheckboxInit(){
	//$(".checkChange").before("<div class='chk'></div>");
	$(".checkChange").each(function(){
		if(!$(this).prev().hasClass("checkContainer"))
		$(this).before("<div class='checkContainer'></div>");
	});
	$('.checkChange').css("opacity", 0);
	
	$('.checkChange').die();
	$('.checkChange').live("click", function(){
		var eleName = $(this).prop("name").split('[')[0];
		if($(this).prop('type')=='radio'){
			$(".radioCont ."+ eleName).removeClass("clicked");
			$(this).prev().toggleClass("clicked");
		}else{
			$(this).prev().toggleClass("clicked");
		}
	});
}

function fancyCheckAll(){
	$('.checkChange').each(function(){
		var eleName = $(this).prop("name").split('[')[0];
		$(this).prev().addClass(eleName);
  
		if ($(this).is(":checked")){
			$(this).prev().addClass("clicked");
		}
	});
}
//========== Fancycheckbox and radio buttons functions ends ==========//

function ajax_popup_init()
{
		$('.ajaxPopup').magnificPopup({
			type: 'ajax',
			overflowY: 'scroll' // as we know that popup content is tall we set scroll overflow by default to avoid jump
		});
		
}