--
-- Dumping data for table `vms_user_visitor`
--

LOCK TABLES `vms_user_visitor` WRITE;
/*!40000 ALTER TABLE `vms_user_visitor` DISABLE KEYS */;
INSERT INTO `vms_user_visitor` VALUES (1,1,'Test','Test','Locale','demo@locale.co.uk','','N');
INSERT INTO `vms_user_visitor` VALUES (2,1,'test','test','test','','','N');
INSERT INTO `vms_user_visitor` VALUES (3,3,'Test','test','','','','N');
INSERT INTO `vms_user_visitor` VALUES (4,1,'test','test','','','','N');
INSERT INTO `vms_user_visitor` VALUES (5,3,'test','test','','','','N');
/*!40000 ALTER TABLE `vms_user_visitor` ENABLE KEYS */;
UNLOCK TABLES;
