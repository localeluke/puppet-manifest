<?php

$autoload['module_version'] = array();
$autoload['module_version']['Helpdesk'] = 2;
$autoload['module_version']['Announcement'] = 2;
$autoload['module_version']['Moving_slot'] = 1;
$autoload['module_version']['ain_core'] = 1;
$autoload['module_version']['Main'] = 1;
$autoload['module_version']['Admin'] = 1;
$autoload['module_version']['Users'] = 1;
$autoload['module_version']['Facility'] = 2;
$autoload['module_version']['Home'] = 3;
$autoload['module_version']['Vms'] = 2;
$autoload['module_version']['Usersdatabase'] = 9;
$autoload['module_version']['Calendar'] = 8;
$autoload['module_version']['Induction'] = 2;
$autoload['module_version']['Offers'] = 10;
$autoload['module_version']['Services'] = 10;

$autoload['tpl_version'] = array();
$autoload['tpl_version']['Announcement'] = 2;
$autoload['tpl_version']['Main'] = 8;
$autoload['tpl_version']['Home'] = 8;
$autoload['tpl_version']['Content'] = 8;
$autoload['tpl_version']['Vms'] = 2;
$autoload['tpl_version']['Helpdesk'] = 2;
$autoload['tpl_version']['Facility'] = 2;
$autoload['tpl_version']['Usersdatabase'] = 9;
$autoload['tpl_version']['Calendar'] = 8;
$autoload['tpl_version']['Induction'] = 2;
$autoload['tpl_version']['Offers'] = 10;
$autoload['tpl_version']['Services'] = 10;
