--
-- Dumping data for table `blocks`
--

LOCK TABLES `blocks` WRITE;
/*!40000 ALTER TABLE `blocks` DISABLE KEYS */;
INSERT INTO `blocks` VALUES (5,0,1,1,'Block A',1,NULL,'SE1 0UE','',NULL,'',NULL,'BA','BA',1,0,'51.504645','-0.101929');
INSERT INTO `blocks` VALUES (6,0,1,1,'The Shard',1,NULL,'SE1 0UE','',NULL,'',NULL,'TS','TS',1,0,'51.504645','-0.101929');
INSERT INTO `blocks` VALUES (7,0,1,1,'Block B',1,NULL,'SE1 0UE','',NULL,'',NULL,'BB','BB',1,0,'51.504645','-0.101929');
/*!40000 ALTER TABLE `blocks` ENABLE KEYS */;
UNLOCK TABLES;
