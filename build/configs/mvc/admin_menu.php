<?php
//Start: Estate Setup ---------------------------------------
$configAdminNavi['Estate Setup']['cls'] 	   = "ES";

// $configAdminNavi['Estate Setup']['module'][1]  = "Regions";
// $configAdminNavi['Estate Setup']['url'][1]     = "regions";

$configAdminNavi['Estate Setup']['module'][2]  = "Blocks";
$configAdminNavi['Estate Setup']['url'][2] 	   = "blocks";

$configAdminNavi['Estate Setup']['module'][3]  = "Companies";
$configAdminNavi['Estate Setup']['url'][3]     = "companies";

// $configAdminNavi['Estate Setup']['module'][4]  = "Departments";
// $configAdminNavi['Estate Setup']['url'][4]     = "departments";

// $configAdminNavi['Estate Setup']['module'][4]  = "Authorized Domains";
// $configAdminNavi['Estate Setup']['url'][4]     = "authdomains";

//End: Estate Setup -----------------------------------------

//Start: Content --------------------------------------------
$configAdminNavi['Content']['cls'] 	   		   = "CT";

$configAdminNavi['Content']['module'][5] 	   = "Navigation Titles";
$configAdminNavi['Content']['url'][5] 		   = "navigation";

$configAdminNavi['Content']['module'][6] 	   = "Documents";
$configAdminNavi['Content']['url'][6]     	   = "documents";

$configAdminNavi['Content']['module'][7] 	   = "Document Categories";
$configAdminNavi['Content']['url'][7] 		   = "documentcategory";

$configAdminNavi['Content']['module'][8] 	   = "Pages";
$configAdminNavi['Content']['url'][8] 		   = "pages";

$configAdminNavi['Content']['module'][9] 	   = "Static Copy";
$configAdminNavi['Content']['url'][9] 		   = "staticcopy";

//End: Content --------------------------------------------

//Start: Users --------------------------------------------
$configAdminNavi['Users']['cls'] 	  		   = "US";

$configAdminNavi['Users']['module'][10] 	   = "User Database";
$configAdminNavi['Users']['url'][10]		   = "users";

$configAdminNavi['Users']['module'][11] 	   = "User Groups";
$configAdminNavi['Users']['url'][11] 		   = "usergroups";

$configAdminNavi['Users']['module'][12] 	   = "Admin User Database";
$configAdminNavi['Users']['url'][12] 		   = "adminusers";

$configAdminNavi['Users']['module'][13] 	   = "User Stats";
$configAdminNavi['Users']['url'][13] 		   = "userstats";

//$configAdminNavi['Users']['module'][14]        = "Pre register users";
//$configAdminNavi['Users']['url'][14] 	       = "preregister";

//$configAdminNavi['Users']['module'][15] 	   = "User Approval";
//$configAdminNavi['Users']['url'][15] 		   = "userapproval";

$configAdminNavi['Users']['module'][14] 	= "Archived Users";
$configAdminNavi['Users']['url'][14] 		= "archivedusers";

$configAdminNavi['Users']['module'][15] 	= "New Starters CSV upload";
$configAdminNavi['Users']['url'][15] 		= "userstarter";

//End: Users ----------------------------------------------

//Start: Functions ----------------------------------------
$configAdminNavi['Functions']['module'][12] 	= "Services Category";
$configAdminNavi['Functions']['url'][12] 		= "servicecategory";

$configAdminNavi['Functions']['module'][13] 	= "Services";
$configAdminNavi['Functions']['url'][13] 		= "service";

$configAdminNavi['Functions']['module'][14] 	= "Induction Type";
$configAdminNavi['Functions']['url'][14] 		= "inductiontype";

$configAdminNavi['Functions']['module'][15] 	= "Online Induction";
$configAdminNavi['Functions']['url'][15] 		= "inductiononline";

$configAdminNavi['Functions']['module'][16] 	= "Module Permissions";
$configAdminNavi['Functions']['url'][16] 		= "modules";

$configAdminNavi['Functions']['module'][17] 	= "Facility Bookings";
$configAdminNavi['Functions']['url'][17] 		= "facility";

$configAdminNavi['Functions']['module'][18] 	= "Lists";
$configAdminNavi['Functions']['url'][18] 		= "lists";

$configAdminNavi['Functions']['module'][19] 	= "Announcement Templates";
$configAdminNavi['Functions']['url'][19] 		= "announcement";

$configAdminNavi['Functions']['module'][20] 	= "Forms";
$configAdminNavi['Functions']['url'][20] 		= "forms";

$configAdminNavi['Functions']['module'][21] 	= "Email Copies";
$configAdminNavi['Functions']['url'][21] 		= "emailcopy";

$configAdminNavi['Functions']['module'][22] 	= "Induction Booking";
$configAdminNavi['Functions']['url'][22] 		= "induction";

$configAdminNavi['Functions']['module'][23] 	= "Induction Departments Setup";
$configAdminNavi['Functions']['url'][23] 		= "inductiondepartmentssetup";

$configAdminNavi['Functions']['module'][24] 	= "Offers Category";
$configAdminNavi['Functions']['url'][24] 		= "promotioncategory";

$configAdminNavi['Functions']['module'][25] 	= "Offers and Promotions";
$configAdminNavi['Functions']['url'][25] 		= "promotion";

$configAdminNavi['Functions']['module'][26] 	= "Shortcut Icons";
$configAdminNavi['Functions']['url'][26] 		= "shortcuticons";

$configAdminNavi['Functions']['module'][28] 	= "Announcement Background Uploader";
$configAdminNavi['Functions']['url'][28] 		= "Announcementuploader";

$configAdminNavi['Functions']['module'][29] 	= "Helpdesk Departments";
$configAdminNavi['Functions']['url'][29] 		= "helpdesk/departments";

$configAdminNavi['Functions']['module'][30] 	= "Helpdesk Priorities";
$configAdminNavi['Functions']['url'][30] 		= "helpdesk/priorities";

$configAdminNavi['Functions']['module'][31] 	= "Calendar";
$configAdminNavi['Functions']['url'][31] 		= "calendar";

$configAdminNavi['Functions']['module'][32]  	= "Google Analytics";
$configAdminNavi['Functions']['url'][32]   		= "ganalytics";

$configAdminNavi['Functions']['module'][33] 	= "VMS Pass ID";
$configAdminNavi['Functions']['url'][33] 		= "vmspass";

$configAdminNavi['Functions']['module'][34] 	= "Form Log";
$configAdminNavi['Functions']['url'][34] 		= "formlog";


$configAdminNavi['Functions']['module'][35] 	= "Forums";
$configAdminNavi['Functions']['url'][35] 		= "forum";

//The following lines have been automatically generated
$configAdminNavi['Functions']['module'][36] = "Floors";
$configAdminNavi['Functions']['url'][36] = "Floors";

//The following lines have been automatically generated
$configAdminNavi['Functions']['module'][37] = "Secondary Departments";
$configAdminNavi['Functions']['url'][37] = "Secondarydepartments";

//End: Functions ----------------------------------------

//The following lines have been automatically generated
$configAdminNavi['SiteConfiguration']['module'][38] = "Mobile App Configuration";
$configAdminNavi['SiteConfiguration']['url'][38] = "siteconfiguration/app_config";

