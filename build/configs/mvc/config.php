<?php

$_SERVER['APPLICATION_DB'] = 'local';

if (! defined('DS')) {
    define('DS', DIRECTORY_SEPARATOR);
}

define("SSL", 1);
$_config['http'] = (defined('SSL') && SSL == 1) ? "https://" : "http://";

switch ($_SERVER['APPLICATION_DB']) {
    case 'local':
        error_reporting(0);
        ini_set('display_errors', 'Off');

        $_config['db'][0] = array(
            'db_host' => 'localhost',
            'db_username' => '${suite.name}',
            'db_password' => 'Locale899#',
            'db_name' => '${suite.name}_test',
            'db_software' => 'mysql',
            'db_logs' => array('select' => false, 'insert' => true, 'update' => true, 'delete' => true)
        );

        $_config['doc_root'] = '/';
        $_config['web_folder'] = 'htdocs';

        $_config['http_host'] = $_config['http'] . $_SERVER['HTTP_HOST'];
        $_config['admin_base_url'] = $_config['http'] . $_SERVER['HTTP_HOST'] . $_config['doc_root'] . 'admin/';
        $_config['web_base_url'] = $_config['http'] . $_SERVER['HTTP_HOST'] . $_config['doc_root'];

        $_config['mailtrap'] = TRUE;
        $_config['mailtrap_user'] = '403de4bd3b61f7';
        $_config['mailtrap_pass'] = '09efc685237d79';
        break;
}

$_config['admin_sess'] = 'LBQ_AUTH_ADMIN';
$_config['web_sess'] = 'LBQ_AUTH';

$_config['site_type'] = 'COMMERCIAL';

$_config['project_name'] = 'Locale';
$_config['admin_cron_email'] = 'localedeveloperemail@gmail.com';

$_config['email_debug'] = array(
    'from_name' => 'Locale Developers',
    'from_email' => 'no-reply@' . $_SERVER['HTTP_HOST'],
    'to' => 'localedevelopers@localhost'
);

//bcc_mail should be comma seperated
//$_config['bcc_mail'] = "localedeveloperemail@gmail.com";
$_config['cc_mail'] = "";
$_config['email_from'] = "localedeveloperemail@gmail.com";

//$_config['email_from']	= array('localedevelopmentemail@gmail.com', 'Locale');
$_config['email_bounce'] = 'localedeveloperemail@gmail.com';

$_config['u_interfaces'][1] = 'admin';
$_config['u_interfaces'][2] = 'superadmin';


$_config['admin_folder'] = 'admin';
$_config['root_path'] = str_replace(DS . 'config' . DS . 'config.php', '', __FILE__);

$_config['server_path'] = $_config['root_path'] . DS . $_config['web_folder'];
$_config['system_path'] = $_config['root_path'] . DS . 'private';

$BINARY_DATA_TEMP = $_config['server_path'] . DS . 'UserFiles/temp/';
$BINARY_DATA = $_config['server_path'] . DS . 'UserFiles/binary_data';

define('GLOBAL_ROOT_PATH', $_config['root_path']);
define('GLOBAL_SYS_PATH', $_config['system_path']);

define('_SESSION_WEB', 'web_sess_locale');
define('_SESSION_ADMIN', 'admin_sess_locale');

define('GLOBAL_DOC_ROOT', $_config['doc_root']);
define('GLOBAL_DOC_ROOT_ADMIN', $_config['doc_root'] . $_config['admin_folder'] . '/');
define('GLOBAL_SERVER_PATH', $_config['server_path']);
define('GLOBAL_ADMIN_PATH', $_config['server_path'] . DS . $_config['admin_folder']);
define('GLOBAL_MAIN_DOC_ROOT', $_config['doc_root']); # used for ckeditor

define('_HTTP_HOST', $_config['http_host']);

define('_FROM_ADMIN_EML', 'no-reply@locale.co.uk');

define('_ENABLE_MEMCACHE', false);    # enable/disable memcache
define('_MEMCACHE_KEY', '');
define('_SALT_KEY', '5RJr5KcxZ7jSh4OkiLWOVSnGJyYZVA');
define('_SALT_KEY_PWD', 'SxpWS3kGLbk4744NnJxpB9Hz3sYkS6bg');

define('_IMG_TMPPATH', $_config['root_path'] . DS . 'tmp' . DS);
define('_IMG_CACHEPATH', $_config['root_path'] . DS . 'tmp' . DS . 'image_cache' . DS);
define('_IMG_UPLOADPATH', $_config['server_path'] . DS . 'uploads/');
define('_FILE_UPLOADPATH', $_config['server_path'] . DS . 'uploads/');

define('DEFAULT_TIMEZONE', 'Europe/London');

date_default_timezone_set(DEFAULT_TIMEZONE);

