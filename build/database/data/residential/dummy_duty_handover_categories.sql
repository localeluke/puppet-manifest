--
-- Dumping data for table `duty_handover_categories`
--

LOCK TABLES `duty_handover_categories` WRITE;
/*!40000 ALTER TABLE `duty_handover_categories` DISABLE KEYS */;
INSERT INTO `duty_handover_categories` VALUES (1,'Facilities',1,0);
INSERT INTO `duty_handover_categories` VALUES (2,'FYI',1,0);
INSERT INTO `duty_handover_categories` VALUES (3,'Maintenance',1,0);
INSERT INTO `duty_handover_categories` VALUES (4,'Other',1,0);
INSERT INTO `duty_handover_categories` VALUES (5,'Security',1,0);
INSERT INTO `duty_handover_categories` VALUES (6,'Visitors',1,0);
/*!40000 ALTER TABLE `duty_handover_categories` ENABLE KEYS */;
UNLOCK TABLES;
