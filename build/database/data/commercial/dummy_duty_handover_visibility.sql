--
-- Dumping data for table `duty_handover_visibility`
--

LOCK TABLES `duty_handover_visibility` WRITE;
/*!40000 ALTER TABLE `duty_handover_visibility` DISABLE KEYS */;
INSERT INTO `duty_handover_visibility` VALUES (1,1,'blocks',1);
INSERT INTO `duty_handover_visibility` VALUES (2,1,'blocks',2);
INSERT INTO `duty_handover_visibility` VALUES (3,2,'blocks',1);
INSERT INTO `duty_handover_visibility` VALUES (4,2,'blocks',2);
INSERT INTO `duty_handover_visibility` VALUES (5,3,'blocks',1);
INSERT INTO `duty_handover_visibility` VALUES (6,3,'blocks',2);
INSERT INTO `duty_handover_visibility` VALUES (7,4,'blocks',1);
INSERT INTO `duty_handover_visibility` VALUES (8,4,'blocks',2);
/*!40000 ALTER TABLE `duty_handover_visibility` ENABLE KEYS */;
UNLOCK TABLES;
