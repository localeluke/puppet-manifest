--
-- Dumping data for table `announcement_bgimage`
--

LOCK TABLES `announcement_bgimage` WRITE;
/*!40000 ALTER TABLE `announcement_bgimage` DISABLE KEYS */;
INSERT INTO `announcement_bgimage` VALUES (16,'Hutong Restaurant',0);
INSERT INTO `announcement_bgimage` VALUES (17,'Lift Maintenance',0);
INSERT INTO `announcement_bgimage` VALUES (18,'Occupier Office Space',0);
INSERT INTO `announcement_bgimage` VALUES (19,'Reception Area',0);
INSERT INTO `announcement_bgimage` VALUES (20,'Restaurant and Bar',0);
INSERT INTO `announcement_bgimage` VALUES (21,'Shangri-La Lobby',0);
INSERT INTO `announcement_bgimage` VALUES (22,'The Shard at dusk',0);
INSERT INTO `announcement_bgimage` VALUES (23,'The Shard Night Skyline ',0);
INSERT INTO `announcement_bgimage` VALUES (24,'The Shard with river view',0);
INSERT INTO `announcement_bgimage` VALUES (25,'Travel and transport',0);
INSERT INTO `announcement_bgimage` VALUES (26,'Window Cleaning',0);
/*!40000 ALTER TABLE `announcement_bgimage` ENABLE KEYS */;
UNLOCK TABLES;
