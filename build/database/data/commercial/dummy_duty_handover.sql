--
-- Dumping data for table `duty_handover`
--

LOCK TABLES `duty_handover` WRITE;
/*!40000 ALTER TABLE `duty_handover` DISABLE KEYS */;
INSERT INTO `duty_handover` VALUES (1,'test',2,NULL,'no_action',0,0,0,NULL,NULL,1,'2019-04-04 15:47:04',0,'0000-00-00 00:00:00',0,0);
INSERT INTO `duty_handover` VALUES (2,'test',1,NULL,'no_action',1,0,0,NULL,NULL,1,'2019-04-04 15:51:15',0,'0000-00-00 00:00:00',0,0);
INSERT INTO `duty_handover` VALUES (3,'test',2,NULL,'no_action',0,0,0,NULL,NULL,1,'2019-04-04 15:51:25',0,'0000-00-00 00:00:00',0,0);
INSERT INTO `duty_handover` VALUES (4,'test',1,NULL,'action_req',6,0,0,NULL,NULL,1,'2019-04-04 15:52:00',0,'0000-00-00 00:00:00',0,0);
/*!40000 ALTER TABLE `duty_handover` ENABLE KEYS */;
UNLOCK TABLES;
