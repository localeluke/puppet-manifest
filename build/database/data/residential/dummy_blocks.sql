--
-- Dumping data for table `blocks`
--

LOCK TABLES `blocks` WRITE;
/*!40000 ALTER TABLE `blocks` DISABLE KEYS */;
INSERT INTO `blocks` VALUES (1,0,1,1,'Park End Gardens',NULL,'OX16 1AR','',NULL,'',NULL,'',1,1,1,'PEG','','');
INSERT INTO `blocks` VALUES (2,0,1,1,'Blue Boar Gardens',NULL,'OX161AR','',NULL,'',NULL,'',1,1,1,'BBG','','');
INSERT INTO `blocks` VALUES (3,0,1,1,'Park End Gardens',NULL,'OX1 4EH','',NULL,'',NULL,'PEG',1,0,2,'PEG','51.751781','-1.255546');
INSERT INTO `blocks` VALUES (4,0,1,1,'Blue Boar Gardens',NULL,'OX1 4EH','',NULL,'',NULL,'BBG',1,0,2,'BBG','51.751781','-1.255546');
INSERT INTO `blocks` VALUES (5,0,1,1,'Block A',NULL,'OX1 4EH','',NULL,'',NULL,'BA',1,0,3,'BA','51.751781','-1.255546');
/*!40000 ALTER TABLE `blocks` ENABLE KEYS */;
UNLOCK TABLES;
