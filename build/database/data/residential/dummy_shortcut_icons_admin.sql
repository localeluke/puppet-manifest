--
-- Dumping data for table `shortcut_icons_admin`
--

LOCK TABLES `shortcut_icons_admin` WRITE;
/*!40000 ALTER TABLE `shortcut_icons_admin` DISABLE KEYS */;
INSERT INTO `shortcut_icons_admin` VALUES (1,'Helpdesk','fa-search','#helpdesk','global',1,0,0);
INSERT INTO `shortcut_icons_admin` VALUES (2,'Announcements','fa-envelope-o','#announcement','global',1,0,0);
INSERT INTO `shortcut_icons_admin` VALUES (3,'Calendar','fa-calendar','#calendar','global',1,0,0);
INSERT INTO `shortcut_icons_admin` VALUES (4,'Loading Bay','fa-truck','#facility/intro/30','global',1,0,0);
INSERT INTO `shortcut_icons_admin` VALUES (5,'Visitor Management','fa-user','#vms','global',1,0,0);
/*!40000 ALTER TABLE `shortcut_icons_admin` ENABLE KEYS */;
UNLOCK TABLES;
