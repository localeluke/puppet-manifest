--
-- Dumping data for table `html_lists`
--

LOCK TABLES `html_lists` WRITE;
/*!40000 ALTER TABLE `html_lists` DISABLE KEYS */;
INSERT INTO `html_lists` VALUES (1,'VMS Entrances','vms_entrance_types',0,0);
INSERT INTO `html_lists` VALUES (2,'Title List','1',0,0);
INSERT INTO `html_lists` VALUES (3,'Access to demise','1',0,1);
INSERT INTO `html_lists` VALUES (4,'Key Holding Key Types','key_holding_key_types',0,1);
INSERT INTO `html_lists` VALUES (5,'1','1',0,1);
INSERT INTO `html_lists` VALUES (6,'User Starter Form - Book VMS Starters','us_book_vms_starters',1,0);
INSERT INTO `html_lists` VALUES (7,'User Starter Form - Type of access','us_type_access',1,0);
INSERT INTO `html_lists` VALUES (26,'Online Induction Form','online_induction_form',1,0);
INSERT INTO `html_lists` VALUES (27,'YES/NO','YN',1,0);
INSERT INTO `html_lists` VALUES (28,'helpdesk_fault_list','helpdesk_fault_list',1,0);
INSERT INTO `html_lists` VALUES (29,'Area of Incident','area-of-incident',1,0);
INSERT INTO `html_lists` VALUES (30,'Accident Category','Accident Category',1,0);
INSERT INTO `html_lists` VALUES (31,'Waste Collections','waste-collections',1,0);
INSERT INTO `html_lists` VALUES (32,'key holding key types','key_holding_key_types',1,0);
INSERT INTO `html_lists` VALUES (33,'Access to demise ','reasons_access_to_demise',1,0);
INSERT INTO `html_lists` VALUES (34,'User Starter Form - Type of access','us_type_access',1,0);
/*!40000 ALTER TABLE `html_lists` ENABLE KEYS */;
UNLOCK TABLES;
