--
-- Dumping data for table `groups_restrictions`
--

LOCK TABLES `groups_restrictions` WRITE;
/*!40000 ALTER TABLE `groups_restrictions` DISABLE KEYS */;
INSERT INTO `groups_restrictions` VALUES (2,3,3,1,2,3,'Auto-generated on group creation','2019-07-15 16:14:14');
INSERT INTO `groups_restrictions` VALUES (3,2,2,1,1,3,'','2019-07-16 07:49:39');
INSERT INTO `groups_restrictions` VALUES (6,1,1,1,3,1,'','2019-07-16 09:09:32');
INSERT INTO `groups_restrictions` VALUES (7,4,4,1,3,1,'','2019-07-16 09:09:40');
/*!40000 ALTER TABLE `groups_restrictions` ENABLE KEYS */;
UNLOCK TABLES;
