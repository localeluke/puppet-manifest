--
-- Dumping data for table `content_permissions`
--

LOCK TABLES `content_permissions` WRITE;
/*!40000 ALTER TABLE `content_permissions` DISABLE KEYS */;
INSERT INTO `content_permissions` VALUES (31,20,'user_groups',5);
INSERT INTO `content_permissions` VALUES (32,20,'user_groups',8);
INSERT INTO `content_permissions` VALUES (33,20,'user_groups',9);
INSERT INTO `content_permissions` VALUES (40,24,'user_groups',3);
INSERT INTO `content_permissions` VALUES (41,24,'user_groups',8);
INSERT INTO `content_permissions` VALUES (42,24,'user_groups',9);
INSERT INTO `content_permissions` VALUES (43,18,'user_groups',5);
INSERT INTO `content_permissions` VALUES (44,18,'user_groups',8);
INSERT INTO `content_permissions` VALUES (45,18,'user_groups',9);
INSERT INTO `content_permissions` VALUES (52,19,'user_groups',5);
INSERT INTO `content_permissions` VALUES (53,19,'user_groups',8);
INSERT INTO `content_permissions` VALUES (54,19,'user_groups',9);
INSERT INTO `content_permissions` VALUES (61,17,'user_groups',5);
INSERT INTO `content_permissions` VALUES (62,17,'user_groups',8);
INSERT INTO `content_permissions` VALUES (63,17,'user_groups',9);
INSERT INTO `content_permissions` VALUES (127,72,'user_groups',30);
INSERT INTO `content_permissions` VALUES (128,72,'user_groups',6);
INSERT INTO `content_permissions` VALUES (129,72,'user_groups',8);
INSERT INTO `content_permissions` VALUES (130,72,'user_groups',68);
INSERT INTO `content_permissions` VALUES (131,72,'user_groups',32);
INSERT INTO `content_permissions` VALUES (132,101,'user_groups',8);
INSERT INTO `content_permissions` VALUES (133,101,'user_groups',56);
INSERT INTO `content_permissions` VALUES (134,101,'user_groups',58);
INSERT INTO `content_permissions` VALUES (135,101,'user_groups',62);
INSERT INTO `content_permissions` VALUES (136,101,'user_groups',64);
INSERT INTO `content_permissions` VALUES (137,101,'user_groups',66);
INSERT INTO `content_permissions` VALUES (138,101,'user_groups',68);
INSERT INTO `content_permissions` VALUES (139,101,'user_groups',53);
/*!40000 ALTER TABLE `content_permissions` ENABLE KEYS */;
UNLOCK TABLES;
