--
-- Dumping data for table `groups_restrictions_types`
--

LOCK TABLES `groups_restrictions_types` WRITE;
/*!40000 ALTER TABLE `groups_restrictions_types` DISABLE KEYS */;
INSERT INTO `groups_restrictions_types` VALUES (1,'archived','2019-07-15 14:42:12');
INSERT INTO `groups_restrictions_types` VALUES (2,'awaiting approval','2019-07-15 14:42:12');
INSERT INTO `groups_restrictions_types` VALUES (3,'deleted','2019-07-15 14:42:12');
/*!40000 ALTER TABLE `groups_restrictions_types` ENABLE KEYS */;
UNLOCK TABLES;
