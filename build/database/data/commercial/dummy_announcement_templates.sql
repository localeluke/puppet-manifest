--
-- Dumping data for table `announcement_templates`
--

LOCK TABLES `announcement_templates` WRITE;
/*!40000 ALTER TABLE `announcement_templates` DISABLE KEYS */;
INSERT INTO `announcement_templates` VALUES (1,'Fire Alarm Test','Fire Alarm Test','<p>Please be aware that there will be a fire alarm test today at 4 pm. It will sound for up to 2 minutes, but there is no need to leave the building. Apologies for any inconvenience caused.</p>\r\n',1,0);
/*!40000 ALTER TABLE `announcement_templates` ENABLE KEYS */;
UNLOCK TABLES;
