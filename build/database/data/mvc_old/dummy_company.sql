--
-- Dumping data for table `company`
--

LOCK TABLES `company` WRITE;
/*!40000 ALTER TABLE `company` DISABLE KEYS */;
INSERT INTO `company` VALUES (1,'Aljazeera','ALJ',1);
INSERT INTO `company` VALUES (2,'Aqua','AQU',1);
INSERT INTO `company` VALUES (4,'Duff & Phelps','DUF',1);
INSERT INTO `company` VALUES (5,'Gem Construct','GEM',1);
INSERT INTO `company` VALUES (6,'Good People','GOO',1);
INSERT INTO `company` VALUES (8,'LBQ','LBQ',1);
INSERT INTO `company` VALUES (37,'Hutong','HUT',1);
INSERT INTO `company` VALUES (10,'Mace','MAC',1);
INSERT INTO `company` VALUES (11,'NEWS','NEW',0);
INSERT INTO `company` VALUES (12,'Oblix','OBL',1);
INSERT INTO `company` VALUES (16,'Real Estate Management (UK) Limited','REM',1);
INSERT INTO `company` VALUES (18,'Sellar','SEL',1);
INSERT INTO `company` VALUES (19,'Shangri-La','SHA',1);
INSERT INTO `company` VALUES (22,'TVFTS','TVF',1);
INSERT INTO `company` VALUES (25,'Modus','MOD',1);
INSERT INTO `company` VALUES (27,'Foresight Group','FSGP',1);
INSERT INTO `company` VALUES (28,'Mathys and Squire','MAS',1);
INSERT INTO `company` VALUES (30,'South Hook Gas','SUHG',1);
INSERT INTO `company` VALUES (31,'HCA','HCA',1);
INSERT INTO `company` VALUES (32,'Arma Partners LLP','APLLP',1);
INSERT INTO `company` VALUES (35,'REM Construction','REMCON',1);
INSERT INTO `company` VALUES (39,'Campari','CAM',1);
INSERT INTO `company` VALUES (41,'Warwick Business School','WBS',1);
INSERT INTO `company` VALUES (43,'Structure Tone','STR',1);
INSERT INTO `company` VALUES (44,'McCue Crafted Fit','MCF',1);
INSERT INTO `company` VALUES (45,'Collins','COL',1);
INSERT INTO `company` VALUES (46,'The Office Group','TOG',1);
INSERT INTO `company` VALUES (47,'Tiffany & Co','TIFF',1);
INSERT INTO `company` VALUES (48,'Arcapita','ARC',1);
INSERT INTO `company` VALUES (49,'Global Water Development','GWD',1);
INSERT INTO `company` VALUES (51,'Paragon','PAR',1);
/*!40000 ALTER TABLE `company` ENABLE KEYS */;
UNLOCK TABLES;
