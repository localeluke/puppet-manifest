--
-- Dumping data for table `induction_type_setup`
--

LOCK TABLES `induction_type_setup` WRITE;
/*!40000 ALTER TABLE `induction_type_setup` DISABLE KEYS */;
INSERT INTO `induction_type_setup` VALUES (1,'Config 1',0,1);
INSERT INTO `induction_type_setup` VALUES (2,'test',0,1);
INSERT INTO `induction_type_setup` VALUES (3,'Config 1',0,0);
INSERT INTO `induction_type_setup` VALUES (4,'Online Induction Demo',0,0);
INSERT INTO `induction_type_setup` VALUES (5,'Online Induction Only',1,0);
INSERT INTO `induction_type_setup` VALUES (6,'Online & Personal',1,0);
/*!40000 ALTER TABLE `induction_type_setup` ENABLE KEYS */;
UNLOCK TABLES;
