--
-- Dumping data for table `promotion_category`
--

LOCK TABLES `promotion_category` WRITE;
/*!40000 ALTER TABLE `promotion_category` DISABLE KEYS */;
INSERT INTO `promotion_category` VALUES (13,'Events','',1,0,0);
INSERT INTO `promotion_category` VALUES (14,'Food & Drink','',1,0,0);
INSERT INTO `promotion_category` VALUES (15,'Health & Fitness','',1,0,0);
INSERT INTO `promotion_category` VALUES (16,'Lifestyle','',1,0,0);
INSERT INTO `promotion_category` VALUES (17,'Launches','',1,0,13);
INSERT INTO `promotion_category` VALUES (18,'Shard Benefits','',1,0,0);
INSERT INTO `promotion_category` VALUES (19,'Travel and transport','',1,0,0);
/*!40000 ALTER TABLE `promotion_category` ENABLE KEYS */;
UNLOCK TABLES;
