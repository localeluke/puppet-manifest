--
-- Dumping data for table `admin_calendar_config`
--

LOCK TABLES `admin_calendar_config` WRITE;
/*!40000 ALTER TABLE `admin_calendar_config` DISABLE KEYS */;
INSERT INTO `admin_calendar_config` VALUES (1,0,'Building Event','#42A1FF',0,1,0);
INSERT INTO `admin_calendar_config` VALUES (2,0,'Maintenance','#FF6242',0,1,0);
INSERT INTO `admin_calendar_config` VALUES (3,1,'Announcements',NULL,1,1,0);
INSERT INTO `admin_calendar_config` VALUES (4,22,'Duty Handover',NULL,1,1,0);
INSERT INTO `admin_calendar_config` VALUES (5,27,'Loading Bay',NULL,1,1,0);
/*!40000 ALTER TABLE `admin_calendar_config` ENABLE KEYS */;
UNLOCK TABLES;
