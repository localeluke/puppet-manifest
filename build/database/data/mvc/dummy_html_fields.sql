--
-- Dumping data for table `html_fields`
--

LOCK TABLES `html_fields` WRITE;
/*!40000 ALTER TABLE `html_fields` DISABLE KEYS */;
INSERT INTO `html_fields` VALUES (12,'hidden','Induction','induction',NULL,'none',6,'',3,NULL,NULL,0,NULL);
INSERT INTO `html_fields` VALUES (13,'hidden','CardaxID','cardax_id',NULL,'none',5,'',3,NULL,NULL,0,NULL);
INSERT INTO `html_fields` VALUES (14,'text','Reason for temporary access','access_reason',NULL,'none',3,'',3,NULL,NULL,0,NULL);
INSERT INTO `html_fields` VALUES (15,'dropdown','Is your new starter permitted to book visitors to site?','vms_permit',6,'none',4,'',3,NULL,NULL,0,NULL);
INSERT INTO `html_fields` VALUES (16,'text','Expiry Date','access_expiry',NULL,'required',2,'',3,NULL,NULL,0,NULL);
INSERT INTO `html_fields` VALUES (17,'dropdown','Type of Access','access_type',7,'required',1,'',3,NULL,'onchange=\"showAccessFields();\"',0,NULL);
INSERT INTO `html_fields` VALUES (121,'radio','I confirm that I have watched and understood the induction film for The Shard.',NULL,0,'required',NULL,'',26,0,NULL,0,202);
INSERT INTO `html_fields` VALUES (122,'dropdown','Category',NULL,30,'required',NULL,'',27,0,NULL,0,0);
INSERT INTO `html_fields` VALUES (123,'dropdown','Area of Incident',NULL,29,'required',NULL,'',27,0,NULL,0,0);
INSERT INTO `html_fields` VALUES (124,'text','Location of Incident',NULL,0,'required',NULL,'',27,0,NULL,0,0);
INSERT INTO `html_fields` VALUES (125,'textarea','Details of any Injuries Sustained',NULL,0,'none',NULL,'',27,0,NULL,0,0);
INSERT INTO `html_fields` VALUES (126,'datepicker','Date of Incident',NULL,0,'required',NULL,'',27,0,NULL,0,0);
INSERT INTO `html_fields` VALUES (127,'timepicker','Time of Incident',NULL,0,'required',NULL,'',27,0,NULL,0,0);
INSERT INTO `html_fields` VALUES (128,'text','Bike Model',NULL,0,'required',NULL,'',28,0,NULL,0,0);
INSERT INTO `html_fields` VALUES (129,'text','Bicycle Colour',NULL,0,'required',NULL,'',28,0,NULL,0,0);
INSERT INTO `html_fields` VALUES (130,'text','Serial Number',NULL,0,'none',NULL,'',28,0,NULL,0,0);
INSERT INTO `html_fields` VALUES (131,'text','Date of Submission',NULL,0,'required',NULL,'',27,0,NULL,0,0);
INSERT INTO `html_fields` VALUES (132,'text','Level',NULL,0,'required',NULL,'',27,0,NULL,0,0);
INSERT INTO `html_fields` VALUES (133,'radio','Anticipated Waste Stream',NULL,0,'required',NULL,'',27,0,NULL,0,202);
INSERT INTO `html_fields` VALUES (134,'textarea','Further Details',NULL,0,'none',NULL,'',27,0,NULL,0,0);
INSERT INTO `html_fields` VALUES (135,'datepicker','Date of Submission',NULL,0,'required',NULL,'',29,0,NULL,0,0);
INSERT INTO `html_fields` VALUES (136,'text','Level',NULL,0,'required',NULL,'',29,0,NULL,0,0);
INSERT INTO `html_fields` VALUES (137,'textarea','Further Details',NULL,0,'none',NULL,'',29,0,NULL,0,0);
INSERT INTO `html_fields` VALUES (138,'topaz','Signature',NULL,0,'none',NULL,'',29,0,NULL,0,0);
INSERT INTO `html_fields` VALUES (139,'text','Recipient\'s Name',NULL,0,'required',NULL,'',30,0,NULL,0,0);
INSERT INTO `html_fields` VALUES (140,'text','Recipient\'s Company',NULL,0,'required',NULL,'',30,0,NULL,0,0);
INSERT INTO `html_fields` VALUES (141,'textarea','Recipient\'s Address',NULL,0,'required',NULL,'',30,0,NULL,0,0);
INSERT INTO `html_fields` VALUES (142,'text','Recipient\'s Postcode',NULL,0,'required',NULL,'',30,0,NULL,0,0);
INSERT INTO `html_fields` VALUES (143,'text','Recipient\'s Courier',NULL,0,'required',NULL,'',30,0,NULL,0,0);
INSERT INTO `html_fields` VALUES (144,'datepicker','Date of Collection',NULL,0,'required',NULL,'',31,0,NULL,0,0);
INSERT INTO `html_fields` VALUES (145,'timepicker','Time of Collection',NULL,0,'required',NULL,'',31,0,NULL,0,0);
INSERT INTO `html_fields` VALUES (146,'textarea','Description',NULL,0,'required',NULL,'',31,0,NULL,0,0);
INSERT INTO `html_fields` VALUES (147,'dropdown','Waste Type',NULL,31,'required',NULL,'',31,0,NULL,0,0);
INSERT INTO `html_fields` VALUES (148,'text','Collect from Level',NULL,0,'required',NULL,'',31,0,NULL,0,0);
INSERT INTO `html_fields` VALUES (149,'text','Location on Level',NULL,0,'required',NULL,'',31,0,NULL,0,0);
/*!40000 ALTER TABLE `html_fields` ENABLE KEYS */;
UNLOCK TABLES;
