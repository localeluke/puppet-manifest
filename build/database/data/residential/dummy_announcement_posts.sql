--
-- Dumping data for table `announcement_posts`
--

LOCK TABLES `announcement_posts` WRITE;
/*!40000 ALTER TABLE `announcement_posts` DISABLE KEYS */;
INSERT INTO `announcement_posts` VALUES (1,1,'Fire Alarm Test','<p>Please be aware that there will be a fire alarm test today at 4 pm. It will sound for up to 2 minutes, but there is no need to leave the building. Apologies for any inconvenience caused.</p>\r\n','2019-04-05','2019-04-19',1,1,0,0,0,'NULL',0,1,'2019-04-05 14:17:18',1,'2019-04-05','00:00:00',0,'N',NULL);
/*!40000 ALTER TABLE `announcement_posts` ENABLE KEYS */;
UNLOCK TABLES;
