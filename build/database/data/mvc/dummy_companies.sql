--
-- Dumping data for table `companies`
--

LOCK TABLES `companies` WRITE;
/*!40000 ALTER TABLE `companies` DISABLE KEYS */;
INSERT INTO `companies` VALUES (81,'Test Company 1',NULL,6,'1','','','SE1 0UE','','1',0,1,NULL);
INSERT INTO `companies` VALUES (82,'Locale',NULL,5,'Locale','','','SE1 0UE','','2',0,1,NULL);
INSERT INTO `companies` VALUES (83,'Test Company 2',NULL,7,'Test Company 2','','','SE1 0UE','','4',0,1,NULL);
INSERT INTO `companies` VALUES (84,'Test Company 3',NULL,5,'3','','','SE1 0UE','','3',0,1,NULL);
INSERT INTO `companies` VALUES (85,'Test Company 4',NULL,5,'4','','','SE1 0UE','','',0,1,NULL);
INSERT INTO `companies` VALUES (86,'Test Company 5',NULL,5,'5','','','SE1 0UE','','',0,1,NULL);
INSERT INTO `companies` VALUES (87,'Test Company 6',NULL,5,'6','','','SE1 0UE','','',0,1,NULL);
INSERT INTO `companies` VALUES (88,'Test Company 7',NULL,6,'7','','','SE1 0UE','','',0,1,NULL);
INSERT INTO `companies` VALUES (89,'Test Company 8',NULL,6,'8','','','SE1 0UE','','',0,1,NULL);
INSERT INTO `companies` VALUES (90,'Test Company 9',NULL,6,'9','','','SE1 0UE','','',0,1,NULL);
INSERT INTO `companies` VALUES (91,'Test Company 10',NULL,6,'L10','','','SE1 0UE','','',0,1,NULL);
INSERT INTO `companies` VALUES (92,'Test Company 11',NULL,7,'11','','','SE1 0UE','','',0,1,NULL);
INSERT INTO `companies` VALUES (93,'Test Company 12',NULL,7,'12','','','SE1 0UE','','',0,1,NULL);
INSERT INTO `companies` VALUES (94,'Test Company 13',NULL,7,'13','','','SE1 0UE','','',0,1,NULL);
INSERT INTO `companies` VALUES (95,'Test Company 14',NULL,7,'14','','','SE1 0UE','','',0,1,NULL);
/*!40000 ALTER TABLE `companies` ENABLE KEYS */;
UNLOCK TABLES;
