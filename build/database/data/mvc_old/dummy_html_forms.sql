--
-- Dumping data for table `html_forms`
--

LOCK TABLES `html_forms` WRITE;
/*!40000 ALTER TABLE `html_forms` DISABLE KEYS */;
INSERT INTO `html_forms` VALUES (1,'Accidents & Incidents','Accidents & Incidents',NULL,0,1,0);
INSERT INTO `html_forms` VALUES (2,'Loading Bay Bookings','Details of booking',NULL,0,1,0);
INSERT INTO `html_forms` VALUES (3,'Users','Additional fields for LBQ users module','$(\'#access_expiry\').datepicker({\r\ndateFormat: \"dd/mm/yy\"\r\n});\r\n\r\nshowAccessFields();\r\n\r\nfunction showAccessFields(){\r\nif($(\'#access_type\').val()==\"Temporary\"){\r\n$(\'#row_div_access_expiry\').show();\r\n$(\'#row_div_access_reason\').show();\r\n}else{\r\n$(\'#row_div_access_expiry\').hide();\r\n$(\'#row_div_access_reason\').hide();\r\n$(\'#access_expiry\').val(\'\');\r\n}\r\n}',0,1,0);
INSERT INTO `html_forms` VALUES (4,'Waste Collection Bookings','Additional information required on waste collection form',NULL,0,1,0);
INSERT INTO `html_forms` VALUES (5,'Test','Test Description',NULL,0,1,1);
INSERT INTO `html_forms` VALUES (6,'Lost & Found','Report lost and found items via the Helpdesk',NULL,0,1,0);
INSERT INTO `html_forms` VALUES (7,'Found Items','Additional custom form for the Helpdesk',NULL,0,1,1);
INSERT INTO `html_forms` VALUES (8,'Test Form','Description',NULL,0,1,0);
INSERT INTO `html_forms` VALUES (9,'Helpdesk additional fields','Additional fields required for Helpdesk Departments',NULL,0,1,1);
INSERT INTO `html_forms` VALUES (10,'Helpdesk-FoH-Events','Additional faults for FoH Events Category',NULL,0,1,1);
INSERT INTO `html_forms` VALUES (11,'Custom Form with signature','Description',NULL,0,1,0);
INSERT INTO `html_forms` VALUES (12,'Dependency Form','Description',NULL,0,1,0);
INSERT INTO `html_forms` VALUES (13,'Form log demo','Description',NULL,0,1,0);
INSERT INTO `html_forms` VALUES (14,'Custom Form with signature','Description',NULL,0,1,1);
INSERT INTO `html_forms` VALUES (15,'Cycle Bay Permit','Cycle Bay Permit Requests',NULL,0,1,0);
INSERT INTO `html_forms` VALUES (16,'Anticipated Waste Production','Follows same format as Premises',NULL,0,1,0);
INSERT INTO `html_forms` VALUES (17,'Waste Streams','Types of waste for anticipated waste production form',NULL,0,1,0);
INSERT INTO `html_forms` VALUES (18,'Other','Additional field for \'Other\' option',NULL,0,1,0);
INSERT INTO `html_forms` VALUES (19,'Form Questionnaire','Description',NULL,1,1,0);
INSERT INTO `html_forms` VALUES (20,'Form rich','Description',NULL,0,1,1);
INSERT INTO `html_forms` VALUES (21,'Quick Book','Quick Book',NULL,0,1,0);
INSERT INTO `html_forms` VALUES (22,'Quick Book Form','Quick Book Form',NULL,0,1,1);
INSERT INTO `html_forms` VALUES (23,'Online Induction Form','Online Induction Form',NULL,1,1,0);
INSERT INTO `html_forms` VALUES (24,'Outgoing Courier Demo','Outgoing Courier Demo',NULL,0,1,0);
INSERT INTO `html_forms` VALUES (25,'wangers','wangers',NULL,0,1,0);
/*!40000 ALTER TABLE `html_forms` ENABLE KEYS */;
UNLOCK TABLES;
