--
-- Dumping data for table `shortcut_icons_admin`
--

LOCK TABLES `shortcut_icons_admin` WRITE;
/*!40000 ALTER TABLE `shortcut_icons_admin` DISABLE KEYS */;
INSERT INTO `shortcut_icons_admin` VALUES (1,'Induction Bookings','fa-check-square-o','#induction','global',1,0,0);
INSERT INTO `shortcut_icons_admin` VALUES (2,'Loading Bay Bookings','fa-truck','#content/page/loading-bay-intro','global',1,0,0);
INSERT INTO `shortcut_icons_admin` VALUES (3,'Calendar','fa-calendar','#calendar','group_specific',1,0,0);
INSERT INTO `shortcut_icons_admin` VALUES (4,'Waste Collection','fa-trash','/#content/page/waste-management','global',1,0,0);
INSERT INTO `shortcut_icons_admin` VALUES (5,'User Database','fa-users','#usersdatabase/logs/active','global',1,0,0);
INSERT INTO `shortcut_icons_admin` VALUES (6,'Fire Wardens','fa-fire','#','global',1,0,0);
INSERT INTO `shortcut_icons_admin` VALUES (7,'Deliveries','fa-envelope-o','#offers','global',1,1,0);
/*!40000 ALTER TABLE `shortcut_icons_admin` ENABLE KEYS */;
UNLOCK TABLES;
