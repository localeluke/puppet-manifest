--
-- Dumping data for table `admin_deliveries_config`
--

LOCK TABLES `admin_deliveries_config` WRITE;
/*!40000 ALTER TABLE `admin_deliveries_config` DISABLE KEYS */;
INSERT INTO `admin_deliveries_config` VALUES (1,'<p>Log or retrieve a delivery by selecting the relevant option&nbsp;and&nbsp;completing the details requested.</p>\r\n',0,0,0,1,1,'<p>The delivery has been successfully logged</p>\r\n','<p>Complete the details below for delivery collection</p>\r\n','ipad');
/*!40000 ALTER TABLE `admin_deliveries_config` ENABLE KEYS */;
UNLOCK TABLES;
