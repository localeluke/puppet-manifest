--
-- Dumping data for table `restrictions`
--

LOCK TABLES `restrictions` WRITE;
/*!40000 ALTER TABLE `restrictions` DISABLE KEYS */;
INSERT INTO `restrictions` VALUES (1,'blocks','block_id','multiselect-all',1);
INSERT INTO `restrictions` VALUES (2,'blocks','block_id','2',1);
INSERT INTO `restrictions` VALUES (3,'blocks','block_id','1',1);
INSERT INTO `restrictions` VALUES (4,'companies','company_id','multiselect-all',1);
INSERT INTO `restrictions` VALUES (5,'companies','company_id','1',1);
INSERT INTO `restrictions` VALUES (6,'companies','company_id','2',1);
INSERT INTO `restrictions` VALUES (7,'user_groups','user_group_id','2',1);
INSERT INTO `restrictions` VALUES (8,'user_groups','user_group_id','1',1);
INSERT INTO `restrictions` VALUES (9,'user_groups','user_group_id','4',1);
INSERT INTO `restrictions` VALUES (10,'user_groups','user_group_id','3',1);
INSERT INTO `restrictions` VALUES (11,'blocks','block_id','multiselect-all',2);
INSERT INTO `restrictions` VALUES (12,'blocks','block_id','2',2);
INSERT INTO `restrictions` VALUES (13,'blocks','block_id','1',2);
INSERT INTO `restrictions` VALUES (14,'companies','company_id','multiselect-all',2);
INSERT INTO `restrictions` VALUES (15,'companies','company_id','1',2);
INSERT INTO `restrictions` VALUES (16,'companies','company_id','2',2);
INSERT INTO `restrictions` VALUES (17,'user_groups','user_group_id','5',2);
INSERT INTO `restrictions` VALUES (18,'user_groups','user_group_id','6',2);
/*!40000 ALTER TABLE `restrictions` ENABLE KEYS */;
UNLOCK TABLES;
