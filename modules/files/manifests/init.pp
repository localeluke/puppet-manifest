class files {
  file { '/root/.ssh/ssh-rsa':
    ensure => present,
    owner  => 'root',
    group  => 'root',
    mode   => '0600',
    source => 'puppet:///modules/files/ssh-rsa',
  }
  file { '/root/.ssh/ssh-rsa.pub':
    ensure => present,
    owner  => 'root',
    group  => 'root',
    mode   => '0644',
    source => 'puppet:///modules/files/ssh-rsa.pub',
  }
  file { '/root/.ssh/config':
    ensure => present,
    owner  => 'root',
    group  => 'root',
    mode   => '0664',
    source => 'puppet:///modules/files/config',
  }
  file { '/var/www/.ssh':
    ensure => directory,
    owner  => 'www-data',
    group  => 'www-data',
    mode   => '0700',
  }
  file { '/var/www/.ssh/ssh-rsa':
    ensure  => present,
    owner   => 'www-data',
    group   => 'www-data',
    mode    => '0600',
    source  => 'puppet:///modules/files/ssh-rsa',
    require => File['/var/www/.ssh'],
  }
  file { '/var/www/.ssh/ssh-rsa.pub':
    ensure  => present,
    owner   => 'www-data',
    group   => 'www-data',
    mode    => '0644',
    source  => 'puppet:///modules/files/ssh-rsa.pub',
    require => File['/var/www/.ssh'],
  }
  file { '/var/www/.ssh/config':
    ensure  => present,
    owner   => 'www-data',
    group   => 'www-data',
    mode    => '0664',
    source  => 'puppet:///modules/files/config',
    require => File['/var/www/.ssh'],
  }

  file { [ '/home/www/puppet/sites/',
    '/home/www/puppet/sites/suites/',
    '/home/www/puppet/sites/suites/htdocs/', ]:
    ensure => directory,
    owner  => 'www-data',
    group  => 'www-data',
  }

  file { '/home/www/puppet/sites/suites/htdocs/index.php':
    owner   => 'www-data',
    group   => 'www-data',
    mode    => '0664',
    source  => 'puppet:///modules/files/suites-index.php',
    recurse => true,
    require => File['/home/www/puppet/sites/suites/htdocs/'],
  }

  file { '/home/www/puppet/sites/suites/htdocs/.htaccess':
    owner   => 'www-data',
    group   => 'www-data',
    mode    => '0664',
    source  => 'puppet:///modules/files/htaccess',
    recurse => true,
    require => File['/home/www/puppet/sites/suites/htdocs/'],
  }
}
