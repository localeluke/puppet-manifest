--
-- Dumping data for table `announcement_visibility`
--

LOCK TABLES `announcement_visibility` WRITE;
/*!40000 ALTER TABLE `announcement_visibility` DISABLE KEYS */;
INSERT INTO `announcement_visibility` VALUES (121,110,'blocks',5);
INSERT INTO `announcement_visibility` VALUES (122,110,'blocks',7);
INSERT INTO `announcement_visibility` VALUES (123,110,'blocks',6);
INSERT INTO `announcement_visibility` VALUES (124,111,'blocks',5);
INSERT INTO `announcement_visibility` VALUES (125,111,'blocks',7);
INSERT INTO `announcement_visibility` VALUES (126,111,'blocks',6);
/*!40000 ALTER TABLE `announcement_visibility` ENABLE KEYS */;
UNLOCK TABLES;
