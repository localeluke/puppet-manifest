--
-- Dumping data for table `calendar_events`
--

LOCK TABLES `calendar_events` WRITE;
/*!40000 ALTER TABLE `calendar_events` DISABLE KEYS */;
INSERT INTO `calendar_events` VALUES (1,0,'test',1,'2019-04-04','2019-04-05','01:00:00','23:00:00',NULL,1,'test',0,0,0,'2019-04-04',0,1);
INSERT INTO `calendar_events` VALUES (2,0,'test',1,'2019-04-04','2019-04-04','08:00:00','19:00:00',NULL,1,'test',0,0,0,'2019-04-04',0,1);
INSERT INTO `calendar_events` VALUES (3,0,'test',1,'2019-04-05','2019-04-05','00:30:00','01:00:00',NULL,2,'test',0,0,0,'2019-04-04',0,1);
INSERT INTO `calendar_events` VALUES (4,0,'test',1,'2019-04-04','2019-04-05','00:30:00','01:00:00',NULL,1,'test',0,0,0,'2019-04-04',0,1);
/*!40000 ALTER TABLE `calendar_events` ENABLE KEYS */;
UNLOCK TABLES;
