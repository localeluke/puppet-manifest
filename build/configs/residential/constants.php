<?php
define("SITE_NAME", "Locale Product - Master");
define("DEFAULT_MODULE_VERSION", 1);
define("GOOGLE_CALENDAR_ACC", "");
define("SHOW_FACILITY_INTRO_LIST", "1");
define("WEATHER_DAYS", "4");
define("PREDICTIVE_USRSEARCH_FACILITY", "1");
define("ANN_CALENDAR", "1");
define("DIFER_EMAIL_HIDE", "1");
define("ANNOUNCEMENT_VALUE", "1");
define("REGISTER_FORM", "5");
define("FACILITY_INTRO_LIST_SUBHEADING", "");
define("ANN_IMAGE_WIDTH", "");
define("ANN_IMAGE_HEIGHT", "");
define("ADMIN_HEADER_HEIGHT", "");
define("ADMIN_CONTENT_MARGIN", "");
define("FACILITY_RECURRANCE", "1");
define("FACILITY_AVAILABILITY", "1");
define("ONBOARDING_USERS", "1");
define("DELIVERY_SEARCH", "1");
define("PRODUCT_DASHBOARD", "1");
define("PRODUCT_LOGS_DESIGN", "1");
define("SIGNATURE_SELECT", "1");
define("LOCAL_OFFERS", "1");
define("ACCOUNT_DROPDOWN", "1");
define("MODAL_BG", "");

$nolayout_functions['Keyholding'] = array('signature_ipad');
$nolayout_functions['Deliveries'] = array('signature_ipad');
$nolayout_functions['Promotion'] = array('offer_preview');
$nolayout_functions['Home'] = array('');
$nolayout_functions['Main'] = array('signature_ipad');
$nonlogged_functions['Home'] = array('get_auth_error_message', 'get_user_count', 'welcome', 'login', 'logout', 'authorize', 'register', 'forgot_password', 'check_user_exists', 'captcha', 'valid_captcha', 'pre_register', 'save_profile', 'confirm', 'get_development_properties', 'reset_pass', 'save_pass', 'sublet_register', 'search');
$nonlogged_functions['Usersdatabase'] = array('resident_registration', 'save_resident_profile');
$nonlogged_functions['Content'] = array('popup', 'download', 'page', 'get_page_info');
$nonlogged_functions['Announcement'] = array('download');

$titles = array('Mr' => 'Mr', 'Miss' => 'Miss', 'Dr' => 'Dr', 'Mrs' => 'Mrs', 'Ms' => 'Ms', 'Sir' => 'Sir');

$mysql_snippets_name_user = 'concat(u.firstname, " ", u.lastname) as name';

$meeting_room_id = 2;
//do not change the keys as they are used in function get_constant_array in general.php
//tag - name for the element | table - to refer to db table name
$config_filters['blocks']['label'] = 'Blocks';                #"Filter by Block";
$config_filters['blocks']['table'] = 'blocks';                #"Filter by Block";
$config_filters['blocks']['field'] = 'block_id';            #"Filter by Block";
$config_filters['company']['label'] = 'Company';            #"Filter by Company";
$config_filters['company']['table'] = 'companies';            #"Filter by Company";
$config_filters['company']['field'] = 'company_id';            #"Filter by Company";
$config_filters['user_groups']['label'] = 'User Groups';        #"Filter by User Group";
$config_filters['user_groups']['table'] = 'user_groups';        #"Filter by User Group";
$config_filters['user_groups']['field'] = 'user_group_id';        #"Filter by User Group";

$config_filters_res['blocks']['label'] = 'Blocks';                #"Filter by Block";
$config_filters_res['blocks']['table'] = 'blocks';                #"Filter by Block";
$config_filters_res['blocks']['field'] = 'block_id';            #"Filter by Block";
$config_filters_res['properties_f']['label'] = 'Properties';        #"Filter by Apartment";
$config_filters_res['properties_f']['table'] = 'properties';        #"Filter by Apartment";
$config_filters_res['properties_f']['field'] = 'property_id';        #"Filter by Apartment";
$config_filters_res['property_type']['label'] = 'Property Types';    #"Filter by Apartment Type";
$config_filters_res['property_type']['table'] = 'property_types';    #"Filter by Apartment Type";
$config_filters_res['property_type']['field'] = 'property_type_id';    #"Filter by Apartment Type";
$config_filters_res['user_groups']['label'] = 'User Groups';        #"Filter by User Group";
$config_filters_res['user_groups']['table'] = 'user_groups';        #"Filter by User Group";
$config_filters_res['user_groups']['field'] = 'user_group_id';        #"Filter by User Group";
$config_filters_res['users']['label'] = 'User';                #"Filter by Users";
$config_filters_res['users']['table'] = 'users';                #"Filter by Users";
$config_filters_res['users']['field'] = 'id';                #"Filter by Users";

$config_activeBookings[1] = "1 Slot";
$config_activeBookings[2] = "2 Slots";
$config_activeBookings[3] = "3 Slots";
$config_activeBookings[4] = "4 Slots";
$config_activeBookings[5] = "5 Slots";
$config_activeBookings[6] = "6 Slots";
$config_activeBookings[7] = "7 Slots";
$config_activeBookings[8] = "8 Slots";
$config_activeBookings[9] = "9 Slots";
$config_activeBookings[10] = "10 Slots";

$config_interval['day'] = "Day(s)";
$config_interval['week'] = "Week(s)";
$config_interval['month'] = "Month(s)";
$config_interval['year'] = "Year";

$config_slotType['Fixed'] = "Fixed";
$config_slotType['Variable'] = "Variable";

$config_fixedStyle['Slot'] = "Slot";
$config_fixedStyle['Dropdown'] = "Dropdown";

//mins
$config_slotsPerDay['1'] = "1 slot";
$config_slotsPerDay['2'] = "2 slots";
$config_slotsPerDay['3'] = "3 slots";
$config_slotsPerDay['4'] = "4 slots";
$config_slotsPerDay['5'] = "5 slots";
$config_slotsPerDay['6'] = "6 slots";
$config_slotsPerDay['7'] = "7 Slots";
$config_slotsPerDay['8'] = "8 Slots";
$config_slotsPerDay['9'] = "9 Slots";
$config_slotsPerDay['10'] = "10 Slots";

$config_emailDays['0'] = "Same day";
$config_emailDays['1'] = "1 day";
$config_emailDays['2'] = "2 days";
$config_emailDays['3'] = "3 days";
$config_emailDays['4'] = "4 days";
$config_emailDays['5'] = "5 days";
$config_emailDays['6'] = "6 days";
$config_emailDays['7'] = "7 days";

// $config_entrance['Office Entrance'] 	= "Office Entrance";
// $config_entrance['Service Entrance']	= "Service Entrance";

// $config_recurring_visit['Weekly'] 	= "Weekly";
// $config_recurring_visit['Monthly']	= "Monthly";
// $config_recurring_visit['Annually']	= "Annually";

$config_fault_location['1'] = "Fault location 1";
$config_fault_location['2'] = "Fault location 2";
$config_fault_location['3'] = "Fault location 3";
$config_fault_location['4'] = "Fault location 4";

$config_fault_type['1'] = "Fault type 1";
$config_fault_type['2'] = "Fault type 2";
$config_fault_type['3'] = "Fault type 3";
$config_fault_type['4'] = "Fault type 4";

$config_blocks['1'] = "The Place";
$config_blocks['2'] = "New Block";

$config_languages['1'] = "English";
$config_languages['2'] = "Chinese Po Kien";
$config_languages['3'] = "Filipino";
$config_languages['4'] = "Farsi";
$config_languages['5'] = "Filipino Tagalog";
$config_languages['6'] = "Eritrea";
$config_languages['7'] = "Ethiopia";
$config_languages['8'] = "French";
$config_languages['9'] = "German";
$config_languages['10'] = "Urdu";
$config_languages['11'] = "Russian";

/* Start : Array for Categories on Whos on duty - add new contact form*/
$dutyCategories[1] = "Building Manager";
$dutyCategories[2] = "Concierge";
$dutyCategories[3] = "Reception";
$dutyCategories[4] = "Block Manager";
$dutyCategories[5] = "Development Manager";
/* End : Array for Categories on Whos on duty - add new contact form*/

$admin_userTypes['superadmin'] = "Superadmin";
$admin_userTypes['ma_access'] = "MA Admin";

$active_array = array('1' => 'Yes', '0' => 'No');


$feeds = array
(
    'tube' => array
    (
        'tpl' => 'tube.tpl',
        'feed_url' => 'tube_url',
        'class' => ''
    ),
    'airport' => array
    (
        'tpl' => '',
        'feed_url' => 'airport_url',
        'class' => ''
    ),
    'train' => array
    (
        'tpl' => '',
        'text' => 'TRAIN',
        'feed_url' => 'trains_url',
        'class' => ''
    ),
    'bike' => array
    (
        'tpl' => '',
        'text' => 'BIKE',
        'feed_url' => 'bike_url',
        'class' => '',
        'postcode' => 'SE1 9SG'
    ),
    'road' => array
    (
        'tpl' => 'road.tpl',
        'text' => 'ROAD',
        'feed_url' => 'roads_url',
        'class' => ''
    ),
    'ferry' => array
    (
        'tpl' => 'road.tpl',
        'feed_url' => 'ferries_url',
        'class' => ''
    ),
    'walking' => array
    (
        'tpl' => '',
        'text' => 'WALKING',
        'feed_url' => 'walking_url',
        'class' => ''
    )
);


define('DB_DATE_FORMAT', 'Y-m-d');
define('DATE_FORMAT', 'd/m/Y');

$pre_table_values = array();
$pre_table_values["header"][0] = array("Column Number", "120");
$pre_table_values["header"][1] = array("Value Required", "220");

$pre_table_values["body"][0] = array("Title", "*", "(Mr,Mrs,Miss,Ms)");
$pre_table_values["body"][1] = array("First Name", "*");
$pre_table_values["body"][2] = array("Last Name", "*");
$pre_table_values["body"][3] = array("Email Address", "*");
$pre_table_values["body"][4] = array("User Group", "*", "(options: see table below)");
$pre_table_values["body"][5] = array("Telephone Number");
$pre_table_values["body"][6] = array("Mobile No");
if ($_config['site_type'] == "RESIDENTIAL")
    $pre_table_values["body"][7] = array("Property");
else
    $pre_table_values["body"][7] = array("Company");
$pre_table_values["body"][8] = array("Block");
$pre_table_values["body"][9] = array("Receive Building Announcements <br/>==> Yes = 1 No = 0");
$pre_table_values["body"][10] = array("Subscribe to the Newsletter <br/>==> Yes = 1 No = 0");

$pre_table_csv = array();
$pre_table_csv["header"][0] = array("CSV Notation", "130");
$pre_table_csv["header"][1] = array("User Group", "210");

$prop_table_values = array();
$prop_table_values["header"][0] = array("Column Number", "120");
$prop_table_values["header"][1] = array("Value Required", "220");

$prop_table_values["body"][0] = array("Property Type", "*");
$prop_table_values["body"][1] = array("Block Ref", "*");
$prop_table_values["body"][2] = array("Property Name", "*");
$prop_table_values["body"][3] = array("Property Ref", "*");
$prop_table_values["body"][4] = array("Street");
$prop_table_values["body"][5] = array("Borough");
$prop_table_values["body"][6] = array("Postcode");
$prop_table_values["body"][7] = array("City");
$prop_table_values["body"][8] = array("Floor");
$prop_table_values["body"][9] = array("Active", "*", "Yes or No");

$userstatsHeader = array("Name", "Location", "Registered On", "Last Accessed", "Total Visits");

define("VMS_ADMIN_EMAIL_ADDR", "localedevelopmentemail@gmail.com");
define("OAUTH_CLIENT_NAME", "");
define("OAUTH_CLIENT_SECRET", "");
define("OAUTH_REDIRECT_URI", "");
define("OAUTH_CLIENT_ID", "");
define("CLIENT_ID", "");
define("SERVICE_ACCOUNT_NAME", "");
define("APP_NAME", "");
define("KEY_FILE", "");


define("SET_LESS_CSS", "1");
$less_files = array('login', 'home', 'style', 'uiPage', 'respond', 'accesstodemise', 'announcement', 'accountsetting', 'calendar', 'contactconcierge', 'deliveries', 'dutyhandover', 'facility', 'helpdesk', 'induction', 'keyholding', 'localamenities', 'onduty', 'reporting', 'subletting', 'usersdatabase', 'vms', 'facilityAvailability');


################################################

# This array is used for user starter export
//$starter_table_values = array();
//$starter_table_values["header"][0] = array("Column Number","120");
//$starter_table_values["header"][1] = array("Value Required","220");
//
//$starter_table_values["body"][0]        = array("Access Type", "*", "(Permanent_Access, Temporary_Access)");
//$starter_table_values["body"][]         = array("Access Expiry", "*", "(For permanent access, leave field blank. For Temporary access, enter expiry date in format: 'dd/mm/yyyy')");
//$starter_table_values["body"][]         = array("Access Reason", "", "(If Access Type is Temporary Access)","*");
//$starter_table_values["body"][]         = array("Title", "*", "Mr,Mrs,Miss,Ms,Dr");
//$starter_table_values["body"][]         = array("First Name", "*");
//$starter_table_values["body"][]         = array("Last Name", "*");
//$starter_table_values["body"][]         = array("Email", "*");
//$starter_table_values["body"][]         = array("Company", "*");
//$starter_table_values["body"][]         = array("Floors", "*");
//$starter_table_values["body"][]         = array("Department", "*");
//$starter_table_values["body"][]         = array("User Type", "*", "(options: see table below)");
//$starter_table_values["body"][]         = array("Secondary Department", "*");
//$starter_table_values["body"][]         = array("Telephone", "*");
//$starter_table_values["body"][]         = array("Mobile", "*");
//$starter_table_values["body"][]         = array("VMS Permit", "", "(options: yes,no)");
//define("LBQ_LOGS_DESIGN", 1);
