--
-- Dumping data for table `user_meta`
--

LOCK TABLES `user_meta` WRITE;
/*!40000 ALTER TABLE `user_meta` DISABLE KEYS */;
INSERT INTO `user_meta` VALUES (1,1,'calendar_token','cd29eab2d7903c6020e0e64ee87918b1d23cc5720f3c6f2f41fef49225e4');
INSERT INTO `user_meta` VALUES (2,5,'calendar_token','4a4ee5f932901734caa03542ee1101075cc04c30446f32a24b24421351a2');
/*!40000 ALTER TABLE `user_meta` ENABLE KEYS */;
UNLOCK TABLES;
