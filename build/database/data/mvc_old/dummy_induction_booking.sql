--
-- Dumping data for table `induction_bookings`
--

LOCK TABLES `induction_bookings` WRITE;
/*!40000 ALTER TABLE `induction_bookings` DISABLE KEYS */;
INSERT INTO `induction_bookings` VALUES (3,25,1,'','2014-11-29','11:00:00','12:00:00',1,'2014-11-28 00:00:00',1,0,NULL,NULL);
INSERT INTO `induction_bookings` VALUES (12,175,1,'','2015-08-25','14:00:00','16:00:00',102,'2015-08-24 00:00:00',1,0,'Aljazeera',NULL);
INSERT INTO `induction_bookings` VALUES (13,176,1,'','2015-08-25','14:00:00','16:00:00',102,'2015-08-24 00:00:00',1,0,'Aljazeera',NULL);
INSERT INTO `induction_bookings` VALUES (14,177,1,'','2015-08-25','08:00:00','10:00:00',102,'2015-08-24 00:00:00',1,0,'Aljazeera',NULL);
INSERT INTO `induction_bookings` VALUES (17,180,1,'','2015-08-26','03:00:00','05:00:00',102,'2015-08-24 00:00:00',0,0,'Aljazeera',NULL);
INSERT INTO `induction_bookings` VALUES (18,181,1,'','2015-08-27','14:00:00','16:00:00',102,'2015-08-24 00:00:00',1,0,'Aljazeera',NULL);
INSERT INTO `induction_bookings` VALUES (19,182,1,'','2015-08-30','03:00:00','05:00:00',102,'2015-08-28 00:00:00',0,0,'Aljazeera',NULL);
INSERT INTO `induction_bookings` VALUES (20,186,20,'','2015-09-16','14:00:00','16:00:00',102,'2015-09-08 00:00:00',0,0,'Chorus Group',NULL);
INSERT INTO `induction_bookings` VALUES (21,193,2,'','2015-09-23','14:00:00','16:00:00',72,'2015-09-14 00:00:00',1,0,'Aqua',NULL);
INSERT INTO `induction_bookings` VALUES (22,194,40,'','2015-09-17','03:00:00','05:00:00',72,'2015-09-15 00:00:00',0,0,'Area Sq',NULL);
INSERT INTO `induction_bookings` VALUES (23,195,2,'','2015-09-24','14:00:00','16:00:00',72,'2015-09-15 00:00:00',0,0,'Aqua',NULL);
INSERT INTO `induction_bookings` VALUES (24,196,2,'','2015-10-21','14:00:00','16:00:00',72,'2015-10-15 00:00:00',0,0,'Aqua',NULL);
INSERT INTO `induction_bookings` VALUES (25,43,8,'','2014-12-05','13:00:00','14:00:00',1,'2014-12-01 00:00:00',1,0,NULL,NULL);
INSERT INTO `induction_bookings` VALUES (26,45,39,'','2014-12-03','11:00:00','12:00:00',1,'2014-12-01 00:00:00',0,0,NULL,NULL);
INSERT INTO `induction_bookings` VALUES (27,46,32,'','2014-12-09','11:00:00','12:00:00',1,'2014-12-01 00:00:00',0,0,NULL,NULL);
INSERT INTO `induction_bookings` VALUES (28,47,32,'','2014-12-09','11:00:00','12:00:00',1,'2014-12-01 00:00:00',0,0,NULL,NULL);
INSERT INTO `induction_bookings` VALUES (29,48,40,'','2014-12-09','13:00:00','14:00:00',1,'2014-12-01 00:00:00',0,0,NULL,NULL);
INSERT INTO `induction_bookings` VALUES (30,49,40,'','2014-12-09','13:00:00','14:00:00',1,'2014-12-01 00:00:00',1,0,NULL,NULL);
INSERT INTO `induction_bookings` VALUES (31,50,2,'','2014-12-11','11:00:00','12:00:00',1,'2014-12-02 00:00:00',1,0,NULL,NULL);
INSERT INTO `induction_bookings` VALUES (32,51,40,'','2014-12-09','09:00:00','10:00:00',1,'2014-12-02 00:00:00',1,0,NULL,NULL);
INSERT INTO `induction_bookings` VALUES (35,54,32,'','2014-12-13','08:00:00','10:00:00',1,'2014-12-03 00:00:00',1,0,NULL,NULL);
INSERT INTO `induction_bookings` VALUES (36,55,8,'','2014-12-10','08:00:00','10:00:00',1,'2014-12-05 00:00:00',1,0,NULL,NULL);
INSERT INTO `induction_bookings` VALUES (37,56,1,'','2014-12-10','08:00:00','10:00:00',1,'2014-12-05 00:00:00',1,0,NULL,NULL);
INSERT INTO `induction_bookings` VALUES (38,57,3,'','2014-12-10','14:00:00','16:00:00',1,'2014-12-05 00:00:00',1,0,NULL,NULL);
INSERT INTO `induction_bookings` VALUES (39,58,2,'','2014-12-10','14:00:00','16:00:00',1,'2014-12-05 00:00:00',1,0,NULL,NULL);
INSERT INTO `induction_bookings` VALUES (40,59,7,'','2014-12-17','14:00:00','16:00:00',1,'2014-12-08 00:00:00',1,0,NULL,NULL);
INSERT INTO `induction_bookings` VALUES (41,60,2,'','2014-12-17','08:00:00','10:00:00',1,'2014-12-08 00:00:00',1,0,NULL,NULL);
INSERT INTO `induction_bookings` VALUES (42,61,32,'','2014-12-16','08:00:00','10:00:00',1,'2014-12-08 00:00:00',1,0,NULL,NULL);
INSERT INTO `induction_bookings` VALUES (43,62,27,'','2014-12-16','08:00:00','10:00:00',1,'2014-12-08 00:00:00',1,0,NULL,NULL);
INSERT INTO `induction_bookings` VALUES (44,63,8,'','2014-12-18','14:00:00','16:00:00',1,'2014-12-11 00:00:00',1,0,NULL,NULL);
INSERT INTO `induction_bookings` VALUES (45,65,40,'','2014-12-19','08:00:00','10:00:00',1,'2014-12-12 00:00:00',1,0,NULL,NULL);
INSERT INTO `induction_bookings` VALUES (46,66,39,'','2014-12-16','14:00:00','16:00:00',1,'2014-12-12 00:00:00',1,0,NULL,NULL);
INSERT INTO `induction_bookings` VALUES (48,68,20,'','2014-12-18','08:00:00','10:00:00',1,'2014-12-12 00:00:00',0,0,NULL,NULL);
INSERT INTO `induction_bookings` VALUES (49,69,31,'','2014-12-23','08:00:00','10:00:00',1,'2014-12-12 00:00:00',1,0,NULL,NULL);
INSERT INTO `induction_bookings` VALUES (50,70,32,'','2014-12-18','14:00:00','16:00:00',1,'2014-12-15 00:00:00',1,0,NULL,NULL);
INSERT INTO `induction_bookings` VALUES (51,74,32,'','2014-12-25','14:00:00','16:00:00',1,'2014-12-15 00:00:00',1,0,NULL,NULL);
INSERT INTO `induction_bookings` VALUES (53,76,32,'','2014-12-26','08:00:00','10:00:00',1,'2014-12-17 00:00:00',1,0,NULL,NULL);
INSERT INTO `induction_bookings` VALUES (54,77,43,'','2014-12-24','08:00:00','10:00:00',1,'2014-12-17 00:00:00',1,0,NULL,NULL);
INSERT INTO `induction_bookings` VALUES (55,78,40,'','2014-12-25','08:00:00','10:00:00',1,'2014-12-17 00:00:00',1,0,NULL,NULL);
INSERT INTO `induction_bookings` VALUES (56,80,2,'','2014-12-24','14:00:00','16:00:00',1,'2014-12-18 00:00:00',1,0,NULL,NULL);
INSERT INTO `induction_bookings` VALUES (59,97,38,'','2015-01-07','08:00:00','10:00:00',1,'2015-01-06 00:00:00',1,0,NULL,NULL);
INSERT INTO `induction_bookings` VALUES (60,98,32,'','2015-01-16','14:00:00','16:00:00',1,'2015-01-09 00:00:00',1,0,NULL,NULL);
INSERT INTO `induction_bookings` VALUES (61,104,8,'','2015-01-19','14:00:00','16:00:00',100,'2015-01-19 00:00:00',1,0,NULL,NULL);
INSERT INTO `induction_bookings` VALUES (63,111,40,'','2015-01-28','08:00:00','10:00:00',1,'2015-01-23 00:00:00',1,0,NULL,NULL);
INSERT INTO `induction_bookings` VALUES (64,113,8,'','2015-02-20','08:00:00','10:00:00',1,'2015-02-16 00:00:00',1,0,NULL,NULL);
INSERT INTO `induction_bookings` VALUES (65,114,38,'','2015-02-25','08:00:00','10:00:00',1,'2015-02-16 00:00:00',1,0,NULL,NULL);
INSERT INTO `induction_bookings` VALUES (66,116,0,'','2015-02-27','08:00:00','10:00:00',102,'2015-02-19 00:00:00',0,0,NULL,NULL);
INSERT INTO `induction_bookings` VALUES (67,117,0,'','2015-02-27','08:00:00','10:00:00',102,'2015-02-19 00:00:00',0,0,NULL,NULL);
INSERT INTO `induction_bookings` VALUES (68,115,1,'','2015-02-27','14:00:00','16:00:00',1,'2015-02-19 00:00:00',0,0,NULL,NULL);
INSERT INTO `induction_bookings` VALUES (69,118,0,'','2015-02-27','14:00:00','16:00:00',1,'2015-02-19 00:00:00',0,0,NULL,NULL);
INSERT INTO `induction_bookings` VALUES (70,119,0,'','2015-02-24','08:00:00','10:00:00',100,'2015-02-23 00:00:00',0,0,NULL,NULL);
INSERT INTO `induction_bookings` VALUES (71,120,0,'','2015-03-04','14:00:00','16:00:00',101,'2015-03-03 00:00:00',0,0,NULL,NULL);
INSERT INTO `induction_bookings` VALUES (72,131,1,'','2015-03-14','08:00:00','10:00:00',1,'2015-03-12 00:00:00',0,0,'Aljazeera',NULL);
INSERT INTO `induction_bookings` VALUES (73,132,8,'','2015-03-22','14:00:00','16:00:00',1,'2015-03-12 00:00:00',1,0,'LBQ',NULL);
INSERT INTO `induction_bookings` VALUES (74,133,8,'','2015-03-22','08:00:00','10:00:00',1,'2015-03-12 00:00:00',1,0,'LBQ',NULL);
INSERT INTO `induction_bookings` VALUES (75,134,8,'','2015-03-14','14:00:00','16:00:00',1,'2015-03-12 00:00:00',1,0,'LBQ',NULL);
INSERT INTO `induction_bookings` VALUES (76,135,1,'','2015-03-20','08:00:00','10:00:00',1,'2015-03-12 00:00:00',1,0,'Aljazeera',NULL);
INSERT INTO `induction_bookings` VALUES (77,125,1,'','2015-03-31','08:00:00','10:00:00',102,'2015-03-30 00:00:00',0,0,'Aljazeera',NULL);
INSERT INTO `induction_bookings` VALUES (78,139,8,'','2015-04-11','14:00:00','16:00:00',102,'2015-03-31 00:00:00',1,0,'LBQ',NULL);
INSERT INTO `induction_bookings` VALUES (79,140,8,'','2015-04-11','14:00:00','16:00:00',102,'2015-03-31 00:00:00',1,0,'LBQ',NULL);
INSERT INTO `induction_bookings` VALUES (80,141,8,'','2015-04-18','08:00:00','10:00:00',102,'2015-04-07 00:00:00',1,0,'LBQ',NULL);
INSERT INTO `induction_bookings` VALUES (81,142,8,'','2015-04-17','14:00:00','16:00:00',102,'2015-04-13 00:00:00',1,0,'LBQ',NULL);
INSERT INTO `induction_bookings` VALUES (82,143,8,'','2015-04-30','08:00:00','10:00:00',102,'2015-04-27 00:00:00',1,0,'LBQ',NULL);
INSERT INTO `induction_bookings` VALUES (83,144,1,'','2015-05-17','03:00:00','05:00:00',102,'2015-05-15 00:00:00',0,0,'Aljazeera',NULL);
INSERT INTO `induction_bookings` VALUES (84,145,1,'','2015-05-24','03:00:00','05:00:00',102,'2015-05-15 00:00:00',0,0,'Aljazeera',NULL);
INSERT INTO `induction_bookings` VALUES (85,146,1,'','2015-05-24','03:00:00','05:00:00',102,'2015-05-15 00:00:00',0,0,'Aljazeera',NULL);
INSERT INTO `induction_bookings` VALUES (86,147,1,'','2015-05-24','08:00:00','10:00:00',102,'2015-05-15 00:00:00',0,0,'Aljazeera',NULL);
INSERT INTO `induction_bookings` VALUES (87,164,1,'','2015-05-23','14:00:00','16:00:00',102,'2015-05-20 00:00:00',0,0,'Aljazeera',NULL);
INSERT INTO `induction_bookings` VALUES (88,121,1,'','2015-05-30','08:00:00','10:00:00',72,'2015-05-21 00:00:00',0,0,'Aljazeera',NULL);
INSERT INTO `induction_bookings` VALUES (89,128,1,'','2015-05-30','08:00:00','10:00:00',72,'2015-05-21 00:00:00',0,0,'Aljazeera',NULL);
INSERT INTO `induction_bookings` VALUES (90,129,8,'','2015-05-30','14:00:00','16:00:00',72,'2015-05-21 00:00:00',0,0,'LBQ',NULL);
INSERT INTO `induction_bookings` VALUES (91,130,1,'','2015-05-30','14:00:00','16:00:00',72,'2015-05-21 00:00:00',0,0,'Aljazeera',NULL);
INSERT INTO `induction_bookings` VALUES (92,165,8,'','2015-05-23','03:00:00','05:00:00',72,'2015-05-21 00:00:00',0,0,'LBQ',NULL);
INSERT INTO `induction_bookings` VALUES (94,167,8,'','2015-06-23','08:00:00','10:00:00',72,'2015-06-11 00:00:00',0,0,'LBQ',NULL);
INSERT INTO `induction_bookings` VALUES (95,168,1,'','2015-06-25','03:00:00','05:00:00',102,'2015-06-19 00:00:00',1,0,'Aljazeera',NULL);
INSERT INTO `induction_bookings` VALUES (96,169,1,'','2015-06-20','03:00:00','05:00:00',102,'2015-06-19 00:00:00',1,0,'Aljazeera',NULL);
INSERT INTO `induction_bookings` VALUES (97,171,1,'','2015-08-22','03:00:00','05:00:00',102,'2015-08-21 00:00:00',1,0,'Aljazeera',NULL);
INSERT INTO `induction_bookings` VALUES (98,173,1,'','2015-08-25','03:00:00','05:00:00',102,'2015-08-24 00:00:00',1,0,'Aljazeera',NULL);
INSERT INTO `induction_bookings` VALUES (99,174,1,'','2015-08-25','03:00:00','05:00:00',102,'2015-08-24 00:00:00',1,0,'Aljazeera',NULL);
INSERT INTO `induction_bookings` VALUES (100,178,1,'','2015-08-25','08:00:00','10:00:00',102,'2015-08-24 00:00:00',1,0,'Aljazeera',NULL);
INSERT INTO `induction_bookings` VALUES (101,179,1,'','2015-08-26','03:00:00','05:00:00',102,'2015-08-24 00:00:00',1,0,'Aljazeera',NULL);
INSERT INTO `induction_bookings` VALUES (102,252,8,'','2016-08-19','03:00:00','05:00:00',72,'2016-08-17 00:00:00',1,0,'LBQ',NULL);
INSERT INTO `induction_bookings` VALUES (103,253,8,'','2016-08-19','03:00:00','05:00:00',72,'2016-08-17 00:00:00',1,0,'LBQ',NULL);
INSERT INTO `induction_bookings` VALUES (104,254,8,'','2016-08-19','14:00:00','16:00:00',72,'2016-08-17 00:00:00',1,0,'LBQ',NULL);
INSERT INTO `induction_bookings` VALUES (105,255,8,'','2016-08-19','14:00:00','16:00:00',72,'2016-08-17 00:00:00',1,0,'LBQ',NULL);
INSERT INTO `induction_bookings` VALUES (106,256,8,'','2016-08-19','14:00:00','16:00:00',72,'2016-08-17 00:00:00',1,0,'LBQ',NULL);
INSERT INTO `induction_bookings` VALUES (107,257,8,'','2016-08-19','14:00:00','16:00:00',72,'2016-08-17 00:00:00',1,0,'LBQ',NULL);
INSERT INTO `induction_bookings` VALUES (108,263,1,'','2016-08-26','03:00:00','05:00:00',72,'2016-08-19 00:00:00',1,0,'Aljazeera',NULL);
INSERT INTO `induction_bookings` VALUES (109,290,8,'','2016-08-26','14:00:00','16:00:00',72,'2016-08-22 00:00:00',1,0,'LBQ',NULL);
INSERT INTO `induction_bookings` VALUES (110,291,8,'','2016-08-26','14:00:00','16:00:00',72,'2016-08-22 00:00:00',1,0,'LBQ',NULL);
INSERT INTO `induction_bookings` VALUES (111,292,8,'','2016-08-26','14:00:00','16:00:00',72,'2016-08-25 00:00:00',1,0,'LBQ',NULL);
INSERT INTO `induction_bookings` VALUES (112,293,8,'','2016-08-26','08:00:00','10:00:00',72,'2016-08-25 00:00:00',1,0,'LBQ',NULL);
INSERT INTO `induction_bookings` VALUES (113,299,2,'','2016-08-26','14:00:00','16:00:00',72,'2016-08-26 00:00:00',0,0,'Aqua',NULL);
INSERT INTO `induction_bookings` VALUES (114,307,8,'','2016-09-20','14:00:00','16:00:00',72,'2016-09-20 00:00:00',0,0,'LBQ',NULL);
INSERT INTO `induction_bookings` VALUES (115,317,2,'','2016-10-22','03:00:00','05:00:00',137,'2016-10-19 00:00:00',1,0,'Aqua',NULL);
INSERT INTO `induction_bookings` VALUES (116,328,1,'','2016-11-12','03:00:00','05:00:00',72,'2016-11-11 00:00:00',0,0,'Aljazeera',NULL);
INSERT INTO `induction_bookings` VALUES (117,331,40,'','2016-11-23','14:00:00','16:00:00',137,'2016-11-21 00:00:00',1,0,'Area Sq',NULL);
INSERT INTO `induction_bookings` VALUES (118,332,2,'','2016-12-03','14:00:00','16:00:00',137,'2016-11-30 00:00:00',0,0,'Aqua',NULL);
INSERT INTO `induction_bookings` VALUES (119,347,8,'','2017-08-10','08:00:00','10:00:00',102,'2017-08-04 00:00:00',0,0,'LBQ',NULL);
INSERT INTO `induction_bookings` VALUES (120,353,8,'','2017-08-10','08:00:00','09:00:00',102,'2017-08-08 00:00:00',0,0,'LBQ',NULL);
INSERT INTO `induction_bookings` VALUES (121,356,8,'','2017-08-11','08:00:00','09:00:00',102,'2017-08-09 00:00:00',0,0,'LBQ',NULL);
INSERT INTO `induction_bookings` VALUES (122,358,8,'','2017-08-17','08:00:00','09:00:00',102,'2017-08-15 00:00:00',0,0,'LBQ',NULL);
INSERT INTO `induction_bookings` VALUES (123,360,8,'','2017-08-17','08:00:00','09:00:00',102,'2017-08-15 00:00:00',0,0,'LBQ',NULL);
INSERT INTO `induction_bookings` VALUES (124,364,2,'','2017-08-19','08:00:00','09:00:00',102,'2017-08-16 00:00:00',1,0,'Aqua',NULL);
INSERT INTO `induction_bookings` VALUES (125,368,20,'','2017-08-31','14:00:00','15:00:00',102,'2017-08-29 00:00:00',1,0,'Chorus Group',NULL);
INSERT INTO `induction_bookings` VALUES (126,369,2,'','2017-09-02','08:00:00','09:00:00',102,'2017-08-31 00:00:00',0,0,'Aqua',NULL);
INSERT INTO `induction_bookings` VALUES (127,334,8,'','2017-09-10','08:00:00','09:00:00',137,'2017-09-06 00:00:00',1,0,'LBQ',NULL);
INSERT INTO `induction_bookings` VALUES (129,373,8,'','2017-09-08','08:00:00','09:00:00',137,'2017-09-06 00:00:00',0,0,'LBQ',NULL);
INSERT INTO `induction_bookings` VALUES (130,382,1,'','2017-09-21','14:00:00','15:00:00',102,'2017-09-19 00:00:00',0,0,'Al Jazeera',NULL);
INSERT INTO `induction_bookings` VALUES (131,387,1,'','2017-11-04','08:00:00','09:00:00',102,'2017-11-03 00:00:00',0,0,'Al Jazeera',NULL);
INSERT INTO `induction_bookings` VALUES (132,388,1,'','2017-11-17','08:00:00','09:00:00',102,'2017-11-03 00:00:00',0,0,'Al Jazeera',NULL);
INSERT INTO `induction_bookings` VALUES (133,425,80,'','2018-06-27','11:00:00','12:00:00',102,'2018-06-26 00:00:00',1,0,'Shard Quarter',NULL);
INSERT INTO `induction_bookings` VALUES (134,437,8,'','2018-08-15','13:00:00','14:00:00',102,'2018-08-14 00:00:00',1,0,'Locale',NULL);
INSERT INTO `induction_bookings` VALUES (135,439,2,'','2018-08-30','09:00:00','10:00:00',102,'2018-08-28 00:00:00',0,0,'Aqua',NULL);
INSERT INTO `induction_bookings` VALUES (136,442,1,'','2018-08-31','14:00:00','15:00:00',102,'2018-08-28 00:00:00',0,0,'Al Jazeera',NULL);
INSERT INTO `induction_bookings` VALUES (137,445,2,'','2018-08-31','15:00:00','16:00:00',102,'2018-08-28 00:00:00',0,0,'Aqua',NULL);
INSERT INTO `induction_bookings` VALUES (138,448,46,'','2018-09-14','11:00:00','12:00:00',102,'2018-09-05 00:00:00',0,0,'Arcapita',NULL);
INSERT INTO `induction_bookings` VALUES (139,449,2,'','2018-09-14','13:00:00','14:00:00',102,'2018-09-05 00:00:00',0,0,'Aqua',NULL);
/*!40000 ALTER TABLE `induction_bookings` ENABLE KEYS */;
UNLOCK TABLES;
