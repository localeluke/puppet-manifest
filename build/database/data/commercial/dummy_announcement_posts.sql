--
-- Dumping data for table `announcement_posts`
--

LOCK TABLES `announcement_posts` WRITE;
/*!40000 ALTER TABLE `announcement_posts` DISABLE KEYS */;
INSERT INTO `announcement_posts` VALUES (1,1,'Fire Alarm Test','<p>Please be aware that there will be a fire alarm test today at 4 pm. It will sound for up to 2 minutes, but there is no need to leave the building. Apologies for any inconvenience caused.</p>\r\n','2019-04-02','2019-04-16',1,1,0,0,0,'NULL',0,1,'2019-04-02 11:45:16',9,'2019-04-02','00:00:00',0,'N',NULL);
INSERT INTO `announcement_posts` VALUES (2,0,'Test','<p>Test</p>\r\n','2019-04-04','2019-04-18',1,1,0,0,0,'NULL',0,1,'2019-04-04 15:35:23',1,'2019-04-04','00:00:00',0,'N',NULL);
/*!40000 ALTER TABLE `announcement_posts` ENABLE KEYS */;
UNLOCK TABLES;
