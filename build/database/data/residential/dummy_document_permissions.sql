--
-- Dumping data for table `document_permissions`
--

LOCK TABLES `document_permissions` WRITE;
/*!40000 ALTER TABLE `document_permissions` DISABLE KEYS */;
INSERT INTO `document_permissions` VALUES (2,3,'blocks',4);
INSERT INTO `document_permissions` VALUES (4,4,'blocks',3);
/*!40000 ALTER TABLE `document_permissions` ENABLE KEYS */;
UNLOCK TABLES;
