--
-- Dumping data for table `property_types`
--

LOCK TABLES `property_types` WRITE;
/*!40000 ALTER TABLE `property_types` DISABLE KEYS */;
INSERT INTO `property_types` VALUES (1,'2 Bed Apartment','2BA',1,1,0);
INSERT INTO `property_types` VALUES (2,'1 Bed Apartment','1BA',1,1,0);
INSERT INTO `property_types` VALUES (3,'Studio','S',1,1,0);
INSERT INTO `property_types` VALUES (4,'4 Bed House','4BH',1,1,0);
INSERT INTO `property_types` VALUES (5,'Penthouse','P',1,1,0);
/*!40000 ALTER TABLE `property_types` ENABLE KEYS */;
UNLOCK TABLES;
