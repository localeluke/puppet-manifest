--
-- Dumping data for table `admin_helpdesk_cat`
--

LOCK TABLES `admin_helpdesk_cat` WRITE;
/*!40000 ALTER TABLE `admin_helpdesk_cat` DISABLE KEYS */;
INSERT INTO `admin_helpdesk_cat` VALUES (1,'Security',21,'Security',0,'S/','',NULL,'0000-00-00 00:00:00',1,1);
INSERT INTO `admin_helpdesk_cat` VALUES (2,'IT and Telecoms',22,'itandtelecoms',0,'ITT/','',NULL,'0000-00-00 00:00:00',1,1);
INSERT INTO `admin_helpdesk_cat` VALUES (3,'Technical Services',23,'technicalservices',0,'TS/','',NULL,'0000-00-00 00:00:00',1,1);
INSERT INTO `admin_helpdesk_cat` VALUES (4,'Housekeeping',24,'housekeeping',0,'HK/','',NULL,'0000-00-00 00:00:00',1,1);
INSERT INTO `admin_helpdesk_cat` VALUES (5,'Lifts and Escalators',25,'LiftsandEscalators',0,'LE/','',NULL,'0000-00-00 00:00:00',1,1);
INSERT INTO `admin_helpdesk_cat` VALUES (6,'My Vertical City',26,'myverticalcity',0,'MVC/','',NULL,'0000-00-00 00:00:00',1,1);
INSERT INTO `admin_helpdesk_cat` VALUES (7,'Accidents and Incidents',27,'accidentsandincidents',1,'AI/','',NULL,'0000-00-00 00:00:00',1,1);
INSERT INTO `admin_helpdesk_cat` VALUES (8,'Dan\'s Department 08',28,'dansdepartment08',0,'DD/','',NULL,'0000-00-00 00:00:00',1,0);
INSERT INTO `admin_helpdesk_cat` VALUES (9,'Lost and Found Items',29,'LostandFoundItems',6,'LF/','',NULL,'0000-00-00 00:00:00',1,1);
INSERT INTO `admin_helpdesk_cat` VALUES (10,'Party Planning',30,'PartyPlanning',0,'PP/','',NULL,'0000-00-00 00:00:00',1,0);
INSERT INTO `admin_helpdesk_cat` VALUES (11,'Front of House & Events',31,'frontofhouseevents',0,'FoH/','',NULL,'0000-00-00 00:00:00',1,1);
INSERT INTO `admin_helpdesk_cat` VALUES (12,'Fabric',32,'fabric',0,'F/','',NULL,'0000-00-00 00:00:00',1,1);
INSERT INTO `admin_helpdesk_cat` VALUES (13,'Window Cleaning',33,'WindowCleaning',0,'WC/','',NULL,'0000-00-00 00:00:00',1,1);
INSERT INTO `admin_helpdesk_cat` VALUES (14,'LBQ Management',34,'LBQManagement',0,'LBQM/','',NULL,'0000-00-00 00:00:00',1,1);
INSERT INTO `admin_helpdesk_cat` VALUES (15,'sweta',35,'sweta',0,'tesr','',NULL,'0000-00-00 00:00:00',1,1);
INSERT INTO `admin_helpdesk_cat` VALUES (16,'Test Department',36,'testdepartment',11,'TD/','',NULL,'0000-00-00 00:00:00',1,1);
INSERT INTO `admin_helpdesk_cat` VALUES (17,'Locale Test',47,'localetest',0,'LT/','',NULL,'0000-00-00 00:00:00',1,1);
INSERT INTO `admin_helpdesk_cat` VALUES (18,'Technical Fault',48,'technicalfault',0,'TF/','',NULL,'0000-00-00 00:00:00',1,1);
INSERT INTO `admin_helpdesk_cat` VALUES (19,'Security Helpdesk',49,'securityhelpdesk',0,'SC/','',NULL,'0000-00-00 00:00:00',1,1);
INSERT INTO `admin_helpdesk_cat` VALUES (20,'New Test Dept.',50,'newtestdept',16,'Tsetsets','',NULL,'0000-00-00 00:00:00',1,1);
INSERT INTO `admin_helpdesk_cat` VALUES (21,'Test Department MVC',52,'testdepartmentmvc',0,'HD/TD/','',NULL,'0000-00-00 00:00:00',0,1);
INSERT INTO `admin_helpdesk_cat` VALUES (22,'Test Department MVC 2',53,'testdepartmentmvc2',0,'H/TD2/','',NULL,'0000-00-00 00:00:00',1,1);
INSERT INTO `admin_helpdesk_cat` VALUES (23,'Front of House & Events',58,'frontofhouseevents',0,'H/FOHE/','',NULL,'0000-00-00 00:00:00',0,1);
INSERT INTO `admin_helpdesk_cat` VALUES (24,'Housekeeping',60,'housekeeping',0,'H/H/','',NULL,'0000-00-00 00:00:00',0,1);
INSERT INTO `admin_helpdesk_cat` VALUES (25,'My Vertical City',61,'myverticalcity',0,'H/MVC/','',NULL,'0000-00-00 00:00:00',0,1);
INSERT INTO `admin_helpdesk_cat` VALUES (26,'Test Department Jade',68,'testdepartmentjade',0,'TDJ','',NULL,'0000-00-00 00:00:00',0,1);
/*!40000 ALTER TABLE `admin_helpdesk_cat` ENABLE KEYS */;
UNLOCK TABLES;
