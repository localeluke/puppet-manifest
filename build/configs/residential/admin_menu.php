<?php 

$site_type = $_SESSION[$_config['admin_sess']]['less_variables']['site_type'];

//Start: Estate Setup --------------------------------------------
$configAdminNavi['Estate Setup']['cls'] 	  		   = "ES";

$configAdminNavi['Estate Setup']['module'][1] 	   = "Developments";
$configAdminNavi['Estate Setup']['url'][1]		   = "developments";
				
$configAdminNavi['Estate Setup']['module'][2] 	   = "Blocks";
$configAdminNavi['Estate Setup']['url'][2]		   = "blocks";
				
if($site_type == 'RESIDENTIAL')
{
$configAdminNavi['Estate Setup']['module'][3] 	   = "Property Types";
$configAdminNavi['Estate Setup']['url'][3]		   = "propertytypes";
				
$configAdminNavi['Estate Setup']['module'][4] 	   = "Properties";
$configAdminNavi['Estate Setup']['url'][4]		   = "properties";
				
}
else if($site_type == 'COMMERCIAL')
{
	$configAdminNavi['Estate Setup']['module'][3] 	   = "Companies";
$configAdminNavi['Estate Setup']['url'][3]		   = "companies";
				
}
//End: Estate Setup ----------------------------------------------

//Start: Content --------------------------------------------
$configAdminNavi['Content']['cls'] 	  		   = "CT";

$configAdminNavi['Content']['module'][5] 	   = "Navigation Titles";
$configAdminNavi['Content']['url'][5]		   = "navigation";
				
$configAdminNavi['Content']['module'][6] 	   = "Documents";
$configAdminNavi['Content']['url'][6]		   = "documents";
				
$configAdminNavi['Content']['module'][7] 	   = "Document Categories";
$configAdminNavi['Content']['url'][7]		   = "documentcategory";
				
$configAdminNavi['Content']['module'][8] 	   = "Pages";
$configAdminNavi['Content']['url'][8]		   = "pages";
				
$configAdminNavi['Content']['module'][9] 	   = "Static Copy";
$configAdminNavi['Content']['url'][9]		   = "staticcopy";
				
//End: Content ----------------------------------------------

//Start: Users --------------------------------------------
$configAdminNavi['Users']['cls'] 	  		   = "US";

$configAdminNavi['Users']['module'][10] 	   = "User Database";
$configAdminNavi['Users']['url'][10]		   = "users";
				
$configAdminNavi['Users']['module'][11] 	   = "User Groups";
$configAdminNavi['Users']['url'][11]		   = "usergroups";
				
$configAdminNavi['Users']['module'][12] 	   = "Admin User Database";
$configAdminNavi['Users']['url'][12]		   = "adminusers";
				
$configAdminNavi['Users']['module'][13] 	   = "Pre register users";
$configAdminNavi['Users']['url'][13]		   = "preregister";
				
$configAdminNavi['Users']['module'][14] 	   = "User Approval";
$configAdminNavi['Users']['url'][14]		   = "userapproval";
				
$configAdminNavi['Users']['module'][15] 	   = "Archived Users";
$configAdminNavi['Users']['url'][15]		   = "archivedusers";
				
//End: Users ----------------------------------------------

//Start: Functions --------------------------------------------


$configAdminNavi['Functions']['module'][16] 	   = "Module Permissions";
$configAdminNavi['Functions']['url'][16]		   = "modules";
				
$configAdminNavi['Functions']['module'][17] 	   = "Facility Bookings";
$configAdminNavi['Functions']['url'][17]		   = "facility";
				
$configAdminNavi['Functions']['module'][18] 	   = "Lists";
$configAdminNavi['Functions']['url'][18]		   = "lists";
				
$configAdminNavi['Functions']['module'][19] 	   = "Announcement Templates";
$configAdminNavi['Functions']['url'][19]		   = "announcement";
				
$configAdminNavi['Functions']['module'][20] 	   = "Forms";
$configAdminNavi['Functions']['url'][20]		   = "forms";
				
$configAdminNavi['Functions']['module'][21] 	   = "Email Copies";
$configAdminNavi['Functions']['url'][21]		   = "emailcopy";
				
$configAdminNavi['Functions']['module'][22] 	   = "Key Holding Configuration";
$configAdminNavi['Functions']['url'][22]		   = "KeyHoldingconfig";
				
$configAdminNavi['Functions']['module'][23] 	   = "Deliveries";
$configAdminNavi['Functions']['url'][23]		   = "deliveries";
				
$configAdminNavi['Functions']['module'][24] 	   = "Duty Configuration";
$configAdminNavi['Functions']['url'][24]		   = "dutyconfiguration";
				
$configAdminNavi['Functions']['module'][25] 	   = "Offers Category";
$configAdminNavi['Functions']['url'][25]		   = "promotioncategory";
				
$configAdminNavi['Functions']['module'][26] 	   = "Offers and Promotions";
$configAdminNavi['Functions']['url'][26]		   = "promotion";
				
$configAdminNavi['Functions']['module'][27] 	   = "Shortcut Icons";
$configAdminNavi['Functions']['url'][27]		   = "shortcuticons";
				
$configAdminNavi['Functions']['module'][28] 	   = "Duty Handover Categories";
$configAdminNavi['Functions']['url'][28]		   = "Dutyhandovercategory";
				
$configAdminNavi['Functions']['module'][29] 	   = "Announcement Background Uploader";
$configAdminNavi['Functions']['url'][29]		   = "Announcementuploader";
				
$configAdminNavi['Functions']['module'][30] 	   = "Access to demise";
$configAdminNavi['Functions']['url'][30]		   = "accesstodemise";
				
$configAdminNavi['Functions']['module'][31] 	   = "Languages";
$configAdminNavi['Functions']['url'][31]		   = "languages";
				
$configAdminNavi['Functions']['module'][32] 	   = "Contact Concierge";
$configAdminNavi['Functions']['url'][32]		   = "Conciergecontacts";
				
$configAdminNavi['Functions']['module'][33] 	   = "Local Amenities";
$configAdminNavi['Functions']['url'][33]		   = "amenities";
				
$configAdminNavi['Functions']['module'][34] 	   = "Subletting Configuration";
$configAdminNavi['Functions']['url'][34]		   = "sublettingconfig";
				
$configAdminNavi['Functions']['module'][35] 	   = "Feeds";
$configAdminNavi['Functions']['url'][35]		   = "Feeds";
				
$configAdminNavi['Functions']['module'][36] 	   = "Helpdesk Departments";
$configAdminNavi['Functions']['url'][36]		   = "helpdesk/departments";
				
$configAdminNavi['Functions']['module'][37] 	   = "Helpdesk Priorities";
$configAdminNavi['Functions']['url'][37]		   = "helpdesk/priorities";
				
$configAdminNavi['Functions']['module'][38] 	   = "Calendar";
$configAdminNavi['Functions']['url'][38]		   = "calendar";
				
$configAdminNavi['Functions']['module'][39] 	   = "Google Analytics";
$configAdminNavi['Functions']['url'][39]		   = "ganalytics";
				
$configAdminNavi['Functions']['module'][40] 	   = "VMS Pass ID";
$configAdminNavi['Functions']['url'][40]		   = "vmspass";
				
$configAdminNavi['Functions']['module'][41] 	   = "Form Log";
$configAdminNavi['Functions']['url'][41]		   = "formlog";
				
//End: Functions ----------------------------------------------

//Start: SiteConfiguration --------------------------------------------


$configAdminNavi['SiteConfiguration']['module'][42] 	   = "Login Configuration";
$configAdminNavi['SiteConfiguration']['url'][42]		   = "siteconfiguration";
				
$configAdminNavi['SiteConfiguration']['module'][43] 	   = "Login Background Images";
$configAdminNavi['SiteConfiguration']['url'][43]		   = "loginbackgrounds";
				
$configAdminNavi['SiteConfiguration']['module'][44] 	   = "Home Page Configuration";
$configAdminNavi['SiteConfiguration']['url'][44]		   = "siteconfiguration/home_config";
				
$configAdminNavi['SiteConfiguration']['module'][45] 	   = "UI Page Configuration";
$configAdminNavi['SiteConfiguration']['url'][45]		   = "siteconfiguration/ui_config";
				
$configAdminNavi['SiteConfiguration']['module'][46] 	   = "Email Customisation";
$configAdminNavi['SiteConfiguration']['url'][46]		   = "siteconfiguration/email_config";
				
//End: SiteConfiguration ----------------------------------------------



  //The following lines have been automatically generated
  $configAdminNavi['SiteConfiguration']['module'][47] = "Mobile App Configuration";
  $configAdminNavi['SiteConfiguration']['url'][47] = "siteconfiguration/app_config";
      
  //The following lines have been automatically generated
  $configAdminNavi['SiteConfiguration']['module'][47] = "Mobile App Configuration";
  $configAdminNavi['SiteConfiguration']['url'][47] = "siteconfiguration/app_config";
      
//The following lines have been automatically generated
$configAdminNavi['SiteConfiguration']['module'][48] = "2 Factor Authorisation";
$configAdminNavi['SiteConfiguration']['url'][48] = "siteconfiguration/two_factor";
			