--
-- Dumping data for table `form_log_values`
--

LOCK TABLES `form_log_values` WRITE;
/*!40000 ALTER TABLE `form_log_values` DISABLE KEYS */;
INSERT INTO `form_log_values` VALUES (1,1,'Test',1,0);
INSERT INTO `form_log_values` VALUES (2,4,'Yes',1,0);
INSERT INTO `form_log_values` VALUES (3,3,'No',1,0);
INSERT INTO `form_log_values` VALUES (4,2,'Temporary',1,0);
INSERT INTO `form_log_values` VALUES (5,5,'',1,0);
INSERT INTO `form_log_values` VALUES (6,1,'test',2,0);
INSERT INTO `form_log_values` VALUES (7,4,'Yes',2,0);
INSERT INTO `form_log_values` VALUES (8,3,'No',2,0);
INSERT INTO `form_log_values` VALUES (9,2,'Temporary',2,0);
INSERT INTO `form_log_values` VALUES (10,5,'',2,0);
/*!40000 ALTER TABLE `form_log_values` ENABLE KEYS */;
UNLOCK TABLES;
