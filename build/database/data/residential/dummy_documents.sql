--
-- Dumping data for table `documents`
--

LOCK TABLES `documents` WRITE;
/*!40000 ALTER TABLE `documents` DISABLE KEYS */;
INSERT INTO `documents` VALUES (1,1,'Dummy Document A','global',1,0,NULL,'0000-00-00','');
INSERT INTO `documents` VALUES (2,1,'Dummy Document B','global',1,0,NULL,'0000-00-00','');
INSERT INTO `documents` VALUES (3,1,'Dummy Document Blue Boar Gardens','development_specific',1,0,NULL,'0000-00-00','');
INSERT INTO `documents` VALUES (4,1,'Dummy Document Park End Gardens','development_specific',1,0,NULL,'0000-00-00','');
/*!40000 ALTER TABLE `documents` ENABLE KEYS */;
UNLOCK TABLES;
