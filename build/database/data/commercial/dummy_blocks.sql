--
-- Dumping data for table `blocks`
--

LOCK TABLES `blocks` WRITE;
/*!40000 ALTER TABLE `blocks` DISABLE KEYS */;
INSERT INTO `blocks` VALUES (1,0,1,1,'B Block',NULL,'OX1 4EH','',NULL,'',NULL,'B',1,0,1,'B','51.751781','-1.255546');
INSERT INTO `blocks` VALUES (2,0,1,1,'A Block ',NULL,'OX1 4EH','',NULL,'',NULL,'A',1,0,1,'A','51.751781','-1.255546');
INSERT INTO `blocks` VALUES (3,0,1,1,'C Block',NULL,'OX16 1AR','',NULL,'',NULL,'C',1,0,2,'C','52.081631','-1.368016');
/*!40000 ALTER TABLE `blocks` ENABLE KEYS */;
UNLOCK TABLES;
