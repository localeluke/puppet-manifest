--
-- Dumping data for table `site_config`
--

LOCK TABLES `site_config` WRITE;
/*!40000 ALTER TABLE `site_config` DISABLE KEYS */;
INSERT INTO `site_config` VALUES (1,'site_title','LOCALE','general',NULL);
INSERT INTO `site_config` VALUES (2,'site_type','COMMERCIAL','general','RESIDENTIAL or COMMERCIAL');
INSERT INTO `site_config` VALUES (3,'show_username','Y','general','Y - Yes or N - No');
INSERT INTO `site_config` VALUES (4,'show_background_images','N','general','Y - Yes or N - No');
INSERT INTO `site_config` VALUES (5,'show_pre_reg','Y','general','Y - Yes or N - No');
INSERT INTO `site_config` VALUES (6,'mobile_mandatory','N','general','Y - Yes or N - No');
INSERT INTO `site_config` VALUES (7,'app_col_header_bg','#FFFFFF','',NULL);
INSERT INTO `site_config` VALUES (8,'app_col_header_fg','#FFFFFF','',NULL);
INSERT INTO `site_config` VALUES (9,'app_col_floating_button','#C94AFF','',NULL);
INSERT INTO `site_config` VALUES (10,'app_col_header_bg','#FFFFFF','app',NULL);
INSERT INTO `site_config` VALUES (11,'app_col_header_fg','#FFFFFF','app',NULL);
INSERT INTO `site_config` VALUES (12,'app_col_floating_button','#BB45FF','app',NULL);
INSERT INTO `site_config` VALUES (13,'ui_font_family','brownlight','',NULL);
INSERT INTO `site_config` VALUES (14,NULL,'3','home',NULL);
INSERT INTO `site_config` VALUES (15,NULL,'3','home',NULL);
INSERT INTO `site_config` VALUES (16,NULL,'3','home',NULL);
INSERT INTO `site_config` VALUES (17,NULL,'3','home',NULL);
INSERT INTO `site_config` VALUES (18,NULL,'3','home',NULL);
INSERT INTO `site_config` VALUES (19,NULL,'3','home',NULL);
INSERT INTO `site_config` VALUES (20,NULL,'3','home',NULL);
INSERT INTO `site_config` VALUES (21,NULL,'3','home',NULL);
/*!40000 ALTER TABLE `site_config` ENABLE KEYS */;
UNLOCK TABLES;
