<?php
session_start();
include dirname(dirname(dirname(__FILE__))).'/config/config.php';

$path = ($_SERVER['HTTPS'] == "on") ? "https" : "http";
$path .= "://".$_SERVER['HTTP_HOST'];
$path .= (stristr($_config['doc_root'],"http")) ? "" : $_config['doc_root'];
$path = rtrim($path,"/");
$path = $path."/";

$email_std_btn 		=	$_SESSION[$_SESSION['load_app_mode']]['less_variables']['email_btn_std_color'];
$email_std_btn_txt 	=	$_SESSION[$_SESSION['load_app_mode']]['less_variables']['email_btn_text_color'];	
$load_base_url		= 	$_SESSION[$_SESSION['load_app_mode']]['load_base_url'];	
?>
var cDocRoot 			= "<?=$_config['doc_root']?>";
var mainPath 			= "<?=$path?>";
var loginPath 			= "<?=$_SESSION["domainName"] ? 'http://'.$_SERVER["SERVER_NAME"].$GLOBALS[config][main_doc_root].$_SESSION["domainName"] : $path?>";
var _globalDocRoot 		= '<?=GLOBAL_MAIN_DOC_ROOT ?>';
var email_std_btn 		= 	"<?=$email_std_btn ?>";
var email_std_btn_txt 	= 	"<?=$email_std_btn_txt ?>";
var load_base_url 		= 	"<?=$load_base_url ?>";
