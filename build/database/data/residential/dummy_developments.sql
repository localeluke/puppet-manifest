--
-- Dumping data for table `developments`
--

LOCK TABLES `developments` WRITE;
/*!40000 ALTER TABLE `developments` DISABLE KEYS */;
INSERT INTO `developments` VALUES (1,'South East Village',1,1);
INSERT INTO `developments` VALUES (2,'South East Village',1,0);
INSERT INTO `developments` VALUES (3,'Development A',1,0);
/*!40000 ALTER TABLE `developments` ENABLE KEYS */;
UNLOCK TABLES;
