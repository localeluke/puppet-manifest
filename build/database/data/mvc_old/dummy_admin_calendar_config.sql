--
-- Dumping data for table `admin_calendar_config`
--

LOCK TABLES `admin_calendar_config` WRITE;
/*!40000 ALTER TABLE `admin_calendar_config` DISABLE KEYS */;
INSERT INTO `admin_calendar_config` VALUES (2,7,'Announcement','#112AA9',1,1,0);
INSERT INTO `admin_calendar_config` VALUES (3,15,'Loading Bay Bookings','#CB6C1C',1,1,0);
INSERT INTO `admin_calendar_config` VALUES (4,16,'Waste Collection Bookings','#84569E',1,1,0);
INSERT INTO `admin_calendar_config` VALUES (5,0,'Events','#911D29',0,1,0);
/*!40000 ALTER TABLE `admin_calendar_config` ENABLE KEYS */;
UNLOCK TABLES;
