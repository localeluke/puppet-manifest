--
-- Dumping data for table `service_category`
--

LOCK TABLES `service_category` WRITE;
/*!40000 ALTER TABLE `service_category` DISABLE KEYS */;
INSERT INTO `service_category` VALUES (6,'Lifestyle Services','',1,0,0);
INSERT INTO `service_category` VALUES (7,'Operational Services','',1,0,0);
INSERT INTO `service_category` VALUES (8,'Test Parent','',1,0,0);
INSERT INTO `service_category` VALUES (9,'Test Child','',1,0,8);
/*!40000 ALTER TABLE `service_category` ENABLE KEYS */;
UNLOCK TABLES;
