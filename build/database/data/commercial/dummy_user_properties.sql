--
-- Dumping data for table `user_properties`
--

LOCK TABLES `user_properties` WRITE;
/*!40000 ALTER TABLE `user_properties` DISABLE KEYS */;
INSERT INTO `user_properties` VALUES (7,3,2,'companies',1);
INSERT INTO `user_properties` VALUES (8,4,2,'companies',1);
INSERT INTO `user_properties` VALUES (9,5,1,'companies',2);
INSERT INTO `user_properties` VALUES (11,2,2,'companies',1);
INSERT INTO `user_properties` VALUES (13,8,1,'companies',2);
INSERT INTO `user_properties` VALUES (15,1,2,'companies',1);
INSERT INTO `user_properties` VALUES (16,1,1,'companies',2);
INSERT INTO `user_properties` VALUES (17,9,3,'companies',3);
INSERT INTO `user_properties` VALUES (18,9,3,'companies',4);
INSERT INTO `user_properties` VALUES (19,6,3,'companies',4);
INSERT INTO `user_properties` VALUES (22,7,1,'companies',2);
/*!40000 ALTER TABLE `user_properties` ENABLE KEYS */;
UNLOCK TABLES;
