--
-- Dumping data for table `html_lists`
--

LOCK TABLES `html_lists` WRITE;
/*!40000 ALTER TABLE `html_lists` DISABLE KEYS */;
INSERT INTO `html_lists` VALUES (1,'VMS Entrances',1,0,'vms_entrance_types');
INSERT INTO `html_lists` VALUES (2,'Title List',1,0,'list_titles');
INSERT INTO `html_lists` VALUES (3,'Access To Demise',1,0,'reasons_access_to_demise');
INSERT INTO `html_lists` VALUES (4,'Key Holding Key Types',1,0,'key_holding_key_types');
INSERT INTO `html_lists` VALUES (5,'Helpdesk Fault List',1,0,'helpdesk_fault_list');
INSERT INTO `html_lists` VALUES (6,'Access Pass Requests',1,0,'access_pass');
/*!40000 ALTER TABLE `html_lists` ENABLE KEYS */;
UNLOCK TABLES;
