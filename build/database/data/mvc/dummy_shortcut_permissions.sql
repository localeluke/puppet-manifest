--
-- Dumping data for table `shortcut_permissions`
--

LOCK TABLES `shortcut_permissions` WRITE;
/*!40000 ALTER TABLE `shortcut_permissions` DISABLE KEYS */;
INSERT INTO `shortcut_permissions` VALUES (1,3,'user_groups',2);
/*!40000 ALTER TABLE `shortcut_permissions` ENABLE KEYS */;
UNLOCK TABLES;
