--
-- Dumping data for table `admin_calendar_config`
--

LOCK TABLES `admin_calendar_config` WRITE;
/*!40000 ALTER TABLE `admin_calendar_config` DISABLE KEYS */;
INSERT INTO `admin_calendar_config` VALUES (6,0,'Announcements','#112AA9',0,1,0);
INSERT INTO `admin_calendar_config` VALUES (7,0,'Events','#911D29',0,1,0);
INSERT INTO `admin_calendar_config` VALUES (8,0,'Loading Bay Bookings','#CB6C1C',0,1,0);
INSERT INTO `admin_calendar_config` VALUES (9,0,'Waste Collection Bookings','#84569E',0,1,0);
INSERT INTO `admin_calendar_config` VALUES (10,1,'Announcements',NULL,1,1,0);
INSERT INTO `admin_calendar_config` VALUES (11,22,'Duty Handover',NULL,1,1,0);
/*!40000 ALTER TABLE `admin_calendar_config` ENABLE KEYS */;
UNLOCK TABLES;
