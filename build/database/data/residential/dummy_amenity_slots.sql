--
-- Dumping data for table `amenity_slots`
--

LOCK TABLES `amenity_slots` WRITE;
/*!40000 ALTER TABLE `amenity_slots` DISABLE KEYS */;
INSERT INTO `amenity_slots` VALUES (1,1,'09:00:00','17:00:00',NULL,1);
INSERT INTO `amenity_slots` VALUES (2,1,'09:00:00','17:00:00',NULL,2);
INSERT INTO `amenity_slots` VALUES (3,1,'09:00:00','17:00:00',NULL,3);
INSERT INTO `amenity_slots` VALUES (4,1,'09:00:00','17:00:00',NULL,4);
INSERT INTO `amenity_slots` VALUES (5,1,'09:00:00','17:00:00',NULL,5);
INSERT INTO `amenity_slots` VALUES (6,2,'09:00:00','17:00:00',NULL,1);
INSERT INTO `amenity_slots` VALUES (7,2,'09:00:00','17:00:00',NULL,2);
INSERT INTO `amenity_slots` VALUES (8,2,'09:00:00','17:00:00',NULL,3);
INSERT INTO `amenity_slots` VALUES (9,2,'09:00:00','17:00:00',NULL,4);
INSERT INTO `amenity_slots` VALUES (10,2,'09:00:00','17:00:00',NULL,5);
/*!40000 ALTER TABLE `amenity_slots` ENABLE KEYS */;
UNLOCK TABLES;
