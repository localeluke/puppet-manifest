--
-- Dumping data for table `languages`
--

LOCK TABLES `languages` WRITE;
/*!40000 ALTER TABLE `languages` DISABLE KEYS */;
INSERT INTO `languages` VALUES (1,'English',1);
INSERT INTO `languages` VALUES (2,'Chinese Po Kiennnn',0);
INSERT INTO `languages` VALUES (3,'Filipino',1);
INSERT INTO `languages` VALUES (4,'Farsi',1);
INSERT INTO `languages` VALUES (5,'Filipino Tagalog',1);
INSERT INTO `languages` VALUES (6,'Eritrea',1);
INSERT INTO `languages` VALUES (7,'Ethiopia',1);
INSERT INTO `languages` VALUES (8,'French',1);
INSERT INTO `languages` VALUES (9,'German',1);
INSERT INTO `languages` VALUES (10,'Urdu',1);
INSERT INTO `languages` VALUES (11,'Russian',1);
/*!40000 ALTER TABLE `languages` ENABLE KEYS */;
UNLOCK TABLES;
