--
-- Dumping data for table `facility_slots`
--

LOCK TABLES `facility_slots` WRITE;
/*!40000 ALTER TABLE `facility_slots` DISABLE KEYS */;
INSERT INTO `facility_slots` VALUES (6,1,'00:00:00','23:59:00',1);
INSERT INTO `facility_slots` VALUES (7,1,'00:00:00','23:59:00',2);
INSERT INTO `facility_slots` VALUES (8,1,'00:00:00','23:59:00',3);
INSERT INTO `facility_slots` VALUES (9,1,'00:00:00','23:59:00',4);
INSERT INTO `facility_slots` VALUES (10,1,'00:00:00','23:59:00',5);
INSERT INTO `facility_slots` VALUES (11,1,'00:00:00','23:59:00',6);
INSERT INTO `facility_slots` VALUES (12,1,'00:00:00','23:59:00',7);
/*!40000 ALTER TABLE `facility_slots` ENABLE KEYS */;
UNLOCK TABLES;
