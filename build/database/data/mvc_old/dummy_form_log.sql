--
-- Dumping data for table `form_log_config`
--

LOCK TABLES `form_log_config` WRITE;
/*!40000 ALTER TABLE `form_log_config` DISABLE KEYS */;
INSERT INTO `form_log_config` VALUES (1,37,'FLD/','<p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don&#39;t look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn&#39;t anything embarrassing hidden in the middle of text.</p>\r\n',13,1,0,1,10,1,2,1,0,NULL);
INSERT INTO `form_log_config` VALUES (2,38,'CP/','<p>Thank you for your submission. The London Bridge Quarter Estate Management Team have been notified and will review your request shortly.&nbsp;</p>\r\n',15,1,0,0,160,0,133,5,0,NULL);
INSERT INTO `form_log_config` VALUES (3,39,'cR','<p>test</p>\r\n',15,0,0,0,0,0,0,0,0,NULL);
INSERT INTO `form_log_config` VALUES (4,41,'CR/','<p>Thank you for your submission. The London Bridge Quarter Estate Management Team have been notified and will review your request shortly.&nbsp;</p>\r\n',15,1,0,0,160,0,133,5,0,NULL);
INSERT INTO `form_log_config` VALUES (5,42,'FM/','<p>Test</p>\r\n',8,1,0,1,0,0,365,10,0,NULL);
INSERT INTO `form_log_config` VALUES (6,43,'WC/','<p>Your booking request has been successfully sent to our Waste Collection&nbsp;Logistics Team, a confirmation email will be issued shortly.</p>\r\n',4,1,0,0,0,1,0,0,0,NULL);
INSERT INTO `form_log_config` VALUES (7,44,'AWP/','<p>Thank you for your submission. You will shortly receive an email confirmation. The Estate Management Team has been informed and will contact you shortly.</p>\r\n',16,1,0,0,0,0,0,0,0,NULL);
INSERT INTO `form_log_config` VALUES (8,69,'OC/','<p>Your Courier Request has been booked.</p>\r\n\r\n<p>Please&nbsp;take your parcel to your nearest collection point, you will be notified once your item has left the building.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><br />\r\n&nbsp;</p>\r\n',24,1,0,0,100,1,0,0,0,NULL);
INSERT INTO `form_log_config` VALUES (9,70,'','',25,0,0,0,0,0,0,0,0,'{\"enabled\":false,\"headers\":[{\"ID\":\"119\",\"default\":\"wiit\",\"custom\":\"wiit\",\"order\":false,\"is_default\":false,\"checked\":0},{\"ID\":\"ID\",\"default\":\"ID\",\"order\":0,\"is_default\":true,\"checked\":1},{\"ID\":\"Name\",\"default\":\"Name\",\"order\":1,\"is_default\":true,\"checked\":1},{\"ID\":\"Development\",\"default\":\"Development\",\"order\":2,\"is_default\":true,\"checked\":1},{\"ID\":\"Block\",\"default\":\"Block\",\"order\":3,\"is_default\":true,\"checked\":1},{\"ID\":\"Company\",\"default\":\"Company\",\"order\":4,\"is_default\":true,\"checked\":1},{\"ID\":\"Submitted\",\"default\":\"Submitted\",\"order\":5,\"is_default\":true,\"checked\":1}]}');
/*!40000 ALTER TABLE `form_log_config` ENABLE KEYS */;
UNLOCK TABLES;
