--
-- Dumping data for table `helpdesk_priorities`
--

LOCK TABLES `helpdesk_priorities` WRITE;
/*!40000 ALTER TABLE `helpdesk_priorities` DISABLE KEYS */;
INSERT INTO `helpdesk_priorities` VALUES (15,'Immediate','#E74C3C',1,'02:00:00',0);
INSERT INTO `helpdesk_priorities` VALUES (16,'High','#FF7E33',2,'04:00:00',0);
INSERT INTO `helpdesk_priorities` VALUES (17,'Medium','#FFCE2E',3,'08:00:00',0);
INSERT INTO `helpdesk_priorities` VALUES (18,'Low','#0AFF23',4,'24:00:00',0);
/*!40000 ALTER TABLE `helpdesk_priorities` ENABLE KEYS */;
UNLOCK TABLES;
