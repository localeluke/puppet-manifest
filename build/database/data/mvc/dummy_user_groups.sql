--
-- Dumping data for table `user_groups`
--

LOCK TABLES `user_groups` WRITE;
/*!40000 ALTER TABLE `user_groups` DISABLE KEYS */;
INSERT INTO `user_groups` VALUES (82,1,'Building Manager',0,NULL,0,0,1,1,0,'',1,0,0);
INSERT INTO `user_groups` VALUES (83,1,'Estate Manager',0,NULL,0,0,1,1,0,'',1,0,0);
INSERT INTO `user_groups` VALUES (84,1,'Security',0,NULL,0,0,1,0,0,'',1,0,0);
INSERT INTO `user_groups` VALUES (85,1,'Reception',0,NULL,0,0,1,0,0,'',1,0,0);
INSERT INTO `user_groups` VALUES (86,1,'Occupier Rep',1,NULL,1,0,1,0,0,'',0,0,0);
INSERT INTO `user_groups` VALUES (87,1,'Occupier',1,NULL,1,0,1,0,0,'',0,0,0);
/*!40000 ALTER TABLE `user_groups` ENABLE KEYS */;
UNLOCK TABLES;
