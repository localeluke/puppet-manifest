--
-- Dumping data for table `company_secondary_departments`
--

LOCK TABLES `company_secondary_departments` WRITE;
/*!40000 ALTER TABLE `company_secondary_departments` DISABLE KEYS */;
INSERT INTO `company_secondary_departments` VALUES (73,53,82);
INSERT INTO `company_secondary_departments` VALUES (117,53,86);
INSERT INTO `company_secondary_departments` VALUES (74,54,82);
INSERT INTO `company_secondary_departments` VALUES (116,54,85);
INSERT INTO `company_secondary_departments` VALUES (112,54,94);
INSERT INTO `company_secondary_departments` VALUES (105,55,81);
INSERT INTO `company_secondary_departments` VALUES (75,55,82);
INSERT INTO `company_secondary_departments` VALUES (76,56,82);
INSERT INTO `company_secondary_departments` VALUES (119,56,88);
INSERT INTO `company_secondary_departments` VALUES (77,57,82);
INSERT INTO `company_secondary_departments` VALUES (78,58,82);
INSERT INTO `company_secondary_departments` VALUES (79,59,82);
INSERT INTO `company_secondary_departments` VALUES (120,59,89);
INSERT INTO `company_secondary_departments` VALUES (80,60,82);
INSERT INTO `company_secondary_departments` VALUES (81,61,82);
INSERT INTO `company_secondary_departments` VALUES (82,62,82);
INSERT INTO `company_secondary_departments` VALUES (83,63,82);
INSERT INTO `company_secondary_departments` VALUES (84,64,82);
INSERT INTO `company_secondary_departments` VALUES (85,65,82);
INSERT INTO `company_secondary_departments` VALUES (114,65,83);
INSERT INTO `company_secondary_departments` VALUES (86,66,82);
INSERT INTO `company_secondary_departments` VALUES (87,67,82);
INSERT INTO `company_secondary_departments` VALUES (88,68,82);
INSERT INTO `company_secondary_departments` VALUES (118,69,87);
INSERT INTO `company_secondary_departments` VALUES (113,71,95);
INSERT INTO `company_secondary_departments` VALUES (115,72,84);
INSERT INTO `company_secondary_departments` VALUES (109,73,91);
INSERT INTO `company_secondary_departments` VALUES (121,76,90);
INSERT INTO `company_secondary_departments` VALUES (111,80,93);
INSERT INTO `company_secondary_departments` VALUES (110,97,92);
INSERT INTO `company_secondary_departments` VALUES (106,99,81);
INSERT INTO `company_secondary_departments` VALUES (107,102,81);
INSERT INTO `company_secondary_departments` VALUES (108,104,81);
/*!40000 ALTER TABLE `company_secondary_departments` ENABLE KEYS */;
UNLOCK TABLES;
