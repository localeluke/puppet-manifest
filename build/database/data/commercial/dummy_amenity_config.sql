--
-- Dumping data for table `amenity_config`
--

LOCK TABLES `amenity_config` WRITE;
/*!40000 ALTER TABLE `amenity_config` DISABLE KEYS */;
INSERT INTO `amenity_config` VALUES (1,27,1,0,'Admin','Fixed',0,60,NULL,0,0,1,'month','1','week',0,1,'admin','',0,1,0,0,'Slot',1,'',1,NULL,1,0,1,'hour');
INSERT INTO `amenity_config` VALUES (2,27,1,0,'Standard','Fixed',0,60,NULL,2,0,1,'week','1','day',2,1,'user','',0,1,0,0,'Slot',1,'',1,NULL,1,0,1,'hour');
/*!40000 ALTER TABLE `amenity_config` ENABLE KEYS */;
UNLOCK TABLES;
