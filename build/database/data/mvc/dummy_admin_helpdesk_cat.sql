--
-- Dumping data for table `admin_helpdesk_cat`
--

LOCK TABLES `admin_helpdesk_cat` WRITE;
/*!40000 ALTER TABLE `admin_helpdesk_cat` DISABLE KEYS */;
INSERT INTO `admin_helpdesk_cat` VALUES (27,'Front of House & Events',74,'frontofhouseevents',0,'H/FOHE/','',NULL,'0000-00-00 00:00:00',0,1);
INSERT INTO `admin_helpdesk_cat` VALUES (28,'Housekeeping',75,'housekeeping',0,'H/H/','',NULL,'0000-00-00 00:00:00',0,1);
INSERT INTO `admin_helpdesk_cat` VALUES (29,'My Vertical City',76,'myverticalcity',0,'H/MVC/','',NULL,'0000-00-00 00:00:00',0,1);
INSERT INTO `admin_helpdesk_cat` VALUES (30,'Test Department MVC',77,'testdepartmentmvc',0,'HD/TD/','',NULL,'0000-00-00 00:00:00',0,1);
/*!40000 ALTER TABLE `admin_helpdesk_cat` ENABLE KEYS */;
UNLOCK TABLES;
